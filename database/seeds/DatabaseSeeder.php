<?php

use App\Anthropometric;
use App\Behavior;
use App\BehaviorLocation;
use App\BehaviorRoute;
use App\Cardiovascular;
use App\Fitness;
use App\GoalName;
use App\GoalUser;
use App\GroupLead;
use App\GroupTeam;
use App\InactivityAssessment;
use App\MixedExperience;
use App\MixedRealityUse;
use App\Outcome;
use App\PhysicalActivity;
use App\PhysicalActivityAssessment;
use App\PhysicalActivityBarriers;
use App\PhysicalActivityChange;
use App\PhysicalActivityConfidence;
use App\PhysicalActivityEfficacy;
use App\PhysicalActivityEnviro;
use App\PhysicalActivityFun;
use App\PhysicalActivityProsCons;
use App\PhysicalActivitySupport;
use App\Sleep;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        User::truncate();
        Sleep::truncate();
        Anthropometric::truncate();
        Fitness::truncate();
        Cardiovascular::truncate();
        Outcome::truncate();
        InactivityAssessment::truncate();
        PhysicalActivity::truncate();
        PhysicalActivityFun::truncate();
        PhysicalActivitySupport::truncate();
        PhysicalActivityEfficacy::truncate();
        PhysicalActivityConfidence::truncate();
        PhysicalActivityProsCons::truncate();
        PhysicalActivityChange::truncate();
        PhysicalActivityAssessment::truncate();
        PhysicalActivityEnviro::truncate();
        PhysicalActivityBarriers::truncate();
        GroupLead::truncate();
        GroupTeam::truncate();
        MixedExperience::truncate();
        MixedRealityUse::truncate();
        GoalName::truncate();
        GoalUser::truncate();
        BehaviorRoute::truncate();
        BehaviorLocation::truncate();
        Behavior::truncate();

        User::flushEventListeners();
        Sleep::flushEventListeners();
        Anthropometric::flushEventListeners();
        Fitness::flushEventListeners();
        Cardiovascular::flushEventListeners();
        Outcome::flushEventListeners();
        InactivityAssessment::flushEventListeners();
        PhysicalActivity::flushEventListeners();
        PhysicalActivityFun::flushEventListeners();
        PhysicalActivitySupport::flushEventListeners();
        PhysicalActivityEfficacy::flushEventListeners();
        PhysicalActivityConfidence::flushEventListeners();
        PhysicalActivityProsCons::flushEventListeners();
        PhysicalActivityChange::flushEventListeners();
        PhysicalActivityAssessment::flushEventListeners();
        PhysicalActivityEnviro::flushEventListeners();
        PhysicalActivityBarriers::flushEventListeners();
        GroupLead::flushEventListeners();
        GroupTeam::flushEventListeners();
        MixedExperience::flushEventListeners();
        MixedRealityUse::flushEventListeners();
        GoalName::flushEventListeners();
        GoalUser::flushEventListeners();
        BehaviorRoute::flushEventListeners();
        BehaviorLocation::flushEventListeners();
        Behavior::flushEventListeners();

        $usersQuantity = 1000;
        $sleepQuantity = 2000;
        $anthroQuantity = 2000;
        $fitnessQuantity = 500;
        $cardioQuantity = 200;
        $outcomeQuantity = 600;
        $inactiveQuantity = 500;
        $paQuantity = 2000;
        $pafunQuantity = 1200;
        $pasupportQuantity = 200;
        $paefficacyQuantity = 500;
        $paconfidenceQuantity = 600;
        $paprosconsQuantity = 300;
        $pachangeQuantity = 900;
        $paassessmentQuantity = 700;
        $paenviroQuantity = 800;
        $pabarriersQuantity = 200;
        $groupleadQuantity = 100;
        $groupteamQuantity = 500;
        $mixedExpQuantity = 20;
        $mixedUserQuantity = 200;
        $goalNameQuantity = 30;
        $goalUserQuantity = 200;
        $behaviorRouteQuantity = 100;
        $behaviorLocationQuantity = 100;
        $behaviorQuantity = 100;

        factory(User::class, $usersQuantity)->create();
        factory(GroupLead::class, $groupleadQuantity)->create();
        factory(GroupTeam::class, $groupteamQuantity)->create();
        factory(MixedExperience::class, $mixedExpQuantity)->create();
        factory(MixedRealityUse::class, $mixedUserQuantity)->create();
        factory(GoalName::class, $goalNameQuantity)->create();
        factory(GoalUser::class, $goalUserQuantity)->create();
        factory(BehaviorRoute::class, $behaviorRouteQuantity)->create();
        factory(BehaviorLocation::class, $behaviorLocationQuantity)->create();
        factory(Behavior::class, $behaviorQuantity)->create();
        factory(Sleep::class, $sleepQuantity)->create();
        factory(Anthropometric::class, $anthroQuantity)->create();
        factory(Fitness::class, $fitnessQuantity)->create();
        factory(Cardiovascular::class, $cardioQuantity)->create();
        factory(Outcome::class, $outcomeQuantity)->create();
        factory(InactivityAssessment::class, $inactiveQuantity)->create();
        factory(PhysicalActivity::class, $paQuantity)->create();
        factory(PhysicalActivityFun::class, $pafunQuantity)->create();
        factory(PhysicalActivitySupport::class, $pasupportQuantity)->create();
        factory(PhysicalActivityEfficacy::class, $paefficacyQuantity)->create();
        factory(PhysicalActivityConfidence::class, $paconfidenceQuantity)->create();
        factory(PhysicalActivityProsCons::class, $paprosconsQuantity)->create();
        factory(PhysicalActivityChange::class, $pachangeQuantity)->create();
        factory(PhysicalActivityAssessment::class, $paassessmentQuantity)->create();
        factory(PhysicalActivityEnviro::class, $paenviroQuantity)->create();
        factory(PhysicalActivityBarriers::class, $pabarriersQuantity)->create();
    }
}

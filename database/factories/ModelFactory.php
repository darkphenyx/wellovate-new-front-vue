<?php

use App\Anthropometric;
use App\Behavior;
use App\BehaviorLocation;
use App\BehaviorRoute;
use App\Cardiovascular;
use App\Fitness;
use App\GoalName;
use App\GoalUser;
use App\GroupLead;
use App\GroupTeam;
use App\InactivityAssessment;
use App\MixedExperience;
use App\MixedRealityUse;
use App\Outcome;
use App\PhysicalActivity;
use App\PhysicalActivityAssessment;
use App\PhysicalActivityBarriers;
use App\PhysicalActivityChange;
use App\PhysicalActivityConfidence;
use App\PhysicalActivityEfficacy;
use App\PhysicalActivityEnviro;
use App\PhysicalActivityFun;
use App\PhysicalActivityProsCons;
use App\PhysicalActivitySupport;
use App\Sleep;
use App\User;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'age' => $faker->numberBetween(12, 105),
        'sex' => $faker->randomElement(['male', 'female']),
        'zip' => $faker->optional()->postcode,
        'facebook_name' => $faker->optional()->userName,
        'remember_token' => str_random(10),
        'verified' => $verified = $faker->randomElement([User::VERIFIED_USER, User::UNVERIFIED_USER]),
        'verification_token' => $verified == User::VERIFIED_USER ? null : User::generateVerificationCode(),
        'admin' => $verified = $faker->randomElement([User::ADMIN_USER, User::REGULAR_USER]),
    ];
});

$factory->define(GroupLead::class, function (Faker\Generator $faker){
    return [
        'coach_id' => User::all()->random()->id,
        'name' => $faker->company,
    ];
});

$factory->define(GroupTeam::class, function (Faker\Generator $faker){
    $coach = GroupLead::all()->random()->coach_id;
    $basics = User::all()->except($coach)->random();
    return [
        'basic_id' => $basics->id,
        'group_lead_id' => GroupLead::all()->random()->id,
    ];
});

$factory->define(MixedExperience::class, function (faker\Generator $faker){
    return [
        'name' => $faker->company,
    ];
});

$factory->define(MixedRealityUse::class, function (faker\Generator $faker){
    $startLobby = Carbon::createFromTimeStamp($faker->dateTimeBetween('-30 days', '+30 days')->getTimestamp());
    $startExp = Carbon::createFromFormat('Y-m-d H:i:s', $startLobby)->addMinutes(mt_rand(5,10));
    $endExp = Carbon::createFromFormat('Y-m-d H:i:s', $startExp)->addMinutes(mt_rand(10,90));
    $endLobby = Carbon::createFromFormat('Y-m-d H:i:s', $endExp)->addMinutes(mt_rand(1,20));
    return [
        'basic_id' => User::all()->random()->id,
        'experience_id' => MixedExperience::all()->random()->id,
        'start_lobby' => $startLobby,
        'end_lobby' => $endLobby,
        'start_experience' => $startExp,
        'end_experience' => $endExp,
    ];
});

$factory->define(GoalName::class, function (faker\Generator $faker){
    return [
        'name' => $faker->realText(400)
    ];
});

$factory->define(GoalUser::class, function (faker\Generator $faker){
    return [
        'basic_id' => User::all()->random()->id,
        'goal_id' => GoalName::all()->random()->id,
        'priority' => $faker->numberBetween(1, 3),
        'completed' => $faker->numberBetween(0, 1),
    ];
});

$factory->define(BehaviorRoute::class, function (faker\Generator $faker){
    return [
        'name' => $faker->streetName,
        'address' => $faker->streetAddress,
        'lat' => $faker->latitude,
        'lng' => $faker->longitude,
    ];
});

$factory->define(BehaviorLocation::class, function (faker\Generator $faker){
    return [
        'name' => $faker->streetName,
        'address' => $faker->streetAddress,
        'lat' => $faker->latitude,
        'lng' => $faker->longitude,
    ];
});

$factory->define(Behavior::class, function (faker\Generator $faker){
    return [
        'basic_id' => User::all()->random()->id,
        'smoking_status' => $faker->randomElement(['never', 'quit in last 6 months', 'quit more than 6 months ago', 'current']),
        'cigs_per_day' => $faker->numberBetween(0, 40),
        'years_smoked' => $faker->randomFloat(2,0,80),
        'location_id' => $faker->unique(true)->numberBetween(1, 100),
        'vape_draws_day' => $faker->numberBetween(0, 200),
        'route_id' => $faker->unique(true)->numberBetween(1, 100),
        'teeth_brushing' => $faker->numberBetween(0, 1),
        'teeth_flossing' => $faker->numberBetween(0, 1),
        'tv_bedroom' => $faker->numberBetween(0, 1),
        'uv_exposure' => $faker->randomFloat(2,0,80),
    ];
});

$factory->define(Sleep::class, function (Faker\Generator $faker){
    $startDate = Carbon::createFromTimeStamp($faker->dateTimeBetween('-30 days', '+30 days')->getTimestamp());
    $endDate = Carbon::createFromFormat('Y-m-d H:i:s', $startDate)->addMinutes(mt_rand(5,90));

    return [
        'basic_id' => User::all()->random()->id,
        'awake_per_sleep' => $faker->optional()->numberBetween(1,6),
        'restless' => $faker->optional()->numberBetween(1,10),
        'sleep_duration' => $faker->randomFloat(3,2,2000),
        'sleep_latency' => $faker->randomFloat(3,0,300),
        'sleep_position' => $faker->randomElement(['supine', 'prone', 'right lateral', 'left lateral']),
        'awake_while_restless' => $faker->randomFloat(3,0,300),
        'length_in_bed' => $faker->randomFloat(3,0,300),
        'wake_up' => $startDate,
        'time_out_bed' => $endDate,
    ];
});

$factory->define(Anthropometric::class, function (Faker\Generator $faker){
    return [
        'basic_id' => User::all()->random()->id,
        'bicep_circum' => $faker->randomFloat(4,7,120),
        'biologic_sex' => $faker->randomElement(['male', 'female', 'intersex']),
        'body_fat_percent' => $faker->randomFloat(3,3,85),
        'body_height' => $faker->randomFloat(3,50,250),
        'body_mass_index' => $faker->randomFloat(2,7,85),
        'bone_density' => $faker->randomFloat(2,-10,10),
        'bust_circum' => $faker->randomFloat(3,15,200),
        'fitzpatrick_skin' => $faker->randomElement(['I', 'II', 'III', 'IV', 'V', 'VI']),
        'height' => $faker->randomFloat(3,50,250),
        'ideal_body_weight' => $faker->randomFloat(4,5,200),
        'lean_body_weight' => $faker->randomFloat(4,5,200),
        'body_water' => $faker->randomFloat(3,5,98),
        'thigh_circum' => $faker->randomFloat(3,5,98),
        'waist_circum' => $faker->randomFloat(3,5,98),
        'water_content' => $faker->randomFloat(3,5,98),
        'weight' => $faker->randomFloat(4,5,200),
    ];
});

$factory->define(Fitness::class, function (Faker\Generator $faker){
    return [
        'basic_id' => User::all()->random()->id,
        'o2_consumption' => $faker->randomFloat(4,5,1200),
        'o2_consumption_max' => $faker->randomFloat(4,5,1200),
        'heart_rate_recovery' => $faker->randomFloat(3,1,120),
        'basal_metabolic' => $faker->randomFloat(3,1500,3900),
        'resting_energy' => $faker->randomFloat(3,500,2300),
    ];
});

$factory->define(Cardiovascular::class, function (Faker\Generator $faker){
    return [
        'basic_id' => User::all()->random()->id,
        'ankle_brachial_index' => $faker->randomFloat(3,20,140),
        'blood_pressure' => $faker->realText($faker->numberBetween(100, 190).'/'.$faker->numberBetween(70,120)),
        'diastolic_blood_pressure' => $faker->numberBetween(70, 120),
        'systolic_blood_pressure' => $faker->numberBetween(100, 190),
        'endothelial' => $faker->randomFloat(3,20,98),
        'reactive_hyperenia' => $faker->randomFloat(3,20,98),
        'heart_rate' => $faker->numberBetween(75, 190),
        'max_heart_rate' => $faker->numberBetween(120, 215),
        'pulse_wave_velocity' => $faker->randomFloat(4,20,250),
        'qtc' => $faker->randomFloat(4,20,250),
        'rr_interval' => $faker->randomFloat(4,20,250),
        'o2_saturation' => $faker->randomFloat(3,20,99),
        'pulse_oximetry' => $faker->randomFloat(3,20,99),
    ];
});

$factory->define(Outcome::class, function (Faker\Generator $faker){
    return [
        'basic_id' => $faker->unique(true)->numberBetween(1, 1000),
        'alzheimers' => $faker->numberBetween(0, 1),
        'gen_anxiety_disorder' => $faker->numberBetween(0, 1),
        'cardiovascular_disease' => $faker->numberBetween(0, 1),
        'coronary_heart_disease' => $faker->numberBetween(0, 1),
        'dementia' => $faker->numberBetween(0, 1),
        'depression' => $faker->numberBetween(0, 1),
        'diabetes' => $faker->numberBetween(0, 1),
        'healthy' => $faker->numberBetween(0, 1),
        'hypertension' => $faker->numberBetween(0, 1),
        'inactive' => $faker->numberBetween(0, 1),
        'mild_cognitive_impairment' => $faker->numberBetween(0, 1),
        'number_falls' => $faker->numberBetween(0, 6),
        'underweight' => $faker->numberBetween(0, 1),
        'normal_weight' => $faker->numberBetween(0, 1),
        'overweight' => $faker->numberBetween(0, 1),
        'obesity_1' => $faker->numberBetween(0, 1),
        'obesity_2' => $faker->numberBetween(0, 1),
        'obesity_3' => $faker->numberBetween(0, 1),
        'partial_sleep_deprivation' => $faker->numberBetween(0, 1),
        'prediabetes' => $faker->numberBetween(0, 1),
        'prehypertension' => $faker->numberBetween(0, 1),
        'subclinical_depression' => $faker->numberBetween(0, 1),
        'subjective_poor_sleep' => $faker->numberBetween(0, 1),
        'total_sleep_deprivation' => $faker->numberBetween(0, 1),
        'high_ldl_concentration' => $faker->numberBetween(0, 1),
        'high_ldl_particle' => $faker->numberBetween(0, 1),
        'high_triglycerides' => $faker->numberBetween(0, 1),
        'low_hdl' => $faker->numberBetween(0, 1),
    ];
});

$factory->define(InactivityAssessment::class, function (Faker\Generator $faker){
    return [
        'basic_id' => $faker->unique(true)->numberBetween(1, 1000),
        'time_watch_tv_weekday' => $faker->randomElement(['Less than 30 minutes', 'Between 30 minutes and 1 hour', 'Between 1 and 2 hours', 'Between 2 and 3 hours', 'Between 3 and 4 hours', 'Between 4 and 5 hours', 'Greater than 5 hours']),
        'time_sitting_weekday' => $faker->randomElement(['Less than 30 minutes', 'Between 30 minutes and 1 hour', 'Between 1 and 2 hours', 'Between 2 and 3 hours', 'Between 3 and 4 hours', 'Between 4 and 5 hours', 'Greater than 5 hours']),
        'time_watch_tv_weekend' => $faker->randomElement(['Less than 30 minutes', 'Between 30 minutes and 1 hour', 'Between 1 and 2 hours', 'Between 2 and 3 hours', 'Between 3 and 4 hours', 'Between 4 and 5 hours', 'Greater than 5 hours']),
        'time_sitting_weekend' => $faker->randomElement(['Less than 30 minutes', 'Between 30 minutes and 1 hour', 'Between 1 and 2 hours', 'Between 2 and 3 hours', 'Between 3 and 4 hours', 'Between 4 and 5 hours', 'Greater than 5 hours']),
    ];
});

$factory->define(PhysicalActivity::class, function (Faker\Generator $faker){
    return [
        'basic_id' => User::all()->random()->id,
        'active_minutes' => $faker->randomFloat(3,20,900),
        'cadence' => $faker->randomFloat(3,20,1500),
        'calories_burned' => $faker->randomFloat(3,20,1500),
        'distance' => $faker->randomFloat(3,20,500),
        'exercise_minutes' => $faker->randomFloat(3,20,800),
        'floors_climbed' => $faker->numberBetween(0, 22),
        'inactivity' => $faker->randomFloat(3,20,2200),
        'moderate_vigorous' => $faker->randomFloat(3,20,600),
        'peak_accel' => $faker->randomFloat(3,20,600),
        'sitting_time' => $faker->randomFloat(3,20,1500),
        'speed' => $faker->randomFloat(3,20,500),
        'step_count' => $faker->numberBetween(0, 20000),
    ];
});

$factory->define(PhysicalActivityFun::class, function (Faker\Generator $faker){
    return [
        'basic_id' => User::all()->random()->id,
        'enjoy_exercise' => $faker->randomElement(['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']),
        'feel_good' => $faker->randomElement(['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']),
        'fun_exercising' => $faker->randomElement(['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']),
        'watching_tv' => $faker->randomElement(['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']),
        'enjoy_computer' => $faker->randomElement(['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']),
        'enjoy_reading' => $faker->randomElement(['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']),
    ];
});

$factory->define(PhysicalActivitySupport::class, function (Faker\Generator $faker){
    return [
        'basic_id' => User::all()->random()->id,
        'encourage_exercise' => $faker->randomElement(['never', 'almost never', 'sometimes', 'often', 'very often']),
        'offered_participate' => $faker->randomElement(['never', 'almost never', 'sometimes', 'often', 'very often']),
        'exercised_with' => $faker->randomElement(['never', 'almost never', 'sometimes', 'often', 'very often']),
        'discouraged' => $faker->randomElement(['never', 'almost never', 'sometimes', 'often', 'very often']),
    ];
});

$factory->define(PhysicalActivityEfficacy::class, function (Faker\Generator $faker){
    return [
        'basic_id' => User::all()->random()->id,
        'seek_information' => $faker->randomElement(['never', 'almost never', 'sometimes', 'often', 'very often']),
        'track_exercise' => $faker->randomElement(['never', 'almost never', 'sometimes', 'often', 'very often']),
        'around_issues' => $faker->randomElement(['never', 'almost never', 'sometimes', 'often', 'very often']),
        'set_reminders' => $faker->randomElement(['never', 'almost never', 'sometimes', 'often', 'very often']),
        'is_rewarding' => $faker->randomElement(['never', 'almost never', 'sometimes', 'often', 'very often']),
        'take_actions' => $faker->randomElement(['never', 'almost never', 'sometimes', 'often', 'very often']),
        'focus_benefits' => $faker->randomElement(['never', 'almost never', 'sometimes', 'often', 'very often']),
        'set_goals' => $faker->randomElement(['never', 'almost never', 'sometimes', 'often', 'very often']),
    ];
});

$factory->define(PhysicalActivityConfidence::class, function (Faker\Generator $faker){
    return [
        'basic_id' => User::all()->random()->id,
        'tired' => $faker->randomElement(['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']),
        'bad_mood' => $faker->randomElement(['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']),
        'stressed' => $faker->randomElement(['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']),
        'no_time' => $faker->randomElement(['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']),
        'bad_weather' => $faker->randomElement(['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']),
    ];
});

$factory->define(PhysicalActivityProsCons::class, function (Faker\Generator $faker){
    return [
        'basic_id' => User::all()->random()->id,
        'improve_health' => $faker->randomElement(['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']),
        'uncomfortable' => $faker->randomElement(['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']),
        'improve_energy' => $faker->randomElement(['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']),
        'improve_mood' => $faker->randomElement(['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']),
        'improve_mental' => $faker->randomElement(['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']),
        'relieve_stress' => $faker->randomElement(['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']),
        'self_image' => $faker->randomElement(['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']),
        'more_attractive' => $faker->randomElement(['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']),
        'others_see' => $faker->randomElement(['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']),
        'lose_weight' => $faker->randomElement(['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']),
        'make_friends' => $faker->randomElement(['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']),
        'waste_time' => $faker->randomElement(['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']),
        'less_time' => $faker->randomElement(['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']),
    ];
});

$factory->define(PhysicalActivityChange::class, function (Faker\Generator $faker){
    return [
        'basic_id' => User::all()->random()->id,
        'on_average' => $faker->randomElement(['0', '1', '2', '3', '4', '5', '6', '7']),
        'how_long' => $faker->randomElement(['less than 6 months', '6 months or more']),
        'going_forward' => $faker->randomElement(['No and not planning to', 'Yes and plan to start in next 6 months', 'Yes and plan to start in next 30 days']),
    ];
});

$factory->define(PhysicalActivityAssessment::class, function (Faker\Generator $faker){
    return [
        'basic_id' => $faker->unique(true)->numberBetween(1, 1000),
        'walking' => $faker->randomElement(['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']),
        'hiking' => $faker->randomElement(['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']),
        'running' => $faker->randomElement(['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']),
        'bicycling' => $faker->randomElement(['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']),
        'weight_training' => $faker->randomElement(['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']),
        'calisthenics' => $faker->randomElement(['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']),
        'swimming' => $faker->randomElement(['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']),
        'yoga' => $faker->randomElement(['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']),
        'tennis' => $faker->randomElement(['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']),
        'table_tennis' => $faker->randomElement(['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']),
        'aerobic' => $faker->randomElement(['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']),
        'kayaking' => $faker->randomElement(['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']),
        'martial_arts' => $faker->randomElement(['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']),
        'basketball' => $faker->randomElement(['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']),
        'soccer' => $faker->randomElement(['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']),
        'golf' => $faker->randomElement(['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']),
        'skiing' => $faker->randomElement(['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']),
        'volleyball' => $faker->randomElement(['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']),
        'gardening' => $faker->randomElement(['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']),
        'skating' => $faker->randomElement(['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']),
        'children' => $faker->randomElement(['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']),
    ];
});

$factory->define(PhysicalActivityEnviro::class, function (Faker\Generator $faker){
    return [
        'basic_id' => $faker->unique(true)->numberBetween(1, 1000),
        'access_equipment' => $faker->randomElement(['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']),
        'walk_traffic' => $faker->randomElement(['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']),
        'walk_safe' => $faker->randomElement(['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']),
    ];
});

$factory->define(PhysicalActivityBarriers::class, function (Faker\Generator $faker){
    return [
        'basic_id' => User::all()->random()->id,
        'lack_someone' => $faker->randomElement(['never', 'almost never', 'sometimes', 'often', 'very often']),
        'lack_of_time' => $faker->randomElement(['never', 'almost never', 'sometimes', 'often', 'very often']),
        'family_obligation' => $faker->randomElement(['never', 'almost never', 'sometimes', 'often', 'very often']),
        'lack_energy' => $faker->randomElement(['never', 'almost never', 'sometimes', 'often', 'very often']),
        'self_conscious' => $faker->randomElement(['never', 'almost never', 'sometimes', 'often', 'very often']),
        'lack_equipment' => $faker->randomElement(['never', 'almost never', 'sometimes', 'often', 'very often']),
        'lack_place' => $faker->randomElement(['never', 'almost never', 'sometimes', 'often', 'very often']),
        'fear_injured' => $faker->randomElement(['never', 'almost never', 'sometimes', 'often', 'very often']),
        'stressed_out' => $faker->randomElement(['never', 'almost never', 'sometimes', 'often', 'very often']),
        'discouraged' => $faker->randomElement(['never', 'almost never', 'sometimes', 'often', 'very often']),
        'not_healthy_enough' => $faker->randomElement(['never', 'almost never', 'sometimes', 'often', 'very often']),
        'find_boring' => $faker->randomElement(['never', 'almost never', 'sometimes', 'often', 'very often']),
        'lack_knowledge' => $faker->randomElement(['never', 'almost never', 'sometimes', 'often', 'very often']),
        'lack_skills' => $faker->randomElement(['never', 'almost never', 'sometimes', 'often', 'very often']),
        'lack_motivation' => $faker->randomElement(['never', 'almost never', 'sometimes', 'often', 'very often']),
        'pain' => $faker->randomElement(['never', 'almost never', 'sometimes', 'often', 'very often']),
        'bad_weather' => $faker->randomElement(['never', 'almost never', 'sometimes', 'often', 'very often']),
        'other' => $faker->realText(800),
    ];
});
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFitnessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fitnesses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('basic_id')->unsigned();
            $table->double('o2_consumption',8,4)->comment('mL/kg/minute');
            $table->double('o2_consumption_max',8,4)->comment('mL/kg/minute maximum rate of oxygen consumption as measured during incremental exercise');
            $table->double('heart_rate_recovery',6,3)->comment('Time from maximum heart rate to return to baseline heart rate');
            $table->double('basal_metabolic',7,3)->comment('kcal/day');
            $table->double('resting_energy',7,3)->comment('kcal/day');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('basic_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fitnesses');
    }
}

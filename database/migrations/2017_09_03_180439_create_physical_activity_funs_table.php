<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhysicalActivityFunsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('physical_activity_funs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('basic_id')->unsigned();
            $table->enum('enjoy_exercise', ['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']);
            $table->enum('feel_good', ['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']);
            $table->enum('fun_exercising', ['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']);
            $table->enum('watching_tv', ['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']);
            $table->enum('enjoy_computer', ['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']);
            $table->enum('enjoy_reading', ['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('basic_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('physical_activity_funs');
    }
}

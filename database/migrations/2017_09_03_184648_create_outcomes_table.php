<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOutcomesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('outcomes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('basic_id')->unsigned();
            $table->tinyInteger('alzheimers')->default(0);
            $table->tinyInteger('gen_anxiety_disorder')->default(0);
            $table->tinyInteger('cardiovascular_disease')->default(0);
            $table->tinyInteger('coronary_heart_disease')->default(0);
            $table->tinyInteger('dementia')->default(0);
            $table->tinyInteger('depression')->default(0);
            $table->tinyInteger('diabetes')->default(0);
            $table->tinyInteger('healthy')->default(0);
            $table->tinyInteger('hypertension')->default(0);
            $table->tinyInteger('inactive')->default(0);
            $table->tinyInteger('mild_cognitive_impairment')->default(0);
            $table->integer('number_falls')->default(0);
            $table->tinyInteger('underweight')->default(0);
            $table->tinyInteger('normal_weight')->default(0);
            $table->tinyInteger('overweight')->default(0);
            $table->tinyInteger('obesity_1')->default(0);
            $table->tinyInteger('obesity_2')->default(0);
            $table->tinyInteger('obesity_3')->default(0);
            $table->tinyInteger('partial_sleep_deprivation')->default(0);
            $table->tinyInteger('prediabetes')->default(0);
            $table->tinyInteger('prehypertension')->default(0);
            $table->tinyInteger('subclinical_depression')->default(0);
            $table->tinyInteger('subjective_poor_sleep')->default(0);
            $table->tinyInteger('total_sleep_deprivation')->default(0);
            $table->tinyInteger('high_ldl_concentration')->default(0);
            $table->tinyInteger('high_ldl_particle')->default(0);
            $table->tinyInteger('high_triglycerides')->default(0);
            $table->tinyInteger('low_hdl')->default(0);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('basic_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('outcomes');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInactivityAssessmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inactivity_assessments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('basic_id')->unsigned();
            $table->enum('time_watch_tv_weekday', ['Less than 30 minutes', 'Between 30 minutes and 1 hour', 'Between 1 and 2 hours', 'Between 2 and 3 hours', 'Between 3 and 4 hours', 'Between 4 and 5 hours', 'Greater than 5 hours']);
            $table->enum('time_sitting_weekday', ['Less than 30 minutes', 'Between 30 minutes and 1 hour', 'Between 1 and 2 hours', 'Between 2 and 3 hours', 'Between 3 and 4 hours', 'Between 4 and 5 hours', 'Greater than 5 hours']);
            $table->enum('time_watch_tv_weekend', ['Less than 30 minutes', 'Between 30 minutes and 1 hour', 'Between 1 and 2 hours', 'Between 2 and 3 hours', 'Between 3 and 4 hours', 'Between 4 and 5 hours', 'Greater than 5 hours']);
            $table->enum('time_sitting_weekend', ['Less than 30 minutes', 'Between 30 minutes and 1 hour', 'Between 1 and 2 hours', 'Between 2 and 3 hours', 'Between 3 and 4 hours', 'Between 4 and 5 hours', 'Greater than 5 hours']);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('basic_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inactivity_assessments');
    }
}

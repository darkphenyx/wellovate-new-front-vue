<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhysicalActivityEfficaciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('physical_activity_efficacies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('basic_id')->unsigned();
            $table->enum('seek_information', ['never', 'almost never', 'sometimes', 'often', 'very often']);
            $table->enum('track_exercise', ['never', 'almost never', 'sometimes', 'often', 'very often']);
            $table->enum('around_issues', ['never', 'almost never', 'sometimes', 'often', 'very often']);
            $table->enum('set_reminders', ['never', 'almost never', 'sometimes', 'often', 'very often']);
            $table->enum('is_rewarding', ['never', 'almost never', 'sometimes', 'often', 'very often']);
            $table->enum('take_actions', ['never', 'almost never', 'sometimes', 'often', 'very often']);
            $table->enum('focus_benefits', ['never', 'almost never', 'sometimes', 'often', 'very often']);
            $table->enum('set_goals', ['never', 'almost never', 'sometimes', 'often', 'very often']);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('basic_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('physical_activity_efficacies');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhysicalActivityProsConsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('physical_activity_pros_cons', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('basic_id')->unsigned();
            $table->enum('improve_health', ['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']);
            $table->enum('uncomfortable', ['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']);
            $table->enum('improve_energy', ['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']);
            $table->enum('improve_mood', ['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']);
            $table->enum('improve_mental', ['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']);
            $table->enum('relieve_stress', ['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']);
            $table->enum('self_image', ['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']);
            $table->enum('more_attractive', ['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']);
            $table->enum('others_see', ['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']);
            $table->enum('lose_weight', ['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']);
            $table->enum('make_friends', ['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']);
            $table->enum('waste_time', ['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']);
            $table->enum('less_time', ['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('basic_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('physical_activity_pros_cons');
    }
}

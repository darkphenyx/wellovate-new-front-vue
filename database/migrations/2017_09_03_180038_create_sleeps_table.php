<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSleepsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sleeps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('basic_id')->unsigned();
            $table->integer('awake_per_sleep')->nullable();
            $table->integer('restless')->nullable();
            $table->double('sleep_duration',6,3);
            $table->double('sleep_latency',6,3);
            $table->enum('sleep_position', ['supine', 'prone', 'right lateral', 'left lateral'])->nullable();
            $table->double('awake_while_restless',6,3);
            $table->double('length_in_bed',6,3);
            $table->timestamp('wake_up');
            $table->timestamp('time_out_bed');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('basic_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sleeps');
    }
}

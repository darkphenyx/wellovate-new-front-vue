<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBehaviorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('behaviors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('basic_id')->unsigned();
            $table->enum('smoking_status', ['never', 'quit in last 6 months', 'quit more than 6 months ago', 'current']);
            $table->integer('cigs_per_day')->default(0);
            $table->double('years_smoked', 4,2);
            $table->integer('location_id')->unsigned();
            $table->integer('vape_draws_day')->default(0);
            $table->integer('route_id')->unsigned();
            $table->tinyInteger('teeth_brushing')->default(0);
            $table->tinyInteger('teeth_flossing')->default(0);
            $table->tinyInteger('tv_bedroom')->default(0);
            $table->double('uv_exposure', 4,2);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('basic_id')->references('id')->on('users');
            $table->foreign('location_id')->references('id')->on('behavior_locations');
            $table->foreign('route_id')->references('id')->on('behavior_routes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('behaviors');
    }
}

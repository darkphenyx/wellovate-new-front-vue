<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePolarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('polars', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('basic_id')->unsigned();
            $table->string('access_token');
            $table->string('token_type');
            $table->string('x_user_id');
            $table->timestamps();

            $table->softDeletes();
            $table->foreign('basic_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('polars');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhysicalActivityEnvirosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('physical_activity_enviros', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('basic_id')->unsigned();
            $table->enum('access_equipment', ['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']);
            $table->enum('walk_traffic', ['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']);
            $table->enum('walk_safe', ['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('basic_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('physical_activity_enviros');
    }
}

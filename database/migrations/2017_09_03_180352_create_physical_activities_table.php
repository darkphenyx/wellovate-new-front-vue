<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhysicalActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('physical_activities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('basic_id')->unsigned();
            $table->double('active_minutes',6,3)->comment('per 24 hour period');
            $table->double('cadence',7,3)->comment('steps/minute');
            $table->double('calories_burned',7,3)->comment('kcal');
            $table->double('distance',6,3)->comment('km');
            $table->double('exercise_minutes',6,3)->comment('minutes');
            $table->integer('floors_climbed');
            $table->double('inactivity',7,3);
            $table->double('moderate_vigorous',6,3);
            $table->double('peak_accel',6,3);
            $table->double('sitting_time',7,3);
            $table->double('speed',6,3);
            $table->integer('step_count');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('basic_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('physical_activities');
    }
}

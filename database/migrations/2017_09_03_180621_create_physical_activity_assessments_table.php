<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhysicalActivityAssessmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('physical_activity_assessments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('basic_id')->unsigned();
            $table->enum('walking', ['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']);
            $table->enum('hiking', ['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']);
            $table->enum('running', ['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']);
            $table->enum('bicycling', ['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']);
            $table->enum('weight_training', ['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']);
            $table->enum('calisthenics', ['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']);
            $table->enum('swimming', ['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']);
            $table->enum('yoga', ['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']);
            $table->enum('tennis', ['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']);
            $table->enum('table_tennis', ['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']);
            $table->enum('aerobic', ['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']);
            $table->enum('kayaking', ['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']);
            $table->enum('martial_arts', ['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']);
            $table->enum('basketball', ['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']);
            $table->enum('soccer', ['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']);
            $table->enum('golf', ['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']);
            $table->enum('skiing', ['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']);
            $table->enum('volleyball', ['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']);
            $table->enum('gardening', ['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']);
            $table->enum('skating', ['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']);
            $table->enum('children', ['strongly disagree', 'somewhat disagree', 'neutral', 'somewhat agree', 'strongly agree']);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('basic_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('physical_activity_assessments');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhysicalActivityBarriersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('physical_activity_barriers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('basic_id')->unsigned();
            $table->enum('lack_someone', ['never', 'almost never', 'sometimes', 'often', 'very often']);
            $table->enum('lack_of_time', ['never', 'almost never', 'sometimes', 'often', 'very often']);
            $table->enum('family_obligation', ['never', 'almost never', 'sometimes', 'often', 'very often']);
            $table->enum('lack_energy', ['never', 'almost never', 'sometimes', 'often', 'very often']);
            $table->enum('self_conscious', ['never', 'almost never', 'sometimes', 'often', 'very often']);
            $table->enum('lack_equipment', ['never', 'almost never', 'sometimes', 'often', 'very often']);
            $table->enum('lack_place', ['never', 'almost never', 'sometimes', 'often', 'very often']);
            $table->enum('fear_injured', ['never', 'almost never', 'sometimes', 'often', 'very often']);
            $table->enum('stressed_out', ['never', 'almost never', 'sometimes', 'often', 'very often']);
            $table->enum('discouraged', ['never', 'almost never', 'sometimes', 'often', 'very often']);
            $table->enum('not_healthy_enough', ['never', 'almost never', 'sometimes', 'often', 'very often']);
            $table->enum('find_boring', ['never', 'almost never', 'sometimes', 'often', 'very often']);
            $table->enum('lack_knowledge', ['never', 'almost never', 'sometimes', 'often', 'very often']);
            $table->enum('lack_skills', ['never', 'almost never', 'sometimes', 'often', 'very often']);
            $table->enum('lack_motivation', ['never', 'almost never', 'sometimes', 'often', 'very often']);
            $table->enum('pain', ['never', 'almost never', 'sometimes', 'often', 'very often']);
            $table->enum('bad_weather', ['never', 'almost never', 'sometimes', 'often', 'very often']);
            $table->string('other', 1000)->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('basic_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('physical_activity_barriers');
    }
}

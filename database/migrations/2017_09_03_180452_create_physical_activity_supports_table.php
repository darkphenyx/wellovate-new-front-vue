<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhysicalActivitySupportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('physical_activity_supports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('basic_id')->unsigned();
            $table->enum('encourage_exercise', ['never', 'almost never', 'sometimes', 'often', 'very often']);
            $table->enum('offered_participate', ['never', 'almost never', 'sometimes', 'often', 'very often']);
            $table->enum('exercised_with', ['never', 'almost never', 'sometimes', 'often', 'very often']);
            $table->enum('discouraged', ['never', 'almost never', 'sometimes', 'often', 'very often']);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('basic_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('physical_activity_supports');
    }
}

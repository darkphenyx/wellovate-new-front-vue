<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardiovascularsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cardiovasculars', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('basic_id')->unsigned();
            $table->double('ankle_brachial_index',6,3)->comment('calculated by dividing the systolic blood pressure in the lower leg (above ankle) by the systolic blood pressure in the arm');
            $table->string('blood_pressure')->comment('systolic mmHg/diastolic mmHg');
            $table->integer('diastolic_blood_pressure')->comment('Measured in mmHg');
            $table->integer('systolic_blood_pressure')->comment('Measured in mmHg');
            $table->double('endothelial',6,3)->comment('percentage flow-mediated dilation (percentage FMD expressed as decimal) index');
            $table->double('reactive_hyperenia',6,3)->comment('measure of endothelial function');
            $table->integer('heart_rate')->comment('Measured in bpm');
            $table->integer('max_heart_rate')->comment('Measured in bpm. Equals 220-age');
            $table->double('pulse_wave_velocity', 8,4)->comment('Measured in m/s. Velocity at which the arterial pulse propagates through the circulatory system');
            $table->double('qtc',8,4);
            $table->double('rr_interval',8,4)->comment('Measured in milliseconds');
            $table->double('o2_saturation',6,3)->comment('Percentage expressed as a decimal in database');
            $table->double('pulse_oximetry',6,3)->comment('Percentage expressed as a decimal in database');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('basic_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cardiovasculars');
    }
}

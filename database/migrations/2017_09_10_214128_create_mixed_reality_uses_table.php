<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMixedRealityUsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mixed_reality_uses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('basic_id')->unsigned();
            $table->integer('experience_id')->unsigned();
            $table->timestamp('start_lobby')->nullable();
            $table->timestamp('end_lobby')->nullable();
            $table->timestamp('start_experience')->nullable();
            $table->timestamp('end_experience')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('basic_id')->references('id')->on('users');
            $table->foreign('experience_id')->references('id')->on('mixed_experiences');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mixed_reality_uses');
    }
}

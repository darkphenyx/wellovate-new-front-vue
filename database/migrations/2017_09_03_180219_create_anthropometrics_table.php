<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnthropometricsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('anthropometrics', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('basic_id')->unsigned();
            $table->double('bicep_circum', 6, 4)->comment('Measured in cm');
            $table->enum('biologic_sex', ['male', 'female', 'intersex']);
            $table->double('body_fat_percent', 6,3);
            $table->double('body_height', 6,3)->comment('Measured in cm');
            $table->double('body_mass_index', 5,2)->comment('Measured in kg/m^2');
            $table->decimal('bone_density',5,2)->comment('T-Score. It is the bone mineral density (BMD) at the site when compared to the young normal reference mean. It is a comparison of a patient\'s BMD to that of a healthy 30-year-old.  Normal DEXA T-score -1 and above, osteopenia T-score -1 to 2.49, osteoporosis T-score -2.5 and below');
            $table->double('bust_circum', 6,3)->comment('Measured in cm');
            $table->enum('fitzpatrick_skin', ['I', 'II', 'III', 'IV', 'V', 'VI'])->comment('Numerical classification schema for human skin color.');
            $table->double('height', 6,3)->comment('Measured in cm');
            $table->double('ideal_body_weight', 7,4)->comment('Measured in kg');
            $table->double('lean_body_weight', 7,4)->comment('Measured in kg');
            $table->double('body_water',6,3)->comment('Same as percent water content. percentage expressed as a decimal in database');
            $table->double('thigh_circum',6,3)->comment('Measured in cm');
            $table->double('waist_circum',6,3)->comment('Measured in cm');
            $table->double('water_content',6,3)->comment('Same as percent body water. percentage expressed as a decimal in database');
            $table->double('weight', 7,4)->comment('Measured in kg');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('basic_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('anthropometrics');
    }
}

<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
 * Anthropometrics
 * */
Route::resource('anthropometrics', 'Anthropometric\AnthropometricController', ['except' => ['create', 'edit', 'update']]);

/*
 * Basics
 * */
Route::resource('basics', 'Basic\BasicController', ['only' => ['index', 'show']]);
Route::resource('basics.anthropometrics', 'Basic\BasicAnthropometricController', ['only' => ['index', 'store']]);
Route::resource('basics.behaviors', 'Basic\BasicBehaviorController', ['only' => ['index', 'store']]);
Route::resource('basics.cardiovasculars', 'Basic\BasicCardiovascularController', ['only' => ['index', 'store']]);
Route::resource('basics.fitness', 'Basic\BasicFitnessController', ['only' => ['index', 'store']]);
Route::resource('basics.fitbit_auths', 'Basic\BasicFitbitAuthController', ['except' => ['create', 'edit', 'show']]);
Route::resource('basics.goal_users', 'Basic\BasicGoalUserController', ['only' => ['index', 'store']]);
Route::resource('basics.group_teams', 'Basic\BasicGroupTeamController', ['only' => ['index', 'store']]);
Route::resource('basics.inactivity', 'Basic\BasicInactivityController', ['only' => ['index', 'store']]);
Route::resource('basics.mixed_reality_uses', 'Basic\BasicMixedRealityUseController', ['only' => ['index', 'store']]);
Route::resource('basics.outcomes', 'Basic\BasicOutcomeController', ['only' => ['index', 'store']]);
Route::resource('basics.physical_activity', 'Basic\BasicPhysicalActivityController', ['only' => ['index', 'store']]);
Route::resource('basics.physical_activity_assessment', 'Basic\BasicPhysicalActivityAssessmentController', ['only' => ['index', 'store']]);
Route::resource('basics.physical_activity_barriers', 'Basic\BasicPhysicalActivityBarrierController', ['only' => ['index', 'store']]);
Route::resource('basics.physical_activity_changes', 'Basic\BasicPhysicalActivityChangeController', ['only' => ['index', 'store']]);
Route::resource('basics.physical_activity_confidences', 'Basic\BasicPhysicalActivityConfidenceController', ['only' => ['index', 'store']]);
Route::resource('basics.physical_activity_efficacys', 'Basic\BasicPhysicalActivityEfficacyController', ['only' => ['index', 'store']]);
Route::resource('basics.physical_activity_enviro', 'Basic\BasicPhysicalActivityEnviroController', ['only' => ['index', 'store']]);
Route::resource('basics.physical_activity_fun', 'Basic\BasicPhysicalActivityFunController', ['only' => ['index', 'store']]);
Route::resource('basics.physical_activity_pros_cons', 'Basic\BasicPhysicalActivityProsConsController', ['only' => ['index', 'store']]);
Route::resource('basics.physical_activity_support', 'Basic\BasicPhysicalActivitySupportController', ['only' => ['index', 'store']]);
Route::resource('basics.polars', 'Basic\BasicPolarController', ['except' => ['create', 'edit', 'show']]);
Route::resource('basics.sleeps', 'Basic\BasicSleepController', ['only' => ['index', 'store']]);
Route::resource('basics.questionnaires', 'Basic\BasicQuestionnaireAssignmentController', ['only' => ['index']]);
Route::resource('basics.questionnaire_names.answers', 'Basic\BasicQuestionAnswerController', ['only' => ['store']]);


/*
 * Behavior
 * */
Route::resource('behaviors', 'Behavior\BehaviorController', ['except' => ['create', 'edit']]);

/*
 * BehaviorLocation
 * */
Route::resource('behavior_locations', 'BehaviorLocation\BehaviorLocationController', ['except' => ['create', 'edit']]);

/*
 * BehaviorRoute
 * */
Route::resource('behavior_routes', 'BehaviorRoute\BehaviorRouteController', ['except' => ['create', 'edit']]);

/*
 * Cardiovasculars
 * */
Route::resource('cardiovasculars', 'Cardiovascular\CardiovascularController', ['except' => ['create', 'edit']]);

/*
 * Coaches
 * */
Route::resource('coaches', 'Coach\CoachController', ['only' => ['index', 'show']]);
Route::resource('coaches.group_leads', 'Coach\CoachGroupLeadController', ['only' => ['index', 'store']]);

/*
 * Fitness
 * */
Route::resource('fitness', 'Fitness\FitnessController', ['except' => ['create', 'edit', 'update']]);

/*
 * GoalName
 * */
Route::resource('goal_names', 'GoalName\GoalNameController', ['except' => ['create', 'edit']]);

/*
 * GoalUser
 * */
Route::resource('goal_users', 'GoalUser\GoalUserController', ['except' => ['create', 'edit']]);

/*
 * GroupLead
 * */
Route::resource('group_lead', 'GroupLead\GroupLeadController', ['except' => ['create', 'edit']]);

/*
 * GroupTeam
 * */
Route::resource('group_team', 'GroupTeam\GroupTeamController', ['except' => ['create', 'edit']]);

/*
 * InactivityAssessment
 * */
Route::resource('inactivity', 'InactivityAssessment\InactivityAssessmentController', ['except' => ['create', 'edit']]);

/*
 * MixedExperience
 * */
Route::resource('mixed_experiences', 'MixedExperience\MixedExperienceController', ['except' => ['create', 'edit']]);

/*
 * MixedRealityUse
 * */
Route::resource('mixed_reality_uses', 'MixedRealityUse\MixedRealityUseController', ['except' => ['create', 'edit']]);

/*
 * Outcome
 * */
Route::resource('outcomes', 'Outcome\OutcomeController', ['except' => ['create', 'edit']]);

/*
 * PhysicalActivity
 * */
Route::resource('physical_activity', 'PhysicalActivity\PhysicalActivityController', ['except' => ['create', 'edit']]);

/**
 * Perspective
 */
Route::resource('perspectives', 'Perspective\PerspectiveController', ['only' => ['index', 'show']]);

/*
 * PhysicalActivityAssessment
 * */
Route::resource('physical_activity_assessment', 'PhysicalActivityAssessment\PhysicalActivityAssessmentController', ['except' => ['create', 'edit']]);

/*
 * PhysicalActivityBarriers
 * */
Route::resource('physical_activity_barriers', 'PhysicalActivityBarriers\PhysicalActivityBarriersController', ['except' => ['create', 'edit']]);

/*
 * PhysicalActivityChange
 * */
Route::resource('physical_activity_change', 'PhysicalActivityChange\PhysicalActivityChangeController', ['except' => ['create', 'edit']]);

/*
 * PhysicalActivityConfidence
 * */
Route::resource('physical_activity_confidence', 'PhysicalActivityConfidence\PhysicalActivityConfidenceController', ['except' => ['create', 'edit']]);

/*
 * PhysicalActivityEfficacy
 * */
Route::resource('physical_activity_efficacy', 'PhysicalActivityEfficacy\PhysicalActivityEfficacyController', ['except' => ['create', 'edit']]);

/*
 * PhysicalActivityEnviro
 * */
Route::resource('physical_activity_enviro', 'PhysicalActivityEnviro\PhysicalActivityEnviroController', ['except' => ['create', 'edit']]);

/*
 * PhysicalActivityFun
 * */
Route::resource('physical_activity_fun', 'PhysicalActivityFun\PhysicalActivityFunController', ['except' => ['create', 'edit']]);

/*
 * PhysicalActivityProsCons
 * */
Route::resource('physical_activity_pros_cons', 'PhysicalActivityProsCons\PhysicalActivityProsConsController', ['except' => ['create', 'edit']]);

/*
 * PhysicalActivitySupport
 * */
Route::resource('physical_activity_support', 'PhysicalActivitySupport\PhysicalActivitySupportController', ['except' => ['create', 'edit']]);

/*
 * Sleep
 * */
Route::resource('sleeps', 'Sleep\SleepController', ['except' => ['create', 'edit']]);

/*
 * Users
 * */
Route::resource('users', 'User\UserController', ['except' => ['create', 'edit']]);
Route::name('verify')->get('users/verify/{token}', 'User\UserController@verify');
Route::name('resend')->get('users/{user}/resend', 'User\UserController@resend');

Route::post('game/login', 'Auth\GameLoginController@tryLogin');

Route::post('oauth/token', '\Laravel\Passport\Http\Controllers\AccessTokenController@issueToken');

Route::post('client/creds', 'Client\ClientController@getClient');

Route::post('site/login', 'Login\LoginController@login');

/*
 *  Questions
 */
Route::resource('enums', 'Enum\EnumController', ['except' => ['create', 'edit']]);
Route::resource('titles', 'Title\TitleController', ['except' => ['create', 'edit']]);
Route::resource('types', 'Type\TypeController', ['except' => ['create', 'edit']]);
Route::resource('questions', 'Question\QuestionController', ['except' => ['create', 'edit']]);
Route::post('question/add', 'Question\QuestionAdditionController@addQuestion');

/*
 *  Questionnaire
 */
Route::resource('questionnaire_answers', 'Questionnaire\QuestionnaireAnswerController', ['except' => ['create', 'edit']]);
Route::resource('questionnaire_assignments', 'Questionnaire\QuestionnaireAssignmentController', ['except' => ['create', 'edit']]);
Route::resource('questionnaire_names', 'Questionnaire\QuestionnaireNameController', ['except' => ['create', 'edit']]);
Route::resource('questionnaire_questions', 'Questionnaire\QuestionnaireQuestionController', ['except' => ['create', 'edit']]);
Route::resource('questionnaire_names.questionnaire_questions', 'Questionnaire\QuestionnaireNameQuestionnaireQuestionController', ['only' => ['index']]);
Route::post('questionnaire_create', 'Questionnaire\QuestionnaireCreationController@createQuestionnaire');
Route::post('assign_questionnaire', 'Questionnaire\QuestionnaireAssignmentController@assignQuestionnaire');

/*
 *  Mail
 */
Route::post('contact_mail', 'Email\EmailController@contactEmail');
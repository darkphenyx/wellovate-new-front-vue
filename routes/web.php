<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', 'WebController@home')->middleware('guest')->name('main');
Route::get('/solutions/', 'WebController@solutions')->middleware('guest')->name('solutions');
Route::get('/solutionsvalue/', 'WebController@solutionsvalue')->middleware('guest')->name('solutionsvalue');
Route::get('/consulting/', 'WebController@consulting')->middleware('guest')->name('consulting');
Route::get('/about/', 'WebController@about')->middleware('guest')->name('about');
Route::get('/mission/', 'WebController@mission')->name('mission');
Route::get('/whatwedo/', 'WebController@whatwedo')->name('whatwedo');
Route::get('/perspectives/', 'WebController@perspectives')->middleware('guest')->name('perspectives');
Route::get('/perspectivesview/', 'WebController@perspectivesview')->middleware('guest')->name('perspectivesview');
Route::get('contactform', 'WebController@contactform')->middleware('guest')->name('contactform');
Route::post('contactform', 'WebController@contact')->middleware('guest')->name('contactform');
Route::get('/realities/', 'WebController@realities')->middleware('guest')->name('realities');

// Authentication Routes...
$this->get('login', 'Auth\LoginController@showLoginForm')->name('login');
$this->post('login', 'Auth\LoginController@login');
$this->post('logout', 'Auth\LoginController@logout')->name('logout');

// Password Reset Routes...
$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
$this->post('password/reset', 'Auth\ResetPasswordController@reset');

Route::get('/home/authorized-clients', 'HomeController@getAuthorizedClients')->name('authorized-clients');
Route::get('/home/my-clients', 'HomeController@getClients')->name('personal-clients');
Route::get('/home/my-tokens', 'HomeController@getTokens')->name('personal-tokens');
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/fitbit', 'FitbitController@index')->name('fitbit');
Route::get('/fitbit/auth', 'FitbitController@goAuth')->name('fitbit-auth');
Route::get('/fitbit/authorized', 'FitbitController@getAuth')->name('fitbit-authorized');

Route::get('/polar', 'PolarController@index')->name('polar');
Route::get('/polar/auth', 'PolarController@goAuth')->name('polar-auth');
Route::get('/polar/authorized', 'PolarController@getAuth')->name('polar-authorized');

Route::get('/home/question-group', 'WebQuestionController@questionGroup')->name('question-group');
Route::get('/home/question-add', 'WebQuestionController@questionAdd')->name('question-add');
Route::get('/home/questionnaire-create', 'WebQuestionController@questionnaireCreate')->name('questionnaire-create');
Route::get('/home/questionnaire-users', 'WebQuestionController@questionnaireUsers')->name('questionnaire-users');*/

Route::get('/', function (){
    return view('welcome');
});

Route::get('{path}', function () {
    return view('welcome');
})->where( 'path', '([A-z\d-\/_.]+)?' );
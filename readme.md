<p align="center"><img src="https://wellovate.com/wp-content/uploads/2016/07/cropped-wellovatelogo03-3-4.jpeg"> with <img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>


## Get Dependant Software

First we must have all the dependant software installed on our machines

- PHP Version 7.1.* at time of writing [Install XAMPP or Other Apache Client](https://www.apachefriends.org/index.html).
- [Install Composer](https://getcomposer.org/).
- [Install Virtual Box](https://www.virtualbox.org/wiki/Downloads).
- [Install Vagrant](https://www.vagrantup.com/).
- [Favorite Text Editor](https://www.jetbrains.com/phpstorm/).
- Since I'm using Windows I need [Git Shell](https://git-scm.com/docs/git-shell). If you are using Linux / Mac you can skip this step.
- [Install Node / NPM](https://nodejs.org/en/) v 6.11.2.
- [Finally get Postman to test your API](https://www.getpostman.com/).

## Get Project Running

- Check project out from GIT repository to your local machine.
- Go to the directory in your local machine and type: `composer install`
- This will install the laravel dependencies.
- Change Homestead.yaml file to match where you have project checked out to on your machine.
- Make sure you have your ssh keys generated using the command `ssh-keygen -t rsa -b 4096` in your git bash terminal. There is no need for a passphrase in testing.

## Where Host File is Located

- For Some Mac and Linux the location is: `/etc/hosts`
- For other Mac versions: `/private/etc/hosts`
- For Windows the location is: `C:\Windows\System32\drivers\etc`

## Add to Host File

- Open host file in editor with admin privileges
- Add this line at the bottom.
- `192.168.10.11   wellovateapi.dev`
- or whatever you name the ip: and sites: map: variables in the Homestead.yaml file.
- Finally save this file (If it's not saving make sure the text editor you have it open in has admin privileges).

## Make Sure You're Up and Running

- Go to your terminal (git bash) into your project directory and type the following command.
- `vagrant up`
- This will start your vagrant virtual host.
- Once it is running type:
- `vagrant ssh`
- This will make it so you are logged into your vagrant VM so you can now issue your php artisan and composer commands.

## Get the database migrated and seeded

- Go to terminal make sure you are ssh'ed in as described above.
- Select the wellovateapi directory `cd wellovateapi`
- Migrate the database `php artisan migrate:refresh` This allows you to drop tables and refresh them.
- Seed database `php artisan db:seed` Wait a few as this process takes a little while.

## If using PhpStorm a.k.a the best IDE

- Once you have migrated your DB you can easily connect PhpStorm for ease of seeing and developing.
- [Setup SSH Connection](https://www.theodo.fr/blog/2017/03/how-to-manipulate-the-mysql-database-of-your-vagrant-from-phpstorm/).
- Then set your general tab based on the connections you have set up in your .env file.

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, yet powerful, providing tools needed for large, robust applications. A superb combination of simplicity, elegance, and innovation give you tools you need to build any application with which you are tasked.

## Learning Laravel

Laravel has the most extensive and thorough documentation and video tutorial library of any modern web application framework. The [Laravel documentation](https://laravel.com/docs) is thorough, complete, and makes it a breeze to get started learning the framework.

If you're not in the mood to read, [Laracasts](https://laracasts.com) contains over 900 video tutorials on a range of topics including Laravel, modern PHP, unit testing, JavaScript, and more. Boost the skill level of yourself and your entire team by digging into our comprehensive video library.

## Laravel Sponsors

We would like to extend our thanks to the following sponsors for helping fund on-going Laravel development. If you are interested in becoming a sponsor, please visit the Laravel [Patreon page](http://patreon.com/taylorotwell):

- **[Vehikl](http://vehikl.com)**
- **[Tighten Co.](https://tighten.co)**
- **[British Software Development](https://www.britishsoftware.co)**
- **[Styde](https://styde.net)**
- [Fragrantica](https://www.fragrantica.com)
- [SOFTonSOFA](https://softonsofa.com/)
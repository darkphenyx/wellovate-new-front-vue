<?php

namespace App;

use App\Transformers\PhysicalActivityAssessmentTransformer;
use Illuminate\Database\Eloquent\Model;
use App\Basic;
use Illuminate\Database\Eloquent\SoftDeletes;

class PhysicalActivityAssessment extends Model
{
    use SoftDeletes;

    public $transformer = PhysicalActivityAssessmentTransformer::class;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'basic_id',
        'walking',
        'hiking',
        'running',
        'bicycling',
        'weight_training',
        'calisthenics',
        'swimming',
        'yoga',
        'tennis',
        'table_tennis',
        'aerobic',
        'kayaking',
        'martial_arts',
        'basketball',
        'soccer',
        'golf',
        'skiing',
        'volleyball',
        'dancing',
        'gardening',
        'skating',
        'children',
    ];

    public function basic()
    {
        return $this->belongsTo(Basic::class);
    }
}

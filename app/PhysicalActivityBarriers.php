<?php

namespace App;

use App\Transformers\PhysicalActivityBarriersTransformer;
use Illuminate\Database\Eloquent\Model;
use App\Basic;
use Illuminate\Database\Eloquent\SoftDeletes;

class PhysicalActivityBarriers extends Model
{
    use SoftDeletes;

    public $transformer = PhysicalActivityBarriersTransformer::class;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'basic_id',
        'lack_someone',
        'lack_of_time',
        'family_obligation',
        'lack_energy',
        'self_conscious',
        'lack_equipment',
        'lack_place',
        'fear_injured',
        'stressed_out',
        'discouraged',
        'not_healthy_enough',
        'find_boring',
        'lack_knowledge',
        'lack_skills',
        'lack_motivation',
        'pain',
        'bad_weather',
        'other',
    ];

    public function basic()
    {
        return $this->belongsTo(Basic::class);
    }
}

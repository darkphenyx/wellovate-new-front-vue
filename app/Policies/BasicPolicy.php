<?php

namespace App\Policies;

use App\Traits\AdminActions;
use App\User;
use App\Basic;
use Illuminate\Auth\Access\HandlesAuthorization;

class BasicPolicy
{
    use HandlesAuthorization, AdminActions;

    /**
     * Determine whether the user can view the basic.
     *
     * @param  \App\User  $user
     * @param  \App\Basic  $basic
     * @return mixed
     */
    public function view(User $user, Basic $basic)
    {
        //dd($user);
        return $user->id === $basic->id;
    }

    /**
     * Determine whether the user can store/update the tracked data
     *
     * @param  \App\User  $user
     * @param  \App\Basic  $basic
     * @return mixed
     */
    public function make(User $user, Basic $basic)
    {
        //dd($user);
        return $user->id === $basic->id;
    }
}

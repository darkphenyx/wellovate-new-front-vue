<?php

namespace App\Policies;

use App\Traits\AdminActions;
use App\User;
use App\Coach;
use Illuminate\Auth\Access\HandlesAuthorization;

class CoachPolicy
{
    use HandlesAuthorization, AdminActions;

    /**
     * Determine whether the user can view the coach.
     *
     * @param  \App\User  $user
     * @param  \App\Coach  $coach
     * @return mixed
     */
    public function view(User $user, Coach $coach)
    {
        return $user->id === $coach->id;
    }

    /**
     * Determine whether the user can update the coach.
     *
     * @param  \App\User  $user
     * @param  \App\Coach  $coach
     * @return mixed
     */
    public function updateTeam(User $user, Coach $coach)
    {
        return $user->id === $coach->id;
    }

    /**
     * Determine whether the user can delete the coach.
     *
     * @param  \App\User  $user
     * @param  \App\Coach  $coach
     * @return mixed
     */
    public function deleteTeam(User $user, Coach $coach)
    {
        return $user->id === $coach->id;
    }
}

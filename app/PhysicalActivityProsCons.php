<?php

namespace App;

use App\Transformers\PhysicalActivityProsConsTransformer;
use Illuminate\Database\Eloquent\Model;
use App\Basic;
use Illuminate\Database\Eloquent\SoftDeletes;

class PhysicalActivityProsCons extends Model
{
    use SoftDeletes;

    public $transformer = PhysicalActivityProsConsTransformer::class;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'basic_id',
        'improve_health',
        'uncomfortable',
        'improve_energy',
        'improve_mood',
        'improve_mental',
        'relieve_stress',
        'self_image',
        'more_attractive',
        'others_see',
        'lose_weight',
        'make_friends',
        'waste_time',
        'less_time',
    ];

    public function basic()
    {
        return $this->belongsTo(Basic::class);
    }
}

<?php

namespace App\Transformers;

use App\PhysicalActivity;
use League\Fractal\TransformerAbstract;

class PhysicalActivityTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param PhysicalActivity $physicalActivity
     * @return array
     */
    public function transform(PhysicalActivity $physicalActivity)
    {
        return [
            'identifier' => (int)$physicalActivity->id,
            'user_identifier' => (int)$physicalActivity->basic_id,
            'minutes_active_per_24hr' => (double)$physicalActivity->active_minutes,
            'steps_per_minute' => (double)$physicalActivity->cadence,
            'calories_burned_per_24hr' => (double)$physicalActivity->calories_burned,
            'distance_traveled' => (double)$physicalActivity->distance,
            'minutes_exercised' => (double)$physicalActivity->exercise_minutes,
            'number_of_floors_climbed' => (int)$physicalActivity->floors_climbed,
            'minutes_inactive' => (double)$physicalActivity->inactivity,
            'minutes_of_moderate_vigorous_exercise' => (double)$physicalActivity->moderate_vigorous,
            'peak_acceleration' => (double)$physicalActivity->peak_accel,
            'minutes_of_sitting_time_per_24hr' => (double)$physicalActivity->sitting_time,
            'speed_in_meters_per_second' => (double)$physicalActivity->speed,
            'step_count_per_24hr' => (int)$physicalActivity->step_count,
            'creation_date' => (string)$physicalActivity->created_at,
            'last_changed' => (string)$physicalActivity->updated_at,
            'deleted_date' => isset($physicalActivity->deleted_at) ? (string)$physicalActivity->deleted_at : null,

            'links' => [
                [
                    'rel' => 'self',
                    'href' => route('physical_activity.show', $physicalActivity->id)
                ]
            ]
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes = [
            'identifier' => 'id',
            'user_identifier' => 'basic_id',
            'minutes_active_per_24hr' => 'active_minutes',
            'steps_per_minute' => 'cadence',
            'calories_burned_per_24hr' => 'calories_burned',
            'distance_traveled' => 'distance',
            'minutes_exercised' => 'exercise_minutes',
            'number_of_floors_climbed' => 'floors_climbed',
            'minutes_inactive' => 'inactivity',
            'minutes_of_moderate_vigorous_exercise' => 'moderate_vigorous',
            'peak_acceleration' => 'peak_accel',
            'minutes_of_sitting_time_per_24hr' => 'sitting_time',
            'speed_in_meters_per_second' => 'speed',
            'step_count_per_24hr' => 'step_count',
            'creation_date' => 'created_at',
            'last_changed' => 'updated_at',
            'deleted_date' => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
            'id' => 'identifier',
            'basic_id' => 'user_identifier',
            'active_minutes' => 'minutes_active_per_24hr',
            'cadence' => 'steps_per_minute',
            'calories_burned' => 'calories_burned_per_24hr',
            'distance' => 'distance_traveled',
            'exercise_minutes' => 'minutes_exercised',
            'floors_climbed' => 'number_of_floors_climbed',
            'inactivity' => 'minutes_inactive',
            'moderate_vigorous' => 'minutes_of_moderate_vigorous_exercise',
            'peak_accel' => 'peak_acceleration',
            'sitting_time' => 'minutes_of_sitting_time_per_24hr',
            'speed' => 'speed_in_meters_per_second',
            'step_count' => 'step_count_per_24hr',
            'created_at' => 'creation_date',
            'updated_at' => 'last_changed',
            'deleted_at' => 'deleted_date',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}

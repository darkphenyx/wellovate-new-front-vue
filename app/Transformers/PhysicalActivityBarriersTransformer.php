<?php

namespace App\Transformers;

use App\PhysicalActivityBarriers;
use League\Fractal\TransformerAbstract;

class PhysicalActivityBarriersTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param PhysicalActivityBarriers $physicalActivityBarriers
     * @return array
     */
    public function transform(PhysicalActivityBarriers $physicalActivityBarriers)
    {
        return [
            'identifier' => (int)$physicalActivityBarriers->id,
            'user_identifier' => (int)$physicalActivityBarriers->basic_id,
            'lack_someone_for_exercise' => (string)$physicalActivityBarriers->lack_someone,
            'lack_of_time_for_exercise' => (string)$physicalActivityBarriers->lack_of_time,
            'family_obligation_issues' => (string)$physicalActivityBarriers->family_obligation,
            'lack_energy_for_exercise' => (string)$physicalActivityBarriers->lack_energy,
            'self_conscious_around_others' => (string)$physicalActivityBarriers->self_conscious,
            'lack_equipment_for_exercise' => (string)$physicalActivityBarriers->lack_equipment,
            'lack_place_to_exercise' => (string)$physicalActivityBarriers->lack_place,
            'fear_of_being_injured' => (string)$physicalActivityBarriers->fear_injured,
            'being_stressed_out' => (string)$physicalActivityBarriers->stressed_out,
            'being_discouraged' => (string)$physicalActivityBarriers->discouraged,
            'not_healthy_enough_for_exercise' => (string)$physicalActivityBarriers->not_healthy_enough,
            'find_exercise_boring' => (string)$physicalActivityBarriers->find_boring,
            'lack_of_knowledge' => (string)$physicalActivityBarriers->lack_knowledge,
            'lack_of_skills' => (string)$physicalActivityBarriers->lack_skills,
            'lack_of_motivation' => (string)$physicalActivityBarriers->lack_motivation,
            'pain' => (string)$physicalActivityBarriers->pain,
            'bad_weather' => (string)$physicalActivityBarriers->bad_weather,
            'other_barriers' => (string)$physicalActivityBarriers->other,
            'creation_date' => (string)$physicalActivityBarriers->created_at,
            'last_changed' => (string)$physicalActivityBarriers->updated_at,
            'deleted_date' => isset($physicalActivityBarriers->deleted_at) ? (string)$physicalActivityBarriers->deleted_at : null,

            'links' => [
                [
                    'rel' => 'self',
                    'href' => route('physical_activity_barriers.show', $physicalActivityBarriers->id)
                ]
            ]
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes = [
            'identifier' => 'id',
            'user_identifier' => 'basic_id',
            'lack_someone_for_exercise' => 'lack_someone',
            'lack_of_time_for_exercise' => 'lack_of_time',
            'family_obligation_issues' => 'family_obligation',
            'lack_energy_for_exercise' => 'lack_energy',
            'self_conscious_around_others' => 'self_conscious',
            'lack_equipment_for_exercise' => 'lack_equipment',
            'lack_place_to_exercise' => 'lack_place',
            'fear_of_being_injured' => 'fear_injured',
            'being_stressed_out' => 'stressed_out',
            'being_discouraged' => 'discouraged',
            'not_healthy_enough_for_exercise' => 'not_healthy_enough',
            'find_exercise_boring' => 'find_boring',
            'lack_of_knowledge' => 'lack_knowledge',
            'lack_of_skills' => 'lack_skills',
            'lack_of_motivation' => 'lack_motivation',
            'pain' => 'pain',
            'bad_weather' => 'bad_weather',
            'other_barriers' => 'other',
            'creation_date' => 'created_at',
            'last_changed' => 'updated_at',
            'deleted_date' => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
            'id' => 'identifier',
            'basic_id' => 'user_identifier',
            'lack_someone' => 'lack_someone_for_exercise',
            'lack_of_time' => 'lack_of_time_for_exercise',
            'family_obligation' => 'family_obligation_issues',
            'lack_energy' => 'lack_energy_for_exercise',
            'self_conscious' => 'self_conscious_around_others',
            'lack_equipment' => 'lack_equipment_for_exercise',
            'lack_place' => 'lack_place_to_exercise',
            'fear_injured' => 'fear_of_being_injured',
            'stressed_out' => 'being_stressed_out',
            'discouraged' => 'being_discouraged',
            'not_healthy_enough' => 'not_healthy_enough_for_exercise',
            'find_boring' => 'find_exercise_boring',
            'lack_knowledge' => 'lack_of_knowledge',
            'lack_skills' => 'lack_of_skills',
            'lack_motivation' => 'lack_of_motivation',
            'pain' => 'pain',
            'bad_weather' => 'bad_weather',
            'other' => 'other_barriers',
            'created_at' => 'creation_date',
            'updated_at' => 'last_changed',
            'deleted_at' => 'deleted_date',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}

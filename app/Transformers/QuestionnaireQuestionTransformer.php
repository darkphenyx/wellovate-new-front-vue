<?php

namespace App\Transformers;

use App\QuestionnaireQuestion;
use League\Fractal\TransformerAbstract;

class QuestionnaireQuestionTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(QuestionnaireQuestion $questionnaireQuestion)
    {
        return [
            'identifier' => (int)$questionnaireQuestion->id,
            'questionnaire_name_identifier' => (int)$questionnaireQuestion->questionnaire_name_id,
            'question_identifier' => (int)$questionnaireQuestion->question_id,
            'creation_date' => (string)$questionnaireQuestion->created_at,
            'last_changed' => (string)$questionnaireQuestion->updated_at,
            'deleted_date' => isset($questionnaireQuestion->deleted_at) ? (string)$questionnaireQuestion->deleted_at : null,
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes = [
            'identifier' => 'id',
            'questionnaire_name_identifier' => 'questionnaire_name_id',
            'question_identifier' => 'question_id',
            'creation_date' => 'created_at',
            'last_changed' => 'updated_at',
            'deleted_date' => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
            'id' => 'identifier',
            'questionnaire_name_id' => 'questionnaire_name_identifier',
            'question_id' => 'question_identifier',
            'created_at' => 'creation_date',
            'updated_at' => 'last_changed',
            'deleted_at' => 'deleted_date',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}

<?php

namespace App\Transformers;

use App\Fitness;
use League\Fractal\TransformerAbstract;

class FitnessTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param Fitness $fitness
     * @return array
     */
    public function transform(Fitness $fitness)
    {
        return [
            'identifier' => (int)$fitness->id,
            'user_identifier' => (int)$fitness->basic_id,
            'oxygen_consumption' => (double)$fitness->o2_consumption,
            'oxygen_consumption_max' => (double)$fitness->o2_consumption_max,
            'max_heart_rate_to_baseline_time' => (double)$fitness->heart_rate_recovery,
            'basal_metabolic_rate' => (double)$fitness->basal_metabolic,
            'resting_energy' => (double)$fitness->resting_energy,
            'creation_date' => (string)$fitness->created_at,
            'last_changed' => (string)$fitness->updated_at,
            'deleted_date' => isset($fitness->deleted_at) ? (string)$fitness->deleted_at : null,

            'links' => [
                [
                    'rel' => 'self',
                    'href' => route('fitness.show', $fitness->id)
                ]
            ]
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes = [
            'identifier' => 'id',
            'user_identifier' => 'basic_id',
            'oxygen_consumption' => 'o2_consumption',
            'oxygen_consumption_max' => 'o2_consumption_max',
            'max_heart_rate_to_baseline_time' => 'heart_rate_recovery',
            'basal_metabolic_rate' => 'basal_metabolic',
            'resting_energy' => 'resting_energy',
            'creation_date' => 'created_at',
            'last_changed' => 'updated_at',
            'deleted_date' => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
            'id' => 'identifier',
            'basic_id' => 'user_identifier',
            'o2_consumption' => 'oxygen_consumption',
            'o2_consumption_max' => 'oxygen_consumption_max',
            'heart_rate_recovery' => 'max_heart_rate_to_baseline_time',
            'basal_metabolic' => 'basal_metabolic_rate',
            'resting_energy' => 'resting_energy',
            'created_at' => 'creation_date',
            'updated_at' => 'last_changed',
            'deleted_at' => 'deleted_date',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}

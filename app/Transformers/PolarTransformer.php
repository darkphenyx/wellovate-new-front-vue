<?php

namespace App\Transformers;

use App\Polar;
use League\Fractal\TransformerAbstract;

class PolarTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Polar $polar)
    {
        return [
            'identifier' => (int)$polar->id,
            'user_identifier' => (int)$polar->basic_id,
            'access_token' => (string)$polar->access_token,
            'token_type' => (string)$polar->token_type,
            'x_user_id' => (string)$polar->x_user_id,
            'creation_date' => (string)$polar->created_at,
            'last_changed' => (string)$polar->updated_at,
            'deleted_date' => isset($polar->deleted_at) ? (string)$polar->deleted_at : null,
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes = [
            'identifier' => 'id',
            'user_identifier' => 'basic_id',
            'access_token' => 'access_token',
            'token_type' => 'token_type',
            'x_user_id' => 'x_user_id',
            'creation_date' => 'created_at',
            'last_changed' => 'updated_at',
            'deleted_date' => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
            'id' => 'identifier',
            'basic_id' => 'user_identifier',
            'access_token' => 'access_token',
            'token_type' => 'token_type',
            'x_user_id' => 'x_user_id',
            'created_at' => 'creation_date',
            'updated_at' => 'last_changed',
            'deleted_at' => 'deleted_date',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}

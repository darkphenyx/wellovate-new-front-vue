<?php

namespace App\Transformers;

use App\Title;
use League\Fractal\TransformerAbstract;

class TitleTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Title $title)
    {
        return [
            'identifier' => (int)$title->id,
            'coach_identifier' => (int)$title->coach_id,
            'title' => (string)$title->question_title,
            'creation_date' => (string)$title->created_at,
            'last_changed' => (string)$title->updated_at,
            'deleted_date' => isset($title->deleted_at) ? (string)$title->deleted_at : null,
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes = [
            'identifier' => 'id',
            'coach_identifier' => 'coach_id',
            'title' => 'question_title',
            'creation_date' => 'created_at',
            'last_changed' => 'updated_at',
            'deleted_date' => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
            'id' => 'identifier',
            'coach_id' => 'coach_identifier',
            'question_title' => 'title',
            'created_at' => 'creation_date',
            'updated_at' => 'last_changed',
            'deleted_at' => 'deleted_date',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}

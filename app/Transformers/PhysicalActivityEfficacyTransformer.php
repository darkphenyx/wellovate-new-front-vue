<?php

namespace App\Transformers;

use App\PhysicalActivityEfficacy;
use League\Fractal\TransformerAbstract;

class PhysicalActivityEfficacyTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param PhysicalActivityEfficacy $physicalActivityEfficacy
     * @return array
     */
    public function transform(PhysicalActivityEfficacy $physicalActivityEfficacy)
    {
        return [
            'identifier' => (int)$physicalActivityEfficacy->id,
            'user_identifier' => (int)$physicalActivityEfficacy->basic_id,
            'seek_relating_information' => (string)$physicalActivityEfficacy->seek_information,
            'track_my_exercise' => (string)$physicalActivityEfficacy->track_exercise,
            'find_ways_around_issues' => (string)$physicalActivityEfficacy->around_issues,
            'set_reminders_to_exercise' => (string)$physicalActivityEfficacy->set_reminders,
            'think_exercise_is_rewarding' => (string)$physicalActivityEfficacy->is_rewarding,
            'take_actions_to_make_exercise_enjoyable' => (string)$physicalActivityEfficacy->take_actions,
            'focus_on_benefits_of_exercise' => (string)$physicalActivityEfficacy->focus_benefits,
            'set_goals_to_engage_in_exercise' => (string)$physicalActivityEfficacy->set_goals,
            'creation_date' => (string)$physicalActivityEfficacy->created_at,
            'last_changed' => (string)$physicalActivityEfficacy->updated_at,
            'deleted_date' => isset($physicalActivityEfficacy->deleted_at) ? (string)$physicalActivityEfficacy->deleted_at : null,

            'links' => [
                [
                    'rel' => 'self',
                    'href' => route('physical_activity_efficacy.show', $physicalActivityEfficacy->id)
                ]
            ]
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes = [
            'identifier' => 'id',
            'user_identifier' => 'basic_id',
            'seek_relating_information' => 'seek_information',
            'track_my_exercise' => 'track_exercise',
            'find_ways_around_issues' => 'around_issues',
            'set_reminders_to_exercise' => 'set_reminders',
            'think_exercise_is_rewarding' => 'is_rewarding',
            'take_actions_to_make_exercise_enjoyable' => 'take_actions',
            'focus_on_benefits_of_exercise' => 'focus_benefits',
            'set_goals_to_engage_in_exercise' => 'set_goals',
            'creation_date' => 'created_at',
            'last_changed' => 'updated_at',
            'deleted_date' => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
            'id' => 'identifier',
            'basic_id' => 'user_identifier',
            'seek_information' => 'seek_relating_information',
            'track_exercise' => 'track_my_exercise',
            'around_issues' => 'find_ways_around_issues',
            'set_reminders' => 'set_reminders_to_exercise',
            'is_rewarding' => 'think_exercise_is_rewarding',
            'take_actions' => 'take_actions_to_make_exercise_enjoyable',
            'focus_benefits' => 'focus_on_benefits_of_exercise',
            'set_goals' => 'set_goals_to_engage_in_exercise',
            'created_at' => 'creation_date',
            'updated_at' => 'last_changed',
            'deleted_at' => 'deleted_date',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}

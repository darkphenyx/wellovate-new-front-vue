<?php

namespace App\Transformers;

use App\QuestionnaireAssignment;
use League\Fractal\TransformerAbstract;

class QuestionnaireAssignmentTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(QuestionnaireAssignment $questionnaireAssignment)
    {
        return [
            'identifier' => (int)$questionnaireAssignment->id,
            'user_identifier' => (int)$questionnaireAssignment->basic_id,
            'question_name_identifier' => (int)$questionnaireAssignment->questionnaire_name_id,
            'been_answered' => (int)$questionnaireAssignment->answered,
            'creation_date' => (string)$questionnaireAssignment->created_at,
            'last_changed' => (string)$questionnaireAssignment->updated_at,
            'deleted_date' => isset($questionnaireAssignment->deleted_at) ? (string)$questionnaireAssignment->deleted_at : null,
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes = [
            'identifier' => 'id',
            'user_identifier' => 'basic_id',
            'question_name_identifier' => 'questionnaire_name_id',
            'been_answered' => 'answered',
            'creation_date' => 'created_at',
            'last_changed' => 'updated_at',
            'deleted_date' => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
            'id' => 'identifier',
            'basic_id' => 'user_identifier',
            'questionnaire_name_id' => 'question_name_identifier',
            'answered' => 'been_answered',
            'created_at' => 'creation_date',
            'updated_at' => 'last_changed',
            'deleted_at' => 'deleted_date',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}

<?php

namespace App\Transformers;

use App\Question;
use League\Fractal\TransformerAbstract;

class QuestionTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Question $question)
    {
        return [
            'identifier' => (int)$question->id,
            'title_identifier' => (int)$question->title_id,
            'type_identifier' => (int)$question->type_id,
            'question' => (string)$question->question,
            'creation_date' => (string)$question->created_at,
            'last_changed' => (string)$question->updated_at,
            'deleted_date' => isset($question->deleted_at) ? (string)$question->deleted_at : null,
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes = [
            'identifier' => 'id',
            'title_identifier' => 'title_id',
            'type_identifier' => 'type_id',
            'question' => 'question',
            'creation_date' => 'created_at',
            'last_changed' => 'updated_at',
            'deleted_date' => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
            'id' => 'identifier',
            'title_id' => 'title_identifier',
            'type_id' => 'type_identifier',
            'question' => 'question',
            'created_at' => 'creation_date',
            'updated_at' => 'last_changed',
            'deleted_at' => 'deleted_date',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}

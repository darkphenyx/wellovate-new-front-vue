<?php

namespace App\Transformers;

use App\FitbitAuth;
use League\Fractal\TransformerAbstract;

class FitbitAuthTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(FitbitAuth $fitbitAuth)
    {
        return [
            'identifier' => (int)$fitbitAuth->id,
            'user_identifier' => (int)$fitbitAuth->basic_id,
            'access_token' => (string)$fitbitAuth->access_token,
            'refresh_token' => (string)$fitbitAuth->refresh_token,
            'token_type' => (string)$fitbitAuth->token_type,
            'fitbit_user_id' => (string)$fitbitAuth->fitbit_user_id,
            'creation_date' => (string)$fitbitAuth->created_at,
            'last_changed' => (string)$fitbitAuth->updated_at,
            'deleted_date' => isset($fitbitAuth->deleted_at) ? (string)$fitbitAuth->deleted_at : null,
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes = [
            'identifier' => 'id',
            'user_identifier' => 'basic_id',
            'access_token' => 'access_token',
            'refresh_token' => 'refresh_token',
            'token_type' => 'token_type',
            'fitbit_user_id' => 'fitbit_user_id',
            'creation_date' => 'created_at',
            'last_changed' => 'updated_at',
            'deleted_date' => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
            'id' => 'identifier',
            'basic_id' => 'user_identifier',
            'access_token' => 'access_token',
            'refresh_token' => 'refresh_token',
            'token_type' => 'token_type',
            'fitbit_user_id' => 'fitbit_user_id',
            'created_at' => 'creation_date',
            'updated_at' => 'last_changed',
            'deleted_at' => 'deleted_date',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}

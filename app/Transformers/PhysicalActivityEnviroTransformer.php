<?php

namespace App\Transformers;

use App\PhysicalActivityEnviro;
use League\Fractal\TransformerAbstract;

class PhysicalActivityEnviroTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param PhysicalActivityEnviro $physicalActivityEnviro
     * @return array
     */
    public function transform(PhysicalActivityEnviro $physicalActivityEnviro)
    {
        return [
            'identifier' => (int)$physicalActivityEnviro->id,
            'user_identifier' => (int)$physicalActivityEnviro->basic_id,
            'have_access_to_equipment' => (string)$physicalActivityEnviro->access_equipment,
            'hard_to_walk_due_to_traffic' => (string)$physicalActivityEnviro->walk_traffic,
            'hard_to_walk_due_to_safety_in_neighborhood' => (string)$physicalActivityEnviro->walk_safe,
            'creation_date' => (string)$physicalActivityEnviro->created_at,
            'last_changed' => (string)$physicalActivityEnviro->updated_at,
            'deleted_date' => isset($physicalActivityEnviro->deleted_at) ? (string)$physicalActivityEnviro->deleted_at : null,

            'links' => [
                [
                    'rel' => 'self',
                    'href' => route('physical_activity_enviro.show', $physicalActivityEnviro->id)
                ]
            ]
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes = [
            'identifier' => 'id',
            'user_identifier' => 'basic_id',
            'have_access_to_equipment' => 'access_equipment',
            'hard_to_walk_due_to_traffic' => 'walk_traffic',
            'hard_to_walk_due_to_safety_in_neighborhood' => 'walk_safe',
            'creation_date' => 'created_at',
            'last_changed' => 'updated_at',
            'deleted_date' => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
            'id' => 'identifier',
            'basic_id' => 'user_identifier',
            'access_equipment' => 'have_access_to_equipment',
            'walk_traffic' => 'hard_to_walk_due_to_traffic',
            'walk_safe' => 'hard_to_walk_due_to_safety_in_neighborhood',
            'created_at' => 'creation_date',
            'updated_at' => 'last_changed',
            'deleted_at' => 'deleted_date',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}

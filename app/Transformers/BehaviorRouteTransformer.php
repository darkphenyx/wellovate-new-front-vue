<?php

namespace App\Transformers;

use App\BehaviorRoute;
use League\Fractal\TransformerAbstract;

class BehaviorRouteTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param BehaviorRoute $behaviorRoute
     * @return array
     */
    public function transform(BehaviorRoute $behaviorRoute)
    {
        return [
            'identifier' => (int)$behaviorRoute->id,
            'name' => (string)$behaviorRoute->name,
            'address' => (string)$behaviorRoute->address,
            'latitude' => (float)$behaviorRoute->lat,
            'longitude' => (float)$behaviorRoute->lng,
            'creation_date' => (string)$behaviorRoute->created_at,
            'last_changed' => (string)$behaviorRoute->updated_at,
            'deleted_date' => isset($behaviorRoute->deleted_at) ? (string)$behaviorRoute->deleted_at : null,

            'links' => [
                [
                    'rel' => 'self',
                    'href' => route('behavior_routes.show', $behaviorRoute->id)
                ]
            ]
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes = [
            'identifier' => 'id',
            'name' => 'name',
            'address' => 'address',
            'latitude' => 'lat',
            'longitude' => 'lng',
            'creation_date' => 'created_at',
            'last_changed' => 'updated_at',
            'deleted_date' => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
            'id' => 'identifier',
            'name' => 'name',
            'address' => 'address',
            'lat' => 'latitude',
            'lng' => 'longitude',
            'created_at' => 'creation_date',
            'updated_at' => 'last_changed',
            'deleted_at' => 'deleted_date',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}

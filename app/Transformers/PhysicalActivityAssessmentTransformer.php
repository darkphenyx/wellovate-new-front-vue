<?php

namespace App\Transformers;

use App\PhysicalActivityAssessment;
use League\Fractal\TransformerAbstract;

class PhysicalActivityAssessmentTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param PhysicalActivityAssessment $physicalActivityAssessment
     * @return array
     */
    public function transform(PhysicalActivityAssessment $physicalActivityAssessment)
    {
        return [
            'identifier' => (int)$physicalActivityAssessment->id,
            'user_identifier' => (int)$physicalActivityAssessment->basic_id,
            'enjoy_walking' => (string)$physicalActivityAssessment->walking,
            'enjoy_hiking' => (string)$physicalActivityAssessment->hiking,
            'enjoy_running' => (string)$physicalActivityAssessment->running,
            'enjoy_bicycling' => (string)$physicalActivityAssessment->bicycling,
            'enjoy_weight_training' => (string)$physicalActivityAssessment->weight_training,
            'enjoy_calisthenics' => (string)$physicalActivityAssessment->calisthenics,
            'enjoy_swimming' => (string)$physicalActivityAssessment->swimming,
            'enjoy_yoga' => (string)$physicalActivityAssessment->yoga,
            'enjoy_tennis' => (string)$physicalActivityAssessment->tennis,
            'enjoy_table_tennis' => (string)$physicalActivityAssessment->table_tennis,
            'enjoy_aerobic' => (string)$physicalActivityAssessment->aerobic,
            'enjoy_kayaking' => (string)$physicalActivityAssessment->kayaking,
            'enjoy_martial_arts' => (string)$physicalActivityAssessment->martial_arts,
            'enjoy_basketball' => (string)$physicalActivityAssessment->basketball,
            'enjoy_soccer' => (string)$physicalActivityAssessment->soccer,
            'enjoy_golf' => (string)$physicalActivityAssessment->golf,
            'enjoy_skiing' => (string)$physicalActivityAssessment->skiing,
            'enjoy_volleyball' => (string)$physicalActivityAssessment->volleyball,
            'enjoy_gardening' => (string)$physicalActivityAssessment->gardening,
            'enjoy_skating' => (string)$physicalActivityAssessment->skating,
            'enjoy_children' => (string)$physicalActivityAssessment->children,
            'creation_date' => (string)$physicalActivityAssessment->created_at,
            'last_changed' => (string)$physicalActivityAssessment->updated_at,
            'deleted_date' => isset($physicalActivityAssessment->deleted_at) ? (string)$physicalActivityAssessment->deleted_at : null,

            'links' => [
                [
                    'rel' => 'self',
                    'href' => route('physical_activity_assessment.show', $physicalActivityAssessment->id)
                ]
            ]
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes = [
            'identifier' => 'id',
            'user_identifier' => 'basic_id',
            'enjoy_walking' => 'walking',
            'enjoy_hiking' => 'hiking',
            'enjoy_running' => 'running',
            'enjoy_bicycling' => 'bicycling',
            'enjoy_weight_training' => 'weight_training',
            'enjoy_calisthenics' => 'calisthenics',
            'enjoy_swimming' => 'swimming',
            'enjoy_yoga' => 'yoga',
            'enjoy_tennis' => 'tennis',
            'enjoy_table_tennis' => 'table_tennis',
            'enjoy_aerobic' => 'aerobic',
            'enjoy_kayaking' => 'kayaking',
            'enjoy_martial_arts' => 'martial_arts',
            'enjoy_basketball' => 'basketball',
            'enjoy_soccer' => 'soccer',
            'enjoy_golf' => 'golf',
            'enjoy_skiing' => 'skiing',
            'enjoy_volleyball' => 'volleyball',
            'enjoy_gardening' => 'gardening',
            'enjoy_skating' => 'skating',
            'enjoy_children' => 'children',
            'creation_date' => 'created_at',
            'last_changed' => 'updated_at',
            'deleted_date' => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
            'id' => 'identifier',
            'basic_id' => 'user_identifier',
            'walking' => 'enjoy_walking',
            'hiking' => 'enjoy_hiking',
            'running' => 'enjoy_running',
            'bicycling' => 'enjoy_bicycling',
            'weight_training' => 'enjoy_weight_training',
            'calisthenics' => 'enjoy_calisthenics',
            'swimming' => 'enjoy_swimming',
            'yoga' => 'enjoy_yoga',
            'tennis' => 'enjoy_tennis',
            'table_tennis' => 'enjoy_table_tennis',
            'aerobic' => 'enjoy_aerobic',
            'kayaking' => 'enjoy_kayaking',
            'martial_arts' => 'enjoy_martial_arts',
            'basketball' => 'enjoy_basketball',
            'soccer' => 'enjoy_soccer',
            'golf' => 'enjoy_golf',
            'skiing' => 'enjoy_skiing',
            'volleyball' => 'enjoy_volleyball',
            'gardening' => 'enjoy_gardening',
            'skating' => 'enjoy_skating',
            'children' => 'enjoy_children',
            'created_at' => 'creation_date',
            'updated_at' => 'last_changed',
            'deleted_at' => 'deleted_date',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}

<?php

namespace App\Transformers;

use App\QuestionnaireName;
use League\Fractal\TransformerAbstract;

class QuestionnaireNameTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(QuestionnaireName $questionnaireName)
    {
        return [
            'identifier' => (int)$questionnaireName->id,
            'questionnaire_name' => (string)$questionnaireName->name,
            'been_assigned' => (int)$questionnaireName->assigned,
            'creation_date' => (string)$questionnaireName->created_at,
            'last_changed' => (string)$questionnaireName->updated_at,
            'deleted_date' => isset($questionnaireName->deleted_at) ? (string)$questionnaireName->deleted_at : null,
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes = [
            'identifier' => 'id',
            'questionnaire_name' => 'name',
            'been_assigned' => 'assigned',
            'creation_date' => 'created_at',
            'last_changed' => 'updated_at',
            'deleted_date' => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
            'id' => 'identifier',
            'name' => 'questionnaire_name',
            'assigned' => 'been_assigned',
            'created_at' => 'creation_date',
            'updated_at' => 'last_changed',
            'deleted_at' => 'deleted_date',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}

<?php

namespace App\Transformers;

use App\Enum;
use League\Fractal\TransformerAbstract;

class EnumTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Enum $enum)
    {
        return [
            'identifier' => (int)$enum->id,
            'quest_identifier' => (int)$enum->question_id,
            'available_answer' => (string)$enum->answer_choice,
            'creation_date' => (string)$enum->created_at,
            'last_changed' => (string)$enum->updated_at,
            'deleted_date' => isset($enum->deleted_at) ? (string)$enum->deleted_at : null,
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes = [
            'identifier' => 'id',
            'quest_identifier' => 'question_id',
            'available_answer' => 'answer_choice',
            'creation_date' => 'created_at',
            'last_changed' => 'updated_at',
            'deleted_date' => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
            'id' => 'identifier',
            'question_id' => 'quest_identifier',
            'answer_choice' => 'available_answer',
            'created_at' => 'creation_date',
            'updated_at' => 'last_changed',
            'deleted_at' => 'deleted_date',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}

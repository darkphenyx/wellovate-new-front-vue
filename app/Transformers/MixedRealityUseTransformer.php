<?php

namespace App\Transformers;

use App\MixedRealityUse;
use League\Fractal\TransformerAbstract;

class MixedRealityUseTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param MixedRealityUse $mixedRealityUse
     * @return array
     */
    public function transform(MixedRealityUse $mixedRealityUse)
    {
        return [
            'identifier' => (int)$mixedRealityUse->id,
            'user_identifier' => (int)$mixedRealityUse->basic_id,
            'experience_identifier' => (int)$mixedRealityUse->experience_id,
            'lobby_start_time' => (string)$mixedRealityUse->start_lobby,
            'lobby_end_time' => (string)$mixedRealityUse->end_lobby,
            'experience_start_time' => (string)$mixedRealityUse->start_experience,
            'experience_end_time' => (string)$mixedRealityUse->end_experience,
            'creation_date' => (string)$mixedRealityUse->created_at,
            'last_changed' => (string)$mixedRealityUse->updated_at,
            'deleted_date' => isset($mixedRealityUse->deleted_at) ? (string)$mixedRealityUse->deleted_at : null,

            'links' => [
                [
                    'rel' => 'self',
                    'href' => route('mixed_reality_uses.show', $mixedRealityUse->id)
                ],
                [
                    'rel' => 'mixed_experiences.show',
                    'href' => route('mixed_experiences.show', $mixedRealityUse->experience_id)
                ],
            ]
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes = [
            'identifier' => 'id',
            'user_identifier' => 'basic_id',
            'experience_identifier' => 'experience_id',
            'lobby_start_time' => 'start_lobby',
            'lobby_end_time' => 'end_lobby',
            'experience_start_time' => 'start_experience',
            'experience_end_time' => 'end_experience',
            'creation_date' => 'created_at',
            'last_changed' => 'updated_at',
            'deleted_date' => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
            'id' => 'identifier',
            'basic_id' => 'user_identifier',
            'experience_id' => 'experience_identifier',
            'start_lobby' => 'lobby_start_time',
            'end_lobby' => 'lobby_end_time',
            'start_experience' => 'experience_start_time',
            'end_experience' => 'experience_end_time',
            'created_at' => 'creation_date',
            'updated_at' => 'last_changed',
            'deleted_at' => 'deleted_date',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}

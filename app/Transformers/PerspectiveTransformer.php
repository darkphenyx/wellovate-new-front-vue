<?php

namespace App\Transformers;

use App\Perspective;
use League\Fractal\TransformerAbstract;

class PerspectiveTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Perspective $perspective)
    {
        return [
            'identifier' => (int)$perspective->id,
            'title' => (string)$perspective->title,
            'content' => (string)$perspective->content,
            'tags' => (string)$perspective->tags,
            'creation_date' => (string)$perspective->created_at,
            'last_changed' => (string)$perspective->updated_at,
            'deleted_date' => isset($perspective->deleted_at) ? (string)$perspective->deleted_at : null,

            'links' => [
                [
                    'rel' => 'self',
                    'href' => route('perspectives.show', $perspective->id),
                ],
            ]
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes = [
            'identifier' => 'id',
            'title' => 'title',
            'content' => 'content',
            'tags' => 'tags',
            'creation_date' => 'created_at',
            'last_changed' => 'updated_at',
            'deleted_date' => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
            'id' => 'identifier',
            'title' => 'title',
            'content' => 'content',
            'tags' => 'tags',
            'creation_date' => 'created_at',
            'last_changed' => 'updated_at',
            'deleted_date' => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}

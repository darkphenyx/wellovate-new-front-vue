<?php

namespace App\Transformers;

use App\PhysicalActivityConfidence;
use League\Fractal\TransformerAbstract;

class PhysicalActivityConfidenceTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param PhysicalActivityConfidence $physicalActivityConfidence
     * @return array
     */
    public function transform(PhysicalActivityConfidence $physicalActivityConfidence)
    {
        return [
            'identifier' => (int)$physicalActivityConfidence->id,
            'user_identifier' => (int)$physicalActivityConfidence->basic_id,
            'am_tired' => (string)$physicalActivityConfidence->tired,
            'am_in_bad_mood' => (string)$physicalActivityConfidence->bad_mood,
            'am_stressed' => (string)$physicalActivityConfidence->stressed,
            'have_no_time' => (string)$physicalActivityConfidence->no_time,
            'when_bad_weather' => (string)$physicalActivityConfidence->bad_weather,
            'creation_date' => (string)$physicalActivityConfidence->created_at,
            'last_changed' => (string)$physicalActivityConfidence->updated_at,
            'deleted_date' => isset($physicalActivityConfidence->deleted_at) ? (string)$physicalActivityConfidence->deleted_at : null,

            'links' => [
                [
                    'rel' => 'self',
                    'href' => route('physical_activity_confidence.show', $physicalActivityConfidence->id)
                ]
            ]
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes = [
            'identifier' => 'id',
            'user_identifier' => 'basic_id',
            'am_tired' => 'tired',
            'am_in_bad_mood' => 'bad_mood',
            'am_stressed' => 'stressed',
            'have_no_time' => 'no_time',
            'when_bad_weather' => 'bad_weather',
            'creation_date' => 'created_at',
            'last_changed' => 'updated_at',
            'deleted_date' => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
            'id' => 'identifier',
            'basic_id' => 'user_identifier',
            'tired' => 'am_tired',
            'bad_mood' => 'am_in_bad_mood',
            'stressed' => 'am_stressed',
            'no_time' => 'have_no_time',
            'bad_weather' => 'when_bad_weather',
            'created_at' => 'creation_date',
            'updated_at' => 'last_changed',
            'deleted_at' => 'deleted_date',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}

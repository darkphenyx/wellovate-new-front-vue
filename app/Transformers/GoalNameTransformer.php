<?php

namespace App\Transformers;

use App\GoalName;
use League\Fractal\TransformerAbstract;

class GoalNameTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param GoalName $goalName
     * @return array
     */
    public function transform(GoalName $goalName)
    {
        return [
            'identifier' => (int)$goalName->id,
            'goal_name' => (string)$goalName->name,
            'creation_date' => (string)$goalName->created_at,
            'last_changed' => (string)$goalName->updated_at,
            'deleted_date' => isset($goalName->deleted_at) ? (string)$goalName->deleted_at : null,

            'links' => [
                [
                    'rel' => 'self',
                    'href' => route('goal_names.show', $goalName->id)
                ]
            ]
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes = [
            'identifier' => 'id',
            'goal_name' => 'name',
            'creation_date' => 'created_at',
            'last_changed' => 'updated_at',
            'deleted_date' => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
            'id' => 'identifier',
            'name' => 'goal_name',
            'created_at' => 'creation_date',
            'updated_at' => 'last_changed',
            'deleted_at' => 'deleted_date',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}

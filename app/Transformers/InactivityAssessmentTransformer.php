<?php

namespace App\Transformers;

use App\InactivityAssessment;
use League\Fractal\TransformerAbstract;

class InactivityAssessmentTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param InactivityAssessment $inactivityAssessment
     * @return array
     */
    public function transform(InactivityAssessment $inactivityAssessment)
    {
        return [
            'identifier' => (int)$inactivityAssessment->id,
            'user_identifier' => (int)$inactivityAssessment->basic_id,
            'weekday_tv_watching' => (string)$inactivityAssessment->time_watch_tv_weekday,
            'weekday_time_sitting' => (string)$inactivityAssessment->time_sitting_weekday,
            'weekend_tv_watching' => (string)$inactivityAssessment->time_watch_tv_weekend,
            'weekend_time_sitting' => (string)$inactivityAssessment->time_sitting_weekend,
            'creation_date' => (string)$inactivityAssessment->created_at,
            'last_changed' => (string)$inactivityAssessment->updated_at,
            'deleted_date' => isset($inactivityAssessment->deleted_at) ? (string)$inactivityAssessment->deleted_at : null,

            'links' => [
                [
                    'rel' => 'self',
                    'href' => route('inactivity.show', $inactivityAssessment->id)
                ]
            ]
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes = [
            'identifier' => 'id',
            'user_identifier' => 'basic_id',
            'weekday_tv_watching' => 'time_watch_tv_weekday',
            'weekday_time_sitting' => 'time_sitting_weekday',
            'weekend_tv_watching' => 'time_watch_tv_weekend',
            'weekend_time_sitting' => 'time_sitting_weekend',
            'creation_date' => 'created_at',
            'last_changed' => 'updated_at',
            'deleted_date' => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
            'id' => 'identifier',
            'basic_id' => 'user_identifier',
            'time_watch_tv_weekday' => 'weekday_tv_watching',
            'time_sitting_weekday' => 'weekday_time_sitting',
            'time_watch_tv_weekend' => 'weekend_tv_watching',
            'time_sitting_weekend' => 'weekend_time_sitting',
            'created_at' => 'creation_date',
            'updated_at' => 'last_changed',
            'deleted_at' => 'deleted_date',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}

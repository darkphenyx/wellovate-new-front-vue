<?php

namespace App\Transformers;

use App\Outcome;
use League\Fractal\TransformerAbstract;

class OutcomeTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param Outcome $outcome
     * @return array
     */
    public function transform(Outcome $outcome)
    {
        return [
            'identifier' => (int)$outcome->id,
            'user_identifier' => (int)$outcome->basic_id,
            'has_alzheimers' => (boolean)$outcome->alzheimers,
            'general_anxiety_disorder' => (boolean)$outcome->gen_anxiety_disorder,
            'has_cardiovascular_disease' => (boolean)$outcome->cardiovascular_disease,
            'has_coronary_heart_disease' => (boolean)$outcome->coronary_heart_disease,
            'has_dementia' => (boolean)$outcome->dementia,
            'has_depression' => (boolean)$outcome->depression,
            'has_diabetes' => (boolean)$outcome->diabetes,
            'is_healthy' => (boolean)$outcome->healthy,
            'has_hypertension' => (boolean)$outcome->hypertension,
            'is_inactive' => (boolean)$outcome->inactive,
            'has_mild_cognitive_impairment' => (boolean)$outcome->mild_cognitive_impairment,
            'number_falls_per_day' => (int)$outcome->number_falls,
            'is_underweight' => (boolean)$outcome->underweight,
            'is_normal_weight' => (boolean)$outcome->normal_weight,
            'is_overweight' => (boolean)$outcome->overweight,
            'is_obesity_1' => (boolean)$outcome->obesity_1,
            'is_obesity_2' => (boolean)$outcome->obesity_2,
            'is_obesity_3' => (boolean)$outcome->obesity_3,
            'has_partial_sleep_deprivation' => (boolean)$outcome->partial_sleep_deprivation,
            'has_prediabetes' => (boolean)$outcome->prediabetes,
            'has_prehypertension' => (boolean)$outcome->prehypertension,
            'has_subclinical_depression' => (boolean)$outcome->subclinical_depression,
            'has_subjective_poor_sleep' => (boolean)$outcome->subjective_poor_sleep,
            'has_total_sleep_deprivation' => (boolean)$outcome->total_sleep_deprivation,
            'has_high_ldl_concentration' => (boolean)$outcome->high_ldl_concentration,
            'has_high_ldl_particle' => (boolean)$outcome->high_ldl_particle,
            'has_high_triglycerides' => (boolean)$outcome->high_triglycerides,
            'has_low_hdl' => (boolean)$outcome->low_hdl,
            'creation_date' => (string)$outcome->created_at,
            'last_changed' => (string)$outcome->updated_at,
            'deleted_date' => isset($outcome->deleted_at) ? (string)$outcome->deleted_at : null,

            'links' => [
                [
                    'rel' => 'self',
                    'href' => route('outcomes.show', $outcome->id)
                ]
            ]
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes = [
            'identifier' => 'id',
            'user_identifier' => 'basic_id',
            'has_alzheimers' => 'alzheimers',
            'general_anxiety_disorder' => 'gen_anxiety_disorder',
            'has_cardiovascular_disease' => 'cardiovascular_disease',
            'has_coronary_heart_disease' => 'coronary_heart_disease',
            'has_dementia' => 'dementia',
            'has_depression' => 'depression',
            'has_diabetes' => 'diabetes',
            'is_healthy' => 'healthy',
            'has_hypertension' => 'hypertension',
            'is_inactive' => 'inactive',
            'has_mild_cognitive_impairment' => 'mild_cognitive_impairment',
            'number_falls_per_day' => 'number_falls',
            'is_underweight' => 'underweight',
            'is_normal_weight' => 'normal_weight',
            'is_overweight' => 'overweight',
            'is_obesity_1' => 'obesity_1',
            'is_obesity_2' => 'obesity_2',
            'is_obesity_3' => 'obesity_3',
            'has_partial_sleep_deprivation' => 'partial_sleep_deprivation',
            'has_prediabetes' => 'prediabetes',
            'has_prehypertension' => 'prehypertension',
            'has_subclinical_depression' => 'subclinical_depression',
            'has_subjective_poor_sleep' => 'subjective_poor_sleep',
            'has_total_sleep_deprivation' => 'total_sleep_deprivation',
            'has_high_ldl_concentration' => 'high_ldl_concentration',
            'has_high_ldl_particle' => 'high_ldl_particle',
            'has_high_triglycerides' => 'high_triglycerides',
            'has_low_hdl' => 'low_hdl',
            'creation_date' => 'created_at',
            'last_changed' => 'updated_at',
            'deleted_date' => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
            'id' => 'identifier',
            'basic_id' => 'user_identifier',
            'alzheimers' => 'has_alzheimers',
            'gen_anxiety_disorder' => 'general_anxiety_disorder',
            'cardiovascular_disease' => 'has_cardiovascular_disease',
            'coronary_heart_disease' => 'has_coronary_heart_disease',
            'dementia' => 'has_dementia',
            'depression' => 'has_depression',
            'diabetes' => 'has_diabetes',
            'healthy' => 'is_healthy',
            'hypertension' => 'has_hypertension',
            'inactive' => 'is_inactive',
            'mild_cognitive_impairment' => 'has_mild_cognitive_impairment',
            'number_falls' => 'number_falls_per_day',
            'underweight' => 'is_underweight',
            'normal_weight' => 'is_normal_weight',
            'overweight' => 'is_overweight',
            'obesity_1' => 'is_obesity_1',
            'obesity_2' => 'is_obesity_2',
            'obesity_3' => 'is_obesity_3',
            'partial_sleep_deprivation' => 'has_partial_sleep_deprivation',
            'prediabetes' => 'has_prediabetes',
            'prehypertension' => 'has_prehypertension',
            'subclinical_depression' => 'has_subclinical_depression',
            'subjective_poor_sleep' => 'has_subjective_poor_sleep',
            'total_sleep_deprivation' => 'has_total_sleep_deprivation',
            'high_ldl_concentration' => 'has_high_ldl_concentration',
            'high_ldl_particle' => 'has_high_ldl_particle',
            'high_triglycerides' => 'has_high_triglycerides',
            'low_hdl' => 'has_low_hdl',
            'created_at' => 'creation_date',
            'updated_at' => 'last_changed',
            'deleted_at' => 'deleted_date',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}

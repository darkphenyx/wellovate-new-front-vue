<?php

namespace App\Transformers;

use App\GoalUser;
use League\Fractal\TransformerAbstract;

class GoalUserTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param GoalUser $goalUser
     * @return array
     */
    public function transform(GoalUser $goalUser)
    {
        return [
            'identifier' => (int)$goalUser->id,
            'user_identifier' => (int)$goalUser->basic_id,
            'goal_identifier' => (int)$goalUser->goal_id,
            'priority_level' => (int)$goalUser->priority,
            'completed_or_not' => (boolean)$goalUser->completed,
            'creation_date' => (string)$goalUser->created_at,
            'last_changed' => (string)$goalUser->updated_at,
            'deleted_date' => isset($goalUser->deleted_at) ? (string)$goalUser->deleted_at : null,

            'links' => [
                [
                    'rel' => 'self',
                    'href' => route('goal_users.show', $goalUser->id)
                ],
                [
                    'rel' => 'goal.name',
                    'href' => route('goal_names.show', $goalUser->goal_id)
                ]
            ]
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes = [
            'identifier' => 'id',
            'user_identifier' => 'basic_id',
            'goal_identifier' => 'goal_id',
            'priority_level' => 'priority',
            'completed_or_not' => 'completed',
            'creation_date' => 'created_at',
            'last_changed' => 'updated_at',
            'deleted_date' => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
            'id' => 'identifier',
            'basic_id' => 'user_identifier',
            'goal_id' => 'goal_identifier',
            'priority' => 'priority_level',
            'completed' => 'completed_or_not',
            'created_at' => 'creation_date',
            'updated_at' => 'last_changed',
            'deleted_at' => 'deleted_date',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}

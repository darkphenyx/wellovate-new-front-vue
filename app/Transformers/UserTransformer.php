<?php

namespace App\Transformers;

use App\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param User $user
     * @return array
     */
    public function transform(User $user)
    {
        return [
            'identifier' => (int)$user->id,
            'name' => (string)$user->name,
            'email' => (string)$user->email,
            'user_age' => (int)$user->age,
            'user_sex' => (string)$user->sex,
            'zip_code' => (int)$user->zip,
            'facebook' => (string)$user->facebook_name,
            'is_verified' => (int)$user->verified,
            'is_admin' => ($user->admin === 'true'),
            'creation_date' => (string)$user->created_at,
            'last_changed' => (string)$user->updated_at,
            'deleted_date' => isset($user->deleted_at) ? (string)$user->deleted_at : null,

            'links' => [
                [
                    'rel' => 'self',
                    'href' => route('users.show', $user->id),
                ],
            ]
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes = [
            'identifier' => 'id',
            'name' => 'name',
            'email' => 'email',
            'password' => 'password',
            'password_confirmation' => 'password_confirmation',
            'user_age' => 'age',
            'user_sex' => 'sex',
            'zip_code' => 'zip',
            'facebook' => 'facebook_name',
            'is_verified' => 'verified',
            'is_admin' => 'admin',
            'creation_date' => 'created_at',
            'last_changed' => 'updated_at',
            'deleted_date' => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
            'id' => 'identifier',
            'name' => 'name',
            'email' => 'email',
            'password' => 'password',
            'password_confirmation' => 'password_confirmation',
            'age' => 'user_age',
            'sex' => 'user_sex',
            'zip' => 'zip_code',
            'facebook_name' => 'facebook',
            'verified' => 'is_verified',
            'admin' => 'is_admin',
            'created_at' => 'creation_date',
            'updated_at' => 'last_changed',
            'deleted_at' => 'deleted_date',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}

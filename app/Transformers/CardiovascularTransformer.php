<?php

namespace App\Transformers;

use App\Cardiovascular;
use League\Fractal\TransformerAbstract;

class CardiovascularTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param Cardiovascular $cardiovascular
     * @return array
     */
    public function transform(Cardiovascular $cardiovascular)
    {
        return [
            'identifier' => (int)$cardiovascular->id,
            'user_identifier' => (int)$cardiovascular->basic_id,
            'ankle_brachial' => (double)$cardiovascular->ankle_brachial_index,
            'blood_pressure' => (string)$cardiovascular->blood_pressure,
            'diastolic' => (int)$cardiovascular->diastolic_blood_pressure,
            'systolic' => (int)$cardiovascular->systolic_blood_pressure,
            'endothelial' => (double)$cardiovascular->endothelial,
            'reactive_hyperenia' => (double)$cardiovascular->reactive_hyperenia,
            'user_heart_rate' => (int)$cardiovascular->heart_rate,
            'user_heart_rate_max' => (int)$cardiovascular->max_heart_rate,
            'pulse_wave_velocity' => (double)$cardiovascular->pulse_wave_velocity,
            'qtc_interval' => (double)$cardiovascular->qtc,
            'r_r_interval' => (double)$cardiovascular->rr_interval,
            'oxygen_saturation' => (double)$cardiovascular->o2_saturation,
            'pulse_oximetry' => (double)$cardiovascular->pulse_oximetry,
            'creation_date' => (string)$cardiovascular->created_at,
            'last_changed' => (string)$cardiovascular->updated_at,
            'deleted_date' => isset($cardiovascular->deleted_at) ? (string)$cardiovascular->deleted_at : null,

            'links' => [
                [
                    'rel' => 'self',
                    'href' => route('cardiovasculars.show', $cardiovascular->id)
                ]
            ]
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes = [
            'identifier' => 'id',
            'user_identifier' => 'basic_id',
            'ankle_brachial' => 'ankle_brachial_index',
            'blood_pressure' => 'blood_pressure',
            'diastolic' => 'diastolic_blood_pressure',
            'systolic' => 'systolic_blood_pressure',
            'endothelial' => 'endothelial',
            'reactive_hyperenia' => 'reactive_hyperenia',
            'user_heart_rate' => 'heart_rate',
            'user_heart_rate_max' => 'max_heart_rate',
            'pulse_wave_velocity' => 'pulse_wave_velocity',
            'qtc_interval' => 'qtc',
            'r_r_interval' => 'rr_interval',
            'oxygen_saturation' => 'o2_saturation',
            'pulse_oximetry' => 'pulse_oximetry',
            'creation_date' => 'created_at',
            'last_changed' => 'updated_at',
            'deleted_date' => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
            'id' => 'identifier',
            'basic_id' => 'user_identifier',
            'ankle_brachial_index' => 'ankle_brachial',
            'blood_pressure' => 'blood_pressure',
            'diastolic_blood_pressure' => 'diastolic',
            'systolic_blood_pressure' => 'systolic',
            'endothelial' => 'endothelial',
            'reactive_hyperenia' => 'reactive_hyperenia',
            'heart_rate' => 'user_heart_rate',
            'max_heart_rate' => 'user_heart_rate_max',
            'pulse_wave_velocity' => 'pulse_wave_velocity',
            'qtc' => 'qtc_interval',
            'rr_interval' => 'r_r_interval',
            'o2_saturation' => 'oxygen_saturation',
            'pulse_oximetry' => 'pulse_oximetry',
            'created_at' => 'creation_date',
            'updated_at' => 'last_changed',
            'deleted_at' => 'deleted_date',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}

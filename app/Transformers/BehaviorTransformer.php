<?php

namespace App\Transformers;

use App\Behavior;
use League\Fractal\TransformerAbstract;

class BehaviorTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param Behavior $behavior
     * @return array
     */
    public function transform(Behavior $behavior)
    {
        return [
            'identifier' => (int)$behavior->id,
            'user_identifier' => (int)$behavior->basic_id,
            'status_of_smoking' => (string)$behavior->smoking_status,
            'number_smoked_daily' => (int)$behavior->cigs_per_day,
            'years_user_smoked' => (double)$behavior->years_smoked,
            'location_identifier' => (int)$behavior->location_id,
            'number_vaped_daily' => (int)$behavior->vape_draws_day,
            'route_identifier' => (int)$behavior->route_id,
            'brush_your_teeth' => (boolean)$behavior->teeth_brushing,
            'floss_your_teeth' => (boolean)$behavior->teeth_flossing,
            'tv_in_bedroom' => (boolean)$behavior->tv_bedroom,
            'uv_exposure_per_day' => (double)$behavior->uv_exposure,
            'creation_date' => (string)$behavior->created_at,
            'last_changed' => (string)$behavior->updated_at,
            'deleted_date' => isset($behavior->deleted_at) ? (string)$behavior->deleted_at : null,

            'links' => [
                [
                    'rel' => 'self',
                    'href' => route('behaviors.show', $behavior->id)
                ],
                [
                    'rel' => 'behavior.location',
                    'href' => route('behavior_locations.show', $behavior->location_id)
                ],
                [
                    'rel' => 'behavior.route',
                    'href' => route('behavior_routes.show', $behavior->route_id)
                ],
            ]
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes = [
            'identifier' => 'id',
            'user_identifier' => 'basic_id',
            'status_of_smoking' => 'smoking_status',
            'number_smoked_daily' => 'cigs_per_day',
            'years_user_smoked' => 'years_smoked',
            'location_identifier' => 'location_id',
            'number_vaped_daily' => 'vape_draws_day',
            'route_identifier' => 'route_id',
            'brush_your_teeth' => 'teeth_brushing',
            'floss_your_teeth' => 'teeth_flossing',
            'tv_in_bedroom' => 'tv_bedroom',
            'uv_exposure_per_day' => 'uv_exposure',
            'creation_date' => 'created_at',
            'last_changed' => 'updated_at',
            'deleted_date' => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
            'id' => 'identifier',
            'basic_id' => 'user_identifier',
            'smoking_status' => 'status_of_smoking',
            'cigs_per_day' => 'number_smoked_daily',
            'years_smoked' => 'years_user_smoked',
            'location_id' => 'location_identifier',
            'vape_draws_day' => 'number_vaped_daily',
            'route_id' => 'route_identifier',
            'teeth_brushing' => 'brush_your_teeth',
            'teeth_flossing' => 'floss_your_teeth',
            'tv_bedroom' => 'tv_in_bedroom',
            'uv_exposure' => 'uv_exposure_per_day',
            'created_at' => 'creation_date',
            'updated_at' => 'last_changed',
            'deleted_at' => 'deleted_date',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}

<?php

namespace App\Transformers;

use App\GroupTeam;
use League\Fractal\TransformerAbstract;

class GroupTeamTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param GroupTeam $groupTeam
     * @return array
     */
    public function transform(GroupTeam $groupTeam)
    {
        return [
            'identifier' => (int)$groupTeam->id,
            'user_identifier' => (int)$groupTeam->basic_id,
            'group_lead_identifier' => (int)$groupTeam->group_lead_id,
            'creation_date' => (string)$groupTeam->created_at,
            'last_changed' => (string)$groupTeam->updated_at,
            'deleted_date' => isset($groupTeam->deleted_at) ? (string)$groupTeam->deleted_at : null,

            'links' => [
                [
                    'rel' => 'self',
                    'href' => route('group_team.show', $groupTeam->id)
                ],
                [
                    'rel' => 'group_lead.show',
                    'href' => route('group_lead.show', $groupTeam->group_lead_id)
                ],
            ]
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes = [
            'identifier' => 'id',
            'user_identifier' => 'basic_id',
            'group_lead_identifier' => 'group_lead_id',
            'creation_date' => 'created_at',
            'last_changed' => 'updated_at',
            'deleted_date' => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
            'id' => 'identifier',
            'basic_id' => 'user_identifier',
            'group_lead_id' => 'group_lead_identifier',
            'created_at' => 'creation_date',
            'updated_at' => 'last_changed',
            'deleted_at' => 'deleted_date',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}

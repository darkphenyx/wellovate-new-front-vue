<?php

namespace App\Transformers;

use App\Coach;
use League\Fractal\TransformerAbstract;

class CoachTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param Coach $coach
     * @return array
     */
    public function transform(Coach $coach)
    {
        return [
            'identifier' => (int)$coach->id,
            'name' => (string)$coach->name,
            'email' => (string)$coach->email,
            'user_age' => (int)$coach->age,
            'user_sex' => (string)$coach->sex,
            'zip_code' => (int)$coach->zip,
            'facebook' => (string)$coach->facebook_name,
            'is_verified' => (int)$coach->verified,
            'creation_date' => (string)$coach->created_at,
            'last_changed' => (string)$coach->updated_at,
            'deleted_date' => isset($coach->deleted_at) ? (string)$coach->deleted_at : null,

            'links' => [
                [
                    'rel' => 'self',
                    'href' => route('coaches.show', $coach->id)
                ],
                [
                    'rel' => 'coaches.group_leads',
                    'href' => route('coaches.group_leads.index', $coach->id)
                ],
            ]
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes = [
            'identifier' => 'id',
            'name' => 'name',
            'email' => 'email',
            'user_age' => 'age',
            'user_sex' => 'sex',
            'zip_code' => 'zip',
            'facebook' => 'facebook_name',
            'is_verified' => 'verified',
            'creation_date' => 'created_at',
            'last_changed' => 'updated_at',
            'deleted_date' => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
            'id' => 'identifier',
            'name' => 'name',
            'email' => 'email',
            'age' => 'user_age',
            'sex' => 'user_sex',
            'zip' => 'zip_code',
            'facebook_name' => 'facebook',
            'verified' => 'is_verified',
            'created_at' => 'creation_date',
            'updated_at' => 'last_changed',
            'deleted_at' => 'deleted_date',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}

<?php

namespace App\Transformers;

use App\MixedExperience;
use League\Fractal\TransformerAbstract;

class MixedExperienceTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param MixedExperience $mixedExperience
     * @return array
     */
    public function transform(MixedExperience $mixedExperience)
    {
        return [
            'identifier' => (int)$mixedExperience->id,
            'experience_name' => (string)$mixedExperience->name,
            'creation_date' => (string)$mixedExperience->created_at,
            'last_changed' => (string)$mixedExperience->updated_at,
            'deleted_date' => isset($mixedExperience->deleted_at) ? (string)$mixedExperience->deleted_at : null,
            'links' => [
                [
                    'rel' => 'self',
                    'href' => route('mixed_experiences.show', $mixedExperience->id)
                ]
            ]
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes = [
            'identifier' => 'id',
            'experience_name' => 'name',
            'creation_date' => 'created_at',
            'last_changed' => 'updated_at',
            'deleted_date' => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
            'id' => 'identifier',
            'name' => 'experience_name',
            'created_at' => 'creation_date',
            'updated_at' => 'last_changed',
            'deleted_at' => 'deleted_date',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}

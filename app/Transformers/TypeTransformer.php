<?php

namespace App\Transformers;

use App\Type;
use League\Fractal\TransformerAbstract;

class TypeTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Type $type)
    {
        return [
            'identifier' => (int)$type->id,
            'quest_type' => (string)$type->question_type,
            'creation_date' => (string)$type->created_at,
            'last_changed' => (string)$type->updated_at,
            'deleted_date' => isset($type->deleted_at) ? (string)$type->deleted_at : null,
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes = [
            'identifier' => 'id',
            'quest_type' => 'question_type',
            'creation_date' => 'created_at',
            'last_changed' => 'updated_at',
            'deleted_date' => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
            'id' => 'identifier',
            'question_type' => 'quest_type',
            'created_at' => 'creation_date',
            'updated_at' => 'last_changed',
            'deleted_at' => 'deleted_date',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}

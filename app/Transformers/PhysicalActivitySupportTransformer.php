<?php

namespace App\Transformers;

use App\PhysicalActivitySupport;
use League\Fractal\TransformerAbstract;

class PhysicalActivitySupportTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param PhysicalActivitySupport $physicalActivitySupport
     * @return array
     */
    public function transform(PhysicalActivitySupport $physicalActivitySupport)
    {
        return [
            'identifier' => (int)$physicalActivitySupport->id,
            'user_identifier' => (int)$physicalActivitySupport->basic_id,
            'encourage_you_to_exercise' => (string)$physicalActivitySupport->encourage_exercise,
            'offered_to_exercise_or_participate' => (string)$physicalActivitySupport->offered_participate,
            'exercised_or_participated_with_you' => (string)$physicalActivitySupport->exercised_with,
            'discouraged_you_from_exercising' => (string)$physicalActivitySupport->discouraged,
            'creation_date' => (string)$physicalActivitySupport->created_at,
            'last_changed' => (string)$physicalActivitySupport->updated_at,
            'deleted_date' => isset($physicalActivitySupport->deleted_at) ? (string)$physicalActivitySupport->deleted_at : null,

            'links' => [
                [
                    'rel' => 'self',
                    'href' => route('physical_activity_support.show', $physicalActivitySupport->id)
                ]
            ]
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes = [
            'identifier' => 'id',
            'user_identifier' => 'basic_id',
            'encourage_you_to_exercise' => 'encourage_exercise',
            'offered_to_exercise_or_participate' => 'offered_participate',
            'exercised_or_participated_with_you' => 'exercised_with',
            'discouraged_you_from_exercising' => 'discouraged',
            'creation_date' => 'created_at',
            'last_changed' => 'updated_at',
            'deleted_date' => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
            'id' => 'identifier',
            'basic_id' => 'user_identifier',
            'encourage_exercise' => 'encourage_you_to_exercise',
            'offered_participate' => 'offered_to_exercise_or_participate',
            'exercised_with' => 'exercised_or_participated_with_you',
            'discouraged' => 'discouraged_you_from_exercising',
            'created_at' => 'creation_date',
            'updated_at' => 'last_changed',
            'deleted_at' => 'deleted_date',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}

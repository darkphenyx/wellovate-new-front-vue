<?php

namespace App\Transformers;

use App\Anthropometric;
use League\Fractal\TransformerAbstract;

class AnthropometricTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param Anthropometric $anthropometric
     * @return array
     */
    public function transform(Anthropometric $anthropometric)
    {
        return [
            'identifier' => (int)$anthropometric->id,
            'user_identifier' => (int)$anthropometric->basic_id,
            'bicep_circumference' => (double)$anthropometric->bicep_circum,
            'biological_sex' => (string)$anthropometric->biologic_sex,
            'percentage_body_fat' => (double)$anthropometric->body_fat_percent,
            'users_height' => (double)$anthropometric->body_height,
            'bmi' => (double)$anthropometric->body_mass_index,
            't_score_bone_density' => (double)$anthropometric->bone_density,
            'bust_circumference' => (double)$anthropometric->bust_circum,
            'fitzpatrick' => (string)$anthropometric->fitzpatrick_skin,
            'height_again' => (double)$anthropometric->height,
            'ideal_weight' => (double)$anthropometric->ideal_body_weight,
            'lean_weight' => (double)$anthropometric->lean_body_weight,
            'body_water_percentage' => (double)$anthropometric->body_water,
            'thigh_circumference' => (double)$anthropometric->thigh_circum,
            'waist_circumference' => (double)$anthropometric->waist_circum,
            'water_content' => (double)$anthropometric->water_content,
            'total_weight' => (double)$anthropometric->weight,
            'creation_date' => (string)$anthropometric->created_at,
            'last_changed' => (string)$anthropometric->updated_at,
            'deleted_date' => isset($anthropometric->deleted_at) ? (string)$anthropometric->deleted_at : null,

            'links' => [
                [
                    'rel' => 'self',
                    'href' => route('anthropometrics.show', $anthropometric->id),
                ],
            ]
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes = [
            'identifier' => 'id',
            'user_identifier' => 'basic_id',
            'bicep_circumference' => 'bicep_circum',
            'biological_sex' => 'biologic_sex',
            'percentage_body_fat' => 'body_fat_percent',
            'users_height' => 'body_height',
            'bmi' => 'body_mass_index',
            't_score_bone_density' => 'bone_density',
            'bust_circumference' => 'bust_circum',
            'fitzpatrick' => 'fitzpatrick_skin',
            'height_again' => 'height',
            'ideal_weight' => 'ideal_body_weight',
            'lean_weight' => 'lean_body_weight',
            'body_water_percentage' => 'body_water',
            'thigh_circumference' => 'thigh_circum',
            'waist_circumference' => 'waist_circum',
            'water_content' => 'water_content',
            'total_weight' => 'weight',
            'creation_date' => 'created_at',
            'last_changed' => 'updated_at',
            'deleted_date' => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
            'id' => 'identifier',
            'basic_id' => 'user_identifier',
            'bicep_circum' => 'bicep_circumference',
            'biologic_sex' => 'biological_sex',
            'body_fat_percent' => 'percentage_body_fat',
            'body_height' => 'users_height',
            'body_mass_index' => 'bmi',
            'bone_density' => 't_score_bone_density',
            'bust_circum' => 'bust_circumference',
            'fitzpatrick_skin' => 'fitzpatrick',
            'height' => 'height_again',
            'ideal_body_weight' => 'ideal_weight',
            'lean_body_weight' => 'lean_weight',
            'body_water' => 'body_water_percentage',
            'thigh_circum' => 'thigh_circumference',
            'waist_circum' => 'waist_circumference',
            'water_content' => 'water_content',
            'weight' => 'total_weight',
            'created_at' => 'creation_date',
            'updated_at' => 'last_changed',
            'deleted_at' => 'deleted_date',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}

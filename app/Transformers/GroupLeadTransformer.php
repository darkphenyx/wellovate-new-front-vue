<?php

namespace App\Transformers;

use App\GroupLead;
use League\Fractal\TransformerAbstract;

class GroupLeadTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param GroupLead $groupLead
     * @return array
     */
    public function transform(GroupLead $groupLead)
    {
        return [
            'identifier' => (int)$groupLead->id,
            'coach_identifier' => (int)$groupLead->coach_id,
            'group_name' => (string)$groupLead->name,
            'creation_date' => (string)$groupLead->created_at,
            'last_changed' => (string)$groupLead->updated_at,
            'deleted_date' => isset($groupLead->deleted_at) ? (string)$groupLead->deleted_at : null,

            'links' => [
                [
                    'rel' => 'self',
                    'href' => route('group_lead.show', $groupLead->id)
                ],
                [
                    'rel' => 'coaches.group_leads',
                    'href' => route('coaches.group_leads.index', $groupLead->coach_id)
                ],
                [
                    'rel' => 'coaches.show',
                    'href' => route('coaches.show', $groupLead->coach_id)
                ],
            ]
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes = [
            'identifier' => 'id',
            'coach_identifier' => 'coach_id',
            'group_name' => 'name',
            'creation_date' => 'created_at',
            'last_changed' => 'updated_at',
            'deleted_date' => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
            'id' => 'identifier',
            'coach_id' => 'coach_identifier',
            'name' => 'group_name',
            'created_at' => 'creation_date',
            'updated_at' => 'last_changed',
            'deleted_at' => 'deleted_date',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}

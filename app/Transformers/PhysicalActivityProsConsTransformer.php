<?php

namespace App\Transformers;

use App\PhysicalActivityProsCons;
use League\Fractal\TransformerAbstract;

class PhysicalActivityProsConsTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param PhysicalActivityProsCons $physicalActivityProsCons
     * @return array
     */
    public function transform(PhysicalActivityProsCons $physicalActivityProsCons)
    {
        return [
            'identifier' => (int)$physicalActivityProsCons->id,
            'user_identifier' => (int)$physicalActivityProsCons->basic_id,
            'improve_my_health' => (string)$physicalActivityProsCons->improve_health,
            'concerned_exercise_makes_me_uncomfortable' => (string)$physicalActivityProsCons->uncomfortable,
            'improve_energy_levels' => (string)$physicalActivityProsCons->improve_energy,
            'improve_my_mood' => (string)$physicalActivityProsCons->improve_mood,
            'improve_my_mental_abilities' => (string)$physicalActivityProsCons->improve_mental,
            'relieve_stress' => (string)$physicalActivityProsCons->relieve_stress,
            'improve_my_self_image' => (string)$physicalActivityProsCons->self_image,
            'become_more_attractive' => (string)$physicalActivityProsCons->more_attractive,
            'concerned_others_see_me_exercising' => (string)$physicalActivityProsCons->others_see,
            'lose_or_maintain_weight' => (string)$physicalActivityProsCons->lose_weight,
            'meet_people_or_make_friends' => (string)$physicalActivityProsCons->make_friends,
            'waste_to_much_time' => (string)$physicalActivityProsCons->waste_time,
            'less_time_for_others' => (string)$physicalActivityProsCons->less_time,
            'creation_date' => (string)$physicalActivityProsCons->created_at,
            'last_changed' => (string)$physicalActivityProsCons->updated_at,
            'deleted_date' => isset($physicalActivityProsCons->deleted_at) ? (string)$physicalActivityProsCons->deleted_at : null,

            'links' => [
                [
                    'rel' => 'self',
                    'href' => route('physical_activity_pros_cons.show', $physicalActivityProsCons->id)
                ]
            ]
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes = [
            'identifier' => 'id',
            'user_identifier' => 'basic_id',
            'improve_my_health' => 'improve_health',
            'concerned_exercise_makes_me_uncomfortable' => 'uncomfortable',
            'improve_energy_levels' => 'improve_energy',
            'improve_my_mood' => 'improve_mood',
            'improve_my_mental_abilities' => 'improve_mental',
            'relieve_stress' => 'relieve_stress',
            'improve_my_self_image' => 'self_image',
            'become_more_attractive' => 'more_attractive',
            'concerned_others_see_me_exercising' => 'others_see',
            'lose_or_maintain_weight' => 'lose_weight',
            'meet_people_or_make_friends' => 'make_friends',
            'waste_to_much_time' => 'waste_time',
            'less_time_for_others' => 'less_time',
            'creation_date' => 'created_at',
            'last_changed' => 'updated_at',
            'deleted_date' => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
            'id' => 'identifier',
            'basic_id' => 'user_identifier',
            'improve_health' => 'improve_my_health',
            'uncomfortable' => 'concerned_exercise_makes_me_uncomfortable',
            'improve_energy' => 'improve_energy_levels',
            'improve_mood' => 'improve_my_mood',
            'improve_mental' => 'improve_my_mental_abilities',
            'relieve_stress' => 'relieve_stress',
            'self_image' => 'improve_my_self_image',
            'more_attractive' => 'become_more_attractive',
            'others_see' => 'concerned_others_see_me_exercising',
            'lose_weight' => 'lose_or_maintain_weight',
            'make_friends' => 'meet_people_or_make_friends',
            'waste_time' => 'waste_to_much_time',
            'less_time' => 'less_time_for_others',
            'created_at' => 'creation_date',
            'updated_at' => 'last_changed',
            'deleted_at' => 'deleted_date',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}

<?php

namespace App\Transformers;

use App\Sleep;
use League\Fractal\TransformerAbstract;

class SleepTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param Sleep $sleep
     * @return array
     */
    public function transform(Sleep $sleep)
    {
        return [
            'identifier' => (int)$sleep->id,
            'user_identifier' => (int)$sleep->basic_id,
            'times_awake_per_sleep_cycle' => isset($sleep->awake_per_sleep) ? (int)$sleep->awake_per_sleep : null,
            'times_restless_per_sleep_cycle' => isset($sleep->restless) ? (int)$sleep->restless : null,
            'time_spent_asleep' => (double)$sleep->sleep_duration,
            'latency_before_sleep' => (double)$sleep->sleep_latency,
            'sleep_position' => isset($sleep->sleep_position) ? (string)$sleep->sleep_position : null,
            'time_awake_when_restless' => (double)$sleep->awake_while_restless,
            'time_spent_in_bed' => (double)$sleep->length_in_bed,
            'wake_up_time' => (string)$sleep->wake_up,
            'time_out_of_bed' => (string)$sleep->time_out_bed,
            'creation_date' => (string)$sleep->created_at,
            'last_changed' => (string)$sleep->updated_at,
            'deleted_date' => isset($sleep->deleted_at) ? (string)$sleep->deleted_at : null,

            'links' => [
                [
                    'rel' => 'self',
                    'href' => route('sleeps.show', $sleep->id)
                ]
            ]
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes = [
            'identifier' => 'id',
            'user_identifier' => 'basic_id',
            'times_awake_per_sleep_cycle' => 'awake_per_sleep',
            'times_restless_per_sleep_cycle' => 'restless',
            'time_spent_asleep' => 'sleep_duration',
            'latency_before_sleep' => 'sleep_latency',
            'sleep_position' => 'sleep_position',
            'time_awake_when_restless' => 'awake_while_restless',
            'time_spent_in_bed' => 'length_in_bed',
            'wake_up_time' => 'wake_up',
            'time_out_of_bed' => 'time_out_bed',
            'creation_date' => 'created_at',
            'last_changed' => 'updated_at',
            'deleted_date' => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
            'id' => 'identifier',
            'basic_id' => 'user_identifier',
            'awake_per_sleep' => 'times_awake_per_sleep_cycle',
            'restless' => 'times_restless_per_sleep_cycle',
            'sleep_duration' => 'time_spent_asleep',
            'sleep_latency' => 'latency_before_sleep',
            'sleep_position' => 'sleep_position',
            'awake_while_restless' => 'time_awake_when_restless',
            'length_in_bed' => 'time_spent_in_bed',
            'wake_up' => 'wake_up_time',
            'time_out_bed' => 'time_out_of_bed',
            'created_at' => 'creation_date',
            'updated_at' => 'last_changed',
            'deleted_at' => 'deleted_date',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}

<?php

namespace App\Transformers;

use App\PhysicalActivityChange;
use League\Fractal\TransformerAbstract;

class PhysicalActivityChangeTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param PhysicalActivityChange $physicalActivityChange
     * @return array
     */
    public function transform(PhysicalActivityChange $physicalActivityChange)
    {
        return [
            'identifier' => (int)$physicalActivityChange->id,
            'user_identifier' => (int)$physicalActivityChange->basic_id,
            'days_per_week_more_than_30_minutes_exercise' => (string)$physicalActivityChange->on_average,
            'how_long_for_5_or_more_days' => (string)$physicalActivityChange->how_long,
            'going_forward_can_you_for_5_days' => (string)$physicalActivityChange->going_forward,
            'creation_date' => (string)$physicalActivityChange->created_at,
            'last_changed' => (string)$physicalActivityChange->updated_at,
            'deleted_date' => isset($physicalActivityChange->deleted_at) ? (string)$physicalActivityChange->deleted_at : null,

            'links' => [
                [
                    'rel' => 'self',
                    'href' => route('physical_activity_change.show', $physicalActivityChange->id)
                ]
            ]
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes = [
            'identifier' => 'id',
            'user_identifier' => 'basic_id',
            'days_per_week_more_than_30_minutes_exercise' => 'on_average',
            'how_long_for_5_or_more_days' => 'how_long',
            'going_forward_can_you_for_5_days' => 'going_forward',
            'creation_date' => 'created_at',
            'last_changed' => 'updated_at',
            'deleted_date' => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
            'id' => 'identifier',
            'basic_id' => 'user_identifier',
            'on_average' => 'days_per_week_more_than_30_minutes_exercise',
            'how_long' => 'how_long_for_5_or_more_days',
            'going_forward' => 'going_forward_can_you_for_5_days',
            'created_at' => 'creation_date',
            'updated_at' => 'last_changed',
            'deleted_at' => 'deleted_date',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}

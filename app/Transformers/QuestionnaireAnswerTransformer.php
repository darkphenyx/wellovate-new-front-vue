<?php

namespace App\Transformers;

use App\QuestionnaireAnswer;
use League\Fractal\TransformerAbstract;

class QuestionnaireAnswerTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(QuestionnaireAnswer $questionnaireAnswer)
    {
        return [
            'identifier' => (int)$questionnaireAnswer->id,
            'user_identifier' => (int)$questionnaireAnswer->basic_id,
            'question_identifier' => (int)$questionnaireAnswer->question_id,
            'answer' => (string)$questionnaireAnswer->question_answer,
            'creation_date' => (string)$questionnaireAnswer->created_at,
            'last_changed' => (string)$questionnaireAnswer->updated_at,
            'deleted_date' => isset($questionnaireAnswer->deleted_at) ? (string)$questionnaireAnswer->deleted_at : null,
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes = [
            'identifier' => 'id',
            'user_identifier' => 'basic_id',
            'question_identifier' => 'question_id',
            'answer' => 'question_answer',
            'creation_date' => 'created_at',
            'last_changed' => 'updated_at',
            'deleted_date' => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
            'id' => 'identifier',
            'basic_id' => 'user_identifier',
            'question_id' => 'question_identifier',
            'question_answer' => 'answer',
            'created_at' => 'creation_date',
            'updated_at' => 'last_changed',
            'deleted_at' => 'deleted_date',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}

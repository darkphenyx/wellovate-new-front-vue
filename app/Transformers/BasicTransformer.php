<?php

namespace App\Transformers;

use App\Basic;
use League\Fractal\TransformerAbstract;

class BasicTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param Basic $basic
     * @return array
     */
    public function transform(Basic $basic)
    {
        return [
            'identifier' => (int)$basic->id,
            'name' => (string)$basic->name,
            'email' => (string)$basic->email,
            'user_age' => (int)$basic->age,
            'user_sex' => (string)$basic->sex,
            'zip_code' => (int)$basic->zip,
            'facebook' => (string)$basic->facebook_name,
            'is_verified' => (int)$basic->verified,
            'creation_date' => (string)$basic->created_at,
            'last_changed' => (string)$basic->updated_at,
            'deleted_date' => isset($basic->deleted_at) ? (string)$basic->deleted_at : null,

            'links' => [
                [
                    'rel' => 'self',
                    'href' => route('basics.show', $basic->id)
                ],
                [
                    'rel' => 'basics.anthropometrics',
                    'href' => route('basics.anthropometrics.index', $basic->id)
                ],
                [
                    'rel' => 'basics.behaviors',
                    'href' => route('basics.behaviors.index', $basic->id)
                ],
                [
                    'rel' => 'basics.cardiovasculars',
                    'href' => route('basics.cardiovasculars.index', $basic->id)
                ],
                [
                    'rel' => 'basics.fitness',
                    'href' => route('basics.fitness.index', $basic->id)
                ],
                [
                    'rel' => 'basics.goal_users',
                    'href' => route('basics.goal_users.index', $basic->id)
                ],
                [
                    'rel' => 'basics.group_teams',
                    'href' => route('basics.group_teams.index', $basic->id)
                ],
                [
                    'rel' => 'basics.inactivity',
                    'href' => route('basics.inactivity.index', $basic->id)
                ],
                [
                    'rel' => 'basics.mixed_reality_uses',
                    'href' => route('basics.mixed_reality_uses.index', $basic->id)
                ],
                [
                    'rel' => 'basics.outcomes',
                    'href' => route('basics.outcomes.index', $basic->id)
                ],
                [
                    'rel' => 'basics.physical_activity',
                    'href' => route('basics.physical_activity.index', $basic->id)
                ],
                [
                    'rel' => 'basics.physical_activity_assessment',
                    'href' => route('basics.physical_activity_assessment.index', $basic->id)
                ],
                [
                    'rel' => 'basics.physical_activity_barriers',
                    'href' => route('basics.physical_activity_barriers.index', $basic->id)
                ],
                [
                    'rel' => 'basics.physical_activity_changes',
                    'href' => route('basics.physical_activity_changes.index', $basic->id)
                ],
                [
                    'rel' => 'basics.physical_activity_confidences',
                    'href' => route('basics.physical_activity_confidences.index', $basic->id)
                ],
                [
                    'rel' => 'basics.physical_activity_efficacys',
                    'href' => route('basics.physical_activity_efficacys.index', $basic->id)
                ],
                [
                    'rel' => 'basics.physical_activity_enviro',
                    'href' => route('basics.physical_activity_enviro.index', $basic->id)
                ],
                [
                    'rel' => 'basics.physical_activity_fun',
                    'href' => route('basics.physical_activity_fun.index', $basic->id)
                ],
                [
                    'rel' => 'basics.physical_activity_pros_cons',
                    'href' => route('basics.physical_activity_pros_cons.index', $basic->id)
                ],
                [
                    'rel' => 'basics.physical_activity_support',
                    'href' => route('basics.physical_activity_support.index', $basic->id)
                ],
                [
                    'rel' => 'basics.sleeps',
                    'href' => route('basics.sleeps.index', $basic->id)
                ],
            ]
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes = [
            'identifier' => 'id',
            'name' => 'name',
            'email' => 'email',
            'user_age' => 'age',
            'user_sex' => 'sex',
            'zip_code' => 'zip',
            'facebook' => 'facebook_name',
            'is_verified' => 'verified',
            'creation_date' => 'created_at',
            'last_changed' => 'updated_at',
            'deleted_date' => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
            'id' => 'identifier',
            'name' => 'name',
            'email' => 'email',
            'age' => 'user_age',
            'sex' => 'user_sex',
            'zip' => 'zip_code',
            'facebook_name' => 'facebook',
            'verified' => 'is_verified',
            'created_at' => 'creation_date',
            'updated_at' => 'last_changed',
            'deleted_at' => 'deleted_date',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}

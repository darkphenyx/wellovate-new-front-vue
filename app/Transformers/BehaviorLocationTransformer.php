<?php

namespace App\Transformers;

use App\BehaviorLocation;
use League\Fractal\TransformerAbstract;

class BehaviorLocationTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param BehaviorLocation $behaviorLocation
     * @return array
     */
    public function transform(BehaviorLocation $behaviorLocation)
    {
        return [
            'identifier' => (int)$behaviorLocation->id,
            'name' => (string)$behaviorLocation->name,
            'address' => (string)$behaviorLocation->address,
            'latitude' => (float)$behaviorLocation->lat,
            'longitude' => (float)$behaviorLocation->lng,
            'creation_date' => (string)$behaviorLocation->created_at,
            'last_changed' => (string)$behaviorLocation->updated_at,
            'deleted_date' => isset($behaviorLocation->deleted_at) ? (string)$behaviorLocation->deleted_at : null,

            'links' => [
                [
                    'rel' => 'self',
                    'href' => route('behavior_locations.show', $behaviorLocation->id)
                ]
            ]
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes = [
            'identifier' => 'id',
            'name' => 'name',
            'address' => 'address',
            'latitude' => 'lat',
            'longitude' => 'lng',
            'creation_date' => 'created_at',
            'last_changed' => 'updated_at',
            'deleted_date' => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
            'id' => 'identifier',
            'name' => 'name',
            'address' => 'address',
            'lat' => 'latitude',
            'lng' => 'longitude',
            'created_at' => 'creation_date',
            'updated_at' => 'last_changed',
            'deleted_at' => 'deleted_date',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}

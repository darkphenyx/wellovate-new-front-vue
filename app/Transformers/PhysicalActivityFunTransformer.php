<?php

namespace App\Transformers;

use App\PhysicalActivityFun;
use League\Fractal\TransformerAbstract;

class PhysicalActivityFunTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param PhysicalActivityFun $physicalActivityFun
     * @return array
     */
    public function transform(PhysicalActivityFun $physicalActivityFun)
    {
        return [
            'identifier' => (int)$physicalActivityFun->id,
            'user_identifier' => (int)$physicalActivityFun->basic_id,
            'enjoy_doing_exercise' => (string)$physicalActivityFun->enjoy_exercise,
            'feel_physically_good_while_exercising' => (string)$physicalActivityFun->feel_good,
            'have_fun_exercising' => (string)$physicalActivityFun->fun_exercising,
            'enjoy_sitting_watching_tv' => (string)$physicalActivityFun->watching_tv,
            'enjoy_using_computer' => (string)$physicalActivityFun->enjoy_computer,
            'enjoy_sitting_and_reading' => (string)$physicalActivityFun->enjoy_reading,
            'creation_date' => (string)$physicalActivityFun->created_at,
            'last_changed' => (string)$physicalActivityFun->updated_at,
            'deleted_date' => isset($physicalActivityFun->deleted_at) ? (string)$physicalActivityFun->deleted_at : null,

            'links' => [
                [
                    'rel' => 'self',
                    'href' => route('physical_activity_fun.show', $physicalActivityFun->id)
                ]
            ]
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes = [
            'identifier' => 'id',
            'user_identifier' => 'basic_id',
            'enjoy_doing_exercise' => 'enjoy_exercise',
            'feel_physically_good_while_exercising' => 'feel_good',
            'have_fun_exercising' => 'fun_exercising',
            'enjoy_sitting_watching_tv' => 'watching_tv',
            'enjoy_using_computer' => 'enjoy_computer',
            'enjoy_sitting_and_reading' => 'enjoy_reading',
            'creation_date' => 'created_at',
            'last_changed' => 'updated_at',
            'deleted_date' => 'deleted_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
            'id' => 'identifier',
            'basic_id' => 'user_identifier',
            'enjoy_exercise' => 'enjoy_doing_exercise',
            'feel_good' => 'feel_physically_good_while_exercising',
            'fun_exercising' => 'have_fun_exercising',
            'watching_tv' => 'enjoy_sitting_watching_tv',
            'enjoy_computer' => 'enjoy_using_computer',
            'enjoy_reading' => 'enjoy_sitting_and_reading',
            'created_at' => 'creation_date',
            'updated_at' => 'last_changed',
            'deleted_at' => 'deleted_date',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}

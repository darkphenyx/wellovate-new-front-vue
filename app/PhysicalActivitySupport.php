<?php

namespace App;

use App\Transformers\PhysicalActivitySupportTransformer;
use Illuminate\Database\Eloquent\Model;
use App\Basic;
use Illuminate\Database\Eloquent\SoftDeletes;

class PhysicalActivitySupport extends Model
{
    use SoftDeletes;

    public $transformer = PhysicalActivitySupportTransformer::class;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'basic_id',
        'encourage_exercise',
        'offered_participate',
        'exercised_with',
        'discouraged',
    ];

    public function basic()
    {
        return $this->belongsTo(Basic::class);
    }
}

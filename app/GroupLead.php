<?php

namespace App;

use App\Transformers\GroupLeadTransformer;
use Illuminate\Database\Eloquent\Model;
use App\Coach;
use App\GroupTeam;
use Illuminate\Database\Eloquent\SoftDeletes;

class GroupLead extends Model
{
    use SoftDeletes;

    public $transformer = GroupLeadTransformer::class;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'coach_id',
        'name',
    ];

    public function coach()
    {
        return $this->belongsTo(Coach::class);
    }

    public function groups()
    {
        return $this->hasMany(GroupTeam::class);
    }
}

<?php

namespace App;

use App\Transformers\EnumTransformer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Enum extends Model
{
    use SoftDeletes;

    public $transformer = EnumTransformer::class;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'question_id',
        'answer_choice'
    ];

    public function question()
    {
        return $this->belongsTo(Question::class);
    }
}

<?php

namespace App\Providers;

use App\Basic;
use App\Coach;
use App\Policies\BasicPolicy;
use App\Policies\CoachPolicy;
use App\Policies\UserPolicy;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Basic::class => BasicPolicy::class,
        Coach::class => CoachPolicy::class,
        User::class => UserPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('admin-action', function ($user) {
            return $user->isAdmin();
        });

        Passport::routes(null, ['middleware' => [ \Barryvdh\Cors\HandleCors::class ]]);
        Passport::tokensExpireIn(Carbon::now()->addMinutes(30));
        Passport::refreshTokensExpireIn(Carbon::now()->addDays(30));
        Passport::enableImplicitGrant();

        Passport::tokensCan([
            'manage-group-teams' => 'Create, read, update, and delete group members.',
            'read-general' => 'Can read individual data about the user. Cannot modify anything',
            'manage-account' => 'Read your account data, id, name, email, if verified, and if admin (cannot read password). Modify your account data (email, password). Cannot delete your account.',
            'mixed-game-dev' => 'Can create and update the experiences',
            'manage-user-data' => 'Create, update, read user related data',
            'manage-perspectives' => 'Create, update, and delete perspectives'
        ]);
    }
}

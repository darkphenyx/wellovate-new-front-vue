<?php

namespace App;

use App\Transformers\GoalUserTransformer;
use Illuminate\Database\Eloquent\Model;
use App\Basic;
use App\GoalName;
use Illuminate\Database\Eloquent\SoftDeletes;

class GoalUser extends Model
{
    use SoftDeletes;

    public $transformer = GoalUserTransformer::class;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'basic_id',
        'goal_id',
        'priority',
        'completed',
    ];

    public function basic()
    {
        return $this->belongsTo(Basic::class);
    }

    public function name()
    {
        return $this->belongsTo(GoalName::class);
    }
}

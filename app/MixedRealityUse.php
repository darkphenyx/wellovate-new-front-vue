<?php

namespace App;

use App\Transformers\MixedRealityUseTransformer;
use Illuminate\Database\Eloquent\Model;
use App\Basic;
use Illuminate\Database\Eloquent\SoftDeletes;

class MixedRealityUse extends Model
{
    use SoftDeletes;

    public $transformer = MixedRealityUseTransformer::class;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'basic_id',
        'experience_id',
        'start_lobby',
        'end_lobby',
        'start_experience',
        'end_experience'
    ];

    public function experiences()
    {
        return $this->belongsTo(Basic::class);
    }
}

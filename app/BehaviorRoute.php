<?php

namespace App;

use App\Transformers\BehaviorRouteTransformer;
use Illuminate\Database\Eloquent\Model;
use App\Behavior;
use Illuminate\Database\Eloquent\SoftDeletes;

class BehaviorRoute extends Model
{
    use SoftDeletes;

    public $transformer = BehaviorRouteTransformer::class;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name',
        'address',
        'lat',
        'lng',
    ];

    public function behavior()
    {
        return $this->hasOne(Behavior::class, 'route_id');
    }
}

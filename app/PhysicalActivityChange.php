<?php

namespace App;

use App\Transformers\PhysicalActivityChangeTransformer;
use Illuminate\Database\Eloquent\Model;
use App\Basic;
use Illuminate\Database\Eloquent\SoftDeletes;

class PhysicalActivityChange extends Model
{
    use SoftDeletes;

    public $transformer = PhysicalActivityChangeTransformer::class;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'basic_id',
        'on_average',
        'how_long',
        'going_forward',
    ];

    public function basic()
    {
        return $this->belongsTo(Basic::class);
    }
}

<?php

namespace App;

use App\Transformers\QuestionnaireQuestionTransformer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class QuestionnaireQuestion extends Model
{
    use SoftDeletes;

    public $transformer = QuestionnaireQuestionTransformer::class;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'questionnaire_name_id',
        'question_id'
    ];

    public function questionnaire_name()
    {
        return $this->belongsTo(QuestionnaireName::class);
    }

    public function question()
    {
        return $this->belongsTo(Question::class);
    }
}

<?php

namespace App;

use App\Transformers\FitnessTransformer;
use Illuminate\Database\Eloquent\Model;
use App\Basic;
use Illuminate\Database\Eloquent\SoftDeletes;

class Fitness extends Model
{
    use SoftDeletes;

    public $transformer = FitnessTransformer::class;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'basic_id',
        'o2_consumption',
        'o2_consumption_max',
        'heart_rate_recovery',
        'basal_metabolic',
        'resting_energy',
    ];

    public function basic()
    {
        return $this->belongsTo(Basic::class);
    }
}

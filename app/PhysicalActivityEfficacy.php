<?php

namespace App;

use App\Transformers\PhysicalActivityEfficacyTransformer;
use Illuminate\Database\Eloquent\Model;
use App\Basic;
use Illuminate\Database\Eloquent\SoftDeletes;

class PhysicalActivityEfficacy extends Model
{
    use SoftDeletes;

    public $transformer = PhysicalActivityEfficacyTransformer::class;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'basic_id',
        'seek_information',
        'track_exercise',
        'around_issues',
        'set_reminders',
        'is_rewarding',
        'take_actions',
        'focus_benefits',
        'set_goals',
    ];

    public function basic()
    {
        return $this->belongsTo(Basic::class);
    }
}

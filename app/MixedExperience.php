<?php

namespace App;

use App\Transformers\MixedExperienceTransformer;
use Illuminate\Database\Eloquent\Model;
use App\MixedRealityUse;
use Illuminate\Database\Eloquent\SoftDeletes;

class MixedExperience extends Model
{
    use SoftDeletes;

    public $transformer = MixedExperienceTransformer::class;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name'
    ];

    public function mixedUsers()
    {
        return $this->hasMany(MixedRealityUse::class);
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: GlaDOS
 * Date: 10/28/2017
 * Time: 8:14 PM
 */

namespace App\Traits;

trait AdminActions
{
    public function before($user, $ability)
    {
        if ($user->isAdmin()){
            return true;
        }
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: GlaDOS
 * Date: 11/12/2017
 * Time: 2:57 PM
 */

namespace App\Traits;

trait OutsideConnection {

    public function getToken($request_url, $client_id, $client_secret, $code, $redirect_uri){

        // base64 encode the client_id and client_secret
        $auth = base64_encode("{$client_id}:{$client_secret}");
        // urlencode the redirect_url
        $redirect_uri = urlencode($redirect_uri);
        $request_url .= "?client_id={$client_id}&grant_type=authorization_code&redirect_uri={$redirect_uri}&code={$code}";

        // Set the headers
        $headers = [
            "Authorization: Basic {$auth}",
            "Content-Type: application/x-www-form-urlencoded",
        ];

        // Initiate curl session
        $ch = curl_init();
        // Set headers
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        // Options (see: http://php.net/manual/en/function.curl-setopt.php)
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_URL, $request_url);
        curl_setopt($ch, CURLOPT_POST, 1);
        //dd($ch);
        // Execute the curl request and get the response
        $response = curl_exec($ch);
        // Throw an exception if there was an error with curl
        if($response === false){
            throw new Exception(curl_error($ch), curl_errno($ch));
        }

        // Get the body of the response
        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $responseBody = substr($response, $header_size);
        // Close curl session
        curl_close($ch);

        // Return response body
        return $responseBody;

    }

    public function getTokenPolar($request_url, $client_id, $client_secret, $code)
    {
        // base64 encode the client_id and client_secret
        $auth = base64_encode("{$client_id}:{$client_secret}");
        $authString = 'Basic ' . $auth;
        //dd($authString);

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $request_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "grant_type=authorization_code&code=".$code,
            CURLOPT_HTTPHEADER => array(
                "accept: application/json;charset=UTF-8",
                "authorization: ".$authString,
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            return $response;
        }
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: GlaDOS
 * Date: 9/15/2017
 * Time: 1:06 AM
 */
namespace App\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class CoachScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        $builder->has('group_lead');
    }
}
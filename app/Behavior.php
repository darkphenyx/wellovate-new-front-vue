<?php

namespace App;

use App\Transformers\BehaviorTransformer;
use Illuminate\Database\Eloquent\Model;
use App\BehaviorLocation;
use App\BehaviorRoute;
use App\Basic;
use Illuminate\Database\Eloquent\SoftDeletes;

class Behavior extends Model
{
    use SoftDeletes;

    public $transformer = BehaviorTransformer::class;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'basic_id',
        'smoking_status',
        'cigs_per_day',
        'years_smoked',
        'location_id',
        'vape_draws_day',
        'route_id',
        'teeth_brushing',
        'teeth_flossing',
        'tv_bedroom',
        'uv_exposure',
    ];

    public function basic()
    {
        return $this->belongsTo(Basic::class);
    }

    public function location()
    {
        return $this->belongsTo(BehaviorLocation::class);
    }

    public function route()
    {
        return $this->belongsTo(BehaviorRoute::class);
    }
}

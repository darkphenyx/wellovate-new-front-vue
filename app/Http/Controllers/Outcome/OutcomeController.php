<?php

namespace App\Http\Controllers\Outcome;

use App\Outcome;
use App\Transformers\OutcomeTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class OutcomeController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . OutcomeTransformer::class)->only(['store', 'update']);
        $this->middleware('scope:read-general')->only('index');
        $this->middleware('scope:manage-user-data')->only(['store', 'show', 'update']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->allowedAdminAction();

        $outcomes = Outcome::all();

        return $this->showAll($outcomes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'basic_id' => 'required|integer|unique:outcomes,basic_id',
            'alzheimers' => 'required|boolean',
            'gen_anxiety_disorder' => 'required|boolean',
            'cardiovascular_disease' => 'required|boolean',
            'coronary_heart_disease' => 'required|boolean',
            'dementia' => 'required|boolean',
            'depression' => 'required|boolean',
            'diabetes' => 'required|boolean',
            'healthy' => 'required|boolean',
            'hypertension' => 'required|boolean',
            'inactive' => 'required|boolean',
            'mild_cognitive_impairment' => 'required|boolean',
            'number_falls' => 'required|integer|min:0|max:20',
            'underweight' => 'required|boolean',
            'normal_weight' => 'required|boolean',
            'overweight' => 'required|boolean',
            'obesity_1' => 'required|boolean',
            'obesity_2' => 'required|boolean',
            'obesity_3' => 'required|boolean',
            'partial_sleep_deprivation' => 'required|boolean',
            'prediabetes' => 'required|boolean',
            'prehypertension' => 'required|boolean',
            'subclinical_depression' => 'required|boolean',
            'subjective_poor_sleep' => 'required|boolean',
            'total_sleep_deprivation' => 'required|boolean',
            'high_ldl_concentration' => 'required|boolean',
            'high_ldl_particle' => 'required|boolean',
            'high_triglycerides' => 'required|boolean',
            'low_hdl' => 'required|boolean',
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $outcome = Outcome::create($data);

        return $this->showOne($outcome, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Outcome $outcome)
    {
        return $this->showOne($outcome);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Outcome $outcome)
    {
        $rules = [
            'alzheimers' => 'boolean',
            'gen_anxiety_disorder' => 'boolean',
            'cardiovascular_disease' => 'boolean',
            'coronary_heart_disease' => 'boolean',
            'dementia' => 'boolean',
            'depression' => 'boolean',
            'diabetes' => 'boolean',
            'healthy' => 'boolean',
            'hypertension' => 'boolean',
            'inactive' => 'boolean',
            'mild_cognitive_impairment' => 'boolean',
            'number_falls' => 'integer|min:0|max:20',
            'underweight' => 'boolean',
            'normal_weight' => 'boolean',
            'overweight' => 'boolean',
            'obesity_1' => 'boolean',
            'obesity_2' => 'boolean',
            'obesity_3' => 'boolean',
            'partial_sleep_deprivation' => 'boolean',
            'prediabetes' => 'boolean',
            'prehypertension' => 'boolean',
            'subclinical_depression' => 'boolean',
            'subjective_poor_sleep' => 'boolean',
            'total_sleep_deprivation' => 'boolean',
            'high_ldl_concentration' => 'boolean',
            'high_ldl_particle' => 'boolean',
            'high_triglycerides' => 'boolean',
            'low_hdl' => 'boolean',
        ];

        if($request->has('alzheimers')){
            $outcome->alzheimers = $request->alzheimers;
        }

        if($request->has('gen_anxiety_disorder')){
            $outcome->gen_anxiety_disorder = $request->gen_anxiety_disorder;
        }

        if($request->has('cardiovascular_disease')){
            $outcome->cardiovascular_disease = $request->cardiovascular_disease;
        }

        if($request->has('coronary_heart_disease')){
            $outcome->coronary_heart_disease = $request->coronary_heart_disease;
        }

        if($request->has('dementia')){
            $outcome->dementia = $request->dementia;
        }

        if($request->has('depression')){
            $outcome->depression = $request->depression;
        }

        if($request->has('diabetes')){
            $outcome->diabetes = $request->diabetes;
        }

        if($request->has('healthy')){
            $outcome->healthy = $request->healthy;
        }

        if($request->has('hypertension')){
            $outcome->hypertension = $request->hypertension;
        }

        if($request->has('inactive')){
            $outcome->inactive = $request->inactive;
        }

        if($request->has('mild_cognitive_impairment')){
            $outcome->mild_cognitive_impairment = $request->mild_cognitive_impairment;
        }
        if($request->has('number_falls')){
            $outcome->number_falls = $request->number_falls;
        }
        if($request->has('underweight')){
            $outcome->underweight = $request->underweight;
        }
        if($request->has('normal_weight')){
            $outcome->normal_weight = $request->normal_weight;
        }
        if($request->has('overweight')){
            $outcome->overweight = $request->overweight;
        }
        if($request->has('obesity_1')){
            $outcome->obesity_1 = $request->obesity_1;
        }
        if($request->has('obesity_2')){
            $outcome->obesity_2 = $request->obesity_2;
        }
        if($request->has('obesity_3')){
            $outcome->obesity_3 = $request->obesity_3;
        }
        if($request->has('partial_sleep_deprivation')){
            $outcome->partial_sleep_deprivation = $request->partial_sleep_deprivation;
        }
        if($request->has('prediabetes')){
            $outcome->prediabetes = $request->prediabetes;
        }
        if($request->has('prehypertension')){
            $outcome->prehypertension = $request->prehypertension;
        }
        if($request->has('subclinical_depression')){
            $outcome->subclinical_depression = $request->subclinical_depression;
        }
        if($request->has('subjective_poor_sleep')){
            $outcome->subjective_poor_sleep = $request->subjective_poor_sleep;
        }
        if($request->has('total_sleep_deprivation')){
            $outcome->total_sleep_deprivation = $request->total_sleep_deprivation;
        }
        if($request->has('high_ldl_concentration')){
            $outcome->high_ldl_concentration = $request->high_ldl_concentration;
        }
        if($request->has('high_ldl_particle')){
            $outcome->high_ldl_particle = $request->high_ldl_particle;
        }
        if($request->has('high_triglycerides')){
            $outcome->high_triglycerides = $request->high_triglycerides;
        }
        if($request->has('low_hdl')){
            $outcome->low_hdl = $request->low_hdl;
        }

        if(!$outcome->isDirty()){
            return $this->errorResponse('You need to specify a different value to update', 422);
        }

        $outcome->save();

        return $this->showOne($outcome);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Outcome $outcome)
    {
        $outcome->delete();

        return $this->showOne($outcome);
    }
}

<?php

namespace App\Http\Controllers;

use App\Traits\ApiResponser;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class ApiController extends Controller
{
    use ApiResponser;

    /**
     * @SWG\Swagger(
     *   schemes={"http"},
     *   host="wellovateapi.dev",
     *   basePath="/api",
     *   @SWG\Info(
     *     title="Wellovate API",
     *     description="This is a sample server for wellovate",
     *     version="1.0.0",
     *     @SWG\Contact(
 *             email="sysadmin@wellovate.com"
 *         ),
     *   )
     * )
     */

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    protected function allowedAdminAction()
    {
        if (Gate::denies('admin-action')) {
            throw new AuthorizationException('This action is unauthorized');
        }
    }
}

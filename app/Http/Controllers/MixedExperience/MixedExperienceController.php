<?php

namespace App\Http\Controllers\MixedExperience;

use App\MixedExperience;
use App\Transformers\MixedExperienceTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class MixedExperienceController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . MixedExperienceTransformer::class)->only(['store', 'update']);
        $this->middleware('scope:read-general')->only('index');
        $this->middleware('scope:mixed-game-dev')->only(['store', 'update', 'destroy']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mixed_experiences = MixedExperience::all();

        return $this->showAll($mixed_experiences);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|string'
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $mixed_experience = MixedExperience::create($data);

        return $this->showOne($mixed_experience, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param MixedExperience $mixed_experience
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function show(MixedExperience $mixed_experience)
    {
        return $this->showOne($mixed_experience);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param MixedExperience $mixed_experience
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update(Request $request, MixedExperience $mixed_experience)
    {
        $rules = [
            'name' => 'string'
        ];

        if ($request->has('name')){
            $mixed_experience->name = $request->name;
        }

        if(!$mixed_experience->isDirty()){
            return $this->errorResponse('You need to specify a different value to update', 422);
        }

        $mixed_experience->save();

        return $this->showOne($mixed_experience);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param MixedExperience $mixed_experience
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function destroy(MixedExperience $mixed_experience)
    {
        $mixed_experience->delete();

        return $this->showOne($mixed_experience);
    }
}

<?php

namespace App\Http\Controllers\PhysicalActivity;

use App\PhysicalActivity;
use App\Transformers\PhysicalActivityTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class PhysicalActivityController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . PhysicalActivityTransformer::class)->only(['store', 'update']);
        $this->middleware('scope:read-general')->only('index');
        $this->middleware('scope:manage-user-data')->only(['store', 'show', 'update']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->allowedAdminAction();

        $physical_activity = PhysicalActivity::all();

        return $this->showAll($physical_activity);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'basic_id' => 'required|integer',
            'active_minutes' => 'required|numeric|min:5|max:999',
            'cadence' => 'required|numeric|min:20|max:1500',
            'calories_burned' => 'required|numeric|min:20|max:1500',
            'distance' => 'required|numeric|min:20|max:450',
            'exercise_minutes' => 'required|numeric|min:20|max:330',
            'floors_climbed' => 'required|integer|min:0|max:100',
            'inactivity' => 'required|numeric|min:20|max:2000',
            'moderate_vigorous' => 'required|numeric|min:5|max:999',
            'peak_accel' => 'required|numeric|min:5|max:999',
            'sitting_time' => 'required|numeric|min:5|max:1500',
            'speed' => 'required|numeric|min:5|max:500',
            'step_count' => 'required|integer',
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $physical_activity = PhysicalActivity::create($data);

        return $this->showOne($physical_activity, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param PhysicalActivity $activity
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function show(PhysicalActivity $physical_activity)
    {
        return $this->showOne($physical_activity);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param PhysicalActivity $activity
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function destroy(PhysicalActivity $physical_activity)
    {
        $physical_activity->delete();

        return $this->showOne($physical_activity);
    }
}

<?php

namespace App\Http\Controllers\PhysicalActivityConfidence;

use App\PhysicalActivityConfidence;
use App\Transformers\PhysicalActivityConfidenceTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class PhysicalActivityConfidenceController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . PhysicalActivityConfidenceTransformer::class)->only(['store', 'update']);
        $this->middleware('scope:read-general')->only('index');
        $this->middleware('scope:manage-user-data')->only(['store', 'show', 'update']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->allowedAdminAction();

        $physical_activity_confidences = PhysicalActivityConfidence::all();

        return $this->showAll($physical_activity_confidences);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'basic_id' => 'required|integer|unique:inactivity_assessments,basic_id',
            'tired' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'bad_mood' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'stressed' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'no_time' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'bad_weather' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $physical_activity_confidence = PhysicalActivityConfidence::create($data);

        return $this->showOne($physical_activity_confidence, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(PhysicalActivityConfidence $physical_activity_confidence)
    {
        return $this->showOne($physical_activity_confidence);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(PhysicalActivityConfidence $physical_activity_confidence)
    {
        $physical_activity_confidence->delete();

        return $this->showOne($physical_activity_confidence);
    }
}

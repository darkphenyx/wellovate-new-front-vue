<?php

namespace App\Http\Controllers\PhysicalActivityProsCons;

use App\PhysicalActivityProsCons;
use App\Transformers\PhysicalActivityProsConsTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class PhysicalActivityProsConsController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . PhysicalActivityProsConsTransformer::class)->only(['store', 'update']);
        $this->middleware('scope:read-general')->only('index');
        $this->middleware('scope:manage-user-data')->only(['store', 'show', 'update']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->allowedAdminAction();

        $physical_activity_pros_cons = PhysicalActivityProsCons::all();

        return $this->showAll($physical_activity_pros_cons);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'basic_id' => 'required|integer',
            'improve_health' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'uncomfortable' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'improve_energy' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'improve_mood' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'improve_mental' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'relieve_stress' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'self_image' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'more_attractive' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'others_see' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'lose_weight' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'make_friends' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'waste_time' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'less_time' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $physical_activity_pros_con = PhysicalActivityProsCons::create($data);

        return $this->showOne($physical_activity_pros_con, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(PhysicalActivityProsCons $physical_activity_pros_con)
    {
        return $this->showOne($physical_activity_pros_con);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(PhysicalActivityProsCons $physical_activity_pros_con)
    {
        $physical_activity_pros_con->delete();

        return $this->showOne($physical_activity_pros_con);
    }
}

<?php

namespace App\Http\Controllers\Questionnaire;

use App\Http\Controllers\ApiController;
use App\QuestionnaireQuestion;
use App\Transformers\QuestionnaireQuestionTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class QuestionnaireQuestionController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . QuestionnaireQuestionTransformer::class)->only(['store', 'update']);
        $this->middleware('scope:read-general')->only('index');
        $this->middleware('scope:manage-user-data')->only(['store', 'show', 'update']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->allowedAdminAction();

        $questionnaire_questions = QuestionnaireQuestion::all();

        return $this->showAll($questionnaire_questions);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'questionnaire_name_id' => 'required|integer',
            'question_id' => 'required|integer',
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $questionnaire_question = QuestionnaireQuestion::create($data);

        return $this->showOne($questionnaire_question, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(QuestionnaireQuestion $questionnaireQuestion)
    {
        return $this->showOne($questionnaireQuestion);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(QuestionnaireQuestion $questionnaireQuestion)
    {
        $questionnaireQuestion->delete();

        return $this->showOne($questionnaireQuestion);
    }
}

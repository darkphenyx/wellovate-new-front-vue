<?php

namespace App\Http\Controllers\Questionnaire;

use App\Http\Controllers\ApiController;
use App\QuestionnaireAnswer;
use App\Transformers\QuestionnaireAnswerTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class QuestionnaireAnswerController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . QuestionnaireAnswerTransformer::class)->only(['store', 'update']);
        $this->middleware('scope:read-general')->only('index');
        $this->middleware('scope:manage-user-data')->only(['store', 'show', 'update']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->allowedAdminAction();

        $questionnaire_answers = QuestionnaireAnswer::all();

        return $this->showAll($questionnaire_answers);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'basic_id' => 'required|integer',
            'question_id' => 'required|integer',
            'question_answer' => 'required|string',
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $questionnaire_answer = QuestionnaireAnswer::create($data);

        return $this->showOne($questionnaire_answer, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(QuestionnaireAnswer $questionnaireAnswer)
    {
        return $this->showOne($questionnaireAnswer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(QuestionnaireAnswer $questionnaireAnswer)
    {
        $questionnaireAnswer->delete();

        return $this->showOne($questionnaireAnswer);
    }
}

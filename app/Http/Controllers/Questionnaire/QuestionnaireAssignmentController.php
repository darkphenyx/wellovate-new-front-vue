<?php

namespace App\Http\Controllers\Questionnaire;

use App\Http\Controllers\ApiController;
use App\QuestionnaireAssignment;
use App\QuestionnaireName;
use App\Transformers\QuestionnaireAssignmentTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class QuestionnaireAssignmentController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . QuestionnaireAssignmentTransformer::class)->only(['store', 'update']);
        $this->middleware('scope:read-general')->only('index');
        $this->middleware('scope:manage-user-data')->only(['store', 'show', 'update', 'assignQuestionnaire']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->allowedAdminAction();

        $questionnaire_assignments = QuestionnaireAssignment::all();

        return $this->showAll($questionnaire_assignments);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'basic_id' => 'required|integer',
            'questionnaire_name_id' => 'required|integer',
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $questionnaire_assignment = QuestionnaireAssignment::create($data);

        return $this->showOne($questionnaire_assignment, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(QuestionnaireAssignment $questionnaireAssignment)
    {
        return $this->showOne($questionnaireAssignment);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(QuestionnaireAssignment $questionnaireAssignment)
    {
        $questionnaireAssignment->delete();

        return $this->showOne($questionnaireAssignment);
    }

    public function assignQuestionnaire(Request $request)
    {
        $requestBody = $request->json()->all();

        $rules = [
            'questionnaire_name_identifier' => 'required|integer|exists:questionnaire_names,id',
            'user_identifiers' => 'required|array',
            'user_identifiers.*.user_identifier' => 'required_with:user_identifiers|integer|exists:users,id'
        ];

        $this->validate($request, $rules);

        foreach ($requestBody['user_identifiers'] as $val) {
            $questionnaire_assignment = new QuestionnaireAssignment;
            $questionnaire_assignment->basic_id = $val['user_identifier'];
            $questionnaire_assignment->questionnaire_name_id = $requestBody['questionnaire_name_identifier'];
            $questionnaire_assignment->answered = 0;
            $questionnaire_assignment->save();
        }

        $questions = new QuestionnaireName;
        $questions = $questions->find($requestBody['questionnaire_name_identifier']);
        $questions->assigned = 1;
        $questions->save();
        $questionnaire_assignments = $questions->questionnaire_assignments;

        return $this->showAll($questionnaire_assignments);
    }
}

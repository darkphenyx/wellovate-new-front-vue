<?php

namespace App\Http\Controllers\Questionnaire;

use App\Enum;
use App\Http\Controllers\ApiController;
use App\Question;
use App\QuestionnaireName;
use App\Title;
use App\Type;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class QuestionnaireNameQuestionnaireQuestionController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        //$this->middleware('transform.input:' . AnthropometricTransformer::class);
        $this->middleware('scope:read-general');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(QuestionnaireName $questionnaireName)
    {
        $questionnaire_answers = $questionnaireName->questionnaire_questions->sortBy('question_id');

        $arr = array();
        $i = 0;
        foreach ($questionnaire_answers as $val) {
            $arr1 = array();
            $arr1['question_identification'] = $val['question_id'];

            $questions = Question::find($val['question_id']);
            $title = Title::find($questions['title_id']);
            $type = Type::find($questions['type_id']);
            //echo $questions['type_id'];
            $arr1['question'] = $questions['question'];
            $arr1['title'] = $title['question_title'];
            $arr1['question_type'] = $type['question_type'];

            if($questions->type_id == 5){
                $question = Question::find($questions->id)->enums;
                $j = 0;
                foreach($question as $val1){
                    $arr1['possible'][$j]['identification'] = $val1['id'];
                    $arr1['possible'][$j]['answer_choice'] = $val1['answer_choice'];
                    $j++;
                }
            }

            $arr[$i] = $arr1;
            $i++;
        }
        return $this->simpleResponse(['data' => $arr], 200);
    }
}

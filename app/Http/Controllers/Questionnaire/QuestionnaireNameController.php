<?php

namespace App\Http\Controllers\Questionnaire;

use App\Http\Controllers\ApiController;
use App\QuestionnaireName;
use App\Transformers\QuestionnaireNameTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class QuestionnaireNameController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . QuestionnaireNameTransformer::class)->only(['store', 'update']);
        $this->middleware('scope:read-general')->only('index');
        $this->middleware('scope:manage-user-data')->only(['store', 'show', 'update']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$this->allowedAdminAction();

        $questionnaire_names = QuestionnaireName::all();

        return $this->showAll($questionnaire_names);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|string',
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $questionnaire_name = QuestionnaireName::create($data);

        return $this->showOne($questionnaire_name, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(QuestionnaireName $questionnaireName)
    {
        return $this->showOne($questionnaireName);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(QuestionnaireName $questionnaireName)
    {
        $this->allowedAdminAction();

        $questionnaireName->delete();

        return $this->showOne($questionnaireName);
    }
}

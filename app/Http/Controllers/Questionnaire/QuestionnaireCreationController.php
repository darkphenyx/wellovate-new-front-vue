<?php

namespace App\Http\Controllers\Questionnaire;

use App\Http\Controllers\ApiController;
use App\QuestionnaireName;
use App\QuestionnaireQuestion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class QuestionnaireCreationController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        //$this->middleware('transform.input:' . AnthropometricTransformer::class);
        $this->middleware('scope:read-general');
    }

    public function createQuestionnaire(Request $request)
    {
        $requestBody = $request->json()->all();

        $rules = [
            'questionnaire_name_identifier' => 'required|integer|exists:questionnaire_names,id',
            'question_identifiers' => 'required|array',
            'question_identifiers.*.question_identifier' => 'required_with:question_identifiers|integer|exists:questions,id'
        ];

        $this->validate($request, $rules);

        foreach ($requestBody['question_identifiers'] as $val) {
            $questionnaire_question = new QuestionnaireQuestion;
            $questionnaire_question->questionnaire_name_id = $requestBody['questionnaire_name_identifier'];
            $questionnaire_question->question_id = $val['question_identifier'];
            $questionnaire_question->save();
        }

        $questions = new QuestionnaireName;
        $questions = $questions->find($requestBody['questionnaire_name_identifier']);
        $questionnaire_questions = $questions->questionnaire_questions;

        return $this->showAll($questionnaire_questions);
    }
}

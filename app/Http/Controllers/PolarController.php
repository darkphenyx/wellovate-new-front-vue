<?php

namespace App\Http\Controllers;

use App\Polar;
use App\Traits\OutsideConnection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class PolarController extends Controller
{
    use OutsideConnection;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('polar');
    }

    public function goAuth()
    {
        $url = env("POLAR_AUTH_URI"). '?response_type=code&client_id='. env("POLAR_CLIENT_ID");
        return Redirect::to($url);
    }

    public function getAuth(Request $request)
    {
        $client_id = env("POLAR_CLIENT_ID", "d4342296-f982-42a0-beb0-ee0f30c4fc0a");
        $client_secret = env("POLAR_CLIENT_SECRET", "da12d1d7-c005-4c0f-ac98-85bf192202a0");
        $request_uri = env("POLAR_TOKEN_REQUEST", 'https://polarremote.com/v2/oauth2/token');
        $code = $request->input('code');

        $token_response = $this->getTokenPolar($request_uri, $client_id, $client_secret, $code);
        $token_response = \GuzzleHttp\json_decode($token_response, true);

        if(array_key_exists('access_token', $token_response)){
            $polar = Polar::firstOrNew(['basic_id' => Auth::user()->id]);
            $polar->access_token = $token_response['access_token'];
            $polar->token_type = $token_response['token_type'];
            $polar->x_user_id = $token_response['x_user_id'];
            $polar->save();
            return view('polar.authorized');
        }else{
            return view('home');
        }
    }
}

<?php

namespace App\Http\Controllers\Login;

use App\Http\Controllers\ApiController;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class LoginController extends ApiController
{
    public function __construct()
    {

    }

    public function login(Request $request)
    {
        $rules = [
            'username' => 'required|email',
            'password' => 'required|min:8'
        ];

        $this->validate($request, $rules);

        if(Auth::attempt(array('email' => $request->username, 'password' => $request->password))) {
            if(Auth::attempt(array('email' => $request->username, 'password' => $request->password, 'verified' => 1))) {
                $user = User::where('email', $request->username)->first();
                $oauth_client = DB::table('oauth_clients')->where('id', 2)->first();
                $secret = $oauth_client->secret;

                $url = env("API_TOKEN_URL", "https://www.wellovate.com/api/oauth/token");
                $fields = array(
                    'grant_type' => 'password',
                    'client_id' => '2',
                    'client_secret' => $secret,
                    'username' => $request->input('username'),
                    'password' => $request->input('password'),
                    'scope' => '*'
                );
                $fields_string = '';
                foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
                rtrim($fields_string, '&');
                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL => $url,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => $fields_string,
                ));

                $response = curl_exec($curl);
                $err = curl_error($curl);

                curl_close($curl);

                $newObj = json_decode($response, true);
                $access = $newObj['access_token'];
                $refresh = $newObj['refresh_token'];
                // dd($refresh);
                // dd($newObj);

                if ($err) {
                    echo "cURL Error #:" . $err;
                } else {
                    return $this->simpleResponse(['token' => $access, 'refresh_token' => $refresh, 'id' => $user->id, 'name' => $user->name, 'sex' => $user->sex, 'age' => $user->age, 'admin' => $user->admin], 200);
                }
            } else {
                return $this->errorResponse('Your user is not verified. Please check your email for the verification email.', 401);
            }
        }else{
            return $this->errorResponse('Your username or password is incorrect.', 401);
        }
    }
}

<?php

namespace App\Http\Controllers\GroupLead;

use App\GroupLead;
use App\Transformers\GroupLeadTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class GroupLeadController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . GroupLeadTransformer::class)->only(['store', 'update']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $group_leads = GroupLead::all();

        return $this->showAll($group_leads);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'coach_id' => 'required|integer',
            'name' => 'required|string'
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $group_lead = GroupLead::create($data);

        return $this->showOne($group_lead, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(GroupLead $group_lead)
    {
        return $this->showOne($group_lead);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(GroupLead $group_lead)
    {
        $group_lead->delete();

        return $this->showOne($group_lead);
    }
}

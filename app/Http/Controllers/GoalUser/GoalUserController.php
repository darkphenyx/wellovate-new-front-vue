<?php

namespace App\Http\Controllers\GoalUser;

use App\GoalUser;
use App\Transformers\GoalUserTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class GoalUserController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . GoalUserTransformer::class)->only(['store', 'update']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->allowedAdminAction();

        $goal_users = GoalUser::all();

        return $this->showAll($goal_users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'basic_id' => 'required|integer',
            'goal_id' => 'required|integer',
            'priority' => 'required|integer|min:1|max:10',
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $data['completed'] = 0;
        $goal_user = GoalUser::create($data);

        return $this->showOne($goal_user, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(GoalUser $goal_user)
    {
        return $this->showOne($goal_user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(GoalUser $goal_user)
    {
        $goal_user->delete();

        return $this->showOne($goal_user);
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: GlaDOS
 * Date: 11/11/2017
 * Time: 12:17 PM
 */
namespace App\Http\Controllers\Auth;

use App\Basic;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\ApiController;

class GameLoginController extends ApiController
{
    public function __construct()
    {
        $this->middleware('client.credentials')->only(['tryLogin']);
    }

    public function tryLogin(Request $request)
    {
        $rules = [
            'email' => 'required|string',
            'password' => 'required|string',
        ];

        $this->validate($request, $rules);

        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            $sendArray = array();
            $user = Auth::user();
            //dd($user);
            $sendArray['id'] = $user['id'];
            $sendArray['name'] = $user['name'];
            $sendArray['age'] = $user['age'];
            $sendArray['sex'] = $user['sex'];
            return $this->simpleResponse(['data' => $sendArray], 200);
        }else{
            return $this->errorResponse('Incorrect Login', 401);
        }
    }
}
<?php

namespace App\Http\Controllers;


use App\Mail\ContactSent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

class WebController extends Controller
{
    public function home(){
        return view('content.home')->with('page','home');
    }

    public function solutions(){
        return view('content.solutions')->with('page','solutions');
    }

    public function solutionsvalue(){
        return view('content.solutionsvalue')->with('page','solutionsvalue');
    }

    /*Odette 10/31/17*/
    public function consulting(){
        return view('content.consulting')->with('page','consulting');
    }/*Odette 10/31/17*/

    public function about(){
        return view('content.about')->with('page','about');
    }
    /*Odette 10/31/17*/
    public function mission(){
        return view('content.mission')->with('page','mission');
    }
    /*Odette 10/31/17*/
    public function whatwedo(){
        return view('content.whatwedo')->with('page','whatwedo');
    }/*Odette 10/31/17*/

    public function perspectives(){
        return view('content.perspectives')->with('page','perspectives');
    }

    public function perspectivesview(){
        return view('content.perspectivesview')->with('page','perspectivesview');
    }

    public function realities(){
        return view('content.realities')->with('page','realities');
    }

    public function contactform(){
        return view('content.contactform')->with('page','contactform');
    }

    public function contact(Request $request){
        $this->validate($request,[
            'name' => 'required|max:60',
            'email' => 'required|email',
            'comment' => 'required'
        ],[
            'name.required' => 'The name field is required',
            'name.max' => 'The name cannot be greater than 60 characters',
        ]);

        Mail::to('contact@wellovate.com')
            ->send(new ContactSent(
            Input::get('name'),
            Input::get('companyName'),
            Input::get('email'),
            Input::get('phone'),
            Input::get('comment')
        ));

        return view('content.contact-success')->with('page','contactform');
    }
}

<?php

namespace App\Http\Controllers\Anthropometric;

use App\Anthropometric;
use App\Transformers\AnthropometricTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class AnthropometricController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . AnthropometricTransformer::class)->only(['store', 'update']);
        $this->middleware('scope:read-general')->only('index');
        $this->middleware('scope:manage-user-data')->only(['store', 'show', 'update']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->allowedAdminAction();

        $anthropometrics = Anthropometric::all();

        return $this->showAll($anthropometrics);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'basic_id' => 'required|integer',
            'bicep_circum' => 'required|numeric|min:4|max:99',
            'biologic_sex' => 'required|in:male,female,intersex',
            'body_fat_percent' => 'required|numeric|min:2|max:100',
            'body_height' => 'required|numeric|min:2|max:300',
            'body_mass_index' => 'required|numeric|min:2|max:99',
            'bone_density' => 'required|numeric|max:99',
            'bust_circum' => 'required|numeric|min:2|max:199',
            'fitzpatrick_skin' => 'required|in:I,II,III,IV,V,VI',
            'height' => 'required|numeric|min:2|max:400',
            'ideal_body_weight' => 'required|numeric|min:2|max:1000',
            'lean_body_weight' => 'required|numeric|min:2|max:1000',
            'body_water' => 'required|numeric|min:2|max:100',
            'thigh_circum' => 'required|numeric|min:2|max:200',
            'waist_circum' => 'required|numeric|min:2|max:200',
            'water_content' => 'required|numeric|min:2|max:100',
            'weight' => 'required|numeric|min:2|max:1000',
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $anthropometric = Anthropometric::create($data);

        return $this->showOne($anthropometric, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param Anthropometric $anthropometric
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function show(Anthropometric $anthropometric)
    {
        return $this->showOne($anthropometric);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Anthropometric $anthropometric
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function destroy(Anthropometric $anthropometric)
    {
        $anthropometric->delete();

        return $this->showOne($anthropometric);
    }
}

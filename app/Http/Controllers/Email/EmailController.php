<?php

namespace App\Http\Controllers\Email;

use App\Http\Controllers\ApiController;
use App\Mail\ContactSent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class EmailController extends ApiController
{
    public function __construct()
    {

    }

    public function contactEmail(Request $request)
    {
        $rules = [
            'name' => 'required|max:60',
            'email' => 'required|email',
            'comment' => 'required',
            'route' => 'required'
        ];

        $this->validate($request, $rules);

        $name = $request->input('name');
        $route = $request->input('route');
        $companyName = $request->input('companyName');
        $email = $request->input('email');
        $phone = $request->input('phone');
        $comment = $request->input('comment');

        Mail::to('contact@wellovate.com')
            ->send(new ContactSent(
                $name,
                $companyName,
                $email,
                $phone,
                $comment,
                $route
            ));

        return $this->showMessage('Email Successfully Sent', 200);
    }
}

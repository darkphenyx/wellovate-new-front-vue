<?php

namespace App\Http\Controllers\Fitness;

use App\Fitness;
use App\Transformers\FitnessTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class FitnessController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . FitnessTransformer::class)->only(['store', 'update']);
        $this->middleware('scope:read-general')->only('index');
        $this->middleware('scope:manage-user-data')->only(['store', 'show', 'update']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->allowedAdminAction();

        $fitnesses = Fitness::all();

        return $this->showAll($fitnesses);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'basic_id' => 'required|integer',
            'o2_consumption' => 'required|numeric|min:5|max:4000',
            'o2_consumption_max' => 'required|numeric|min:5|max:4000',
            'heart_rate_recovery' => 'required|numeric|min:10|max:120',
            'basal_metabolic' => 'required|numeric|min:100|max:3500',
            'resting_energy' => 'required|numeric|min:400|max:2200',
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $fitness = Fitness::create($data);

        return $this->showOne($fitness, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Fitness $fitness)
    {
        return $this->showOne($fitness);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Fitness $fitness)
    {
        $fitness->delete();

        return $this->showOne($fitness);
    }
}

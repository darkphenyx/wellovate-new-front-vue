<?php

namespace App\Http\Controllers\Enum;

use App\Enum;
use App\Transformers\EnumTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class EnumController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . EnumTransformer::class)->only(['store', 'update']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $enums = Enum::all();

        return $this->showAll($enums);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->allowedAdminAction();

        $rules = [
            'question_id' => 'required|integer',
            'answer_choice' => 'required|string'
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $enum = Enum::create($data);

        return $this->showOne($enum, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Enum $enum)
    {
        return $this->showOne($enum);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Enum $enum)
    {
        $this->allowedAdminAction();

        $enum->delete();

        return $this->showOne($enum);
    }
}

<?php

namespace App\Http\Controllers\PhysicalActivityBarriers;

use App\PhysicalActivityBarriers;
use App\Transformers\PhysicalActivityBarriersTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class PhysicalActivityBarriersController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . PhysicalActivityBarriersTransformer::class)->only(['store', 'update']);
        $this->middleware('scope:read-general')->only('index');
        $this->middleware('scope:manage-user-data')->only(['store', 'show', 'update']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->allowedAdminAction();

        $physical_activity_barriers = PhysicalActivityBarriers::all();

        return $this->showAll($physical_activity_barriers);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'basic_id' => 'required|integer|unique:physical_activity_barriers,basic_id',
            'lack_someone' => 'required|in:never,almost never,sometimes,often,very often',
            'lack_of_time' => 'required|in:never,almost never,sometimes,often,very often',
            'family_obligation' => 'required|in:never,almost never,sometimes,often,very often',
            'lack_energy' => 'required|in:never,almost never,sometimes,often,very often',
            'self_conscious' => 'required|in:never,almost never,sometimes,often,very often',
            'lack_equipment' => 'required|in:never,almost never,sometimes,often,very often',
            'lack_place' => 'required|in:never,almost never,sometimes,often,very often',
            'fear_injured' => 'required|in:never,almost never,sometimes,often,very often',
            'stressed_out' => 'required|in:never,almost never,sometimes,often,very often',
            'discouraged' => 'required|in:never,almost never,sometimes,often,very often',
            'not_healthy_enough' => 'required|in:never,almost never,sometimes,often,very often',
            'find_boring' => 'required|in:never,almost never,sometimes,often,very often',
            'lack_knowledge' => 'required|in:never,almost never,sometimes,often,very often',
            'lack_skills' => 'required|in:never,almost never,sometimes,often,very often',
            'lack_motivation' => 'required|in:never,almost never,sometimes,often,very often',
            'pain' => 'required|in:never,almost never,sometimes,often,very often',
            'bad_weather' => 'required|in:never,almost never,sometimes,often,very often',
            'other' => 'string|max:1000|nullable'
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $physical_activity_barrier = PhysicalActivityBarriers::create($data);

        return $this->showOne($physical_activity_barrier, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(PhysicalActivityBarriers $physical_activity_barrier)
    {
        return $this->showOne($physical_activity_barrier);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PhysicalActivityBarriers $physical_activity_barrier)
    {
        $rules = [
            'lack_someone' => 'in:never,almost never,sometimes,often,very often',
            'lack_of_time' => 'in:never,almost never,sometimes,often,very often',
            'family_obligation' => 'in:never,almost never,sometimes,often,very often',
            'lack_energy' => 'in:never,almost never,sometimes,often,very often',
            'self_conscious' => 'in:never,almost never,sometimes,often,very often',
            'lack_equipment' => 'in:never,almost never,sometimes,often,very often',
            'lack_place' => 'in:never,almost never,sometimes,often,very often',
            'fear_injured' => 'in:never,almost never,sometimes,often,very often',
            'stressed_out' => 'in:never,almost never,sometimes,often,very often',
            'discouraged' => 'in:never,almost never,sometimes,often,very often',
            'not_healthy_enough' => 'in:never,almost never,sometimes,often,very often',
            'find_boring' => 'in:never,almost never,sometimes,often,very often',
            'lack_knowledge' => 'in:never,almost never,sometimes,often,very often',
            'lack_skills' => 'in:never,almost never,sometimes,often,very often',
            'lack_motivation' => 'in:never,almost never,sometimes,often,very often',
            'pain' => 'in:never,almost never,sometimes,often,very often',
            'bad_weather' => 'in:never,almost never,sometimes,often,very often',
            'other' => 'string|max:1000|nullable'
        ];

        if($request->has('lack_someone')){
            $physical_activity_barrier->lack_someone = $request->lack_someone;
        }

        if($request->has('lack_of_time')){
            $physical_activity_barrier->lack_of_time = $request->lack_of_time;
        }

        if($request->has('family_obligation')){
            $physical_activity_barrier->family_obligation = $request->family_obligation;
        }

        if($request->has('lack_energy')){
            $physical_activity_barrier->lack_energy = $request->lack_energy;
        }

        if($request->has('self_conscious')){
            $physical_activity_barrier->self_conscious = $request->self_conscious;
        }

        if($request->has('lack_equipment')){
            $physical_activity_barrier->lack_equipment = $request->lack_equipment;
        }

        if($request->has('lack_place')){
            $physical_activity_barrier->lack_place = $request->lack_place;
        }

        if($request->has('fear_injured')){
            $physical_activity_barrier->fear_injured = $request->fear_injured;
        }

        if($request->has('stressed_out')){
            $physical_activity_barrier->stressed_out = $request->stressed_out;
        }

        if($request->has('discouraged')){
            $physical_activity_barrier->discouraged = $request->discouraged;
        }

        if($request->has('not_healthy_enough')){
            $physical_activity_barrier->not_healthy_enough = $request->not_healthy_enough;
        }

        if($request->has('find_boring')){
            $physical_activity_barrier->find_boring = $request->find_boring;
        }

        if($request->has('lack_knowledge')){
            $physical_activity_barrier->lack_knowledge = $request->lack_knowledge;
        }

        if($request->has('lack_skills')){
            $physical_activity_barrier->lack_skills = $request->lack_skills;
        }

        if($request->has('lack_motivation')){
            $physical_activity_barrier->lack_motivation = $request->lack_motivation;
        }

        if($request->has('pain')){
            $physical_activity_barrier->pain = $request->pain;
        }

        if($request->has('bad_weather')){
            $physical_activity_barrier->bad_weather = $request->bad_weather;
        }

        if($request->has('other')){
            $physical_activity_barrier->other = $request->other;
        }

        if(!$physical_activity_barrier->isDirty()){
            return $this->errorResponse('You need to specify a different value to update', 422);
        }

        $physical_activity_barrier->save();

        return $this->showOne($physical_activity_barrier);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(PhysicalActivityBarriers $physical_activity_barrier)
    {
        $physical_activity_barrier->delete();

        return $this->showOne($physical_activity_barrier);
    }
}

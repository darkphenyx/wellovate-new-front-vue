<?php

namespace App\Http\Controllers\PhysicalActivityFun;

use App\PhysicalActivityFun;
use App\Transformers\PhysicalActivityFunTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class PhysicalActivityFunController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . PhysicalActivityFunTransformer::class)->only(['store', 'update']);
        $this->middleware('scope:read-general')->only('index');
        $this->middleware('scope:manage-user-data')->only(['store', 'show', 'update']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->allowedAdminAction();

        $physical_activity_fun = PhysicalActivityFun::all();

        return $this->showAll($physical_activity_fun);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'basic_id' => 'required|integer|unique:physical_activity_funs,basic_id',
            'enjoy_exercise' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'feel_good' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'fun_exercising' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'watching_tv' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'enjoy_computer' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'enjoy_reading' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $physical_activity_fun = PhysicalActivityFun::create($data);

        return $this->showOne($physical_activity_fun, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(PhysicalActivityFun $physical_activity_fun)
    {
        return $this->showOne($physical_activity_fun);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PhysicalActivityFun $physical_activity_fun)
    {
        $rules = [
            'enjoy_exercise' => 'in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'feel_good' => 'in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'fun_exercising' => 'in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'watching_tv' => 'in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'enjoy_computer' => 'in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'enjoy_reading' => 'in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
        ];

        if($request->has('enjoy_exercise')){
            $physical_activity_fun->enjoy_exercise = $request->enjoy_exercise;
        }

        if($request->has('feel_good')){
            $physical_activity_fun->feel_good = $request->feel_good;
        }

        if($request->has('fun_exercising')){
            $physical_activity_fun->fun_exercising = $request->fun_exercising;
        }

        if($request->has('watching_tv')){
            $physical_activity_fun->watching_tv = $request->watching_tv;
        }

        if($request->has('enjoy_computer')){
            $physical_activity_fun->enjoy_computer = $request->enjoy_computer;
        }

        if($request->has('enjoy_reading')){
            $physical_activity_fun->enjoy_reading = $request->enjoy_reading;
        }

        $physical_activity_fun->save();

        return $this->showOne($physical_activity_fun);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(PhysicalActivityFun $physical_activity_fun)
    {
        $physical_activity_fun->delete();

        return $this->showOne($physical_activity_fun);
    }
}

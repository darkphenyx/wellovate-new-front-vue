<?php

namespace App\Http\Controllers\Question;

use App\Http\Controllers\ApiController;
use App\Question;
use App\Transformers\QuestionTransformer;
use Illuminate\Http\Request;

class QuestionController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . QuestionTransformer::class)->only(['store', 'update']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions = Question::all();

        return $this->showAll($questions);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->allowedAdminAction();

        $rules = [
            'title_id' => 'required|integer',
            'type_id' => 'required|integer',
            'question' => 'required|string'
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $question = Question::create($data);

        return $this->showOne($question, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Question $question)
    {
        return $this->showOne($question);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Question $question)
    {
        $this->allowedAdminAction();

        $question->delete();

        return $this->showOne($question);
    }
}

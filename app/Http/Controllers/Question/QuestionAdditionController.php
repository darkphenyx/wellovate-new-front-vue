<?php

namespace App\Http\Controllers\Question;

use App\Enum;
use App\Http\Controllers\ApiController;
use App\Question;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class QuestionAdditionController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        //$this->middleware('transform.input:' . AnthropometricTransformer::class);
        $this->middleware('scope:read-general');
    }

    public function addQuestion(Request $request)
    {
        $this->allowedAdminAction();

        $requestBody = $request->json()->all();

        if($requestBody['type_identifier'] == 5){
            $rules = [
                'title_identifier' => 'required|integer',
                'type_identifier' => 'required|integer',
                'question' => 'required|string',
                'answers' => 'required|array',
                'answers.*.available_answer' => 'required_with:answers|string'
            ];
        }else{
            $rules = [
                'title_identifier' => 'required|integer',
                'type_identifier' => 'required|integer',
                'question' => 'required|string',
                'answers' => 'array',
                'answers.*.available_answer' => 'required_with:answers|string'
            ];
        }

        $this->validate($request, $rules);

        $question = new Question;

        $question->title_id = $requestBody['title_identifier'];

        $question->type_id = $requestBody['type_identifier'];

        $question->question = $requestBody['question'];

        $question->save();

        /*
         *  I can't tell which is the best way of checking the if condition.
         *  The $requestBody['type_identifier'] == 5 way checks for only enums
         *
         *  The array_key_exists("answers", $requestBody) condition will let any question have a secondary string
         *  As well as store enum answers. If we decide to change the two methods are listed here
         * */

        if($requestBody['type_identifier'] == 5){
            foreach($requestBody['answers'] as $val){
                $enum = new Enum;

                $enum->question_id = $question->id;

                $enum->answer_choice = $val['available_answer'];

                $enum->save();
            }
        }

        if($question->type_id == 5){
            /*$questions = DB::table('questions')
                ->join('enums', 'questions.id', '=', 'enums.question_id')
                ->where('id', '$question->id')
                ->get();*/
            $questions = Question::find($question->id)->enums;

            //dd($questions);

            $sendArray = array();

            $sendArray['identifier'] = $question->id;
            $sendArray['title_identifier'] = $question->title_id;
            $sendArray['type_identifier'] = $question->type_id;
            $sendArray['question'] = $question->question;

            $answerArray = array();
            $i = 0;

            foreach($questions as $q){
                $answerArray['identifier'] = $q->id;
                $answerArray['quest_identifier'] = $q->question_id;
                $answerArray['available_answer'] = $q->answer_choice;
                $sendArray['answer'][$i] = $answerArray;
                $i++;
            }

            return $this->simpleResponse(['data' => $sendArray], 200);
        }else{
            return $this->showOne($question);
        }
    }
}

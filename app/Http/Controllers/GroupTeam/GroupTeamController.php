<?php

namespace App\Http\Controllers\GroupTeam;

use App\GroupTeam;
use App\Transformers\GroupTeamTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class GroupTeamController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . GroupTeamTransformer::class)->only(['store', 'update']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $group_teams = GroupTeam::all();

        return $this->showAll($group_teams);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'basic_id' => 'required|integer',
            'group_lead_id' => 'required|integer',
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $group_team = GroupTeam::create($data);

        return $this->showOne($group_team, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(GroupTeam $group_team)
    {
        return $this->showOne($group_team);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(GroupTeam $group_team)
    {
        $group_team->delete();

        return $this->showOne($group_team);
    }
}

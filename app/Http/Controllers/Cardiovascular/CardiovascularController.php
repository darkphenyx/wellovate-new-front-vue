<?php

namespace App\Http\Controllers\Cardiovascular;

use App\Cardiovascular;
use App\Transformers\CardiovascularTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class CardiovascularController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . CardiovascularTransformer::class)->only(['store', 'update']);
        $this->middleware('scope:read-general')->only('index');
        $this->middleware('scope:manage-user-data')->only(['store', 'show', 'update']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->allowedAdminAction();

        $cardiovasculars = Cardiovascular::all();

        return $this->showAll($cardiovasculars);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'basic_id' => 'required|integer',
            'ankle_brachial_index' => 'required|numeric|min:20|max:130',
            'blood_pressure' => 'required|alpha_num',
            'diastolic_blood_pressure' => 'required|integer|min:40|max:140',
            'systolic_blood_pressure' => 'required|integer|min:60|max:220',
            'endothelial' => 'required|numeric|min:22|max:99',
            'reactive_hyperenia' => 'required|numeric|min:22|max:99',
            'heart_rate' => 'required|integer|min:60|max:220',
            'max_heart_rate' => 'required|integer|min:60|max:220',
            'pulse_wave_velocity' => 'required|numeric|min:100|max:230',
            'qtc' => 'required|numeric|min:40|max:260',
            'rr_interval' => 'required|numeric|min:40|max:260',
            'o2_saturation' => 'required|numeric|min:15|max:99',
            'pulse_oximetry' => 'required|numeric|min:15|max:99',
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $cardiovascular = Cardiovascular::create($data);

        return $this->showOne($cardiovascular, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param Cardiovascular $cardio
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function show(Cardiovascular $cardiovascular)
    {
        return $this->showOne($cardiovascular);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Cardiovascular $cardiovascular
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function destroy(Cardiovascular $cardiovascular)
    {
        $cardiovascular->delete();

        return $this->showOne($cardiovascular);
    }
}

<?php

namespace App\Http\Controllers\Basic;

use App\Basic;
use App\Fitness;
use App\Transformers\FitnessTransformer;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class BasicFitnessController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . FitnessTransformer::class)->only(['store']);
        $this->middleware('scope:read-general')->only('index');
        $this->middleware('scope:manage-user-data')->only(['store', 'update']);
        $this->middleware('can:view,basic')->only(['index']);
        $this->middleware('can:make,basic')->only(['store', 'update']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * @SWG\Get(
     *     path="/basics/{basic}/fitness",
     *     summary="Get fitness data for user",
     *     description="Returns all fitness data",
     *     operationId="index",
     *     tags={"basic_user"},
     *     produces={"application/json"},
     *     @SWG\Parameter(description="ID of basic_user",in="path",name="basic",required=true,type="integer",format="int64"),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(
     *              type="array",
     *              @SWG\Items(
     *                  type="object",
     *                  @SWG\Property(property="identifier", type="integer"),
     *                  @SWG\Property(property="user_identifier", type="integer"),
     *                  @SWG\Property(property="oxygen_consumption", type="number"),
     *                  @SWG\Property(property="oxygen_consumption_max", type="number"),
     *                  @SWG\Property(property="max_heart_rate_to_baseline_time", type="number"),
     *                  @SWG\Property(property="basal_metabolic_rate", type="number"),
     *                  @SWG\Property(property="resting_energy", type="number"),
     *                  @SWG\Property(property="creation_date", type="string"),
     *                  @SWG\Property(property="last_changed", type="string"),
     *                  @SWG\Property(property="deleted_date", type="string")
     *              )
     *         )
     *     ),
     *     @SWG\Response(response="400",description="Invalid ID supplied"),
     *     @SWG\Response(response="404",description="Data not found"),
     *     security={{"oauth2_security": {"read-general", "manage-user-data"}}}
     * )
     */
    public function index(Basic $basic)
    {
        $fitnesses = $basic->fitness;

        return $this->showAll($fitnesses);
    }


    /**
     * @SWG\Post(
     *   path="/basics/{basic}/fitness",
     *   tags={"basic_user"},
     *   summary="Creates new fitness record for user",
     *   description="",
     *   operationId="store",
     *   consumes={"application/x-www-form-urlencoded"},
     *   produces={"application/json"},
     *   @SWG\Parameter(name="basic", in="path", description="ID of basic_user",required=true,type="integer", format="int64"),
     *   @SWG\Parameter(name="oxygen_consumption", in="formData", description="", required=true, type="number"),
     *   @SWG\Parameter(name="oxygen_consumption_max", in="formData", description="", required=true, type="number"),
     *   @SWG\Parameter(name="max_heart_rate_to_baseline_time", in="formData", description="", required=true, type="number"),
     *   @SWG\Parameter(name="basal_metabolic_rate", in="formData", description="", required=true, type="number"),
     *   @SWG\Parameter(name="resting_energy", in="formData", description="", required=true, type="number"),
     *   @SWG\Response(response="405",description="Invalid input"),
     *   security={{
     *     "oauth2_security": {"read-general", "manage-user-data"}
     *   }}
     * )
     */
    public function store(Request $request, Basic $basic)
    {
        $rules = [
            'o2_consumption' => 'required|numeric|min:5|max:4000',
            'o2_consumption_max' => 'required|numeric|min:5|max:4000',
            'heart_rate_recovery' => 'required|numeric|min:10|max:120',
            'basal_metabolic' => 'required|numeric|min:100|max:3500',
            'resting_energy' => 'required|numeric|min:400|max:2200',
        ];

        $this->validate($request, $rules);

        if(!$basic->isVerified()) {
            return $this->errorResponse('The client must be a verified user.', 409);
        }

        $data = $request->all();
        $data['basic_id'] = $basic->id;

        $fitness = Fitness::create($data);

        return $this->showOne($fitness);
    }
}

<?php

namespace App\Http\Controllers\Basic;

use App\Basic;
use App\PhysicalActivityFun;
use App\Transformers\PhysicalActivityFunTransformer;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class BasicPhysicalActivityFunController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . PhysicalActivityFunTransformer::class)->only(['store', 'update']);
        $this->middleware('scope:read-general')->only('index');
        $this->middleware('scope:manage-user-data')->only(['store', 'update']);
        $this->middleware('can:view,basic')->only(['index']);
        $this->middleware('can:make,basic')->only(['store', 'update']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * @SWG\Get(
     *     path="/basics/{basic}/physical_activity_fun",
     *     summary="Get physical_activity_fun data for user",
     *     description="Returns all physical_activity_fun data",
     *     operationId="index",
     *     tags={"basic_user"},
     *     produces={"application/json"},
     *     @SWG\Parameter(description="ID of basic_user",in="path",name="basic",required=true,type="integer",format="int64"),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(
     *              type="array",
     *              @SWG\Items(
     *                  type="object",
     *                  @SWG\Property(property="identifier", type="integer"),
     *                  @SWG\Property(property="user_identifier", type="integer"),
     *                  @SWG\Property(property="enjoy_doing_exercise", type="integer"),
     *                  @SWG\Property(property="feel_physically_good_while_exercising", type="integer"),
     *                  @SWG\Property(property="have_fun_exercising", type="integer"),
     *                  @SWG\Property(property="enjoy_sitting_watching_tv", type="integer"),
     *                  @SWG\Property(property="enjoy_using_computer", type="integer"),
     *                  @SWG\Property(property="enjoy_sitting_and_reading", type="integer"),
     *
     *                  @SWG\Property(property="creation_date", type="string"),
     *                  @SWG\Property(property="last_changed", type="string"),
     *                  @SWG\Property(property="deleted_date", type="string")
     *              )
     *         )
     *     ),
     *     @SWG\Response(response="400",description="Invalid ID supplied"),
     *     @SWG\Response(response="404",description="Data not found"),
     *     security={{"oauth2_security": {"read-general", "manage-user-data"}}}
     * )
     */
    public function index(Basic $basic)
    {
        $physical_activity_funs = $basic->physical_activity_fun;

        return $this->showAll($physical_activity_funs);
    }

    /**
     * @SWG\Post(
     *   path="/basics/{basic}/physical_activity_fun",
     *   tags={"basic_user"},
     *   summary="Creates new physical_activity_fun record for user",
     *   description="",
     *   operationId="store",
     *   consumes={"application/x-www-form-urlencoded"},
     *   produces={"application/json"},
     *   @SWG\Parameter(name="basic", in="path", description="ID of basic_user",required=true,type="integer", format="int64"),
     *   @SWG\Parameter(name="enjoy_doing_exercise", in="formData", description="", required=true, type="string", enum={"strongly disagree","somewhat disagree","neutral","somewhat agree","strongly agree"}),
     *   @SWG\Parameter(name="feel_physically_good_while_exercising", in="formData", description="", required=true, type="string", enum={"strongly disagree","somewhat disagree","neutral","somewhat agree","strongly agree"}),
     *   @SWG\Parameter(name="have_fun_exercising", in="formData", description="", required=true, type="string", enum={"strongly disagree","somewhat disagree","neutral","somewhat agree","strongly agree"}),
     *   @SWG\Parameter(name="enjoy_sitting_watching_tv", in="formData", description="", required=true, type="string", enum={"strongly disagree","somewhat disagree","neutral","somewhat agree","strongly agree"}),
     *   @SWG\Parameter(name="enjoy_using_computer", in="formData", description="", required=true, type="string", enum={"strongly disagree","somewhat disagree","neutral","somewhat agree","strongly agree"}),
     *   @SWG\Parameter(name="enjoy_sitting_and_reading", in="formData", description="", required=true, type="string", enum={"strongly disagree","somewhat disagree","neutral","somewhat agree","strongly agree"}),
     *
     *   @SWG\Response(response="405",description="Invalid input"),
     *   security={{
     *     "oauth2_security": {"read-general", "manage-user-data"}
     *   }}
     * )
     */
    public function store(Request $request, Basic $basic)
    {
        $rules = [
            'enjoy_exercise' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'feel_good' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'fun_exercising' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'watching_tv' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'enjoy_computer' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'enjoy_reading' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
        ];

        $this->validate($request, $rules);

        if(!$basic->isVerified()) {
            return $this->errorResponse('The client must be a verified user.', 409);
        }

        $data = $request->all();
        $data['basic_id'] = $basic->id;

        $physical_activity_fun = PhysicalActivityFun::create($data);

        return $this->showOne($physical_activity_fun);
    }
}

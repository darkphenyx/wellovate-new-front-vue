<?php

namespace App\Http\Controllers\Basic;

use App\Basic;
use App\InactivityAssessment;
use App\Transformers\InactivityAssessmentTransformer;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class BasicInactivityController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . InactivityAssessmentTransformer::class)->only(['store']);
        $this->middleware('scope:read-general')->only('index');
        $this->middleware('scope:manage-user-data')->only(['store', 'update']);
        $this->middleware('can:view,basic')->only(['index']);
        $this->middleware('can:make,basic')->only(['store', 'update']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * @SWG\Get(
     *     path="/basics/{basic}/inactivity",
     *     summary="Get inactivity data for user",
     *     description="Returns all inactivity data",
     *     operationId="index",
     *     tags={"basic_user"},
     *     produces={"application/json"},
     *     @SWG\Parameter(description="ID of basic_user",in="path",name="basic",required=true,type="integer",format="int64"),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(
     *              type="array",
     *              @SWG\Items(
     *                  type="object",
     *                  @SWG\Property(property="identifier", type="integer"),
     *                  @SWG\Property(property="user_identifier", type="integer"),
     *                  @SWG\Property(property="weekday_tv_watching", type="string"),
     *                  @SWG\Property(property="weekday_time_sitting", type="string"),
     *                  @SWG\Property(property="weekend_tv_watching", type="string"),
     *                  @SWG\Property(property="weekend_time_sitting", type="string"),
     *                  @SWG\Property(property="creation_date", type="string"),
     *                  @SWG\Property(property="last_changed", type="string"),
     *                  @SWG\Property(property="deleted_date", type="string")
     *              )
     *         )
     *     ),
     *     @SWG\Response(response="400",description="Invalid ID supplied"),
     *     @SWG\Response(response="404",description="Data not found"),
     *     security={{"oauth2_security": {"read-general", "manage-user-data"}}}
     * )
     */
    public function index(Basic $basic)
    {
        $inactivity = $basic->inactivity;

        return $this->showOne($inactivity);
    }

    /**
     * @param Request $request
     * @param User $basic
     * @return \Illuminate\Http\JsonResponse
     */

    /**
     * @SWG\Post(
     *   path="/basics/{basic}/inactivity",
     *   tags={"basic_user"},
     *   summary="Creates new inactivity record for user",
     *   description="",
     *   operationId="store",
     *   consumes={"application/x-www-form-urlencoded"},
     *   produces={"application/json"},
     *   @SWG\Parameter(name="basic", in="path", description="ID of basic_user",required=true,type="integer", format="int64"),
     *   @SWG\Parameter(name="weekday_tv_watching", in="formData", description="", required=true, type="string", enum={"Less than 30 minutes","Between 30 minutes and 1 hour","Between 1 and 2 hours","Between 2 and 3 hours","Between 3 and 4 hours","Between 4 and 5 hours","Greater than 5 hours"}),
     *   @SWG\Parameter(name="weekday_time_sitting", in="formData", description="", required=true, type="string", enum={"Less than 30 minutes","Between 30 minutes and 1 hour","Between 1 and 2 hours","Between 2 and 3 hours","Between 3 and 4 hours","Between 4 and 5 hours","Greater than 5 hours"}),
     *   @SWG\Parameter(name="weekend_tv_watching", in="formData", description="", required=true, type="string", enum={"Less than 30 minutes","Between 30 minutes and 1 hour","Between 1 and 2 hours","Between 2 and 3 hours","Between 3 and 4 hours","Between 4 and 5 hours","Greater than 5 hours"}),
     *   @SWG\Parameter(name="weekend_time_sitting", in="formData", description="", required=true, type="string", enum={"Less than 30 minutes","Between 30 minutes and 1 hour","Between 1 and 2 hours","Between 2 and 3 hours","Between 3 and 4 hours","Between 4 and 5 hours","Greater than 5 hours"}),
     *   @SWG\Response(response="405",description="Invalid input"),
     *   security={{
     *     "oauth2_security": {"read-general", "manage-user-data"}
     *   }}
     * )
     */
    public function store(Request $request, Basic $basic)
    {
        $rules = [
            'time_watch_tv_weekday' => 'required|in:Less than 30 minutes,Between 30 minutes and 1 hour,Between 1 and 2 hours,Between 2 and 3 hours,Between 3 and 4 hours,Between 4 and 5 hours,Greater than 5 hours',
            'time_sitting_weekday' => 'required|in:Less than 30 minutes,Between 30 minutes and 1 hour,Between 1 and 2 hours,Between 2 and 3 hours,Between 3 and 4 hours,Between 4 and 5 hours,Greater than 5 hours',
            'time_watch_tv_weekend' => 'required|in:Less than 30 minutes,Between 30 minutes and 1 hour,Between 1 and 2 hours,Between 2 and 3 hours,Between 3 and 4 hours,Between 4 and 5 hours,Greater than 5 hours',
            'time_sitting_weekend' => 'required|in:Less than 30 minutes,Between 30 minutes and 1 hour,Between 1 and 2 hours,Between 2 and 3 hours,Between 3 and 4 hours,Between 4 and 5 hours,Greater than 5 hours',
        ];

        $this->validate($request, $rules);

        if(!$basic->isVerified()) {
            return $this->errorResponse('The client must be a verified user.', 409);
        }

        $data = $request->all();
        $data['basic_id'] = $basic->id;

        $inactivity = InactivityAssessment::create($data);

        return $this->showOne($inactivity);
    }
}

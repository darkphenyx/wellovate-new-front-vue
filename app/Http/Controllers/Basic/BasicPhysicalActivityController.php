<?php

namespace App\Http\Controllers\Basic;

use App\Basic;
use App\PhysicalActivity;
use App\Transformers\PhysicalActivityTransformer;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class BasicPhysicalActivityController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . PhysicalActivityTransformer::class)->only(['store', 'update']);
        $this->middleware('scope:read-general')->only('index');
        $this->middleware('scope:manage-user-data')->only(['store', 'update']);
        $this->middleware('can:view,basic')->only(['index']);
        $this->middleware('can:make,basic')->only(['store', 'update']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * @SWG\Get(
     *     path="/basics/{basic}/physical_activity",
     *     summary="Get physical_activity data for user",
     *     description="Returns all physical_activity data",
     *     operationId="index",
     *     tags={"basic_user"},
     *     produces={"application/json"},
     *     @SWG\Parameter(description="ID of basic_user",in="path",name="basic",required=true,type="integer",format="int64"),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(
     *              type="array",
     *              @SWG\Items(
     *                  type="object",
     *                  @SWG\Property(property="identifier", type="integer"),
     *                  @SWG\Property(property="user_identifier", type="integer"),
     *                  @SWG\Property(property="minutes_active_per_24hr", type="number"),
     *                  @SWG\Property(property="steps_per_minute", type="number"),
     *                  @SWG\Property(property="calories_burned_per_24hr", type="number"),
     *                  @SWG\Property(property="distance_traveled", type="number"),
     *                  @SWG\Property(property="minutes_exercised", type="number"),
     *                  @SWG\Property(property="number_of_floors_climbed", type="integer"),
     *                  @SWG\Property(property="minutes_inactive", type="number"),
     *                  @SWG\Property(property="minutes_of_moderate_vigorous_exercise", type="number"),
     *                  @SWG\Property(property="peak_acceleration", type="number"),
     *                  @SWG\Property(property="minutes_of_sitting_time_per_24hr", type="number"),
     *                  @SWG\Property(property="speed_in_meters_per_second", type="number"),
     *                  @SWG\Property(property="step_count_per_24hr", type="integer"),
     *
     *                  @SWG\Property(property="creation_date", type="string"),
     *                  @SWG\Property(property="last_changed", type="string"),
     *                  @SWG\Property(property="deleted_date", type="string")
     *              )
     *         )
     *     ),
     *     @SWG\Response(response="400",description="Invalid ID supplied"),
     *     @SWG\Response(response="404",description="Data not found"),
     *     security={{"oauth2_security": {"read-general", "manage-user-data"}}}
     * )
     */
    public function index(Basic $basic)
    {
        $physical_activitys = $basic->physical_activity;

        return $this->showAll($physical_activitys);
    }


    /**
     * @SWG\Post(
     *   path="/basics/{basic}/physical_activity",
     *   tags={"basic_user"},
     *   summary="Creates new physical_activity record for user",
     *   description="",
     *   operationId="store",
     *   consumes={"application/x-www-form-urlencoded"},
     *   produces={"application/json"},
     *   @SWG\Parameter(name="basic", in="path", description="ID of basic_user",required=true,type="integer", format="int64"),
     *   @SWG\Parameter(name="minutes_active_per_24hr", in="formData", description="", required=true, type="number"),
     *   @SWG\Parameter(name="steps_per_minute", in="formData", description="", required=true, type="number"),
     *   @SWG\Parameter(name="calories_burned_per_24hr", in="formData", description="", required=true, type="number"),
     *   @SWG\Parameter(name="distance_traveled", in="formData", description="", required=true, type="number"),
     *   @SWG\Parameter(name="minutes_exercised", in="formData", description="", required=true, type="number"),
     *   @SWG\Parameter(name="number_of_floors_climbed", in="formData", description="", required=true, type="integer"),
     *   @SWG\Parameter(name="minutes_inactive", in="formData", description="", required=true, type="number"),
     *   @SWG\Parameter(name="minutes_of_moderate_vigorous_exercise", in="formData", description="", required=true, type="number"),
     *   @SWG\Parameter(name="peak_acceleration", in="formData", description="", required=true, type="number"),
     *   @SWG\Parameter(name="minutes_of_sitting_time_per_24hr", in="formData", description="", required=true, type="number"),
     *   @SWG\Parameter(name="speed_in_meters_per_second", in="formData", description="", required=true, type="number"),
     *   @SWG\Parameter(name="step_count_per_24hr", in="formData", description="", required=true, type="integer"),
     *
     *   @SWG\Response(response="405",description="Invalid input"),
     *   security={{
     *     "oauth2_security": {"read-general", "manage-user-data"}
     *   }}
     * )
     */
    public function store(Request $request, Basic $basic)
    {
        $rules = [
            'active_minutes' => 'required|numeric|min:5|max:999',
            'cadence' => 'required|numeric|min:20|max:1500',
            'calories_burned' => 'required|numeric|min:20|max:1500',
            'distance' => 'required|numeric|min:20|max:450',
            'exercise_minutes' => 'required|numeric|min:20|max:330',
            'floors_climbed' => 'required|integer|min:0|max:100',
            'inactivity' => 'required|numeric|min:20|max:2000',
            'moderate_vigorous' => 'required|numeric|min:5|max:999',
            'peak_accel' => 'required|numeric|min:5|max:999',
            'sitting_time' => 'required|numeric|min:5|max:1500',
            'speed' => 'required|numeric|min:5|max:500',
            'step_count' => 'required|integer',
        ];

        $this->validate($request, $rules);

        if(!$basic->isVerified()) {
            return $this->errorResponse('The client must be a verified user.', 409);
        }

        $data = $request->all();
        $data['basic_id'] = $basic->id;

        $physical_activity = PhysicalActivity::create($data);

        return $this->showOne($physical_activity);
    }
}

<?php

namespace App\Http\Controllers\Basic;

use App\Basic;
use App\Outcome;
use App\Transformers\OutcomeTransformer;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class BasicOutcomeController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . OutcomeTransformer::class)->only(['store', 'update']);
        $this->middleware('scope:read-general')->only('index');
        $this->middleware('scope:manage-user-data')->only(['store', 'update']);
        $this->middleware('can:view,basic')->only(['index']);
        $this->middleware('can:make,basic')->only(['store', 'update']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * @SWG\Get(
     *     path="/basics/{basic}/outcomes",
     *     summary="Get outcome data for user",
     *     description="Returns all outcome data",
     *     operationId="index",
     *     tags={"basic_user"},
     *     produces={"application/json"},
     *     @SWG\Parameter(description="ID of basic_user",in="path",name="basic",required=true,type="integer",format="int64"),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(
     *              type="array",
     *              @SWG\Items(
     *                  type="object",
     *                  @SWG\Property(property="identifier", type="integer"),
     *                  @SWG\Property(property="user_identifier", type="integer"),
     *                  @SWG\Property(property="has_alzheimers", type="boolean"),
     *                  @SWG\Property(property="general_anxiety_disorder", type="boolean"),
     *                  @SWG\Property(property="has_cardiovascular_disease", type="boolean"),
     *                  @SWG\Property(property="has_coronary_heart_disease", type="boolean"),
     *                  @SWG\Property(property="has_dementia", type="boolean"),
     *                  @SWG\Property(property="has_depression", type="boolean"),
     *                  @SWG\Property(property="has_diabetes", type="boolean"),
     *                  @SWG\Property(property="is_healthy", type="boolean"),
     *                  @SWG\Property(property="has_hypertension", type="boolean"),
     *                  @SWG\Property(property="is_inactive", type="boolean"),
     *                  @SWG\Property(property="has_mild_cognitive_impairment", type="boolean"),
     *                  @SWG\Property(property="number_falls_per_day", type="integer"),
     *                  @SWG\Property(property="is_underweight", type="boolean"),
     *                  @SWG\Property(property="is_normal_weight", type="boolean"),
     *                  @SWG\Property(property="is_overweight", type="boolean"),
     *                  @SWG\Property(property="is_obesity_1", type="boolean"),
     *                  @SWG\Property(property="is_obesity_2", type="boolean"),
     *                  @SWG\Property(property="is_obesity_3", type="boolean"),
     *                  @SWG\Property(property="has_partial_sleep_deprivation", type="boolean"),
     *                  @SWG\Property(property="has_prediabetes", type="boolean"),
     *                  @SWG\Property(property="has_prehypertension", type="boolean"),
     *                  @SWG\Property(property="has_subclinical_depression", type="boolean"),
     *                  @SWG\Property(property="has_subjective_poor_sleep", type="boolean"),
     *                  @SWG\Property(property="has_total_sleep_deprivation", type="boolean"),
     *                  @SWG\Property(property="has_high_ldl_concentration", type="boolean"),
     *                  @SWG\Property(property="has_high_ldl_particle", type="boolean"),
     *                  @SWG\Property(property="has_high_triglycerides", type="boolean"),
     *                  @SWG\Property(property="has_low_hdl", type="boolean"),
     *                  @SWG\Property(property="creation_date", type="string"),
     *                  @SWG\Property(property="last_changed", type="string"),
     *                  @SWG\Property(property="deleted_date", type="string")
     *              )
     *         )
     *     ),
     *     @SWG\Response(response="400",description="Invalid ID supplied"),
     *     @SWG\Response(response="404",description="Data not found"),
     *     security={{"oauth2_security": {"read-general", "manage-user-data"}}}
     * )
     */
    public function index(Basic $basic)
    {
        $outcome = $basic->outcome;

        return $this->showOne($outcome);
    }

    /**
     * @SWG\Post(
     *   path="/basics/{basic}/outcomes",
     *   tags={"basic_user"},
     *   summary="Creates new outcome record for user",
     *   description="",
     *   operationId="store",
     *   consumes={"application/x-www-form-urlencoded"},
     *   produces={"application/json"},
     *   @SWG\Parameter(name="basic", in="path", description="ID of basic_user",required=true,type="integer", format="int64"),
     *   @SWG\Parameter(name="has_alzheimers", in="formData", description="", required=true, type="boolean"),
     *   @SWG\Parameter(name="general_anxiety_disorder", in="formData", description="", required=true, type="boolean"),
     *   @SWG\Parameter(name="has_cardiovascular_disease", in="formData", description="", required=true, type="boolean"),
     *   @SWG\Parameter(name="has_coronary_heart_disease", in="formData", description="", required=true, type="boolean"),
     *   @SWG\Parameter(name="has_dementia", in="formData", description="", required=true, type="boolean"),
     *   @SWG\Parameter(name="has_depression", in="formData", description="", required=true, type="boolean"),
     *   @SWG\Parameter(name="has_diabetes", in="formData", description="", required=true, type="boolean"),
     *   @SWG\Parameter(name="is_healthy", in="formData", description="", required=true, type="boolean"),
     *   @SWG\Parameter(name="has_hypertension", in="formData", description="", required=true, type="boolean"),
     *   @SWG\Parameter(name="is_inactive", in="formData", description="", required=true, type="boolean"),
     *   @SWG\Parameter(name="has_mild_cognitive_impairment", in="formData", description="", required=true, type="boolean"),
     *   @SWG\Parameter(name="number_falls_per_day", in="formData", description="", required=true, type="integer"),
     *   @SWG\Parameter(name="is_underweight", in="formData", description="", required=true, type="boolean"),
     *   @SWG\Parameter(name="is_normal_weight", in="formData", description="", required=true, type="boolean"),
     *   @SWG\Parameter(name="is_overweight", in="formData", description="", required=true, type="boolean"),
     *   @SWG\Parameter(name="is_obesity_1", in="formData", description="", required=true, type="boolean"),
     *   @SWG\Parameter(name="is_obesity_2", in="formData", description="", required=true, type="boolean"),
     *   @SWG\Parameter(name="is_obesity_3", in="formData", description="", required=true, type="boolean"),
     *   @SWG\Parameter(name="has_partial_sleep_deprivation", in="formData", description="", required=true, type="boolean"),
     *   @SWG\Parameter(name="has_prediabetes", in="formData", description="", required=true, type="boolean"),
     *   @SWG\Parameter(name="has_prehypertension", in="formData", description="", required=true, type="boolean"),
     *   @SWG\Parameter(name="has_subclinical_depression", in="formData", description="", required=true, type="boolean"),
     *   @SWG\Parameter(name="has_subjective_poor_sleep", in="formData", description="", required=true, type="boolean"),
     *   @SWG\Parameter(name="has_total_sleep_deprivation", in="formData", description="", required=true, type="boolean"),
     *   @SWG\Parameter(name="has_high_ldl_concentration", in="formData", description="", required=true, type="boolean"),
     *   @SWG\Parameter(name="has_high_ldl_particle", in="formData", description="", required=true, type="boolean"),
     *   @SWG\Parameter(name="has_high_triglycerides", in="formData", description="", required=true, type="boolean"),
     *   @SWG\Parameter(name="has_low_hdl", in="formData", description="", required=true, type="boolean"),
     *
     *   @SWG\Response(response="405",description="Invalid input"),
     *   security={{
     *     "oauth2_security": {"read-general", "manage-user-data"}
     *   }}
     * )
     */
    public function store(Request $request, Basic $basic)
    {
        $rules = [
            'alzheimers' => 'required|boolean',
            'gen_anxiety_disorder' => 'required|boolean',
            'cardiovascular_disease' => 'required|boolean',
            'coronary_heart_disease' => 'required|boolean',
            'dementia' => 'required|boolean',
            'depression' => 'required|boolean',
            'diabetes' => 'required|boolean',
            'healthy' => 'required|boolean',
            'hypertension' => 'required|boolean',
            'inactive' => 'required|boolean',
            'mild_cognitive_impairment' => 'required|boolean',
            'number_falls' => 'required|integer|min:0|max:20',
            'underweight' => 'required|boolean',
            'normal_weight' => 'required|boolean',
            'overweight' => 'required|boolean',
            'obesity_1' => 'required|boolean',
            'obesity_2' => 'required|boolean',
            'obesity_3' => 'required|boolean',
            'partial_sleep_deprivation' => 'required|boolean',
            'prediabetes' => 'required|boolean',
            'prehypertension' => 'required|boolean',
            'subclinical_depression' => 'required|boolean',
            'subjective_poor_sleep' => 'required|boolean',
            'total_sleep_deprivation' => 'required|boolean',
            'high_ldl_concentration' => 'required|boolean',
            'high_ldl_particle' => 'required|boolean',
            'high_triglycerides' => 'required|boolean',
            'low_hdl' => 'required|boolean',
        ];

        $this->validate($request, $rules);

        if(!$basic->isVerified()) {
            return $this->errorResponse('The client must be a verified user.', 409);
        }

        $data = $request->all();
        $data['basic_id'] = $basic->id;

        $outcome = Outcome::create($data);

        return $this->showOne($outcome);
    }
}

<?php

namespace App\Http\Controllers\Basic;

use App\Basic;
use App\FitbitAuth;
use App\Http\Controllers\ApiController;
use App\Transformers\FitbitAuthTransformer;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BasicFitbitAuthController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . FitbitAuthTransformer::class)->only(['store', 'update']);
        $this->middleware('scope:read-general')->only('index');
        $this->middleware('scope:manage-user-data')->only(['store', 'update']);
        $this->middleware('can:view,basic')->only(['index']);
        $this->middleware('can:make,basic')->only(['store', 'update']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Basic $basic)
    {
        $fitbit_auth = $basic->fitbit_auth;

        return $this->showOne($fitbit_auth);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Basic $basic)
    {
        $rules = [
            'access_token' => 'required|string',
            'refresh_token' => 'required|string',
            'token_type' => 'required|string',
            'fitbit_user_id' => 'required|string',
        ];

        $this->validate($request, $rules);

        if(!$basic->isVerified()) {
            return $this->errorResponse('The client must be a verified user.', 409);
        }

        $data = $request->all();
        $data['basic_id'] = $basic->id;

        $fitbit_auth = FitbitAuth::create($data);

        return $this->showOne($fitbit_auth);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Basic  $basic
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Basic $basic, FitbitAuth $fitbitAuth)
    {
        $rules = [
            'access_token' => 'required|string',
            'refresh_token' => 'required|string',
            'token_type' => 'string',
            'fitbit_user_id' => 'string',
        ];

        $this->validate($request, $rules);

        $this->checkUser($basic, $fitbitAuth);

        if($request->has('access_token')){
            $fitbitAuth->access_token = $request->access_token;
        }

        if($request->has('refresh_token')){
            $fitbitAuth->refresh_token = $request->refresh_token;
        }

        if($request->has('token_type')){
            $fitbitAuth->token_type = $request->token_type;
        }

        if($request->has('fitbit_user_id')){
            $fitbitAuth->fitbit_user_id = $request->fitbit_user_id;
        }

        if(!$fitbitAuth->isDirty()){
            return $this->errorResponse('You need to specify a different value to update', 422);
        }

        $fitbitAuth->save();

        return $this->showOne($fitbitAuth);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Basic  $basic
     * @return \Illuminate\Http\Response
     */
    public function destroy(Basic $basic)
    {
        //
    }

    protected function checkUser(Basic $basic, FitbitAuth $fitbitAuth)
    {
        if($basic->id != $fitbitAuth->basic_id){
            throw new HttpException(422, 'The specified user is not the actual user of this fitbit');
        }
    }
}

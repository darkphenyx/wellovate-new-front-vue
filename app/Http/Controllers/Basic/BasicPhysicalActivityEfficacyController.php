<?php

namespace App\Http\Controllers\Basic;

use App\Basic;
use App\PhysicalActivityEfficacy;
use App\Transformers\PhysicalActivityEfficacyTransformer;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class BasicPhysicalActivityEfficacyController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . PhysicalActivityEfficacyTransformer::class)->only(['store', 'update']);
        $this->middleware('scope:read-general')->only('index');
        $this->middleware('scope:manage-user-data')->only(['store', 'update']);
        $this->middleware('can:view,basic')->only(['index']);
        $this->middleware('can:make,basic')->only(['store', 'update']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * @SWG\Get(
     *     path="/basics/{basic}/physical_activity_efficacys",
     *     summary="Get physical_activity_efficacys data for user",
     *     description="Returns all physical_activity_efficacys data",
     *     operationId="index",
     *     tags={"basic_user"},
     *     produces={"application/json"},
     *     @SWG\Parameter(description="ID of basic_user",in="path",name="basic",required=true,type="integer",format="int64"),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(
     *              type="array",
     *              @SWG\Items(
     *                  type="object",
     *                  @SWG\Property(property="identifier", type="integer"),
     *                  @SWG\Property(property="user_identifier", type="integer"),
     *                  @SWG\Property(property="seek_relating_information", type="string"),
     *                  @SWG\Property(property="track_my_exercise", type="string"),
     *                  @SWG\Property(property="find_ways_around_issues", type="string"),
     *                  @SWG\Property(property="set_reminders_to_exercise", type="string"),
     *                  @SWG\Property(property="think_exercise_is_rewarding", type="string"),
     *                  @SWG\Property(property="take_actions_to_make_exercise_enjoyable", type="string"),
     *                  @SWG\Property(property="focus_on_benefits_of_exercise", type="string"),
     *                  @SWG\Property(property="set_goals_to_engage_in_exercise", type="string"),
     *
     *                  @SWG\Property(property="creation_date", type="string"),
     *                  @SWG\Property(property="last_changed", type="string"),
     *                  @SWG\Property(property="deleted_date", type="string")
     *              )
     *         )
     *     ),
     *     @SWG\Response(response="400",description="Invalid ID supplied"),
     *     @SWG\Response(response="404",description="Data not found"),
     *     security={{"oauth2_security": {"read-general", "manage-user-data"}}}
     * )
     */
    public function index(Basic $basic)
    {
        $physical_activity_efficacys = $basic->physical_activity_efficacy;

        return $this->showAll($physical_activity_efficacys);
    }

    /**
     * @SWG\Post(
     *   path="/basics/{basic}/physical_activity_efficacys",
     *   tags={"basic_user"},
     *   summary="Creates new physical_activity_efficacy record for user",
     *   description="",
     *   operationId="store",
     *   consumes={"application/x-www-form-urlencoded"},
     *   produces={"application/json"},
     *   @SWG\Parameter(name="basic", in="path", description="ID of basic_user",required=true,type="integer", format="int64"),
     *   @SWG\Parameter(name="seek_relating_information", in="formData", description="", required=true, type="string", enum={"never","almost never","sometimes","often","very often"}),
     *   @SWG\Parameter(name="track_my_exercise", in="formData", description="", required=true, type="string", enum={"never","almost never","sometimes","often","very often"}),
     *   @SWG\Parameter(name="find_ways_around_issues", in="formData", description="", required=true, type="string", enum={"never","almost never","sometimes","often","very often"}),
     *   @SWG\Parameter(name="set_reminders_to_exercise", in="formData", description="", required=true, type="string", enum={"never","almost never","sometimes","often","very often"}),
     *   @SWG\Parameter(name="think_exercise_is_rewarding", in="formData", description="", required=true, type="string", enum={"never","almost never","sometimes","often","very often"}),
     *   @SWG\Parameter(name="take_actions_to_make_exercise_enjoyable", in="formData", description="", required=true, type="string", enum={"never","almost never","sometimes","often","very often"}),
     *   @SWG\Parameter(name="focus_on_benefits_of_exercise", in="formData", description="", required=true, type="string", enum={"never","almost never","sometimes","often","very often"}),
     *   @SWG\Parameter(name="set_goals_to_engage_in_exercise", in="formData", description="", required=true, type="string", enum={"never","almost never","sometimes","often","very often"}),
     *
     *   @SWG\Response(response="405",description="Invalid input"),
     *   security={{
     *     "oauth2_security": {"read-general", "manage-user-data"}
     *   }}
     * )
     */
    public function store(Request $request, Basic $basic)
    {
        $rules = [
            'seek_information' => 'required|in:never,almost never,sometimes,often,very often',
            'track_exercise' => 'required|in:never,almost never,sometimes,often,very often',
            'around_issues' => 'required|in:never,almost never,sometimes,often,very often',
            'set_reminders' => 'required|in:never,almost never,sometimes,often,very often',
            'is_rewarding' => 'required|in:never,almost never,sometimes,often,very often',
            'take_actions' => 'required|in:never,almost never,sometimes,often,very often',
            'focus_benefits' => 'required|in:never,almost never,sometimes,often,very often',
            'set_goals' => 'required|in:never,almost never,sometimes,often,very often',
        ];

        $this->validate($request, $rules);

        if(!$basic->isVerified()) {
            return $this->errorResponse('The client must be a verified user.', 409);
        }

        $data = $request->all();
        $data['basic_id'] = $basic->id;

        $physical_activity_efficacy = PhysicalActivityEfficacy::create($data);

        return $this->showOne($physical_activity_efficacy);
    }
}

<?php

namespace App\Http\Controllers\Basic;

use App\Basic;
use App\Http\Controllers\ApiController;
use App\Polar;
use App\Transformers\PolarTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpKernel\Exception\HttpException;

class BasicPolarController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . PolarTransformer::class)->only(['store', 'update']);
        $this->middleware('scope:read-general')->only('index');
        $this->middleware('scope:manage-user-data')->only(['store', 'update']);
        $this->middleware('can:view,basic')->only(['index']);
        $this->middleware('can:make,basic')->only(['store', 'update']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Basic $basic)
    {
        $polar = $basic->polar;

        return $this->showOne($polar);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Basic $basic)
    {
        $rules = [
            'access_token' => 'required|string',
            'token_type' => 'required|string',
            'x_user_id' => 'required|string',
        ];

        $this->validate($request, $rules);

        if(!$basic->isVerified()) {
            return $this->errorResponse('The client must be a verified user.', 409);
        }

        $data = $request->all();
        $data['basic_id'] = $basic->id;

        $polar = Polar::create($data);

        return $this->showOne($polar);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Basic  $basic
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Basic $basic, Polar $polar)
    {
        $rules = [
            'access_token' => 'required|string',
            'token_type' => 'string',
            'x_user_id' => 'required|string',
        ];

        $this->validate($request, $rules);

        $this->checkUser($basic, $polar);

        if($request->has('access_token')){
            $polar->access_token = $request->access_token;
        }

        if($request->has('token_type')){
            $polar->token_type = $request->token_type;
        }

        if($request->has('x_user_id')){
            $polar->x_user_id = $request->x_user_id;
        }

        if(!$polar->isDirty()){
            return $this->errorResponse('You need to specify a different value to update', 422);
        }

        $polar->save();

        return $this->showOne($polar);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Basic  $basic
     * @return \Illuminate\Http\Response
     */
    public function destroy(Basic $basic)
    {
        //
    }

    protected function checkUser(Basic $basic, Polar $polar)
    {
        if($basic->id != $polar->basic_id){
            throw new HttpException(422, 'The specified user is not the actual user of this fitbit');
        }
    }
}

<?php

namespace App\Http\Controllers\Basic;

use App\Basic;
use App\MixedRealityUse;
use App\Transformers\MixedRealityUseTransformer;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class BasicMixedRealityUseController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . MixedRealityUseTransformer::class)->only(['store']);
        $this->middleware('scope:read-general')->only('index');
        $this->middleware('scope:manage-user-data')->only(['store', 'update']);
        $this->middleware('can:view,basic')->only(['index']);
        $this->middleware('can:make,basic')->only(['store', 'update']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * @SWG\Get(
     *     path="/basics/{basic}/mixed_reality_uses",
     *     summary="Get mixed_reality_use data for user",
     *     description="Returns all mixed_reality_use data",
     *     operationId="index",
     *     tags={"basic_user"},
     *     produces={"application/json"},
     *     @SWG\Parameter(description="ID of basic_user",in="path",name="basic",required=true,type="integer",format="int64"),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(
     *              type="array",
     *              @SWG\Items(
     *                  type="object",
     *                  @SWG\Property(property="identifier", type="integer"),
     *                  @SWG\Property(property="user_identifier", type="integer"),
     *                  @SWG\Property(property="experience_identifier", type="integer"),
     *                  @SWG\Property(property="lobby_start_time", type="string"),
     *                  @SWG\Property(property="lobby_end_time", type="string"),
     *                  @SWG\Property(property="experience_start_time", type="string"),
     *                  @SWG\Property(property="experience_end_time", type="string"),
     *                  @SWG\Property(property="creation_date", type="string"),
     *                  @SWG\Property(property="last_changed", type="string"),
     *                  @SWG\Property(property="deleted_date", type="string")
     *              )
     *         )
     *     ),
     *     @SWG\Response(response="400",description="Invalid ID supplied"),
     *     @SWG\Response(response="404",description="Data not found"),
     *     security={{"oauth2_security": {"read-general", "manage-user-data"}}}
     * )
     */
    public function index(Basic $basic)
    {
        $mixed_reality_uses = $basic->mixed_reality_us;

        return $this->showAll($mixed_reality_uses);
    }

    /**
     * @SWG\Post(
     *   path="/basics/{basic}/mixed_reality_uses",
     *   tags={"basic_user"},
     *   summary="Creates new mixed_reality_uses record for user",
     *   description="",
     *   operationId="store",
     *   consumes={"application/x-www-form-urlencoded"},
     *   produces={"application/json"},
     *   @SWG\Parameter(name="basic", in="path", description="ID of basic_user",required=true,type="integer", format="int64"),
     *   @SWG\Parameter(name="experience_identifier", in="formData", description="", required=true, type="string"),
     *   @SWG\Parameter(name="lobby_start_time", in="formData", description="date-time", required=true, type="string", format="date-time"),
     *   @SWG\Parameter(name="lobby_end_time", in="formData", description="date-time", required=true, type="string", format="date-time"),
     *   @SWG\Parameter(name="experience_start_time", in="formData", description="date-time", required=true, type="string", format="date-time"),
     *   @SWG\Parameter(name="experience_end_time", in="formData", description="date-time", required=true, type="string", format="date-time"),
     *
     *   @SWG\Response(response="405",description="Invalid input"),
     *   security={{
     *     "oauth2_security": {"read-general", "manage-user-data"}
     *   }}
     * )
     */
    public function store(Request $request, Basic $basic)
    {
        $rules = [
            'experience_id' => 'required|integer',
            'start_lobby' => 'required|date|date_format:Y-m-d H:i:s',
            'end_lobby' => 'required|date|date_format:Y-m-d H:i:s',
            'start_experience' => 'required|date|date_format:Y-m-d H:i:s',
            'end_experience' => 'required|date|date_format:Y-m-d H:i:s',
        ];

        $this->validate($request, $rules);

        if(!$basic->isVerified()) {
            return $this->errorResponse('The client must be a verified user.', 409);
        }

        $data = $request->all();
        $data['basic_id'] = $basic->id;

        $mixed_reality_use = MixedRealityUse::create($data);

        return $this->showOne($mixed_reality_use);
    }
}

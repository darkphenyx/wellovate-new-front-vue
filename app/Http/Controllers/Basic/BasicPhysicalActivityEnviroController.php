<?php

namespace App\Http\Controllers\Basic;

use App\Basic;
use App\PhysicalActivityEnviro;
use App\Transformers\PhysicalActivityEnviroTransformer;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class BasicPhysicalActivityEnviroController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . PhysicalActivityEnviroTransformer::class)->only(['store', 'update']);
        $this->middleware('scope:read-general')->only('index');
        $this->middleware('scope:manage-user-data')->only(['store', 'update']);
        $this->middleware('can:view,basic')->only(['index']);
        $this->middleware('can:make,basic')->only(['store', 'update']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * @SWG\Get(
     *     path="/basics/{basic}/physical_activity_enviro",
     *     summary="Get physical_activity_enviro data for user",
     *     description="Returns all physical_activity_enviro data",
     *     operationId="index",
     *     tags={"basic_user"},
     *     produces={"application/json"},
     *     @SWG\Parameter(description="ID of basic_user",in="path",name="basic",required=true,type="integer",format="int64"),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(
     *              type="array",
     *              @SWG\Items(
     *                  type="object",
     *                  @SWG\Property(property="identifier", type="integer"),
     *                  @SWG\Property(property="user_identifier", type="integer"),
     *                  @SWG\Property(property="have_access_to_equipment", type="string"),
     *                  @SWG\Property(property="hard_to_walk_due_to_traffic", type="string"),
     *                  @SWG\Property(property="hard_to_walk_due_to_safety_in_neighborhood", type="string"),
     *
     *                  @SWG\Property(property="creation_date", type="string"),
     *                  @SWG\Property(property="last_changed", type="string"),
     *                  @SWG\Property(property="deleted_date", type="string")
     *              )
     *         )
     *     ),
     *     @SWG\Response(response="400",description="Invalid ID supplied"),
     *     @SWG\Response(response="404",description="Data not found"),
     *     security={{"oauth2_security": {"read-general", "manage-user-data"}}}
     * )
     */
    public function index(Basic $basic)
    {
        $physical_activity_enviros = $basic->physical_activity_enviro;

        return $this->showOne($physical_activity_enviros);
    }

    /**
     * @SWG\Post(
     *   path="/basics/{basic}/physical_activity_enviro",
     *   tags={"basic_user"},
     *   summary="Creates new physical_activity_enviro record for user",
     *   description="",
     *   operationId="store",
     *   consumes={"application/x-www-form-urlencoded"},
     *   produces={"application/json"},
     *   @SWG\Parameter(name="basic", in="path", description="ID of basic_user",required=true,type="integer", format="int64"),
     *   @SWG\Parameter(name="have_access_to_equipment", in="formData", description="", required=true, type="string", enum={"strongly disagree","somewhat disagree","neutral","somewhat agree","strongly agree"}),
     *   @SWG\Parameter(name="hard_to_walk_due_to_traffic", in="formData", description="", required=true, type="string", enum={"strongly disagree","somewhat disagree","neutral","somewhat agree","strongly agree"}),
     *   @SWG\Parameter(name="hard_to_walk_due_to_safety_in_neighborhood", in="formData", description="", required=true, type="string", enum={"strongly disagree","somewhat disagree","neutral","somewhat agree","strongly agree"}),
     *
     *   @SWG\Response(response="405",description="Invalid input"),
     *   security={{
     *     "oauth2_security": {"read-general", "manage-user-data"}
     *   }}
     * )
     */
    public function store(Request $request, Basic $basic)
    {
        $rules = [
            'access_equipment' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'walk_traffic' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'walk_safe' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
        ];

        $this->validate($request, $rules);

        if(!$basic->isVerified()) {
            return $this->errorResponse('The client must be a verified user.', 409);
        }

        $data = $request->all();
        $data['basic_id'] = $basic->id;

        $physical_activity_enviro = PhysicalActivityEnviro::create($data);

        return $this->showOne($physical_activity_enviro);
    }
}

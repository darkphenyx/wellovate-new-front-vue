<?php

namespace App\Http\Controllers\Basic;

use App\Basic;
use App\PhysicalActivityBarriers;
use App\Transformers\PhysicalActivityBarriersTransformer;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class BasicPhysicalActivityBarrierController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . PhysicalActivityBarriersTransformer::class)->only(['store', 'update']);
        $this->middleware('scope:read-general')->only('index');
        $this->middleware('scope:manage-user-data')->only(['store', 'update']);
        $this->middleware('can:view,basic')->only(['index']);
        $this->middleware('can:make,basic')->only(['store', 'update']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * @SWG\Get(
     *     path="/basics/{basic}/physical_activity_barriers",
     *     summary="Get physical_activity_barriers data for user",
     *     description="Returns all physical_activity_barriers data",
     *     operationId="index",
     *     tags={"basic_user"},
     *     produces={"application/json"},
     *     @SWG\Parameter(description="ID of basic_user",in="path",name="basic",required=true,type="integer",format="int64"),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(
     *              type="array",
     *              @SWG\Items(
     *                  type="object",
     *                  @SWG\Property(property="identifier", type="integer"),
     *                  @SWG\Property(property="user_identifier", type="string"),
     *                  @SWG\Property(property="lack_someone_for_exercise", type="string"),
     *                  @SWG\Property(property="lack_of_time_for_exercise", type="string"),
     *                  @SWG\Property(property="family_obligation_issues", type="string"),
     *                  @SWG\Property(property="lack_energy_for_exercise", type="string"),
     *                  @SWG\Property(property="self_conscious_around_others", type="string"),
     *                  @SWG\Property(property="lack_equipment_for_exercise", type="string"),
     *                  @SWG\Property(property="lack_place_to_exercise", type="string"),
     *                  @SWG\Property(property="fear_of_being_injured", type="string"),
     *                  @SWG\Property(property="being_stressed_out", type="string"),
     *                  @SWG\Property(property="being_discouraged", type="string"),
     *                  @SWG\Property(property="not_healthy_enough_for_exercise", type="string"),
     *                  @SWG\Property(property="find_exercise_boring", type="string"),
     *                  @SWG\Property(property="lack_of_knowledge", type="string"),
     *                  @SWG\Property(property="lack_of_skills", type="string"),
     *                  @SWG\Property(property="lack_of_motivation", type="string"),
     *                  @SWG\Property(property="pain", type="string"),
     *                  @SWG\Property(property="bad_weather", type="string"),
     *                  @SWG\Property(property="other_barriers", type="string"),
     *
     *                  @SWG\Property(property="creation_date", type="string"),
     *                  @SWG\Property(property="last_changed", type="string"),
     *                  @SWG\Property(property="deleted_date", type="string")
     *              )
     *         )
     *     ),
     *     @SWG\Response(response="400",description="Invalid ID supplied"),
     *     @SWG\Response(response="404",description="Data not found"),
     *     security={{"oauth2_security": {"read-general", "manage-user-data"}}}
     * )
     */
    public function index(Basic $basic)
    {
        $physical_activity_barriers = $basic->physical_activity_barrier;

        return $this->showAll($physical_activity_barriers);
    }

    /**
     * @SWG\Post(
     *   path="/basics/{basic}/physical_activity_barriers",
     *   tags={"basic_user"},
     *   summary="Creates new physical_activity_barriers record for user",
     *   description="",
     *   operationId="store",
     *   consumes={"application/x-www-form-urlencoded"},
     *   produces={"application/json"},
     *   @SWG\Parameter(name="basic", in="path", description="ID of basic_user",required=true,type="integer", format="int64"),
     *   @SWG\Parameter(name="lack_someone_for_exercise", in="formData", description="", required=true, type="string", enum={"never","almost never","sometimes","often","very often"}),
     *   @SWG\Parameter(name="lack_of_time_for_exercise", in="formData", description="", required=true, type="string", enum={"never","almost never","sometimes","often","very often"}),
     *   @SWG\Parameter(name="family_obligation_issues", in="formData", description="", required=true, type="string", enum={"never","almost never","sometimes","often","very often"}),
     *   @SWG\Parameter(name="lack_energy_for_exercise", in="formData", description="", required=true, type="string", enum={"never","almost never","sometimes","often","very often"}),
     *   @SWG\Parameter(name="self_conscious_around_others", in="formData", description="", required=true, type="string", enum={"never","almost never","sometimes","often","very often"}),
     *   @SWG\Parameter(name="lack_equipment_for_exercise", in="formData", description="", required=true, type="string", enum={"never","almost never","sometimes","often","very often"}),
     *   @SWG\Parameter(name="lack_place_to_exercise", in="formData", description="", required=true, type="string", enum={"never","almost never","sometimes","often","very often"}),
     *   @SWG\Parameter(name="fear_of_being_injured", in="formData", description="", required=true, type="string", enum={"never","almost never","sometimes","often","very often"}),
     *   @SWG\Parameter(name="being_stressed_out", in="formData", description="", required=true, type="string", enum={"never","almost never","sometimes","often","very often"}),
     *   @SWG\Parameter(name="being_discouraged", in="formData", description="", required=true, type="string", enum={"never","almost never","sometimes","often","very often"}),
     *   @SWG\Parameter(name="not_healthy_enough_for_exercise", in="formData", description="", required=true, type="string", enum={"never","almost never","sometimes","often","very often"}),
     *   @SWG\Parameter(name="find_exercise_boring", in="formData", description="", required=true, type="string", enum={"never","almost never","sometimes","often","very often"}),
     *   @SWG\Parameter(name="lack_of_knowledge", in="formData", description="", required=true, type="string", enum={"never","almost never","sometimes","often","very often"}),
     *   @SWG\Parameter(name="lack_of_skills", in="formData", description="", required=true, type="string", enum={"never","almost never","sometimes","often","very often"}),
     *   @SWG\Parameter(name="lack_of_motivation", in="formData", description="", required=true, type="string", enum={"never","almost never","sometimes","often","very often"}),
     *   @SWG\Parameter(name="pain", in="formData", description="", required=true, type="string", enum={"never","almost never","sometimes","often","very often"}),
     *   @SWG\Parameter(name="bad_weather", in="formData", description="", required=true, type="string", enum={"never","almost never","sometimes","often","very often"}),
     *   @SWG\Parameter(name="other_barriers", in="formData", description="", required=true, type="string"),
     *
     *   @SWG\Response(response="405",description="Invalid input"),
     *   security={{
     *     "oauth2_security": {"read-general", "manage-user-data"}
     *   }}
     * )
     */
    public function store(Request $request, Basic $basic)
    {
        $rules = [
            'lack_someone' => 'required|in:never,almost never,sometimes,often,very often',
            'lack_of_time' => 'required|in:never,almost never,sometimes,often,very often',
            'family_obligation' => 'required|in:never,almost never,sometimes,often,very often',
            'lack_energy' => 'required|in:never,almost never,sometimes,often,very often',
            'self_conscious' => 'required|in:never,almost never,sometimes,often,very often',
            'lack_equipment' => 'required|in:never,almost never,sometimes,often,very often',
            'lack_place' => 'required|in:never,almost never,sometimes,often,very often',
            'fear_injured' => 'required|in:never,almost never,sometimes,often,very often',
            'stressed_out' => 'required|in:never,almost never,sometimes,often,very often',
            'discouraged' => 'required|in:never,almost never,sometimes,often,very often',
            'not_healthy_enough' => 'required|in:never,almost never,sometimes,often,very often',
            'find_boring' => 'required|in:never,almost never,sometimes,often,very often',
            'lack_knowledge' => 'required|in:never,almost never,sometimes,often,very often',
            'lack_skills' => 'required|in:never,almost never,sometimes,often,very often',
            'lack_motivation' => 'required|in:never,almost never,sometimes,often,very often',
            'pain' => 'required|in:never,almost never,sometimes,often,very often',
            'bad_weather' => 'required|in:never,almost never,sometimes,often,very often',
            'other' => 'string|max:1000|nullable'
        ];

        $this->validate($request, $rules);

        if(!$basic->isVerified()) {
            return $this->errorResponse('The client must be a verified user.', 409);
        }

        $data = $request->all();
        $data['basic_id'] = $basic->id;

        $physical_activity_barrier = PhysicalActivityBarriers::create($data);

        return $this->showOne($physical_activity_barrier);
    }
}

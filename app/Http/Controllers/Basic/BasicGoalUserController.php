<?php

namespace App\Http\Controllers\Basic;

use App\Basic;
use App\GoalUser;
use App\Transformers\GoalUserTransformer;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class BasicGoalUserController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . GoalUserTransformer::class)->only(['store']);
        $this->middleware('scope:read-general')->only('index');
        $this->middleware('scope:manage-user-data')->only(['store', 'update']);
        $this->middleware('can:view,basic')->only(['index']);
        $this->middleware('can:make,basic')->only(['store', 'update']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * @SWG\Get(
     *     path="/basics/{basic}/goal_users",
     *     summary="Get goal_users data for user",
     *     description="Returns all goal_users data",
     *     operationId="index",
     *     tags={"basic_user"},
     *     produces={"application/json"},
     *     @SWG\Parameter(description="ID of basic_user",in="path",name="basic",required=true,type="integer",format="int64"),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(
     *              type="array",
     *              @SWG\Items(
     *                  type="object",
     *                  @SWG\Property(property="identifier", type="integer"),
     *                  @SWG\Property(property="user_identifier", type="integer"),
     *                  @SWG\Property(property="goal_identifier", type="integer"),
     *                  @SWG\Property(property="priority_level", type="integer"),
     *                  @SWG\Property(property="completed_or_not", type="boolean"),
     *                  @SWG\Property(property="creation_date", type="string"),
     *                  @SWG\Property(property="last_changed", type="string"),
     *                  @SWG\Property(property="deleted_date", type="string")
     *              )
     *         )
     *     ),
     *     @SWG\Response(response="400",description="Invalid ID supplied"),
     *     @SWG\Response(response="404",description="Data not found"),
     *     security={{"oauth2_security": {"read-general", "manage-user-data"}}}
     * )
     */
    public function index(Basic $basic)
    {
        $goal_users = $basic->goal_user;

        return $this->showAll($goal_users);
    }


    /**
     * @SWG\Post(
     *   path="/basics/{basic}/goal_users",
     *   tags={"basic_user"},
     *   summary="Creates new goal_users record for user",
     *   description="",
     *   operationId="store",
     *   consumes={"application/x-www-form-urlencoded"},
     *   produces={"application/json"},
     *   @SWG\Parameter(name="basic", in="path", description="ID of basic_user",required=true,type="integer", format="int64"),
     *   @SWG\Parameter(name="goal_identifier", in="formData", description="", required=true, type="integer"),
     *   @SWG\Parameter(name="priority_level", in="formData", description="", required=true, type="integer"),
     *   @SWG\Response(response="405",description="Invalid input"),
     *   security={{
     *     "oauth2_security": {"read-general", "manage-user-data"}
     *   }}
     * )
     */
    public function store(Request $request, Basic $basic)
    {
        $rules = [
            'goal_id' => 'required|integer',
            'priority' => 'required|integer|min:1|max:10',
        ];

        $this->validate($request, $rules);

        if(!$basic->isVerified()) {
            return $this->errorResponse('The client must be a verified user.', 409);
        }

        $data = $request->all();
        $data['basic_id'] = $basic->id;

        $goal_user = GoalUser::create($data);

        return $this->showOne($goal_user);
    }
}

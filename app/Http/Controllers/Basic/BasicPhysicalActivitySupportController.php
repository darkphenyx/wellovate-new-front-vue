<?php

namespace App\Http\Controllers\Basic;

use App\Basic;
use App\PhysicalActivitySupport;
use App\Transformers\PhysicalActivitySupportTransformer;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class BasicPhysicalActivitySupportController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . PhysicalActivitySupportTransformer::class)->only(['store', 'update']);
        $this->middleware('scope:read-general')->only('index');
        $this->middleware('scope:manage-user-data')->only(['store', 'update']);
        $this->middleware('can:view,basic')->only(['index']);
        $this->middleware('can:make,basic')->only(['store', 'update']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * @SWG\Get(
     *     path="/basics/{basic}/physical_activity_support",
     *     summary="Get physical_activity_support data for user",
     *     description="Returns all physical_activity_support data",
     *     operationId="index",
     *     tags={"basic_user"},
     *     produces={"application/json"},
     *     @SWG\Parameter(description="ID of basic_user",in="path",name="basic",required=true,type="integer",format="int64"),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(
     *              type="array",
     *              @SWG\Items(
     *                  type="object",
     *                  @SWG\Property(property="identifier", type="integer"),
     *                  @SWG\Property(property="user_identifier", type="integer"),
     *                  @SWG\Property(property="encourage_you_to_exercise", type="string"),
     *                  @SWG\Property(property="offered_to_exercise_or_participate", type="string"),
     *                  @SWG\Property(property="exercised_or_participated_with_you", type="string"),
     *                  @SWG\Property(property="discouraged_you_from_exercising", type="string"),
     *
     *                  @SWG\Property(property="creation_date", type="string"),
     *                  @SWG\Property(property="last_changed", type="string"),
     *                  @SWG\Property(property="deleted_date", type="string")
     *              )
     *         )
     *     ),
     *     @SWG\Response(response="400",description="Invalid ID supplied"),
     *     @SWG\Response(response="404",description="Data not found"),
     *     security={{"oauth2_security": {"read-general", "manage-user-data"}}}
     * )
     */
    public function index(Basic $basic)
    {
        $physical_activity_supports = $basic->physical_activity_support;

        return $this->showAll($physical_activity_supports);
    }

    /**
     * @SWG\Post(
     *   path="/basics/{basic}/physical_activity_support",
     *   tags={"basic_user"},
     *   summary="Creates new physical_activity_support record for user",
     *   description="",
     *   operationId="store",
     *   consumes={"application/x-www-form-urlencoded"},
     *   produces={"application/json"},
     *   @SWG\Parameter(name="basic", in="path", description="ID of basic_user",required=true,type="integer", format="int64"),
     *   @SWG\Parameter(name="encourage_you_to_exercise", in="formData", description="", required=true, type="string", enum={"never","almost never","sometimes","often","very often"}),
     *   @SWG\Parameter(name="offered_to_exercise_or_participate", in="formData", description="", required=true, type="string", enum={"never","almost never","sometimes","often","very often"}),
     *   @SWG\Parameter(name="exercised_or_participated_with_you", in="formData", description="", required=true, type="string", enum={"never","almost never","sometimes","often","very often"}),
     *   @SWG\Parameter(name="discouraged_you_from_exercising", in="formData", description="", required=true, type="string", enum={"never","almost never","sometimes","often","very often"}),
     *
     *   @SWG\Response(response="405",description="Invalid input"),
     *   security={{
     *     "oauth2_security": {"read-general", "manage-user-data"}
     *   }}
     * )
     */
    public function store(Request $request, Basic $basic)
    {
        $rules = [
            'encourage_exercise' => 'required|in:never,almost never,sometimes,often,very often',
            'offered_participate' => 'required|in:never,almost never,sometimes,often,very often',
            'exercised_with' => 'required|in:never,almost never,sometimes,often,very often',
            'discouraged' => 'required|in:never,almost never,sometimes,often,very often',
        ];

        $this->validate($request, $rules);

        if(!$basic->isVerified()) {
            return $this->errorResponse('The client must be a verified user.', 409);
        }

        $data = $request->all();
        $data['basic_id'] = $basic->id;

        $physical_activity_support = PhysicalActivitySupport::create($data);

        return $this->showOne($physical_activity_support);
    }
}

<?php

namespace App\Http\Controllers\Basic;

use App\Basic;
use App\PhysicalActivityAssessment;
use App\Transformers\PhysicalActivityAssessmentTransformer;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class BasicPhysicalActivityAssessmentController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . PhysicalActivityAssessmentTransformer::class)->only(['store', 'update']);
        $this->middleware('scope:read-general')->only('index');
        $this->middleware('scope:manage-user-data')->only(['store', 'update']);
        $this->middleware('can:view,basic')->only(['index']);
        $this->middleware('can:make,basic')->only(['store', 'update']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * @SWG\Get(
     *     path="/basics/{basic}/physical_activity_assessment",
     *     summary="Get physical_activity_assessment data for user",
     *     description="Returns all physical_activity_assessment data",
     *     operationId="index",
     *     tags={"basic_user"},
     *     produces={"application/json"},
     *     @SWG\Parameter(description="ID of basic_user",in="path",name="basic",required=true,type="integer",format="int64"),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(
     *              type="array",
     *              @SWG\Items(
     *                  type="object",
     *                  @SWG\Property(property="identifier", type="integer"),
     *                  @SWG\Property(property="user_identifier", type="integer"),
     *                  @SWG\Property(property="enjoy_walking", type="string"),
     *                  @SWG\Property(property="enjoy_hiking", type="string"),
     *                  @SWG\Property(property="enjoy_running", type="string"),
     *                  @SWG\Property(property="enjoy_bicycling", type="string"),
     *                  @SWG\Property(property="enjoy_weight_training", type="string"),
     *                  @SWG\Property(property="enjoy_calisthenics", type="string"),
     *                  @SWG\Property(property="enjoy_swimming", type="string"),
     *                  @SWG\Property(property="enjoy_yoga", type="string"),
     *                  @SWG\Property(property="enjoy_tennis", type="string"),
     *                  @SWG\Property(property="enjoy_table_tennis", type="string"),
     *                  @SWG\Property(property="enjoy_aerobic", type="string"),
     *                  @SWG\Property(property="enjoy_kayaking", type="string"),
     *                  @SWG\Property(property="enjoy_martial_arts", type="string"),
     *                  @SWG\Property(property="enjoy_basketball", type="string"),
     *                  @SWG\Property(property="enjoy_soccer", type="string"),
     *                  @SWG\Property(property="enjoy_golf", type="string"),
     *                  @SWG\Property(property="enjoy_skiing", type="string"),
     *                  @SWG\Property(property="enjoy_volleyball", type="string"),
     *                  @SWG\Property(property="enjoy_gardening", type="string"),
     *                  @SWG\Property(property="enjoy_skating", type="string"),
     *                  @SWG\Property(property="enjoy_children", type="string"),
     *
     *                  @SWG\Property(property="creation_date", type="string"),
     *                  @SWG\Property(property="last_changed", type="string"),
     *                  @SWG\Property(property="deleted_date", type="string")
     *              )
     *         )
     *     ),
     *     @SWG\Response(response="400",description="Invalid ID supplied"),
     *     @SWG\Response(response="404",description="Data not found"),
     *     security={{"oauth2_security": {"read-general", "manage-user-data"}}}
     * )
     */
    public function index(Basic $basic)
    {
        $physical_activity_assessment = $basic->physical_activity_assessment;

        return $this->showOne($physical_activity_assessment);
    }

    /**
     * @SWG\Post(
     *   path="/basics/{basic}/physical_activity_assessment",
     *   tags={"basic_user"},
     *   summary="Creates new anthro record for user",
     *   description="",
     *   operationId="store",
     *   consumes={"application/x-www-form-urlencoded"},
     *   produces={"application/json"},
     *   @SWG\Parameter(name="basic", in="path", description="ID of basic_user",required=true,type="integer", format="int64"),
     *   @SWG\Parameter(name="enjoy_walking", in="formData", description="", required=true, type="string", enum={"strongly disagree","somewhat disagree","neutral","somewhat agree","strongly agree"}),
     *   @SWG\Parameter(name="enjoy_hiking", in="formData", description="", required=true, type="string", enum={"strongly disagree","somewhat disagree","neutral","somewhat agree","strongly agree"}),
     *   @SWG\Parameter(name="enjoy_running", in="formData", description="", required=true, type="string", enum={"strongly disagree","somewhat disagree","neutral","somewhat agree","strongly agree"}),
     *   @SWG\Parameter(name="enjoy_bicycling", in="formData", description="", required=true, type="string", enum={"strongly disagree","somewhat disagree","neutral","somewhat agree","strongly agree"}),
     *   @SWG\Parameter(name="enjoy_weight_training", in="formData", description="", required=true, type="string", enum={"strongly disagree","somewhat disagree","neutral","somewhat agree","strongly agree"}),
     *   @SWG\Parameter(name="enjoy_calisthenics", in="formData", description="", required=true, type="string", enum={"strongly disagree","somewhat disagree","neutral","somewhat agree","strongly agree"}),
     *   @SWG\Parameter(name="enjoy_swimming", in="formData", description="", required=true, type="string", enum={"strongly disagree","somewhat disagree","neutral","somewhat agree","strongly agree"}),
     *   @SWG\Parameter(name="enjoy_yoga", in="formData", description="", required=true, type="string", enum={"strongly disagree","somewhat disagree","neutral","somewhat agree","strongly agree"}),
     *   @SWG\Parameter(name="enjoy_tennis", in="formData", description="", required=true, type="string", enum={"strongly disagree","somewhat disagree","neutral","somewhat agree","strongly agree"}),
     *   @SWG\Parameter(name="enjoy_table_tennis", in="formData", description="", required=true, type="string", enum={"strongly disagree","somewhat disagree","neutral","somewhat agree","strongly agree"}),
     *   @SWG\Parameter(name="enjoy_aerobic", in="formData", description="", required=true, type="string", enum={"strongly disagree","somewhat disagree","neutral","somewhat agree","strongly agree"}),
     *   @SWG\Parameter(name="enjoy_kayaking", in="formData", description="", required=true, type="string", enum={"strongly disagree","somewhat disagree","neutral","somewhat agree","strongly agree"}),
     *   @SWG\Parameter(name="enjoy_martial_arts", in="formData", description="", required=true, type="string", enum={"strongly disagree","somewhat disagree","neutral","somewhat agree","strongly agree"}),
     *   @SWG\Parameter(name="enjoy_basketball", in="formData", description="", required=true, type="string", enum={"strongly disagree","somewhat disagree","neutral","somewhat agree","strongly agree"}),
     *   @SWG\Parameter(name="enjoy_soccer", in="formData", description="", required=true, type="string", enum={"strongly disagree","somewhat disagree","neutral","somewhat agree","strongly agree"}),
     *   @SWG\Parameter(name="enjoy_golf", in="formData", description="", required=true, type="string", enum={"strongly disagree","somewhat disagree","neutral","somewhat agree","strongly agree"}),
     *   @SWG\Parameter(name="enjoy_skiing", in="formData", description="", required=true, type="string", enum={"strongly disagree","somewhat disagree","neutral","somewhat agree","strongly agree"}),
     *   @SWG\Parameter(name="enjoy_volleyball", in="formData", description="", required=true, type="string", enum={"strongly disagree","somewhat disagree","neutral","somewhat agree","strongly agree"}),
     *   @SWG\Parameter(name="enjoy_gardening", in="formData", description="", required=true, type="string", enum={"strongly disagree","somewhat disagree","neutral","somewhat agree","strongly agree"}),
     *   @SWG\Parameter(name="enjoy_skating", in="formData", description="", required=true, type="string", enum={"strongly disagree","somewhat disagree","neutral","somewhat agree","strongly agree"}),
     *   @SWG\Parameter(name="enjoy_children", in="formData", description="", required=true, type="string", enum={"strongly disagree","somewhat disagree","neutral","somewhat agree","strongly agree"}),
     *
     *   @SWG\Response(response="405",description="Invalid input"),
     *   security={{
     *     "oauth2_security": {"read-general", "manage-user-data"}
     *   }}
     * )
     */
    public function store(Request $request, Basic $basic)
    {
        $rules = [
            'walking' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'hiking' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'running' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'bicycling' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'weight_training' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'calisthenics' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'swimming' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'yoga' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'tennis' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'table_tennis' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'aerobic' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'kayaking' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'martial_arts' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'basketball' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'soccer' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'golf' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'skiing' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'volleyball' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'gardening' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'skating' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'children' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
        ];

        $this->validate($request, $rules);

        if(!$basic->isVerified()) {
            return $this->errorResponse('The client must be a verified user.', 409);
        }

        $data = $request->all();
        $data['basic_id'] = $basic->id;

        $physical_activity_assessment = PhysicalActivityAssessment::create($data);

        return $this->showOne($physical_activity_assessment);
    }
}

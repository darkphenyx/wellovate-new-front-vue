<?php

namespace App\Http\Controllers\Basic;

use App\Basic;
use App\PhysicalActivityProsCons;
use App\Transformers\PhysicalActivityProsConsTransformer;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class BasicPhysicalActivityProsConsController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . PhysicalActivityProsConsTransformer::class)->only(['store', 'update']);
        $this->middleware('scope:read-general')->only('index');
        $this->middleware('scope:manage-user-data')->only(['store', 'update']);
        $this->middleware('can:view,basic')->only(['index']);
        $this->middleware('can:make,basic')->only(['store', 'update']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * @SWG\Get(
     *     path="/basics/{basic}/physical_activity_pros_cons",
     *     summary="Get physical_activity_pros_cons data for user",
     *     description="Returns all physical_activity_pros_cons data",
     *     operationId="index",
     *     tags={"basic_user"},
     *     produces={"application/json"},
     *     @SWG\Parameter(description="ID of basic_user",in="path",name="basic",required=true,type="integer",format="int64"),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(
     *              type="array",
     *              @SWG\Items(
     *                  type="object",
     *                  @SWG\Property(property="identifier", type="integer"),
     *                  @SWG\Property(property="user_identifier", type="integer"),
     *                  @SWG\Property(property="improve_my_health", type="string"),
     *                  @SWG\Property(property="concerned_exercise_makes_me_uncomfortable", type="string"),
     *                  @SWG\Property(property="improve_energy_levels", type="string"),
     *                  @SWG\Property(property="improve_my_mood", type="string"),
     *                  @SWG\Property(property="improve_my_mental_abilities", type="string"),
     *                  @SWG\Property(property="relieve_stress", type="string"),
     *                  @SWG\Property(property="improve_my_self_image", type="string"),
     *                  @SWG\Property(property="become_more_attractive", type="string"),
     *                  @SWG\Property(property="concerned_others_see_me_exercising", type="string"),
     *                  @SWG\Property(property="lose_or_maintain_weight", type="string"),
     *                  @SWG\Property(property="meet_people_or_make_friends", type="string"),
     *                  @SWG\Property(property="waste_to_much_time", type="string"),
     *                  @SWG\Property(property="less_time_for_others", type="string"),
     *
     *                  @SWG\Property(property="creation_date", type="string"),
     *                  @SWG\Property(property="last_changed", type="string"),
     *                  @SWG\Property(property="deleted_date", type="string")
     *              )
     *         )
     *     ),
     *     @SWG\Response(response="400",description="Invalid ID supplied"),
     *     @SWG\Response(response="404",description="Data not found"),
     *     security={{"oauth2_security": {"read-general", "manage-user-data"}}}
     * )
     */
    public function index(Basic $basic)
    {
        $physical_activity_pros_cons = $basic->physical_activity_pros_con;

        return $this->showAll($physical_activity_pros_cons);
    }

    /**
     * @SWG\Post(
     *   path="/basics/{basic}/physical_activity_pros_cons",
     *   tags={"basic_user"},
     *   summary="Creates new physical_activity_pros_cons record for user",
     *   description="",
     *   operationId="store",
     *   consumes={"application/x-www-form-urlencoded"},
     *   produces={"application/json"},
     *   @SWG\Parameter(name="basic", in="path", description="ID of basic_user",required=true,type="integer", format="int64"),
     *   @SWG\Parameter(name="improve_my_health", in="formData", description="", required=true, type="string", enum={"strongly disagree","somewhat disagree","neutral","somewhat agree","strongly agree"}),
     *   @SWG\Parameter(name="concerned_exercise_makes_me_uncomfortable", in="formData", description="", required=true, type="string", enum={"strongly disagree","somewhat disagree","neutral","somewhat agree","strongly agree"}),
     *   @SWG\Parameter(name="improve_energy_levels", in="formData", description="", required=true, type="string", enum={"strongly disagree","somewhat disagree","neutral","somewhat agree","strongly agree"}),
     *   @SWG\Parameter(name="improve_my_mood", in="formData", description="", required=true, type="string", enum={"strongly disagree","somewhat disagree","neutral","somewhat agree","strongly agree"}),
     *   @SWG\Parameter(name="improve_my_mental_abilities", in="formData", description="", required=true, type="string", enum={"strongly disagree","somewhat disagree","neutral","somewhat agree","strongly agree"}),
     *   @SWG\Parameter(name="relieve_stress", in="formData", description="", required=true, type="string", enum={"strongly disagree","somewhat disagree","neutral","somewhat agree","strongly agree"}),
     *   @SWG\Parameter(name="improve_my_self_image", in="formData", description="", required=true, type="string", enum={"strongly disagree","somewhat disagree","neutral","somewhat agree","strongly agree"}),
     *   @SWG\Parameter(name="become_more_attractive", in="formData", description="", required=true, type="string", enum={"strongly disagree","somewhat disagree","neutral","somewhat agree","strongly agree"}),
     *   @SWG\Parameter(name="concerned_others_see_me_exercising", in="formData", description="", required=true, type="string", enum={"strongly disagree","somewhat disagree","neutral","somewhat agree","strongly agree"}),
     *   @SWG\Parameter(name="lose_or_maintain_weight", in="formData", description="", required=true, type="string", enum={"strongly disagree","somewhat disagree","neutral","somewhat agree","strongly agree"}),
     *   @SWG\Parameter(name="meet_people_or_make_friends", in="formData", description="", required=true, type="string", enum={"strongly disagree","somewhat disagree","neutral","somewhat agree","strongly agree"}),
     *   @SWG\Parameter(name="waste_to_much_time", in="formData", description="", required=true, type="string", enum={"strongly disagree","somewhat disagree","neutral","somewhat agree","strongly agree"}),
     *   @SWG\Parameter(name="less_time_for_others", in="formData", description="", required=true, type="string", enum={"strongly disagree","somewhat disagree","neutral","somewhat agree","strongly agree"}),
     *
     *   @SWG\Response(response="405",description="Invalid input"),
     *   security={{
     *     "oauth2_security": {"read-general", "manage-user-data"}
     *   }}
     * )
     */
    public function store(Request $request, Basic $basic)
    {
        $rules = [
            'improve_health' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'uncomfortable' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'improve_energy' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'improve_mood' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'improve_mental' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'relieve_stress' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'self_image' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'more_attractive' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'others_see' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'lose_weight' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'make_friends' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'waste_time' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'less_time' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
        ];

        $this->validate($request, $rules);

        if(!$basic->isVerified()) {
            return $this->errorResponse('The client must be a verified user.', 409);
        }

        $data = $request->all();
        $data['basic_id'] = $basic->id;

        $physical_activity_pros_cons = PhysicalActivityProsCons::create($data);

        return $this->showOne($physical_activity_pros_cons);
    }
}

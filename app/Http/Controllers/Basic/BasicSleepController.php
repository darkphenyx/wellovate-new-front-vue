<?php

namespace App\Http\Controllers\Basic;

use App\Basic;
use App\Sleep;
use App\Transformers\SleepTransformer;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class BasicSleepController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . SleepTransformer::class)->only(['store', 'update']);
        $this->middleware('scope:read-general')->only('index');
        $this->middleware('scope:manage-user-data')->only(['store', 'update']);
        $this->middleware('can:view,basic')->only(['index']);
        $this->middleware('can:make,basic')->only(['store', 'update']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * @SWG\Get(
     *     path="/basics/{basic}/sleeps",
     *     summary="Get sleeps data for user",
     *     description="Returns all sleeps data",
     *     operationId="index",
     *     tags={"basic_user"},
     *     produces={"application/json"},
     *     @SWG\Parameter(description="ID of basic_user",in="path",name="basic",required=true,type="integer",format="int64"),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(
     *              type="array",
     *              @SWG\Items(
     *                  type="object",
     *                  @SWG\Property(property="identifier", type="integer"),
     *                  @SWG\Property(property="user_identifier", type="integer"),
     *                  @SWG\Property(property="times_awake_per_sleep_cycle", type="integer"),
     *                  @SWG\Property(property="times_restless_per_sleep_cycle", type="integer"),
     *                  @SWG\Property(property="time_spent_asleep", type="number"),
     *                  @SWG\Property(property="latency_before_sleep", type="number"),
     *                  @SWG\Property(property="sleep_position", type="string"),
     *                  @SWG\Property(property="time_awake_when_restless", type="number"),
     *                  @SWG\Property(property="time_spent_in_bed", type="number"),
     *                  @SWG\Property(property="wake_up_time", type="string"),
     *                  @SWG\Property(property="time_out_of_bed", type="string"),
     *
     *                  @SWG\Property(property="creation_date", type="string"),
     *                  @SWG\Property(property="last_changed", type="string"),
     *                  @SWG\Property(property="deleted_date", type="string")
     *              )
     *         )
     *     ),
     *     @SWG\Response(response="400",description="Invalid ID supplied"),
     *     @SWG\Response(response="404",description="Data not found"),
     *     security={{"oauth2_security": {"read-general", "manage-user-data"}}}
     * )
     */
    public function index(Basic $basic)
    {
        $sleeps = $basic->sleep;

        return $this->showAll($sleeps);
    }


    /**
     * @SWG\Post(
     *   path="/basics/{basic}/sleeps",
     *   tags={"basic_user"},
     *   summary="Creates new sleeps record for user",
     *   description="",
     *   operationId="store",
     *   consumes={"application/x-www-form-urlencoded"},
     *   produces={"application/json"},
     *   @SWG\Parameter(name="basic", in="path", description="ID of basic_user",required=true,type="integer", format="int64"),
     *   @SWG\Parameter(name="times_awake_per_sleep_cycle", in="formData", description="", required=true, type="integer"),
     *   @SWG\Parameter(name="times_restless_per_sleep_cycle", in="formData", description="", required=true, type="integer"),
     *   @SWG\Parameter(name="time_spent_asleep", in="formData", description="", required=true, type="number"),
     *   @SWG\Parameter(name="latency_before_sleep", in="formData", description="", required=true, type="number"),
     *   @SWG\Parameter(name="sleep_position", in="formData", description="", required=true, type="string", enum={"supine", "prone", "right lateral", "left lateral"}),
     *   @SWG\Parameter(name="time_awake_when_restless", in="formData", description="", required=true, type="number"),
     *   @SWG\Parameter(name="time_spent_in_bed", in="formData", description="", required=true, type="number"),
     *   @SWG\Parameter(name="wake_up_time", in="formData", description="", required=true, type="string"),
     *   @SWG\Parameter(name="time_out_of_bed", in="formData", description="", required=true, type="string"),
     *
     *   @SWG\Response(response="405",description="Invalid input"),
     *   security={{
     *     "oauth2_security": {"read-general", "manage-user-data"}
     *   }}
     * )
     */
    public function store(Request $request, Basic $basic)
    {
        $rules = [
            'awake_per_sleep' => 'integer|nullable',
            'restless' => 'integer|nullable',
            'sleep_duration' => 'required|numeric|min:10|max:999',
            'sleep_latency' => 'required|numeric|min:10|max:999',
            'sleep_position' => 'required|in:supine,prone,right lateral,left lateral|nullable',
            'awake_while_restless' => 'required|numeric|min:10|max:999',
            'length_in_bed' => 'required|numeric|min:10|max:999',
            'wake_up' => 'required|date|date_format:Y-m-d H:i:s',
            'time_out_bed' => 'required|date|date_format:Y-m-d H:i:s',
        ];

        $this->validate($request, $rules);

        if(!$basic->isVerified()) {
            return $this->errorResponse('The client must be a verified user.', 409);
        }

        $data = $request->all();
        $data['basic_id'] = $basic->id;

        $sleep = Sleep::create($data);

        return $this->showOne($sleep);
    }
}

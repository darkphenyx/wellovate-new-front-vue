<?php

namespace App\Http\Controllers\Basic;

use App\Basic;
use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BasicQuestionnaireAssignmentController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('scope:read-general')->only('index');
        $this->middleware('can:view,basic')->only(['index']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Basic $basic)
    {
        $questionnaire_assignments = $basic->questionnaire_assignment;

        return $this->showAll($questionnaire_assignments);
    }
}

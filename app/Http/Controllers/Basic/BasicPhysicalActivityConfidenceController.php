<?php

namespace App\Http\Controllers\Basic;

use App\Basic;
use App\PhysicalActivityConfidence;
use App\Transformers\PhysicalActivityConfidenceTransformer;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class BasicPhysicalActivityConfidenceController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . PhysicalActivityConfidenceTransformer::class)->only(['store', 'update']);
        $this->middleware('scope:read-general')->only('index');
        $this->middleware('scope:manage-user-data')->only(['store', 'update']);
        $this->middleware('can:view,basic')->only(['index']);
        $this->middleware('can:make,basic')->only(['store', 'update']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * @SWG\Get(
     *     path="/basics/{basic}/physical_activity_confidences",
     *     summary="Get physical_activity_confidences data for user",
     *     description="Returns all physical_activity_confidences data",
     *     operationId="index",
     *     tags={"basic_user"},
     *     produces={"application/json"},
     *     @SWG\Parameter(description="ID of basic_user",in="path",name="basic",required=true,type="integer",format="int64"),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(
     *              type="array",
     *              @SWG\Items(
     *                  type="object",
     *                  @SWG\Property(property="identifier", type="integer"),
     *                  @SWG\Property(property="user_identifier", type="integer"),
     *                  @SWG\Property(property="am_tired", type="string"),
     *                  @SWG\Property(property="am_in_bad_mood", type="string"),
     *                  @SWG\Property(property="am_stressed", type="string"),
     *                  @SWG\Property(property="have_no_time", type="string"),
     *                  @SWG\Property(property="when_bad_weather", type="string"),
     *
     *                  @SWG\Property(property="creation_date", type="string"),
     *                  @SWG\Property(property="last_changed", type="string"),
     *                  @SWG\Property(property="deleted_date", type="string")
     *              )
     *         )
     *     ),
     *     @SWG\Response(response="400",description="Invalid ID supplied"),
     *     @SWG\Response(response="404",description="Data not found"),
     *     security={{"oauth2_security": {"read-general", "manage-user-data"}}}
     * )
     */
    public function index(Basic $basic)
    {
        $physical_activity_confidences = $basic->physical_activity_confidence;

        return $this->showAll($physical_activity_confidences);
    }

    /**
     * @SWG\Post(
     *   path="/basics/{basic}/physical_activity_confidences",
     *   tags={"basic_user"},
     *   summary="Creates new physical_activity_confidences record for user",
     *   description="",
     *   operationId="store",
     *   consumes={"application/x-www-form-urlencoded"},
     *   produces={"application/json"},
     *   @SWG\Parameter(name="basic", in="path", description="ID of basic_user",required=true,type="integer", format="int64"),
     *   @SWG\Parameter(name="am_tired", in="formData", description="", required=true, type="string", enum={"strongly disagree","somewhat disagree","neutral","somewhat agree","strongly agree"}),
     *   @SWG\Parameter(name="am_in_bad_mood", in="formData", description="", required=true, type="string", enum={"strongly disagree","somewhat disagree","neutral","somewhat agree","strongly agree"}),
     *   @SWG\Parameter(name="am_stressed", in="formData", description="", required=true, type="string", enum={"strongly disagree","somewhat disagree","neutral","somewhat agree","strongly agree"}),
     *   @SWG\Parameter(name="have_no_time", in="formData", description="", required=true, type="string", enum={"strongly disagree","somewhat disagree","neutral","somewhat agree","strongly agree"}),
     *   @SWG\Parameter(name="when_bad_weather", in="formData", description="", required=true, type="string", enum={"strongly disagree","somewhat disagree","neutral","somewhat agree","strongly agree"}),
     *
     *   @SWG\Response(response="405",description="Invalid input"),
     *   security={{
     *     "oauth2_security": {"read-general", "manage-user-data"}
     *   }}
     * )
     */
    public function store(Request $request, Basic $basic)
    {
        $rules = [
            'tired' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'bad_mood' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'stressed' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'no_time' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'bad_weather' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
        ];

        $this->validate($request, $rules);

        if(!$basic->isVerified()) {
            return $this->errorResponse('The client must be a verified user.', 409);
        }

        $data = $request->all();
        $data['basic_id'] = $basic->id;

        $physical_activity_confidence = PhysicalActivityConfidence::create($data);

        return $this->showOne($physical_activity_confidence);
    }
}

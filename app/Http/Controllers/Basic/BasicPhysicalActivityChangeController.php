<?php

namespace App\Http\Controllers\Basic;

use App\Basic;
use App\PhysicalActivityChange;
use App\Transformers\PhysicalActivityChangeTransformer;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class BasicPhysicalActivityChangeController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . PhysicalActivityChangeTransformer::class)->only(['store', 'update']);
        $this->middleware('scope:read-general')->only('index');
        $this->middleware('scope:manage-user-data')->only(['store', 'update']);
        $this->middleware('can:view,basic')->only(['index']);
        $this->middleware('can:make,basic')->only(['store', 'update']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * @SWG\Get(
     *     path="/basics/{basic}/physical_activity_changes",
     *     summary="Get physical_activity_changes data for user",
     *     description="Returns all physical_activity_changes data",
     *     operationId="index",
     *     tags={"basic_user"},
     *     produces={"application/json"},
     *     @SWG\Parameter(description="ID of basic_user",in="path",name="basic",required=true,type="integer",format="int64"),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(
     *              type="array",
     *              @SWG\Items(
     *                  type="object",
     *                  @SWG\Property(property="identifier", type="integer"),
     *                  @SWG\Property(property="user_identifier", type="integer"),
     *                  @SWG\Property(property="days_per_week_more_than_30_minutes_exercise", type="string"),
     *                  @SWG\Property(property="how_long_for_5_or_more_days", type="string"),
     *                  @SWG\Property(property="going_forward_can_you_for_5_days", type="string"),
     *
     *                  @SWG\Property(property="creation_date", type="string"),
     *                  @SWG\Property(property="last_changed", type="string"),
     *                  @SWG\Property(property="deleted_date", type="string")
     *              )
     *         )
     *     ),
     *     @SWG\Response(response="400",description="Invalid ID supplied"),
     *     @SWG\Response(response="404",description="Data not found"),
     *     security={{"oauth2_security": {"read-general", "manage-user-data"}}}
     * )
     */
    public function index(Basic $basic)
    {
        $physical_activity_changes = $basic->physical_activity_change;

        return $this->showAll($physical_activity_changes);
    }


    /**
     * @SWG\Post(
     *   path="/basics/{basic}/physical_activity_changes",
     *   tags={"basic_user"},
     *   summary="Creates new physical_activity_changes record for user",
     *   description="",
     *   operationId="store",
     *   consumes={"application/x-www-form-urlencoded"},
     *   produces={"application/json"},
     *   @SWG\Parameter(name="basic", in="path", description="ID of basic_user",required=true,type="integer", format="int64"),
     *   @SWG\Parameter(name="days_per_week_more_than_30_minutes_exercise", in="formData", description="", required=true, type="string", enum={"0","1","2","3","4","5","6","7"}),
     *   @SWG\Parameter(name="how_long_for_5_or_more_days", in="formData", description="", required=true, type="string", enum={"less than 6 months","6 months or more"}),
     *   @SWG\Parameter(name="going_forward_can_you_for_5_days", in="formData", description="", required=true, type="string", enum={"No and not planning to","Yes and plan to start in next 6 months","Yes and plan to start in next 30 days"}),
     *
     *   @SWG\Response(response="405",description="Invalid input"),
     *   security={{
     *     "oauth2_security": {"read-general", "manage-user-data"}
     *   }}
     * )
     */
    public function store(Request $request, Basic $basic)
    {
        $rules = [
            'on_average' => 'required|in:0,1,2,3,4,5,6,7',
            'how_long' => 'required|in:less than 6 months,6 months or more',
            'going_forward' => 'required|in:No and not planning to,Yes and plan to start in next 6 months,Yes and plan to start in next 30 days',
        ];

        $this->validate($request, $rules);

        if(!$basic->isVerified()) {
            return $this->errorResponse('The client must be a verified user.', 409);
        }

        $data = $request->all();
        $data['basic_id'] = $basic->id;

        $physical_activity_change = PhysicalActivityChange::create($data);

        return $this->showOne($physical_activity_change);
    }
}

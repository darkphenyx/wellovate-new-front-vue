<?php

namespace App\Http\Controllers\Basic;

use App\Basic;
use App\Http\Controllers\ApiController;
use App\Question;
use App\QuestionnaireAnswer;
use App\QuestionnaireAssignment;
use App\QuestionnaireName;
use App\QuestionnaireQuestion;
use App\User;
use Illuminate\Http\Request;

class BasicQuestionAnswerController extends ApiController
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $basic, QuestionnaireName $questionnaire_name)
    {
        $assignment = QuestionnaireAssignment::where('basic_id', '=', $basic->id)->where('questionnaire_name_id', '=', $questionnaire_name->id)->firstOrFail();

        //dd($assignment->answered);

        if($assignment['answered'] == 1){
            return $this->errorResponse('This questionnaire has already been answered.', 409);
        }

        if(!$basic->isVerified()) {
            return $this->errorResponse('The user must be verified.', 409);
        }

        $requestBody = $request->json()->all();

        $rules = [
            'questions' => 'required|array',
            'questions.*.question_identification' => 'required_with:questions|integer|exists:questions,id',
            'questions.*.answer' => 'required_with:questions',
        ];

        $this->validate($request, $rules);

        $questionnaire_answers = $questionnaire_name->questionnaire_questions->sortBy('question_id');

        if(count($questionnaire_answers) != count($requestBody['questions'])){
            return $this->errorResponse('The number of questions you are trying to answer is incorrect for the questionnaire', 409);
        }

        foreach ($requestBody['questions'] as $val){
            $question = Question::find($val['question_identification']);

            switch ($question->type_id) {
                case 1;
                    // Boolean
                    if(is_bool($val['answer']) === false){
                        return $this->errorResponse($question->question . ' must be answered as a boolean.', 409);
                    }
                    break;
                case 2;
                    // String
                    if(is_string($val['answer']) === false){
                        return $this->errorResponse($question->question . ' must be answered as a string.', 409);
                    }
                    break;
                case 3;
                    // Integer
                    if(is_int($val['answer']) === false){
                        return $this->errorResponse($question->question . ' must be answered as an integer.', 409);
                    }
                    break;
                case 4;
                    // Double
                    if(is_numeric($val['answer']) === false){
                        return $this->errorResponse($question->question . ' must be answered as a double.', 409);
                    }
                    break;
                case 5;
                    // Enum
                    $enums = Question::find($val['question_identification'])->enums;
                    $validEnums = array();
                    foreach ($enums as $v){
                        $validEnums[] = $v['answer_choice'];
                    }
                    if(!in_array($val['answer'], $validEnums)){
                        $needed = "";
                        $lastElement = end($validEnums);
                        //dd($lastElement);
                        foreach ($validEnums as $ve){
                            $needed .= $ve;
                            if($ve != $lastElement){
                                $needed .= ', ';
                            }
                        }
                        return $this->errorResponse($question->question . ' must be answered with one of the following ' . $needed, 409);
                    }
                    break;
                default:
                    dd("Something failed");
            }
        }

        foreach($requestBody['questions'] as $val){
            $questionnaire_answer = new QuestionnaireAnswer;
            $questionnaire_answer->basic_id = $basic->id;
            $questionnaire_answer->question_id = $val['question_identification'];
            $questionnaire_answer->question_answer = $val['answer'];
            $questionnaire_answer->save();
        }

        $assignment->answered = 1;
        $assignment->save();

        $sendArray = array();
        $sendArray['response'] = 'success';

        return $this->simpleResponse(['data' => $sendArray], 200);
    }
}


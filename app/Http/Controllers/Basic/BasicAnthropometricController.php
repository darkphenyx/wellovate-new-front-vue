<?php

namespace App\Http\Controllers\Basic;

use App\Anthropometric;
use App\Basic;
use App\Transformers\AnthropometricTransformer;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class BasicAnthropometricController extends ApiController
{

    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . AnthropometricTransformer::class)->only(['store']);
        $this->middleware('scope:read-general')->only('index');
        $this->middleware('scope:manage-user-data')->only(['store', 'update']);
        $this->middleware('can:view,basic')->only(['index']);
        $this->middleware('can:make,basic')->only(['store', 'update']);

    }

    /**
     * Display a listing of the resource.
     *
     * @param Basic $basic
     * @return \Illuminate\Http\Response
     */

    /**
     * @SWG\Get(
     *     path="/basics/{basic}/anthropometrics",
     *     summary="Get anthropometric data for user",
     *     description="Returns all anthropometric data",
     *     operationId="index",
     *     tags={"basic_user"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="ID of basic_user",
     *         in="path",
     *         name="basic",
     *         required=true,
     *         type="integer",
     *         format="int64"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(
     *              type="array",
     *              @SWG\Items(
     *                  type="object",
     *                  @SWG\Property(property="identifier", type="integer"),
     *                  @SWG\Property(property="user_identifier", type="integer"),
     *                  @SWG\Property(property="bicep_circumference", type="integer"),
     *                  @SWG\Property(property="biological_sex", type="string"),
     *                  @SWG\Property(property="percentage_body_fat", type="number"),
     *                  @SWG\Property(property="users_height", type="number"),
     *                  @SWG\Property(property="bmi", type="number"),
     *                  @SWG\Property(property="t_score_bone_density", type="number"),
     *                  @SWG\Property(property="bust_circumference", type="number"),
     *                  @SWG\Property(property="fitzpatrick", type="string"),
     *                  @SWG\Property(property="height_again", type="number"),
     *                  @SWG\Property(property="ideal_weight", type="number"),
     *                  @SWG\Property(property="lean_weight", type="number"),
     *                  @SWG\Property(property="body_water_percentage", type="number"),
     *                  @SWG\Property(property="thigh_circumference", type="number"),
     *                  @SWG\Property(property="waist_circumference", type="number"),
     *                  @SWG\Property(property="water_content", type="number"),
     *                  @SWG\Property(property="total_weight", type="number"),
     *                  @SWG\Property(property="creation_date", type="string"),
     *                  @SWG\Property(property="last_changed", type="string"),
     *                  @SWG\Property(property="deleted_date", type="string")
     *              )
     *         )
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="Invalid ID supplied"
     *     ),
     *     @SWG\Response(
     *         response="404",
     *         description="Data not found"
     *     ),
     *     security={{
     *     "oauth2_security": {"read-general", "manage-user-data"}
     *      }}
     * )
     */
    public function index(Basic $basic)
    {
        $anthropometrics = $basic->anthropometric;

        return $this->showAll($anthropometrics);
    }

    /**
     * @param Request $request
     * @param Basic $basic
     * @return \Illuminate\Http\JsonResponse
     */

    /**
     * @SWG\Post(
     *   path="/basics/{basic}/anthropometrics",
     *   tags={"basic_user"},
     *   summary="Creates new anthro record for user",
     *   description="",
     *   operationId="store",
     *   consumes={"application/x-www-form-urlencoded"},
     *   produces={"application/json"},
     *   @SWG\Parameter(
     *     name="basic",
     *     in="path",
     *     description="ID of basic_user",
     *     required=true,
     *     type="integer",
     *     format="int64"
     *   ),
     *   @SWG\Parameter(name="bicep_circumference", in="formData", description="", required=true, type="number"),
     *   @SWG\Parameter(name="biological_sex", in="formData", description="", required=true, type="string"),
     *   @SWG\Parameter(name="percentage_body_fat", in="formData", description="", required=true, type="number"),
     *   @SWG\Parameter(name="users_height", in="formData", description="", required=true, type="number"),
     *   @SWG\Parameter(name="bmi", in="formData", description="", required=true, type="number"),
     *   @SWG\Parameter(name="t_score_bone_density", in="formData", description="", required=true, type="number"),
     *   @SWG\Parameter(name="bust_circumference", in="formData", description="", required=true, type="number"),
     *   @SWG\Parameter(name="fitzpatrick", in="formData", description="", required=true, type="string"),
     *   @SWG\Parameter(name="height_again", in="formData", description="", required=true, type="number"),
     *   @SWG\Parameter(name="ideal_weight", in="formData", description="", required=true, type="number"),
     *   @SWG\Parameter(name="lean_weight", in="formData", description="", required=true, type="number"),
     *   @SWG\Parameter(name="body_water_percentage", in="formData", description="", required=true, type="number"),
     *   @SWG\Parameter(name="thigh_circumference", in="formData", description="", required=true, type="number"),
     *   @SWG\Parameter(name="waist_circumference", in="formData", description="", required=true, type="number"),
     *   @SWG\Parameter(name="water_content", in="formData", description="", required=true, type="number"),
     *   @SWG\Parameter(name="total_weight", in="formData", description="", required=true, type="number"),
     *   @SWG\Response(response="405",description="Invalid input"),
     *   security={{
     *     "oauth2_security": {"read-general", "manage-user-data"}
     *   }}
     * )
     */
    public function store(Request $request, Basic $basic)
    {
        $rules = [
            'bicep_circum' => 'required|numeric|min:4|max:99',
            'biologic_sex' => 'required|in:male,female,intersex',
            'body_fat_percent' => 'required|numeric|min:2|max:100',
            'body_height' => 'required|numeric|min:2|max:300',
            'body_mass_index' => 'required|numeric|min:2|max:99',
            'bone_density' => 'required|numeric|max:99',
            'bust_circum' => 'required|numeric|min:2|max:199',
            'fitzpatrick_skin' => 'required|in:I,II,III,IV,V,VI',
            'height' => 'required|numeric|min:2|max:400',
            'ideal_body_weight' => 'required|numeric|min:2|max:1000',
            'lean_body_weight' => 'required|numeric|min:2|max:1000',
            'body_water' => 'required|numeric|min:2|max:100',
            'thigh_circum' => 'required|numeric|min:2|max:200',
            'waist_circum' => 'required|numeric|min:2|max:200',
            'water_content' => 'required|numeric|min:2|max:100',
            'weight' => 'required|numeric|min:2|max:1000',
        ];

        $this->validate($request, $rules);

        if(!$basic->isVerified()) {
            return $this->errorResponse('The client must be a verified user.', 409);
        }

        $data = $request->all();
        $data['basic_id'] = $basic->id;

        $anthropometric = Anthropometric::create($data);

        return $this->showOne($anthropometric);
    }
}

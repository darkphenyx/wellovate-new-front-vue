<?php

namespace App\Http\Controllers\Basic;

use App\Basic;
use App\Cardiovascular;
use App\Transformers\CardiovascularTransformer;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class BasicCardiovascularController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . CardiovascularTransformer::class)->only(['store']);
        $this->middleware('scope:read-general')->only('index');
        $this->middleware('scope:manage-user-data')->only(['store', 'update']);
        $this->middleware('can:view,basic')->only(['index']);
        $this->middleware('can:make,basic')->only(['store', 'update']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * @SWG\Get(
     *     path="/basics/{basic}/cardiovasculars",
     *     summary="Get cardiovascular data for user",
     *     description="Returns all cardiovascular data",
     *     operationId="index",
     *     tags={"basic_user"},
     *     produces={"application/json"},
     *     @SWG\Parameter(description="ID of basic_user",in="path",name="basic",required=true,type="integer",format="int64"),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(
     *              type="array",
     *              @SWG\Items(
     *                  type="object",
     *                  @SWG\Property(property="identifier", type="integer"),
     *                  @SWG\Property(property="user_identifier", type="integer"),
     *                  @SWG\Property(property="ankle_brachial", type="number"),
     *                  @SWG\Property(property="blood_pressure", type="string"),
     *                  @SWG\Property(property="diastolic", type="integer"),
     *                  @SWG\Property(property="systolic", type="integer"),
     *                  @SWG\Property(property="endothelial", type="number"),
     *                  @SWG\Property(property="reactive_hyperenia", type="number"),
     *                  @SWG\Property(property="user_heart_rate", type="integer"),
     *                  @SWG\Property(property="user_heart_rate_max", type="integer"),
     *                  @SWG\Property(property="pulse_wave_velocity", type="number"),
     *                  @SWG\Property(property="qtc_interval", type="number"),
     *                  @SWG\Property(property="r_r_interval", type="number"),
     *                  @SWG\Property(property="oxygen_saturation", type="number"),
     *                  @SWG\Property(property="pulse_oximetry", type="number"),
     *                  @SWG\Property(property="creation_date", type="string"),
     *                  @SWG\Property(property="last_changed", type="string"),
     *                  @SWG\Property(property="deleted_date", type="string")
     *              )
     *         )
     *     ),
     *     @SWG\Response(response="400",description="Invalid ID supplied"),
     *     @SWG\Response(response="404",description="Data not found"),
     *     security={{"oauth2_security": {"read-general", "manage-user-data"}}}
     * )
     */
    public function index(Basic $basic)
    {
        $cardiovasculars = $basic->cardiovascular;

        return $this->showAll($cardiovasculars);
    }

    /**
     * @SWG\Post(
     *   path="/basics/{basic}/cardiovasculars",
     *   tags={"basic_user"},
     *   summary="Creates new cardiovascular record for user",
     *   description="",
     *   operationId="store",
     *   consumes={"application/x-www-form-urlencoded"},
     *   produces={"application/json"},
     *   @SWG\Parameter(name="basic", in="path", description="ID of basic_user",required=true,type="integer", format="int64"),
     *   @SWG\Parameter(name="ankle_brachial", in="formData", description="", required=true, type="number"),
     *   @SWG\Parameter(name="blood_pressure", in="formData", description="", required=true, type="string"),
     *   @SWG\Parameter(name="diastolic", in="formData", description="", required=true, type="integer"),
     *   @SWG\Parameter(name="systolic", in="formData", description="", required=true, type="integer"),
     *   @SWG\Parameter(name="endothelial", in="formData", description="", required=true, type="number"),
     *   @SWG\Parameter(name="reactive_hyperenia", in="formData", description="", required=true, type="number"),
     *   @SWG\Parameter(name="user_heart_rate", in="formData", description="", required=true, type="integer"),
     *   @SWG\Parameter(name="user_heart_rate_max", in="formData", description="", required=true, type="integer"),
     *   @SWG\Parameter(name="pulse_wave_velocity", in="formData", description="", required=true, type="number"),
     *   @SWG\Parameter(name="qtc_interval", in="formData", description="", required=true, type="number"),
     *   @SWG\Parameter(name="r_r_interval", in="formData", description="", required=true, type="number"),
     *   @SWG\Parameter(name="oxygen_saturation", in="formData", description="", required=true, type="number"),
     *   @SWG\Parameter(name="pulse_oximetry", in="formData", description="", required=true, type="number"),
     *   @SWG\Response(response="405",description="Invalid input"),
     *   security={{
     *     "oauth2_security": {"read-general", "manage-user-data"}
     *   }}
     * )
     */

    public function store(Request $request, Basic $basic)
    {
        $rules = [
            'ankle_brachial_index' => 'required|numeric|min:20|max:130',
            'blood_pressure' => 'required|alpha_num',
            'diastolic_blood_pressure' => 'required|integer|min:40|max:140',
            'systolic_blood_pressure' => 'required|integer|min:60|max:220',
            'endothelial' => 'required|numeric|min:22|max:99',
            'reactive_hyperenia' => 'required|numeric|min:22|max:99',
            'heart_rate' => 'required|integer|min:60|max:220',
            'max_heart_rate' => 'required|integer|min:60|max:220',
            'pulse_wave_velocity' => 'required|numeric|min:100|max:230',
            'qtc' => 'required|numeric|min:40|max:260',
            'rr_interval' => 'required|numeric|min:40|max:260',
            'o2_saturation' => 'required|numeric|min:15|max:99',
            'pulse_oximetry' => 'required|numeric|min:15|max:99',
        ];

        $this->validate($request, $rules);

        if(!$basic->isVerified()) {
            return $this->errorResponse('The client must be a verified user.', 409);
        }

        $data = $request->all();
        $data['basic_id'] = $basic->id;

        $cardiovascular = Cardiovascular::create($data);

        return $this->showOne($cardiovascular);
    }
}

<?php

namespace App\Http\Controllers\Basic;

use App\Basic;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class BasicController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('scope:read-general')->only(['index']);
        $this->middleware('can:view,basic')->only(['show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->allowedAdminAction();

        $basics = Basic::has('group_team')->get();

        return $this->showAll($basics);
    }

    /**
     * Display the specified resource.
     *
     * @param Basic $basic
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */

    /**
     * @SWG\Get(
     *     path="/basics/{basic}",
     *     summary="Get user information",
     *     description="Returns single basic user data",
     *     operationId="show",
     *     tags={"basic_user"},
     *     produces={"application/json"},
     *     @SWG\Parameter(description="ID of basic_user",in="path",name="basic",required=true,type="integer",format="int64"),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(
     *              type="array",
     *              @SWG\Items(
     *                  type="object",
     *                  @SWG\Property(property="identifier", type="integer"),
     *                  @SWG\Property(property="name", type="string"),
     *                  @SWG\Property(property="email", type="string"),
     *                  @SWG\Property(property="user_age", type="integer"),
     *                  @SWG\Property(property="user_sex", type="string"),
     *                  @SWG\Property(property="zip_code", type="integer"),
     *                  @SWG\Property(property="facebook", type="string"),
     *                  @SWG\Property(property="is_verified", type="integer"),
     *                  @SWG\Property(property="creation_date", type="string"),
     *                  @SWG\Property(property="last_changed", type="string"),
     *                  @SWG\Property(property="deleted_date", type="string")
     *              )
     *         )
     *     ),
     *     @SWG\Response(response="400",description="Invalid ID supplied"),
     *     @SWG\Response(response="404",description="Data not found"),
     *     security={{"oauth2_security": {"read-general", "manage-user-data"}}}
     * )
     */
    public function show(Basic $basic)
    {
        return $this->showOne($basic);
    }

}

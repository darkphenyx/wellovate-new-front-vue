<?php

namespace App\Http\Controllers\Basic;

use App\Basic;
use App\Behavior;
use App\Transformers\BehaviorTransformer;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class BasicBehaviorController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . BehaviorTransformer::class)->only(['store']);
        $this->middleware('scope:read-general')->only('index');
        $this->middleware('scope:manage-user-data')->only(['store', 'update']);
        $this->middleware('can:view,basic')->only(['index']);
        $this->middleware('can:make,basic')->only(['store', 'update']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * @SWG\Get(
     *     path="/basics/{basic}/behaviors",
     *     summary="Get behaviors data for user",
     *     description="Returns all behaviors data",
     *     operationId="index",
     *     tags={"basic_user"},
     *     produces={"application/json"},
     *     @SWG\Parameter(description="ID of basic_user",in="path",name="basic",required=true,type="integer",format="int64"),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(
     *              type="array",
     *              @SWG\Items(
     *                  type="object",
     *                  @SWG\Property(property="identifier", type="integer"),
     *                  @SWG\Property(property="user_identifier", type="integer"),
     *                  @SWG\Property(property="status_of_smoking", type="string"),
     *                  @SWG\Property(property="number_smoked_daily", type="integer"),
     *                  @SWG\Property(property="years_user_smoked", type="number"),
     *                  @SWG\Property(property="location_identifier", type="integer"),
     *                  @SWG\Property(property="number_vaped_daily", type="integer"),
     *                  @SWG\Property(property="route_identifier", type="integer"),
     *                  @SWG\Property(property="brush_your_teeth", type="boolean"),
     *                  @SWG\Property(property="floss_your_teeth", type="boolean"),
     *                  @SWG\Property(property="tv_in_bedroom", type="boolean"),
     *                  @SWG\Property(property="uv_exposure_per_day", type="number"),
     *                  @SWG\Property(property="creation_date", type="string"),
     *                  @SWG\Property(property="last_changed", type="string"),
     *                  @SWG\Property(property="deleted_date", type="string")
     *              )
     *         )
     *     ),
     *     @SWG\Response(response="400",description="Invalid ID supplied"),
     *     @SWG\Response(response="404",description="Data not found"),
     *     security={{"oauth2_security": {"read-general", "manage-user-data"}}}
     * )
     */
    public function index(Basic $basic)
    {
        $behaviors = $basic->behavior;

        return $this->showAll($behaviors);
    }


    /**
     * @SWG\Post(
     *   path="/basics/{basic}/behaviors",
     *   tags={"basic_user"},
     *   summary="Creates new behaviors record for user",
     *   description="",
     *   operationId="store",
     *   consumes={"application/x-www-form-urlencoded"},
     *   produces={"application/json"},
     *   @SWG\Parameter(name="basic", in="path", description="ID of basic_user",required=true,type="integer", format="int64"),
     *   @SWG\Parameter(name="status_of_smoking", in="formData", description="", required=true, type="string"),
     *   @SWG\Parameter(name="number_smoked_daily", in="formData", description="", required=true, type="integer"),
     *   @SWG\Parameter(name="years_user_smoked", in="formData", description="", required=true, type="number"),
     *   @SWG\Parameter(name="location_identifier", in="formData", description="", required=true, type="integer"),
     *   @SWG\Parameter(name="number_vaped_daily", in="formData", description="", required=true, type="integer"),
     *   @SWG\Parameter(name="route_identifier", in="formData", description="", required=true, type="integer"),
     *   @SWG\Parameter(name="brush_your_teeth", in="formData", description="", required=true, type="boolean"),
     *   @SWG\Parameter(name="floss_your_teeth", in="formData", description="", required=true, type="boolean"),
     *   @SWG\Parameter(name="tv_in_bedroom", in="formData", description="", required=true, type="boolean"),
     *   @SWG\Parameter(name="uv_exposure_per_day", in="formData", description="", required=true, type="number"),
     *   @SWG\Response(response="405",description="Invalid input"),
     *   security={{
     *     "oauth2_security": {"read-general", "manage-user-data"}
     *   }}
     * )
     */
    public function store(Request $request, Basic $basic)
    {
        $rules = [
            'smoking_status' => 'required|in:never,quit in last 6 months,quit more than 6 months ago,current',
            'cigs_per_day' => 'required|integer|min:0|max:60',
            'years_smoked' => 'required|numeric|min:0|max:50',
            'location_id' => 'required|integer',
            'vape_draws_day' => 'required|integer|min:0|max:300',
            'route_id' => 'required|integer',
            'teeth_brushing' => 'required|boolean',
            'teeth_flossing' => 'required|boolean',
            'tv_bedroom' => 'required|boolean',
            'uv_exposure' => 'required|numeric|min:0|max:24',
        ];

        $this->validate($request, $rules);

        if(!$basic->isVerified()) {
            return $this->errorResponse('The client must be a verified user.', 409);
        }

        $data = $request->all();
        $data['basic_id'] = $basic->id;

        $behavior = Behavior::create($data);

        return $this->showOne($behavior);
    }
}

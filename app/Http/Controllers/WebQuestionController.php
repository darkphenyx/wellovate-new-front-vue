<?php

namespace App\Http\Controllers;

use App\Basic;
use App\QuestionnaireName;
use App\Title;
use App\Type;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WebQuestionController extends Controller
{
    public function questionGroup(){
        $user = User::find(Auth::user()->id);
        $token = $user->createToken('This is my dev page Personal Access Client', ['manage-group-teams', 'read-general', 'manage-account', 'mixed-game-dev', 'manage-user-data', 'manage-perspectives'])->accessToken;
        $userID = Auth::user()->id;
        //dd($token);
        return view('questions.question-group', compact('token', 'userID'));
    }

    public function questionAdd(){
        $user = User::find(Auth::user()->id);
        $token = $user->createToken('This is my dev page Personal Access Client', ['manage-group-teams', 'read-general', 'manage-account', 'mixed-game-dev', 'manage-user-data', 'manage-perspectives'])->accessToken;
        $userID = Auth::user()->id;
        $titles = Title::all();
        $types = Type::all();
        //dd($titles);
        return view('questions.question-add', compact('token', 'userID', 'titles', 'types'));
    }

    public function questionnaireCreate(){
        $user = User::find(Auth::user()->id);
        $token = $user->createToken('This is my dev page Personal Access Client', ['manage-group-teams', 'read-general', 'manage-account', 'mixed-game-dev', 'manage-user-data', 'manage-perspectives'])->accessToken;
        $titles = Title::all();
        return view('questions.questionnaire-create', compact('token', 'titles'));
    }

    public function questionnaireUsers(){
        $user = User::find(Auth::user()->id);
        $token = $user->createToken('This is my dev page Personal Access Client', ['manage-group-teams', 'read-general', 'manage-account', 'mixed-game-dev', 'manage-user-data', 'manage-perspectives'])->accessToken;
        $questionnaire_names = QuestionnaireName::all();
        $basics = Basic::all();
        return view('questions.questionnaire-users', compact('token', 'questionnaire_names', 'basics'));
    }
}

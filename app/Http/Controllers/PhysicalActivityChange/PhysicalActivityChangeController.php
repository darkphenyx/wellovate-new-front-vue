<?php

namespace App\Http\Controllers\PhysicalActivityChange;

use App\PhysicalActivityChange;
use App\Transformers\PhysicalActivityChangeTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class PhysicalActivityChangeController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . PhysicalActivityChangeTransformer::class)->only(['store', 'update']);
        $this->middleware('scope:read-general')->only('index');
        $this->middleware('scope:manage-user-data')->only(['store', 'show', 'update']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->allowedAdminAction();

        $physical_activity_changes = PhysicalActivityChange::all();

        return $this->showAll($physical_activity_changes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'basic_id' => 'required|integer',
            'on_average' => 'required|in:0,1,2,3,4,5,6,7',
            'how_long' => 'required|in:less than 6 months,6 months or more',
            'going_forward' => 'required|in:No and not planning to,Yes and plan to start in next 6 months,Yes and plan to start in next 30 days',
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $physical_activity_change = PhysicalActivityChange::create($data);

        return $this->showOne($physical_activity_change, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(PhysicalActivityChange $physical_activity_change)
    {
        return $this->showOne($physical_activity_change);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(PhysicalActivityChange $physical_activity_change)
    {
        $physical_activity_change->delete();

        return $this->showOne($physical_activity_change);
    }
}

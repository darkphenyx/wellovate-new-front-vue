<?php

namespace App\Http\Controllers\PhysicalActivityAssessment;

use App\PhysicalActivityAssessment;
use App\Transformers\PhysicalActivityAssessmentTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class PhysicalActivityAssessmentController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . PhysicalActivityAssessmentTransformer::class)->only(['store', 'update']);
        $this->middleware('scope:read-general')->only('index');
        $this->middleware('scope:manage-user-data')->only(['store', 'show', 'update']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->allowedAdminAction();

        $physical_activity_assessments = PhysicalActivityAssessment::all();

        return $this->showAll($physical_activity_assessments);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'basic_id' => 'required|integer|unique:physical_activity_assessments,basic_id',
            'walking' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'hiking' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'running' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'bicycling' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'weight_training' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'calisthenics' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'swimming' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'yoga' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'tennis' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'table_tennis' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'aerobic' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'kayaking' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'martial_arts' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'basketball' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'soccer' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'golf' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'skiing' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'volleyball' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'gardening' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'skating' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'children' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $physical_activity_assessment = PhysicalActivityAssessment::create($data);

        return $this->showOne($physical_activity_assessment, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(PhysicalActivityAssessment $physical_activity_assessment)
    {
        return $this->showOne($physical_activity_assessment);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PhysicalActivityAssessment $physical_activity_assessment)
    {
        $rules = [
            'walking' => 'in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'hiking' => 'in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'running' => 'in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'bicycling' => 'in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'weight_training' => 'in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'calisthenics' => 'in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'swimming' => 'in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'yoga' => 'in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'tennis' => 'in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'table_tennis' => 'in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'aerobic' => 'in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'kayaking' => 'in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'martial_arts' => 'in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'basketball' => 'in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'soccer' => 'in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'golf' => 'in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'skiing' => 'in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'volleyball' => 'in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'gardening' => 'in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'skating' => 'in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'children' => 'in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
        ];

        if($request->has('walking')){
            $physical_activity_assessment->walking = $request->walking;
        }

        if($request->has('hiking')){
            $physical_activity_assessment->hiking = $request->hiking;
        }

        if($request->has('running')){
            $physical_activity_assessment->running = $request->running;
        }

        if($request->has('bicycling')){
            $physical_activity_assessment->bicycling = $request->bicycling;
        }

        if($request->has('weight_training')){
            $physical_activity_assessment->weight_training = $request->weight_training;
        }

        if($request->has('calisthenics')){
            $physical_activity_assessment->calisthenics = $request->calisthenics;
        }

        if($request->has('swimming')){
            $physical_activity_assessment->swimming = $request->swimming;
        }

        if($request->has('yoga')){
            $physical_activity_assessment->yoga = $request->yoga;
        }

        if($request->has('tennis')){
            $physical_activity_assessment->tennis = $request->tennis;
        }

        if($request->has('table_tennis')){
            $physical_activity_assessment->table_tennis = $request->table_tennis;
        }

        if($request->has('aerobic')){
            $physical_activity_assessment->aerobic = $request->aerobic;
        }

        if($request->has('kayaking')){
            $physical_activity_assessment->kayaking = $request->kayaking;
        }

        if($request->has('martial_arts')){
            $physical_activity_assessment->martial_arts = $request->martial_arts;
        }

        if($request->has('basketball')){
            $physical_activity_assessment->basketball = $request->basketball;
        }

        if($request->has('soccer')){
            $physical_activity_assessment->soccer = $request->soccer;
        }

        if($request->has('golf')){
            $physical_activity_assessment->golf = $request->golf;
        }

        if($request->has('skiing')){
            $physical_activity_assessment->skiing = $request->skiing;
        }

        if($request->has('volleyball')){
            $physical_activity_assessment->volleyball = $request->volleyball;
        }

        if($request->has('gardening')){
            $physical_activity_assessment->gardening = $request->gardening;
        }

        if($request->has('skating')){
            $physical_activity_assessment->skating = $request->skating;
        }

        if($request->has('children')){
            $physical_activity_assessment->children = $request->children;
        }

        if(!$physical_activity_assessment->isDirty()){
            return $this->errorResponse('You need to specify a different value to update', 422);
        }

        $physical_activity_assessment->save();

        return $this->showOne($physical_activity_assessment);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(PhysicalActivityAssessment $physical_activity_assessment)
    {
        $physical_activity_assessment->delete();

        return $this->showOne($physical_activity_assessment);
    }
}

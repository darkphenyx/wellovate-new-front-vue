<?php

namespace App\Http\Controllers\BehaviorRoute;

use App\BehaviorRoute;
use App\Transformers\BehaviorRouteTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class BehaviorRouteController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . BehaviorRouteTransformer::class)->only(['store', 'update']);
        $this->middleware('scope:read-general')->only('index');
        $this->middleware('scope:manage-user-data')->only(['store', 'show', 'update']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->allowedAdminAction();

        $behavior_routes = BehaviorRoute::all();

        return $this->showAll($behavior_routes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|string|max:191',
            'address' => 'required|string|max:191',
            'lat' => 'required|numeric|min:-360|max:360',
            'lng' => 'required|numeric|min:-360|max:360',
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $behavior_route = BehaviorRoute::create($data);

        return $this->showOne($behavior_route, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(BehaviorRoute $behavior_route)
    {
        return $this->showOne($behavior_route);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BehaviorRoute $behavior_route)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(BehaviorRoute $behavior_route)
    {
        $behavior_route->delete();

        return $this->showOne($behavior_route);
    }
}

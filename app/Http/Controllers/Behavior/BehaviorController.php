<?php

namespace App\Http\Controllers\Behavior;

use App\Behavior;
use App\Transformers\BehaviorTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class BehaviorController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . BehaviorTransformer::class)->only(['store', 'update']);
        $this->middleware('scope:read-general')->only('index');
        $this->middleware('scope:manage-user-data')->only(['store', 'show', 'update']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->allowedAdminAction();

        $behaviors = Behavior::all();

        return $this->showAll($behaviors);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'basic_id' => 'required|integer',
            'smoking_status' => 'required|in:never,quit in last 6 months,quit more than 6 months ago,current',
            'cigs_per_day' => 'required|integer|min:0|max:60',
            'years_smoked' => 'required|numeric|min:0|max:50',
            'location_id' => 'required|integer',
            'vape_draws_day' => 'required|integer|min:0|max:300',
            'route_id' => 'required|integer',
            'teeth_brushing' => 'required|boolean',
            'teeth_flossing' => 'required|boolean',
            'tv_bedroom' => 'required|boolean',
            'uv_exposure' => 'required|numeric|min:0|max:24',
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $behavior = Behavior::create($data);

        return $this->showOne($behavior, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Behavior $behavior)
    {
        return $this->showOne($behavior);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Behavior $behavior)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Behavior $behavior)
    {
        $behavior->delete();

        return $this->showOne($behavior);
    }
}

<?php

namespace App\Http\Controllers\PhysicalActivityEfficacy;

use App\PhysicalActivityEfficacy;
use App\Transformers\PhysicalActivityEfficacyTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class PhysicalActivityEfficacyController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . PhysicalActivityEfficacyTransformer::class)->only(['store', 'update']);
        $this->middleware('scope:read-general')->only('index');
        $this->middleware('scope:manage-user-data')->only(['store', 'show', 'update']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->allowedAdminAction();

        $physical_activity_efficacy = PhysicalActivityEfficacy::all();

        return $this->showAll($physical_activity_efficacy);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'basic_id' => 'required|integer',
            'seek_information' => 'required|in:never,almost never,sometimes,often,very often',
            'track_exercise' => 'required|in:never,almost never,sometimes,often,very often',
            'around_issues' => 'required|in:never,almost never,sometimes,often,very often',
            'set_reminders' => 'required|in:never,almost never,sometimes,often,very often',
            'is_rewarding' => 'required|in:never,almost never,sometimes,often,very often',
            'take_actions' => 'required|in:never,almost never,sometimes,often,very often',
            'focus_benefits' => 'required|in:never,almost never,sometimes,often,very often',
            'set_goals' => 'required|in:never,almost never,sometimes,often,very often',
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $physical_activity_efficacy = PhysicalActivityEfficacy::create($data);

        return $this->showOne($physical_activity_efficacy, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(PhysicalActivityEfficacy $physical_activity_efficacy)
    {
        return $this->showOne($physical_activity_efficacy);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(PhysicalActivityEfficacy $physical_activity_efficacy)
    {
        $physical_activity_efficacy->delete();

        return $this->showOne($physical_activity_efficacy);
    }
}

<?php

namespace App\Http\Controllers;

use App\FitbitAuth;
use App\Traits\OutsideConnection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class FitbitController extends Controller
{
    use OutsideConnection;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('fitbit');
    }

    public function goAuth()
    {
        $url = env("FITBIT_AUTH_URI").
            '?response_type=code&client_id='.
            env("FITBIT_CLIENT_ID").
            '&redirect_uri='.
            env("FITBIT_CALLBACK_URL").
            '&scope=activity%20nutrition%20heartrate%20location%20nutrition%20profile%20settings%20sleep%20social%20weight&prompt=login%20consent';
        return Redirect::to($url);
    }

    public function getAuth(Request $request)
    {
        $client_id = env("FITBIT_CLIENT_ID", "22CKVG");
        $client_secret = env("FITBIT_CLIENT_SECRET", "4633be904f96f9e24cc06cfbd8644839");
        $request_uri = env("FITBIT_TOKEN_REQUEST", 'https://api.fitbit.com/oauth2/token');
        $redirect_uri = env("FITBIT_CALLBACK_URL", 'http://wellovateapi.dev/fitbit/authorized');
        $code = $request->input('code');


        $token_response = $this->getToken($request_uri, $client_id, $client_secret, $code, $redirect_uri);

        $token_response = \GuzzleHttp\json_decode($token_response, true);
        if(array_key_exists('access_token', $token_response)){
            $fitbit = FitbitAuth::firstOrNew(['basic_id' => Auth::user()->id]);
            $fitbit->access_token = $token_response['access_token'];
            $fitbit->refresh_token = $token_response['refresh_token'];
            $fitbit->token_type = $token_response['token_type'];
            $fitbit->fitbit_user_id = $token_response['user_id'];
            $fitbit->save();
            return view('fitbit.authorized');
        }else{
            return view('home');
        }
    }
}

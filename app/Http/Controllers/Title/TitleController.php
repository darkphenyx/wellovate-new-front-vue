<?php

namespace App\Http\Controllers\Title;

use App\Http\Controllers\ApiController;
use App\Title;
use App\Transformers\TitleTransformer;
use Illuminate\Http\Request;

class TitleController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . TitleTransformer::class)->only(['store', 'update']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $titles = Title::all();

        return $this->showAll($titles);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->allowedAdminAction();

        $rules = [
            'coach_id' => 'required|integer',
            'question_title' => 'required|string'
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $title = Title::create($data);

        return $this->showOne($title, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Title $title)
    {
        return $this->showOne($title);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Title $title)
    {
        $this->allowedAdminAction();

        $title->delete();

        return $this->showOne($title);
    }
}

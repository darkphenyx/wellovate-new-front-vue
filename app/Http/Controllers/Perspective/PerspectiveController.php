<?php

namespace App\Http\Controllers\Perspective;

use App\Http\Controllers\ApiController;
use App\Perspective;
use Illuminate\Http\Request;

class PerspectiveController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perspectives = Perspective::all();

        return $this->showAll($perspectives);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Perspective $perspective)
    {
        return $this->showOne($perspective);
    }
}

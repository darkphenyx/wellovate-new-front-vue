<?php

namespace App\Http\Controllers\MixedRealityUse;

use App\MixedRealityUse;
use App\Transformers\MixedRealityUseTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class MixedRealityUseController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . MixedRealityUseTransformer::class)->only(['store', 'update']);
        $this->middleware('scope:read-general')->only('index');
        $this->middleware('scope:manage-user-data')->only(['store', 'show', 'update']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->allowedAdminAction();

        $mixed_reality_uses = MixedRealityUse::all();

        return $this->showAll($mixed_reality_uses);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'basic_id' => 'required|integer',
            'experience_id' => 'required|integer',
            'start_lobby' => 'required|date|date_format:Y-m-d H:i:s',
            'end_lobby' => 'required|date|date_format:Y-m-d H:i:s',
            'start_experience' => 'required|date|date_format:Y-m-d H:i:s',
            'end_experience' => 'required|date|date_format:Y-m-d H:i:s',
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $mixed_reality_us = MixedRealityUse::create($data);

        return $this->showOne($mixed_reality_us, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(MixedRealityUse $mixed_reality_us)
    {
        return $this->showOne($mixed_reality_us);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MixedRealityUse $mixed_reality_us)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(MixedRealityUse $mixed_reality_us)
    {
        $mixed_reality_us->delete();

        return $this->showOne($mixed_reality_us);
    }
}

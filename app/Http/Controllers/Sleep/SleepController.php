<?php

namespace App\Http\Controllers\Sleep;

use App\Sleep;
use App\Transformers\SleepTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class SleepController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . SleepTransformer::class)->only(['store', 'update']);
        $this->middleware('scope:read-general')->only('index');
        $this->middleware('scope:manage-user-data')->only(['store', 'show', 'update']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->allowedAdminAction();

        $sleeps = Sleep::all();

        return $this->showAll($sleeps);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'basic_id' => 'required|integer',
            'awake_per_sleep' => 'integer|nullable',
            'restless' => 'integer|nullable',
            'sleep_duration' => 'required|numeric|min:10|max:999',
            'sleep_latency' => 'required|numeric|min:10|max:999',
            'sleep_position' => 'required|in:supine,prone,right lateral,left lateral|nullable',
            'awake_while_restless' => 'required|numeric|min:10|max:999',
            'length_in_bed' => 'required|numeric|min:10|max:999',
            'wake_up' => 'required|date|date_format:Y-m-d H:i:s',
            'time_out_bed' => 'required|date|date_format:Y-m-d H:i:s',
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $sleep = Sleep::create($data);

        return $this->showOne($sleep, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Sleep $sleep)
    {
        return $this->showOne($sleep);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sleep $sleep)
    {
        $sleep->delete();

        return $this->showOne($sleep);
    }
}

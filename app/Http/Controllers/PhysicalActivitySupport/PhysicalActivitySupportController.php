<?php

namespace App\Http\Controllers\PhysicalActivitySupport;

use App\PhysicalActivitySupport;
use App\Transformers\PhysicalActivitySupportTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class PhysicalActivitySupportController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . PhysicalActivitySupportTransformer::class)->only(['store', 'update']);
        $this->middleware('scope:read-general')->only('index');
        $this->middleware('scope:manage-user-data')->only(['store', 'show', 'update']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->allowedAdminAction();

        $physical_activity_support = PhysicalActivitySupport::all();

        return $this->showAll($physical_activity_support);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'basic_id' => 'required|integer',
            'encourage_exercise' => 'required|in:never,almost never,sometimes,often,very often',
            'offered_participate' => 'required|in:never,almost never,sometimes,often,very often',
            'exercised_with' => 'required|in:never,almost never,sometimes,often,very often',
            'discouraged' => 'required|in:never,almost never,sometimes,often,very often',
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $physical_activity_support = PhysicalActivitySupport::create($data);

        return $this->showOne($physical_activity_support, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(PhysicalActivitySupport $physical_activity_support)
    {
        return $this->showOne($physical_activity_support);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(PhysicalActivitySupport $physical_activity_support)
    {
        $physical_activity_support->delete();

        return $this->showOne($physical_activity_support);
    }
}

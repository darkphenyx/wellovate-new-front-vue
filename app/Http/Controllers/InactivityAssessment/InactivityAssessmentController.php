<?php

namespace App\Http\Controllers\InactivityAssessment;

use App\InactivityAssessment;
use App\Transformers\InactivityAssessmentTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class InactivityAssessmentController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . InactivityAssessmentTransformer::class)->only(['store', 'update']);
        $this->middleware('scope:read-general')->only('index');
        $this->middleware('scope:manage-user-data')->only(['store', 'show', 'update']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->allowedAdminAction();

        $inactives = InactivityAssessment::all();

        return $this->showAll($inactives);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'basic_id' => 'required|integer|unique:inactivity_assessments,basic_id',
            'time_watch_tv_weekday' => 'required|in:Less than 30 minutes,Between 30 minutes and 1 hour,Between 1 and 2 hours,Between 2 and 3 hours,Between 3 and 4 hours,Between 4 and 5 hours,Greater than 5 hours',
            'time_sitting_weekday' => 'required|in:Less than 30 minutes,Between 30 minutes and 1 hour,Between 1 and 2 hours,Between 2 and 3 hours,Between 3 and 4 hours,Between 4 and 5 hours,Greater than 5 hours',
            'time_watch_tv_weekend' => 'required|in:Less than 30 minutes,Between 30 minutes and 1 hour,Between 1 and 2 hours,Between 2 and 3 hours,Between 3 and 4 hours,Between 4 and 5 hours,Greater than 5 hours',
            'time_sitting_weekend' => 'required|in:Less than 30 minutes,Between 30 minutes and 1 hour,Between 1 and 2 hours,Between 2 and 3 hours,Between 3 and 4 hours,Between 4 and 5 hours,Greater than 5 hours',
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $inactivity = InactivityAssessment::create($data);

        return $this->showOne($inactivity, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(InactivityAssessment $inactivity)
    {
        return $this->showOne($inactivity);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InactivityAssessment $inactivity)
    {
        $rules = [
            'time_watch_tv_weekday' => 'in:Less than 30 minutes,Between 30 minutes and 1 hour,Between 1 and 2 hours,Between 2 and 3 hours,Between 3 and 4 hours,Between 4 and 5 hours,Greater than 5 hours',
            'time_sitting_weekday' => 'in:Less than 30 minutes,Between 30 minutes and 1 hour,Between 1 and 2 hours,Between 2 and 3 hours,Between 3 and 4 hours,Between 4 and 5 hours,Greater than 5 hours',
            'time_watch_tv_weekend' => 'in:Less than 30 minutes,Between 30 minutes and 1 hour,Between 1 and 2 hours,Between 2 and 3 hours,Between 3 and 4 hours,Between 4 and 5 hours,Greater than 5 hours',
            'time_sitting_weekend' => 'in:Less than 30 minutes,Between 30 minutes and 1 hour,Between 1 and 2 hours,Between 2 and 3 hours,Between 3 and 4 hours,Between 4 and 5 hours,Greater than 5 hours',
        ];

        if($request->has('time_watch_tv_weekday')){
            $inactivity->time_watch_tv_weekday = $request->time_watch_tv_weekday;
        }

        if($request->has('time_sitting_weekday')){
            $inactivity->time_sitting_weekday = $request->time_sitting_weekday;
        }

        if($request->has('time_watch_tv_weekend')){
            $inactivity->time_watch_tv_weekend = $request->time_watch_tv_weekend;
        }

        if($request->has('time_sitting_weekend')){
            $inactivity->time_sitting_weekend = $request->time_sitting_weekend;
        }

        if(!$inactivity->isDirty()){
            return $this->errorResponse('You need to specify a different value to update', 422);
        }

        $inactivity->save();

        return $this->showOne($inactivity);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(InactivityAssessment $inactivity)
    {
        $inactivity->delete();

        return $this->showOne($inactivity);
    }
}

<?php

namespace App\Http\Controllers\PhysicalActivityEnviro;

use App\PhysicalActivityEnviro;
use App\Transformers\PhysicalActivityEnviroTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class PhysicalActivityEnviroController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . PhysicalActivityEnviroTransformer::class)->only(['store', 'update']);
        $this->middleware('scope:read-general')->only('index');
        $this->middleware('scope:manage-user-data')->only(['store', 'show', 'update']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->allowedAdminAction();

        $physical_activity_enviros = PhysicalActivityEnviro::all();

        return $this->showAll($physical_activity_enviros);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'basic_id' => 'required|integer|unique:physical_activity_enviros,basic_id',
            'access_equipment' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'walk_traffic' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'walk_safe' => 'required|in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $physical_activity_enviro = PhysicalActivityEnviro::create($data);

        return $this->showOne($physical_activity_enviro, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(PhysicalActivityEnviro $physical_activity_enviro)
    {
        return $this->showOne($physical_activity_enviro);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PhysicalActivityEnviro $physical_activity_enviro)
    {
        $rules = [
            'access_equipment' => 'in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'walk_traffic' => 'in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
            'walk_safe' => 'in:strongly disagree,somewhat disagree,neutral,somewhat agree,strongly agree',
        ];

        if($request->has('access_equipment')){
            $physical_activity_enviro->access_equipment = $request->access_equipment;
        }

        if($request->has('walk_traffic')){
            $physical_activity_enviro->walk_traffic = $request->walk_traffic;
        }

        if($request->has('walk_safe')){
            $physical_activity_enviro->walk_safe = $request->walk_safe;
        }

        if(!$physical_activity_enviro->isDirty()){
            return $this->errorResponse('You need to specify a different value to update', 422);
        }

        $physical_activity_enviro->save();

        return $this->showOne($physical_activity_enviro);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(PhysicalActivityEnviro $physical_activity_enviro)
    {
        $physical_activity_enviro->delete();

        return $this->showOne($physical_activity_enviro);
    }
}

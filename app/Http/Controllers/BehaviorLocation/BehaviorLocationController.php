<?php

namespace App\Http\Controllers\BehaviorLocation;

use App\BehaviorLocation;
use App\Transformers\BehaviorLocationTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class BehaviorLocationController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . BehaviorLocationTransformer::class)->only(['store', 'update']);
        $this->middleware('scope:read-general')->only('index');
        $this->middleware('scope:manage-user-data')->only(['store', 'show', 'update']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->allowedAdminAction();

        $behavior_locations = BehaviorLocation::all();

        return $this->showAll($behavior_locations);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|string|max:191',
            'address' => 'required|string|max:191',
            'lat' => 'required|numeric|min:-360|max:360',
            'lng' => 'required|numeric|min:-360|max:360',
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $behavior_location = BehaviorLocation::create($data);

        return $this->showOne($behavior_location, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(BehaviorLocation $behavior_location)
    {
        return $this->showOne($behavior_location);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BehaviorLocation $behavior_location)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(BehaviorLocation $behavior_location)
    {
        $behavior_location->delete();

        return $this->showOne($behavior_location);
    }
}

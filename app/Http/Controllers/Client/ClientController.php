<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ClientController extends ApiController
{
    public function __construct()
    {
        $this->middleware('auth:api')->except(['getClient']);
    }

    public function getClient(Request $request)
    {
        $oauth_client = DB::table('oauth_clients')->where('id', 1)->first();
        $secret = $oauth_client->secret;
        $url = env("API_TOKEN_URL", "https://www.wellovate.com/api/oauth/token");
        $fields = array(
            'grant_type' => 'client_credentials',
            'client_id' => '1',
            'client_secret' => $secret
        );
        $fields_string = '';
        foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
        rtrim($fields_string, '&');
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $fields_string,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            // $this->simpleResponse($response, 200);
            echo $response;
        }
    }
}

<?php

namespace App\Http\Controllers\GoalName;

use App\GoalName;
use App\Transformers\GoalNameTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class GoalNameController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . GoalNameTransformer::class)->only(['store', 'update']);
        $this->middleware('scope:read-general')->only('index');
        $this->middleware('scope:manage-user-data')->only(['store', 'show', 'update']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $goal_names = GoalName::all();

        return $this->showAll($goal_names);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|string|max:500',
        ];

        $this->validate($request, $rules);

        $data = $request->all();
        $goal_name = GoalName::create($data);

        return $this->showOne($goal_name, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(GoalName $goal_name)
    {
        return $this->showOne($goal_name);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GoalName $goal_name)
    {
        $rules = [
            'name' => 'string|max:500',
        ];

        if($request->has('name')){
            $goal_name->name = $request->name;
        }

        if(!$goal_name->isDirty()){
            return $this->errorResponse('You need to specify a different value to update', 422);
        }

        $goal_name->save();

        return $this->showOne($goal_name);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(GoalName $goal_name)
    {
        $goal_name->delete();

        return $this->showOne($goal_name);
    }
}

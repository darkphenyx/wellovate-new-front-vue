<?php

namespace App\Http\Controllers\Coach;

use App\Coach;
use App\GroupLead;
use App\Transformers\GroupLeadTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class CoachGroupLeadController extends ApiController
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('transform.input:' . GroupLeadTransformer::class)->only(['store', 'update']);
        $this->middleware('can:view,coach')->only('index');
        $this->middleware('can:update-team,coach')->only(['store', 'update']);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Coach $coach
     * @return \Illuminate\Http\Response
     */
    public function index(Coach $coach)
    {
        $group_leads = $coach->group_lead;

        return $this->showAll($group_leads);
    }

    public function store(Request $request, Coach $coach)
    {
        $rules = [
            'name' => 'required|string'
        ];

        $this->validate($request, $rules);

        if(!$coach->isVerified()) {
            return $this->errorResponse('The client must be a verified user.', 409);
        }

        $data = $request->all();
        $data['coach_id'] = $coach->id;

        $group_lead = GroupLead::create($data);

        return $this->showOne($group_lead);
    }
}

<?php

namespace App\Http\Controllers\Coach;

use App\Coach;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class CoachController extends ApiController
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('can:view,coach')->only('show');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coaches = Coach::has('group_lead')->get();

        return $this->showAll($coaches);
    }

    /**
     * Display the specified resource.
     *
     * @param Coach $coach
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function show(Coach $coach)
    {
        return $this->showOne($coach);
    }

}

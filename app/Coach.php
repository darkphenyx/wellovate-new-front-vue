<?php

namespace App;

use App\GroupLead;
use App\Scopes\CoachScope;
use App\Transformers\CoachTransformer;

class Coach extends User
{
    public $transformer = CoachTransformer::class;

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new CoachScope);
    }

    public function group_lead()
    {
        return $this->hasMany(GroupLead::class);
    }
}

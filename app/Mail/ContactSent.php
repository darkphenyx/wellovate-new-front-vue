<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactSent extends Mailable
{
    use Queueable, SerializesModels;

    public $name,
        $companyName,
        $email,
        $phone,
        $comment,
        $route;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $companyName, $email, $phone, $comment, $route)
    {
        $this->name = $name;
        $this->companyName = $companyName;
        $this->email = $email;
        $this->phone = $phone;
        $this->comment = $comment;
        $this->route = $route;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = [
            'name' => $this->name,
            'companyName' => $this->companyName,
            'email' => $this->email,
            'phone' => $this->phone,
            'comment' => $this->comment
        ];
        return $this->view('emails.contact', $data)->subject('Contact Form From ' . $this->name. ': Coming from the ' . $this->route . ' page');
    }
}

<?php

namespace App;

use App\Transformers\AnthropometricTransformer;
use Illuminate\Database\Eloquent\Model;
use App\Basic;
use Illuminate\Database\Eloquent\SoftDeletes;

class Anthropometric extends Model
{
    use SoftDeletes;

    public $transformer = AnthropometricTransformer::class;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'basic_id',
        'bicep_circum',
        'biologic_sex',
        'body_fat_percent',
        'body_height',
        'body_mass_index',
        'bone_density',
        'bust_circum',
        'fitzpatrick_skin',
        'height',
        'ideal_body_weight',
        'lean_body_weight',
        'body_water',
        'thigh_circum',
        'waist_circum',
        'water_content',
        'weight',
    ];

    public function basic()
    {
        return $this->belongsTo(Basic::class);
    }
}

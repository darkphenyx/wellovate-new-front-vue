<?php

namespace App;

use App\Transformers\InactivityAssessmentTransformer;
use Illuminate\Database\Eloquent\Model;
use App\Basic;
use Illuminate\Database\Eloquent\SoftDeletes;

class InactivityAssessment extends Model
{
    use SoftDeletes;

    public $transformer = InactivityAssessmentTransformer::class;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'basic_id',
        'time_watch_tv_weekday',
        'time_sitting_weekday',
        'time_watch_tv_weekend',
        'time_sitting_weekend',
    ];

    public function basic()
    {
        return $this->belongsTo(Basic::class);
    }
}

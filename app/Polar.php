<?php

namespace App;

use App\Transformers\PolarTransformer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Polar extends Model
{
    use SoftDeletes;

    public $transformer = PolarTransformer::class;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'basic_id',
        'access_token',
        'token_type',
        'x_user_id',
    ];

    public function basic()
    {
        return $this->belongsTo(Basic::class);
    }
}

<?php

namespace App;

use App\Transformers\PhysicalActivityTransformer;
use Illuminate\Database\Eloquent\Model;
use App\Basic;
use Illuminate\Database\Eloquent\SoftDeletes;

class PhysicalActivity extends Model
{
    use SoftDeletes;

    public $transformer = PhysicalActivityTransformer::class;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'basic_id',
        'active_minutes',
        'cadence',
        'calories_burned',
        'distance',
        'exercise_minutes',
        'floors_climbed',
        'inactivity',
        'moderate_vigorous',
        'peak_accel',
        'sitting_time',
        'speed',
        'step_count',
    ];

    public function basic()
    {
        return $this->belongsTo(Basic::class);
    }
}

<?php

namespace App;

use App\Transformers\BehaviorLocationTransformer;
use Illuminate\Database\Eloquent\Model;
use App\Behavior;
use Illuminate\Database\Eloquent\SoftDeletes;

class BehaviorLocation extends Model
{
    use SoftDeletes;

    public $transformer = BehaviorLocationTransformer::class;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name',
        'address',
        'lat',
        'lng',
    ];

    public function behavior()
    {
        return $this->hasOne(Behavior::class, 'location_id');
    }
}

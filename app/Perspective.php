<?php

namespace App;

use App\Transformers\PerspectiveTransformer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Perspective extends Model
{
    use SoftDeletes;

    public $transformer = PerspectiveTransformer::class;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'title',
        'content',
        'tags'
    ];
}

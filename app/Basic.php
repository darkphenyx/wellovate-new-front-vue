<?php

namespace App;

use App\Anthropometric;
use App\Cardiovascular;
use App\Fitness;
use App\InactivityAssessment;
use App\Outcome;
use App\PhysicalActivity;
use App\PhysicalActivityAssessment;
use App\PhysicalActivityBarriers;
use App\PhysicalActivityChange;
use App\PhysicalActivityConfidence;
use App\PhysicalActivityEfficacy;
use App\PhysicalActivityEnviro;
use App\PhysicalActivityFun;
use App\PhysicalActivityProsCons;
use App\PhysicalActivitySupport;
use App\Scopes\BasicScope;
use App\Sleep;
use App\GroupTeam;
use App\MixedRealityUse;
use App\GoalUser;
use App\Behavior;
use App\Transformers\BasicTransformer;

class Basic extends User
{
    public $transformer = BasicTransformer::class;

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new BasicScope);
    }

    public function anthropometric()
    {
        return $this->hasMany(Anthropometric::class);
    }

    public function cardiovascular()
    {
        return $this->hasMany(Cardiovascular::class);
    }

    public function fitness()
    {
        return $this->hasMany(Fitness::class);
    }

    public function inactivity()
    {
        return $this->hasOne(InactivityAssessment::class);
    }

    public function outcome()
    {
        return $this->hasOne(Outcome::class);
    }

    public function physical_activity()
    {
        return $this->hasMany(PhysicalActivity::class);
    }

    public function physical_activity_assessment()
    {
        return $this->hasOne(PhysicalActivityAssessment::class);
    }

    public function physical_activity_barrier()
    {
        return $this->hasMany(PhysicalActivityBarriers::class);
    }

    public function physical_activity_change()
    {
        return $this->hasMany(PhysicalActivityChange::class);
    }

    public function physical_activity_confidence()
    {
        return $this->hasMany(PhysicalActivityConfidence::class);
    }

    public function physical_activity_efficacy()
    {
        return $this->hasMany(PhysicalActivityEfficacy::class);
    }

    public function physical_activity_enviro()
    {
        return $this->hasOne(PhysicalActivityEnviro::class);
    }

    public function physical_activity_fun()
    {
        return $this->hasMany(PhysicalActivityFun::class);
    }

    public function physical_activity_pros_con()
    {
        return $this->hasMany(PhysicalActivityProsCons::class);
    }

    public function physical_activity_support()
    {
        return $this->hasMany(PhysicalActivitySupport::class);
    }

    public function sleep()
    {
        return $this->hasMany(Sleep::class);
    }

    public function group_team()
    {
        return $this->hasMany(GroupTeam::class);
    }

    public function mixed_reality_us()
    {
        return $this->hasMany(MixedRealityUse::class);
    }

    public function goal_user()
    {
        return $this->hasMany(GoalUser::class);
    }

    public function behavior()
    {
        return $this->hasMany(Behavior::class);
    }

    public function fitbit_auth()
    {
        return $this->hasOne(FitbitAuth::class);
    }

    public function polar()
    {
        return $this->hasOne(Polar::class);
    }

    public function questionnaire_assignment()
    {
        return $this->hasMany(QuestionnaireAssignment::class);
    }

    public function questionnaire_answer()
    {
        return $this->hasMany(QuestionnaireAnswer::class);
    }
}

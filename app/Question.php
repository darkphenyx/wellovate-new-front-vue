<?php

namespace App;

use App\Transformers\QuestionTransformer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Question extends Model
{
    use SoftDeletes;

    public $transformer = QuestionTransformer::class;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'title_id',
        'type_id',
        'question'
    ];

    public function enums()
    {
        return $this->hasMany(Enum::class);
    }

    public function title()
    {
        return $this->belongsTo(Title::class);
    }

    public function type()
    {
        return $this->belongsTo(Type::class);
    }

    public function questionnaire_questions()
    {
        return $this->hasMany(QuestionnaireQuestion::class);
    }

    public function questionnaire_answers()
    {
        return $this->hasMany(QuestionnaireAnswer::class);
    }
}

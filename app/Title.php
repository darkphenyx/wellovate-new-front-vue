<?php

namespace App;

use App\Transformers\TitleTransformer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Coach;

class Title extends Model
{
    use SoftDeletes;

    public $transformer = TitleTransformer::class;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'coach_id',
        'question_title'
    ];

    public function coach()
    {
        return $this->belongsTo(Coach::class);
    }

    public function questions()
    {
        return $this->hasMany(Question::class);
    }
}

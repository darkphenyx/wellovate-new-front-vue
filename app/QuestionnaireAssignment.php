<?php

namespace App;

use App\Transformers\QuestionnaireAssignmentTransformer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class QuestionnaireAssignment extends Model
{
    use SoftDeletes;

    public $transformer = QuestionnaireAssignmentTransformer::class;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'basic_id',
        'questionnaire_name_id',
        'answered'
    ];

    public function basic()
    {
        return $this->belongsTo(Basic::class);
    }

    public function questionnaire_name()
    {
        return $this->belongsTo(QuestionnaireName::class);
    }
}

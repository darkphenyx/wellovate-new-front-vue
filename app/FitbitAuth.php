<?php

namespace App;

use App\Transformers\FitbitAuthTransformer;
use Illuminate\Database\Eloquent\Model;
use App\Basic;
use Illuminate\Database\Eloquent\SoftDeletes;

class FitbitAuth extends Model
{
    use SoftDeletes;

    public $transformer = FitbitAuthTransformer::class;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'basic_id',
        'access_token',
        'refresh_token',
        'token_type',
        'fitbit_user_id',
    ];

    public function basic()
    {
        return $this->belongsTo(Basic::class);
    }
}

<?php

namespace App;

use App\Transformers\SleepTransformer;
use Illuminate\Database\Eloquent\Model;
use App\Basic;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sleep extends Model
{
    use SoftDeletes;

    public $transformer = SleepTransformer::class;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'basic_id',
        'awake_per_sleep',
        'restless',
        'sleep_duration',
        'sleep_latency',
        'sleep_position',
        'awake_while_restless',
        'length_in_bed',
        'wake_up',
        'time_out_bed',
    ];

    public function basic()
    {
        return $this->belongsTo(Basic::class);
    }
}

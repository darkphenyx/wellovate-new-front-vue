<?php

namespace App;

use App\Transformers\TypeTransformer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Type extends Model
{
    use SoftDeletes;

    public $transformer = TypeTransformer::class;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'question_type'
    ];

    public function question()
    {
        return $this->hasMany(Question::class);
    }
}

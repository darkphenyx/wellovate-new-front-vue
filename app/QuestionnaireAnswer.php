<?php

namespace App;

use App\Transformers\QuestionnaireAnswerTransformer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class QuestionnaireAnswer extends Model
{
    use SoftDeletes;

    public $transformer = QuestionnaireAnswerTransformer::class;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'basic_id',
        'question_id',
        'question_answer'
    ];

    public function basic()
    {
        return $this->belongsTo(Basic::class);
    }

    public function question()
    {
        return $this->belongsTo(Question::class);
    }
}

<?php

namespace App;

use App\Transformers\PhysicalActivityEnviroTransformer;
use Illuminate\Database\Eloquent\Model;
use App\Basic;
use Illuminate\Database\Eloquent\SoftDeletes;

class PhysicalActivityEnviro extends Model
{
    use SoftDeletes;

    public $transformer = PhysicalActivityEnviroTransformer::class;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'basic_id',
        'access_equipment',
        'walk_traffic',
        'walk_safe',
    ];

    public function basic()
    {
        return $this->belongsTo(Basic::class);
    }
}

<?php

namespace App;

use App\Transformers\GroupTeamTransformer;
use Illuminate\Database\Eloquent\Model;
use App\Basic;
use App\GroupLead;
use Illuminate\Database\Eloquent\SoftDeletes;

class GroupTeam extends Model
{
    use SoftDeletes;

    public $transformer = GroupTeamTransformer::class;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'basic_id',
        'group_lead_id',
    ];

    public function basic()
    {
        return $this->belongsTo(Basic::class);
    }

    public function groupLead()
    {
        return $this->belongsTo(GroupLead::class);
    }
}

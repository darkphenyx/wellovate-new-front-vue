<?php

namespace App;

use App\Transformers\QuestionnaireNameTransformer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class QuestionnaireName extends Model
{
    use SoftDeletes;

    public $transformer = QuestionnaireNameTransformer::class;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name',
        'assigned'
    ];

    public function questionnaire_assignments()
    {
        return $this->hasMany(QuestionnaireAssignment::class);
    }

    public function questionnaire_questions()
    {
        return $this->hasMany(QuestionnaireQuestion::class);
    }
}

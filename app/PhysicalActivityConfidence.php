<?php

namespace App;

use App\Transformers\PhysicalActivityConfidenceTransformer;
use Illuminate\Database\Eloquent\Model;
use App\Basic;
use Illuminate\Database\Eloquent\SoftDeletes;

class PhysicalActivityConfidence extends Model
{
    use SoftDeletes;

    public $transformer = PhysicalActivityConfidenceTransformer::class;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'basic_id',
        'tired',
        'bad_mood',
        'stressed',
        'no_time',
        'bad_weather',
    ];

    public function basic()
    {
        return $this->belongsTo(Basic::class);
    }
}

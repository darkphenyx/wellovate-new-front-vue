<?php

namespace App;

use App\Transformers\CardiovascularTransformer;
use Illuminate\Database\Eloquent\Model;
use App\Basic;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cardiovascular extends Model
{
    use SoftDeletes;

    public $transformer = CardiovascularTransformer::class;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'basic_id',
        'ankle_brachial_index',
        'blood_pressure',
        'diastolic_blood_pressure',
        'systolic_blood_pressure',
        'endothelial',
        'reactive_hyperenia',
        'heart_rate',
        'max_heart_rate',
        'pulse_wave_velocity',
        'qtc',
        'rr_interval',
        'o2_saturation',
        'pulse_oximetry',
    ];

    public function basic()
    {
        return $this->belongsTo(Basic::class);
    }
}

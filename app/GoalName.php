<?php

namespace App;

use App\Transformers\GoalNameTransformer;
use Illuminate\Database\Eloquent\Model;
use App\GoalUser;
use Illuminate\Database\Eloquent\SoftDeletes;

class GoalName extends Model
{
    use SoftDeletes;

    public $transformer = GoalNameTransformer::class;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name'
    ];

    public function mixedUsers()
    {
        return $this->hasMany(GoalUser::class);
    }
}

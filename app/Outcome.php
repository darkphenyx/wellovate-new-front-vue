<?php

namespace App;

use App\Transformers\OutcomeTransformer;
use Illuminate\Database\Eloquent\Model;
use App\Basic;
use Illuminate\Database\Eloquent\SoftDeletes;

class Outcome extends Model
{
    use SoftDeletes;

    public $transformer = OutcomeTransformer::class;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'basic_id',
        'alzheimers',
        'gen_anxiety_disorder',
        'cardiovascular_disease',
        'coronary_heart_disease',
        'dementia',
        'depression',
        'diabetes',
        'healthy',
        'hypertension',
        'inactive',
        'mild_cognitive_impairment',
        'number_falls',
        'underweight',
        'normal_weight',
        'overweight',
        'obesity_1',
        'obesity_2',
        'obesity_3',
        'partial_sleep_deprivation',
        'prediabetes',
        'prehypertension',
        'subclinical_depression',
        'subjective_poor_sleep',
        'total_sleep_deprivation',
        'high_ldl_concentration',
        'high_ldl_particle',
        'high_triglycerides',
        'low_hdl',
    ];

    public function basic()
    {
        return $this->belongsTo(Basic::class);
    }
}

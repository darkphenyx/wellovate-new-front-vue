<?php

namespace App;

use App\Transformers\PhysicalActivityFunTransformer;
use Illuminate\Database\Eloquent\Model;
use App\Basic;
use Illuminate\Database\Eloquent\SoftDeletes;

class PhysicalActivityFun extends Model
{
    use SoftDeletes;

    public $transformer = PhysicalActivityFunTransformer::class;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'basic_id',
        'enjoy_exercise',
        'feel_good',
        'fun_exercising',
        'watching_tv',
        'enjoy_computer',
        'enjoy_reading',
    ];

    public function basic()
    {
        return $this->belongsTo(Basic::class);
    }
}

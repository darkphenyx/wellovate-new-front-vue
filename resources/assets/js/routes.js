import Home from './components/Home'
import HealthProvider from './components/Solutions/HealthProvider'
import EmployeeWellness from './components/Solutions/EmployeeWellness'
import CommunityWellness from './components/Solutions/CommunityWellness'
import Contact from './components/Contact/Contact'
import Login from './components/Login/Login'
import About from './components/About/AboutUs'
import Register from './components/Register/Register'
import Welcome from './components/LoggedIn/Welcome'
import QuestionAdd from './components/Questions/QuestionAdd'
import QuestionGroup from './components/Questions/QuestionGroup'
import QuestionnaireCreate from './components/Questions/QuestionnaireCreate'
import QuestionnaireUsers from './components/Questions/QuestionnaireUsers'
import ConnectDevices from './components/Devices/ConnectDevices'

const routes = [
    { path: '/', component: Home, name: 'Home' },
    { path: '/contact', component: Contact, name: 'Contact' },
    { path: '/login', component: Login, name: 'Login' },
    { path: '/about_us', component: About, name: 'About' },
    { path: '/health_provider', component: HealthProvider, name: 'Health Provider' },
    { path: '/employee_wellness', component: EmployeeWellness, name: 'Employee Wellness' },
    { path: '/community_wellness', component: CommunityWellness, name: 'Community Wellness' },
    { path: '/register', component: Register, name: 'Register' },
    { path: '/welcome', component: Welcome, name: 'Welcome', meta: { requireLogin: true, requiresAuth: false } },
    { path: '/question/question_group', component: QuestionGroup, name: 'QuestionGroup', meta: { requireLogin: true, requiresAuth: true } },
    { path: '/question/question_add', component: QuestionAdd, name: 'QuestionAdd', meta: { requireLogin: true, requiresAuth: true } },
    { path: '/question/questionnaire_create', component: QuestionnaireCreate, name: 'QuestionnaireCreate', meta: { requireLogin: true, requiresAuth: true } },
    { path: '/question/questionnaire_users', component: QuestionnaireUsers, name: 'QuestionnaireUsers', meta: { requireLogin: true, requiresAuth: true } },
    { path: '/devices/connect', component: ConnectDevices, name: 'ConnectDevices', meta: { requireLogin: true, requiresAuth: false } },
];

export default routes;
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state:{
        quest_types:{},
        quest_type_array:{},
        liveAPI: 'http://www.admin.wellovate.com/wellovateAPI/public/index.php/api',
        testAPI: 'http://wellovate/api',
        loggedIn: false,
        userInfo: {}
    },
    getters: {
        getQuestType: state => {
            return state.quest_types;
        },
        getQuestTypeArray: state => {
            return state.quest_type_array;
        },
        getUserInfo: state => {
            return state.userInfo;
        }
    },
    mutations: {
        updateQuestType: (state, payload) => {
            state.quest_types = payload;
        },
        updateQuestTypeArray: (state, payload) => {
            state.quest_type_array = payload;
        },
        updateLoggedIn: (state, payload) => {
            state.loggedIn = true;
        },
        updateUserInfo: (state, payload) => {
            state.userInfo = payload;
        }
    },
    actions: {
        updateQuestType: (context, payload) => {
            context.commit('updateQuestType', payload);
        },
        updateQuestTypeArray: (context, payload) => {
            context.commit('updateQuestTypeArray', payload);
        },
        updateLoggedIn: (context, payload) => {
            context.commit('updateLoggedIn', payload)
        },
        updateUserInfo: (context, payload) => {
            context.commit('updateUserInfo', payload);
        }
    }
});
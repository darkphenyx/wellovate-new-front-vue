
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import Vue from 'vue';
import VueRouter from 'vue-router';
import VueCarousel from 'vue-carousel';
import routes from './routes';
import App from './App.vue';
import { store } from './store/store';
window.swal = require('sweetalert2');
require('./bootstrap');

Vue.use(VueRouter);
Vue.use(VueCarousel);

/**
 * Uncomment below when compiling to production
 */
Vue.config.devtools = false;
Vue.config.debug = false;
Vue.config.silent = true;

const router = new VueRouter({
    mode: 'history',
    routes
});

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requireLogin)) {
        const record = to.matched.find(record => record.meta.requireLogin === true);
        if (record.meta.requireLogin) {
            if (!store.state.loggedIn || Object.keys(store.state.userInfo).length === 0) {
                next({ path: '/login' })
            } else {
                if (record.meta.requiresAuth) {
                    if (!store.state.userInfo.admin == 'true') {
                        next({ path: '/login' })
                    } else {
                        next()
                    }
                } else {
                    next()
                }
            }
        }
    } else {
        next()
    }
});

window.events = new Vue();

new Vue({
    el: '#app',
    render: h => h(App),
    router,
    store
});

/*require('./bootstrap');
window.swal = require('sweetalert2');
require('bootstrap-responsive-tabs');
window.Vue = require('vue');
import { store } from './store/store';

Vue.component('passport-personal-access-tokens', require('./components/passport/PersonalAccessTokens.vue'));

Vue.component(
    'passport-clients',
    require('./components/passport/Clients.vue'));

Vue.component(
    'passport-authorized-clients',
    require('./components/passport/AuthorizedClients.vue'));

const app = new Vue({
    el: '#app',
    store
});*/

/*window.Vue = require('vue');

/!**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 *!/

import Vue from 'vue';

import VueRouter from 'vue-router';
Vue.use(VueRouter);

import VueAxios from 'vue-axios';
import axios from 'axios';
Vue.use(VueAxios, axios);

import App from './App.vue';
import Home from './components/Home.vue';
import Services from './components/Services.vue';
import Products from './components/Products.vue';
import About from './components/About.vue';
import Contact from './components/Contact.vue';
import NavBar from './components/Navbar.vue';

const routes = [
    {path: '', component: Home},
    {path: '/services', component: Services},
    {path: '/products', component: Products},
    {path: '/about', component: About},
    {path: '/contact', component: Contact}
];

const router = new VueRouter({routes});

Vue.component('navbar',{
    props: ['links'],
    template: NavBar});

var app = new Vue({
    el: '#app',
    router: router,
    render: h => h(App)
});*/


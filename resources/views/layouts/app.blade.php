<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" style="height: 77px;" href="{{ route('main') }}">
                        <img src="{{asset('images/logo.png')}}" style="height: 100%; margin-left: 0px;"  alt="Wellovate Home"/>
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @guest
                            <li><a href="{{ route('login') }}">Login</a></li>
                        @else
                            @if(Auth::user()->admin === 'true')
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Admin Actions<span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="{{route('personal-tokens')}}">My Tokens</a></li>
                                    <li><a href="{{route('personal-clients')}}">My Clients</a></li>
                                    <li><a href="{{route('authorized-clients')}}">Authorized Clients</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="{{route('question-group')}}">Create Question Group Title</a></li>
                                    <li><a href="{{route('question-add')}}">Add Question To Group</a></li>
                                    <li><a href="{{route('questionnaire-create')}}">Create Questionnaire From Available Questions</a></li>
                                    <li><a href="{{route('questionnaire-users')}}">Assign Questionnaires To Users</a></li>
                                </ul>
                            </li>
                            @endif
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>
    <footer class="footer-basic navbar-fixed-bottom">
        <div class="container" style="margin: 0; width: 100%;">
            <div class="col-sm-12 col-xs-12 footer-basic-right" style="text-align: center">
                <div class="col-sm-6 col-xs-6">
                    <p class="footer-links" style="margin-bottom: 10px; margin-top: 10px;">
                        <a href="#">Terms of Use</a>
                        |
                        <a href="#">Privacy Policy</a>
                        |
                        <a href="#">Disclaimer</a>
                    </p>
                </div>
                <div class="col-sm-6 col-xs-6" style="margin-top: 10px;">
                    <p class="footer-company-name">Copyright 2017 Wellovate, LLC. All rights reserved.</p>
                </div>
            </div>
        </div>
    </footer>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>

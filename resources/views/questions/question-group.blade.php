@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="form-horizontal" id="contact_form" style="padding-top: 40px;">
                <div class="form-group">
                    <div class="col-xs-12">
                        <label for="group-title">Please Enter Question Group Title</label>
                        <input id="group-title" placeholder="Enter Question Group Title" class="form-control" type="text">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <button type="button" class="btn btn-warning" id="group-submit-button" onclick="createTitle()">Submit <span class="glyphicon glyphicon-send"></span></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<script>
    function createTitle() {
        let token = {!! json_encode($token) !!};
        let userID = {!! json_encode($userID) !!};
        let title = $("#group-title").val();
        if(title === ""){
            swal('Field is empty', 'You have to fill out the Question Group Title', 'error');
            return;
        }
        let config = {
            headers: {'Authorization': 'Bearer ' + token, 'Content-Type': 'application/json'}
        };
        let data = {"coach_identifier": userID, "title": title};
        //console.log(data);
        axios.post('/api/titles', data, config)
            .then(response => {
                //console.log(response.data)
                swal({
                    title: "Title Created",
                    type: 'success',
                    html: 'Identifier:<br>' + '<b>' + response.data.data.identifier + '</b><br>' + 'Title:<br>' + '<b>' + response.data.data.title + '</b><br>' + 'Creation Date:<br>' + '<b>' + response.data.data.creation_date + '</b>'
                })
                $("#group-title").val('');
            })
            .catch(error => {
                //console.log(error.response)
                swal({
                    title: 'Error Status:<br>' + error.response.data.code,
                    type: 'error',
                    html: 'Error Message:<br>' + '<b>'+error.response.data.error+'</b>'
                })
            })
    }
</script>
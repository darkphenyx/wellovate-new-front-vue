<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-alpha1/jquery.min.js"></script>
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="form-horizontal" id="contact-form" style="padding-top: 40px;">
            @if(count($titles) > 0)
                <div class="form-group">
                    <div class="col-xs-12">
                        <label for="group-title">Please Select the Question Group Title To Add a Question</label>
                        <select id="group-title" class="form-control">
                            <option value="" selected="selected" disabled="disabled">-Select Question Group-</option>
                            @foreach($titles as $title)
                                <option value="{{$title->id}}">{{$title->question_title}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            @else
            <p>There are no question titles available</p>
            @endif
                
            @if(count($types) > 0)
                <div class="form-group">
                    <div class="col-xs-12">
                        <label for="group-type">Please Select The Question Answer Type</label>
                        <select id="group-type" class="form-control" onchange="seeIfEnum();">
                            <option value="" selected="selected" disabled="disabled">-Select Question Type-</option>
                            @foreach($types as $type)
                                <option value="{{$type->id}}">{{$type->question_type}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            @else
                <p>There are no question types available</p>
            @endif

                <div class="form-group">
                    <div class="col-xs-12">
                        <label for="question">Enter the Question</label>
                        <input type="text" id="question" class="form-control" placeholder="Enter Question Here">
                    </div>
                </div>

                <div id="enumDiv" style="display: none;">
                    <label for="enumGroup">Here are for enum answers</label>
                    <div class="input_fields_wrap" id="enumGroup">
                        <br>
                        <div class="form-group">
                            <input type="text" class="form-control" name="mytext[]">
                        </div>
                    </div>
                    <button class="add_field_button">Add More Fields</button>
                </div>

                <div class="form-group">
                    <div class="col-xs-12">
                        <button type="button" class="btn btn-warning" id="group-submit-button" onclick="createQuestion()">Submit <span class="glyphicon glyphicon-send"></span></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<script>
    $(document).ready(function () {
        let max_fields = 10;
        let wrapper = $(".input_fields_wrap");
        let add_button = $(".add_field_button");

        let x = 1;
        $(add_button).click(function(e){ //on add input button click
            e.preventDefault();
            if(x < max_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper).append('<div class="form-group"><input type="text" class="form-control" name="mytext[]"/><a href="#" class="remove_field">Remove</a></div>'); //add input box
            }
        });

        $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
            e.preventDefault(); $(this).parent('div').remove(); x--;
        })
    });

    function seeIfEnum() {
        let type = $("#group-type").val();
        if(type === '5'){
            $("#enumDiv").show();
        }else{
            $("#enumDiv").hide();
        }
    }

    function createQuestion() {
        let token = {!! json_encode($token) !!};
        let questionGroup = $("#group-title").val();
        let questionType = $("#group-type").val();
        let question = $("#question").val();
        let enumArray = [];
        let arrayError = false;

        if(questionGroup === null){
            swal('Field not selected', 'You have to select the Question Group Title', 'error');
            return;
        }

        if(questionType === null){
            swal('Field not selected', 'You have to select the Question Answer Type', 'error');
            return;
        }

        if(question === ""){
            swal('Field is empty', 'You have to fill out the actual question', 'error');
            return;
        }

        if(questionType === '5'){
            $('input[name^="mytext"]').each(function () {
                if($(this).val() === ""){
                    arrayError = true;
                }else{
                    enumArray.push($(this).val());
                }
            })
        }

        if(arrayError){
            swal('Empty Enum Answer', 'You cannot leave an enum answer blank', 'error');
            return;
        }

        let sendObject = {};
        let answerArray = [];

        if(questionType === '5'){
            sendObject.title_identifier = parseInt(questionGroup);
            sendObject.type_identifier = parseInt(questionType);
            sendObject.question = question;
            for(let i = 0; i < enumArray.length; i++){
                answerArray.push({"available_answer": enumArray[i]})
            }
            sendObject.answers = answerArray;
        }else{
            sendObject.title_identifier = parseInt(questionGroup);
            sendObject.type_identifier = parseInt(questionType);
            sendObject.question = question;
        }

        let config = {
            headers: {'Authorization': 'Bearer ' + token, 'Content-Type': 'application/json'}
        };

        axios.post('/api/question/add', sendObject, config)
            .then(response => {
                //console.log(response.data)
                swal({
                    title: "Question Created",
                    type: 'success',
                    html: 'Identifier:<br>' + '<b>' + response.data.data.identifier + '</b><br>' + 'Question:<br>' + '<b>' + response.data.data.question + '</b><br>'
                });

                setTimeout(function () {
                    window.location.reload(true);
                }, 2000);
            })
            .catch(error => {
                //console.log(error.response)
                swal({
                    title: 'Error Status:<br>' + error.response.data.code,
                    type: 'error',
                    html: 'Error Message:<br>' + '<b>'+error.response.data.error+'</b>'
                })
            })
    }
</script>
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="form-horizontal" id="contact-form" style="padding-top: 40px;">

                <div class="form-group">
                    <div class="col-xs-12">
                        <label for="question">Enter the Questionnaire Name</label>
                        <input type="text" id="questionnaire" class="form-control" placeholder="Enter Questionnaire Name Here">
                    </div>
                </div>

                @if(count($titles) > 0)
                    <div class="form-group">
                        <div class="col-xs-12">
                            <label for="group-title">Please Select the Question Group Title To Get Questions From</label>
                            <select id="group-title" class="form-control" onchange="getQuestions();">
                                <option value="" selected="selected" disabled="disabled">-Select Question Group-</option>
                                @foreach($titles as $title)
                                    <option value="{{$title->id}}">{{$title->question_title}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                @else
                    <p>There are no question titles available</p>
                @endif

                <table id="questionTable" class="table table-striped table-responsive" style="display: none; margin-bottom: 50px;">
                    <thead>
                        <tr>
                            <th>Add To Questionnaire</th>
                            <th>Question</th>
                            <th>ID</th>
                        </tr>
                    </thead>
                    <tbody id="questionBody">

                    </tbody>
                </table>
                <br>
                <div class="form-group" id="submitGroup" style="display: none; margin-bottom: 75px;">
                    <div class="col-xs-12">
                        <button type="button" class="btn btn-warning" id="group-submit-button" onclick="createQuestionnaire()">Submit <span class="glyphicon glyphicon-send"></span></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<script>
    let questionnaire_id = 0;
    function getQuestions() {
        $("#questionTable").show();
        $("#questionBody").empty();
        let titleID = $("#group-title").val();
        let token = {!! json_encode($token) !!};
        let config = {
            headers: {'Authorization': 'Bearer ' + token}
        };

        axios.get('/api/questions?title_identifier='+titleID+'&per_page=50', config)
            .then(response => {
                //console.log(response.data.data.length);
                if(response.data.data.length === 0){
                    swal('Group Title Empty', 'This title has no questions added yet. You need to add these first before you can proceed', 'error')
                }else{
                    //console.log(response.data.data);
                    for(let i = 0; i < response.data.data.length; i++){
                        let newRowContent = "<tr><td><input type='checkbox' name='myQuestions[]' value='"+response.data.data[i].identifier+"'></td><td>"+response.data.data[i].question+"</td><td>"+response.data.data[i].identifier+"</td></tr>";
                        $("#questionBody").append(newRowContent);
                    }
                    $("#submitGroup").show();
                }
            })
            .catch(error => {
                console.log(error.response)
            })
    }

    function createQuestionnaire() {
        let token = {!! json_encode($token) !!};
        let config = {
            headers: {'Authorization': 'Bearer ' + token, 'Content-Type': 'application/json'}
        };
        let questionArray = [];
        let questionnaireName = $("#questionnaire").val();

        if(questionnaireName === ""){
            swal('Questionnaire Name Empty', 'You need to enter a questionnaire name to proceed', 'error');
            return;
        }

        $('input[name^=myQuestions]').each(function () {
            if($(this).is(":checked")){
                //alert($(this).val());
                questionArray.push($(this).val());
            }
        });

        if(questionArray.length === 0){
            swal('No Questions Added', 'You must add questions before you can proceed', 'error');
            return;
        }

        let data1 = {"questionnaire_name": questionnaireName};
        let questionnaireArray = [];
        for(let j = 0; j < questionArray.length; j++){
            questionnaireArray.push({"question_identifier": questionArray[j]});
        }
        if(questionnaire_id === 0){
            axios.post('/api/questionnaire_names', data1, config)
                .then(response => {
                    //console.log(response.data)
                    questionnaire_id = response.data.data.identifier;
                    let data2 = {};
                    data2.questionnaire_name_identifier = parseInt(questionnaire_id);
                    data2.question_identifiers = questionnaireArray;
                    axios.post('/api/questionnaire_create', data2, config)
                        .then(response => {
                            //console.log(response.data)
                            swal('Questions Added to Questionnaire', 'The questions have been successfully added. To add from other question groups just select them and select the questions.', 'success');
                            $("#questionnaire").prop('disabled', true);
                            $("#group-title").find("option:selected").attr('disabled', 'disabled');
                        })
                        .catch(error => {
                            swal({
                                title: 'Error Status:<br>' + error.response.data.code,
                                type: 'error',
                                html: 'Error Message:<br>' + '<b>'+error.response.data.error+'</b>'
                            })
                        })
                })
                .catch(error => {
                    //console.log(error.response)
                    swal({
                        title: 'Error Status:<br>' + error.response.data.code,
                        type: 'error',
                        html: 'Error Message:<br>' + '<b>'+error.response.data.error+'</b>'
                    })
                })
        }else{
            let data2 = {};
            data2.questionnaire_name_identifier = parseInt(questionnaire_id);
            data2.question_identifiers = questionnaireArray;
            axios.post('/api/questionnaire_create', data2, config)
                .then(response => {
                    swal('Questions Added to Questionnaire', 'The questions have been successfully added. To add from other question groups just select them and select the questions.', 'success');
                    $("#group-title").find("option:selected").attr('disabled', 'disabled');
                })
                .catch(error => {
                    swal({
                        title: 'Error Status:<br>' + error.response.data.code,
                        type: 'error',
                        html: 'Error Message:<br>' + '<b>'+error.response.data.error+'</b>'
                    })
                })
        }

    }
</script>
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="form-horizontal" id="contact-form" style="padding-top: 40px;">
                @if(count($questionnaire_names) > 0)
                    <div class="form-group">
                        <div class="col-xs-12">
                            <label for="group-title">Please Select the Questionnaire to Assign</label>
                            <select id="group-title" class="form-control">
                                <option value="" selected="selected" disabled="disabled">-Select Questionnaire-</option>
                                @foreach($questionnaire_names as $questionnaire_name)
                                    <option value="{{$questionnaire_name->id}}">{{$questionnaire_name->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                @else
                    <p>There are no questionnaires available to assign</p>
                @endif

                <table id="basicTable" class="table table-striped table-responsive" style="margin-bottom: 50px;">
                    <thead>
                    <tr>
                        <th>Assign To Questionnaire</th>
                        <th>Name</th>
                        <th>ID</th>
                    </tr>
                    </thead>
                    <tbody id="basicBody">
                        @foreach($basics as $basic)
                            <tr>
                                <td><input type="checkbox" name="myUsers[]" value="{{$basic->id}}"></td>
                                <td>{{$basic->name}}</td>
                                <td>{{$basic->id}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                    <br>
                    <div class="form-group" id="submitGroup" style="margin-bottom: 75px;">
                        <div class="col-xs-12">
                            <button type="button" class="btn btn-warning" id="group-submit-button" onclick="addUsers()">Submit <span class="glyphicon glyphicon-send"></span></button>
                        </div>
                    </div>
            </div>
        </div>
    </div>
@endsection

<script>
    function addUsers() {
        let token = {!! json_encode($token) !!};
        let config = {
            headers: {'Authorization': 'Bearer ' + token, 'Content-Type': 'application/json'}
        };
        let userArray = [];
        let questionnaire_name_id = $("#group-title").val();

        if(questionnaire_name_id === ""){
            swal('Questionnaire Not Selected', 'You have to select a questionnaire to assign users to before proceeding', 'error');
            return;
        }

        $('input[name^=myUsers]').each(function () {
            if($(this).is(":checked")){
                userArray.push($(this).val());
            }
        });

        if(userArray.length === 0){
            swal('No Users Selected', 'You must select users to assign the questionnaire to before you can proceed', 'error');
            return;
        }

        let data = {};
        data.questionnaire_name_identifier = parseInt(questionnaire_name_id);
        let usersArray = [];
        for(let i = 0; i < userArray.length; i++){
            usersArray.push({"user_identifier": userArray[i]});
        }
        data.user_identifiers = usersArray;

        axios.post('/api/assign_questionnaire', data, config)
            .then(response => {
                swal('Questionnaire Assigned', 'The users have been assigned to the selected questionnaire', 'success');
                setTimeout(function () {
                    window.location.reload(true);
                }, 2000);
            })
            .catch(error => {
                swal({
                    title: 'Error Status:<br>' + error.response.data.code,
                    type: 'error',
                    html: 'Error Message:<br>' + '<b>'+error.response.data.error+'</b>'
                })
            })
    }
</script>
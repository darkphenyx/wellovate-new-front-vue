@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading"><b>Register Your Polar Device With Wellovate</b></div>

                    <div class="panel-body">
                        <p>
                            You are going to leave Wellovates site to grant permission for us to read your polar data.
                        </p>
                        <br>
                        <div>
                            <div style="text-align: center;">
                                <a href="{{route('polar-auth')}}">
                                    <button type="button" class="btn btn-block btn-primary">Go To Polar site</button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
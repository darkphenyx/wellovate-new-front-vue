@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading"><b>Your polar account is now registered</b></div>

                    <div class="panel-body">
                        <br>
                        <div>
                            <div style="text-align: center;">
                                <a href="{{route('home')}}">
                                    <button type="button" class="btn btn-block btn-primary">Go back to the home page</button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
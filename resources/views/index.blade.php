<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Welcome To Wellovate</title>
    <link href="{{asset('css/app.css')}}" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
</head>
<body class="<?=$page?>Body">
<div class="main">
    <nav class="navbar navbar-fixed-top navbar-light container" style="box-shadow: 0px 3px 1px rgba(245, 245, 245, 0.8); background-color: white; width:100%;">
        <div class="container-fluid">
            <div class="navbar-header">
                <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" style="height: 77px;" href="{{ route('main') }}">
                    <img src="{{asset('images/logo.png')}}" style="height: 100%; margin-left: 0px;"  alt="Wellovate Home"/>
                </a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right" style="color: black;">
                    <li class="dropdown">
                        <a  @if ($page == 'about'|| $page == 'mission' || $page == 'whatwedo')
                            class="dropdown-toggle activePage" data-toggle="dropdown" role="button"
                            aria-expanded="false"
                            @else
                            class="dropdown-toggle nonActivePage" data-toggle="dropdown" role="button"
                            aria-expanded="false"
                            @endif
                            href="#" >About </a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="{{ route('mission') }}">Our Mission</a>
                            </li>
                            <li role="presentation" class="divider"></li>
                            <li>
                                <a href="{{ route('whatwedo') }}">What We Do</a>
                            </li>
                            <li role="presentation" class="divider"></li>
                            <li>
                                <a href="{{ route('about') }}">Who We Are</a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a  @if ($page == 'solutions'|| $page == 'solutionsvalue' || $page == 'consulting')
                            class="dropdown-toggle activePage" data-toggle="dropdown" role="button"
                            aria-expanded="false"
                            @else
                            class="dropdown-toggle nonActivePage" data-toggle="dropdown" role="button"
                            aria-expanded="false"
                            @endif
                            href="#" >Solutions</a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="{{ route('solutions') }}">Education & Content</a>
                            </li>
                            <li role="presentation" class="divider"></li>
                            <li>
                                <a href="{{ route('solutionsvalue') }}">Value Added Solutions</a>
                            </li>
                            <li role="presentation" class="divider"></li>
                            <li>
                                <a href="{{ route('consulting') }}">Consulting Services</a>
                            </li>
                        </ul>
                    </li>
                    <li><a
                                @if( $page == 'realities')
                                class="activePage"
                                @else
                                class="nonActivePage"
                                @endif
                                href="{{ route('realities') }}">AR/VR</a></li>
                    <li><a
                                @if( $page == 'perspectives')
                                class="activePage"
                                @else
                                class="nonActivePage"
                                @endif
                                href="http://perspectives.wellovate.com">Perspectives</a></li>
                    <li><a
                                @if( $page == 'contactform')
                                class="activePage"
                                @else
                                class="nonActivePage"
                                @endif
                                href="{{ route('contactform') }}">Contact</a></li>
                    <li><a href="{{ route('login') }}">Login</a></li>

                </ul>
            </div>
        </div>
    </nav>
    <div style="height: 80px;"></div>
    @yield('content')
</div>
<footer class="footer-basic navbar-fixed-bottom">
    <div class="container" style="margin: 0; width: 100%;background: rgb(105,105,105);">
        <div class="col-sm-12 col-xs-12 footer-basic-right" style="text-align: center">
            <div class="col-sm-6 col-xs-6" >
                <p class="footer-links">
                    <a href="" id="terms" data-toggle="modal" data-target="#myModal">Terms of Use</a>
                    |
                    <a href="" id="policy" data-toggle="modal" data-target="#myModal">Privacy Policy</a>
                </p>
            </div>
            <div class="col-sm-6 col-xs-6" style="margin-top: 5px;">
                <p class="footer-company-name">
                    Copyright 2017 Wellovate, LLC. All rights reserved.</p>
            </div>
        </div>
    </div>
</footer>
<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button id="modalclosetop" type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 id="modaltitle"  style="font-size: 16px;text-align: center;margin-top: 20px;"></h4>
            </div>
            <div class="modal-body">
                <p id="modaltext"  style="font-size: 12px;"></p>
            </div>
            <div class="modal-footer">
                <button id="modalclose" type="button" class="btn btn-default" data-dismiss="modal" >Close</button>
            </div>
        </div>

    </div>
</div>
<div style="display: none">
    <span id="titleterms" >WELLOVATE.COM<br/>WELLOVATE.ORG<br/><br/>TERMS OF USE POLICY</span>
    <span id="titlepolicy">WELLOVATE.COM<br/>WELLOVATE.ORG<br/><br/>PRIVACY AND INFORMATION SECURITY POLICY</span>
    <p id="readterms">@include('content.terms')</p>
    <p id="readpolicy">@include('content.policy')</p>
</div>
<script src="{{asset('js/app.js')}}"></script>
@yield('scripts')
    <script>
        var options = {"male": "male", "female": "female"};
        function login() {
            swal({
                title: 'Login or Sign Up',
                html:
                '<input id="swal-input1" class="swal2-input" placeholder="Enter Name">' +
                '<input id="swal-input2" class="swal2-input" placeholder="Enter Email Address">',
                showCancelButton: true,
                confirmButtonText: 'Login',
                cancelButtonText: 'Sign Up',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-default',
                focusConfirm: false,
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        resolve([
                            $('#swal-input1').val(),
                            $('#swal-input2').val()
                        ])
                    })
                }
            }).then(function () {
                swal(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'
                )
            }, function (dismiss) {
                // dismiss can be 'cancel', 'overlay',
                // 'close', and 'timer'
                if (dismiss === 'cancel') {
                    swal({
                        title: 'Sign Up',
                        html:
                        '<input id="swal-input1" class="swal2-input" placeholder="Enter Name">' +
                        '<input id="swal-input2" class="swal2-input" placeholder="Enter Email Address">' +
                        '<input id="password1" type="password" class="swal2-input" placeholder="Enter Password">' +
                        '<input id="password2" type="password_confirm" class="swal2-input" placeholder="Enter it again">' +
                        '<input id="age" type="number" class="swal2-input" placeholder="Enter Age">' +
                        '<br>' +
                        '<label class="radio-inline"><input type="radio" name="gender" value="male">Male</label>' +
                        '<label class="radio-inline"><input type="radio" name="gender" value="female">Female</label>',
                        focusConfirm: false,
                        preConfirm: function () {
                            return new Promise(function (resolve) {
                                resolve([
                                    $('#swal-input1').val(),
                                    $('#swal-input2').val()
                                ])
                            })
                        }
                    }).then(function () {
                        swal(
                            'Deleted!',
                            'Your file has been deleted.',
                            'success'
                        )
                    })
                }
            })
        }

        function contact() {
            swal({
                title: 'Contact',
                html:
                '<input id="swal-input1" class="swal2-input" placeholder="Your Name">' +
                '<input id="swal-input2" class="swal2-input" placeholder="Company Name">' +
                '<input id="swal-input3" class="swal2-input" placeholder="Email Address">' +
                '<input id="swal-input4" class="swal2-input" placeholder="Phone">'+
                '<textarea id="swal-input5" rows="5" onfocus="this.value=\'\'" class="swal2-input"' +
                '>Please give us some more information about you</textarea>',
                showCancelButton: false,
                confirmButtonText: 'Submit',
                confirmButtonClass: 'btn btn-success',
                focusConfirm: false,
                showLoaderOnConfirm: true,
                preConfirm: function (data) {
                    return new Promise(function (resolve) {
                        setTimeout(function() {
                            var data = {
                                'name': $('#swal-input1').val(),
                                'companyName': $('#swal-input2').val(),
                                'email': $('#swal-input3').val(),
                                'phone': $('#swal-input4').val(),
                                'comment': $('#swal-input5').val()
                            };
                            $.ajax({
                                method: "GET",
                                url: "/contact/",
                                data: data
                            })
                                .done(function() {
                                resolve()
                            });
                        }, 2000);
                    })
                }
            }).then(function () {
                swal(
                    'Thank You!',
                    'For sending us your comments.',
                    'success'
                );
            });
        }

    window.signup = function() {
        console.log('Hello there')
    }

    $('.footer-basic').find('a').on('click', function (e) {
       // console.log(e.target.id);
        $( "h4#modaltitle" ).addClass( 'modaltitle'+e.target.id);
        $( "p#modaltext" ).addClass( 'modaltext'+e.target.id);
        $('.modaltitle'+e.target.id).append($('#title'+e.target.id).html());
        $('.modaltext'+e.target.id).append($('#read'+e.target.id).html());
        $( "#modalclose" ).addClass( 'close:'+e.target.id);
        $( "#modalclosetop" ).addClass( 'close:'+e.target.id);

    });
    $('#modalclose').on('click', function (e) {

        var str = e.target;
        var name = str.className;
        console.log(name);

        var res = name.split(':');
        //console.log(res[1]);
        $('.modaltitle'+res[1]).empty();
        $('.modaltext'+res[1]).empty();
        $( "h4#modaltitle" ).removeClass( 'modaltitle'+res[1]);
        $( "p#modaltex" ).removeClass( 'modaltext'+res[1]);
        $( "#modalclose" ).removeClass( 'close:'+res[1]);
       // console.log('true');
    });

    $('#modalclosetop').on('click', function (e) {

        var str = e.target;
        var name = str.className;
        var res = name.split(':');
       // console.log(res[1]);
        $('.modaltitle'+res[1]).empty();
        $('.modaltext'+res[1]).empty();
        $( "h4#modaltitle" ).removeClass( 'modaltitle'+res[1]);
        $( "p#modaltex" ).removeClass( 'modaltext'+res[1]);
        $( "#modalclosetop" ).removeClass( 'close:'+res[1]);
        //console.log('true');
    });

</script>
</body>
</html>
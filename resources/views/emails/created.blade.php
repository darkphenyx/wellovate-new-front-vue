@component('mail::message')
    # New User {{$user->name}}

    They have created an account. It has not been verified yet.

    Thanks,<br>
    {{ config('app.name') }}
@endcomponent
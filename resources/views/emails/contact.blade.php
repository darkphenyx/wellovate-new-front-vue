<div class="container">
    <p>This is an email from the contact form</p>
    <table class="table">
        <thead>
        <tr>
            <th>Name</th>
            <th>Company Name</th>
            <th>Email Address</th>
            <th>Phone Number</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>{{$name}}</td>
            <td>{{$companyName}}</td>
            <td>{{$email}}</td>
            <td>{{$phone}}</td>
        </tr>
        <tr>
            <th colspan="4">Comment</th>
        </tr>
        <tr>
            <td colspan="4">{{$comment}}</td>
        </tr>
        </tbody>
    </table>
</div>
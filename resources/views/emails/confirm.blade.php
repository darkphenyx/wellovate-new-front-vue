@component('mail::message')
    # Hello {{$user->name}}

    I see you changed email accounts. Please verify your new email using the button below:

    @component('mail::button', ['url' => route('verify', $user->verification_token)])
        Verify Account
    @endcomponent

    Thanks,<br>
    {{ config('app.name') }}
@endcomponent
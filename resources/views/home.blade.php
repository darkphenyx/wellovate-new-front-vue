@extends('layouts.app')

@section('content')
    <div>
        <div class="altHeaderImageContainerDiv"></div>
        <div class="tint">
            <br>
            <br>
            <div class="row">
                <div class="col-sm-1 col-xs-0"></div>
                <div class="col-sm-10 col-xs-12">
                    <p style="font-weight: bold; font-size: x-large; text-align: center">
                        We are a next-gen health and wellness company providing solutions that integrate
                        new technologies into real-life practice.
                    </p>
                </div>
                <div class="col-sm-1 col-xs-0"></div>
            </div>
            <br>
            <br>
            <div class="row">
                <div class="col-sm-1 col-xs-1"></div>
                <div class="col-sm-10 col-xs-10" style="text-align: center">
                    <span style="font-size: x-large">First register your Fitbit or Pulse wearable so we can better serve your needs.</span>
                </div>
                <div class="col-sm-1 col-xs-1"></div>
            </div>
            <div class="row">
                <div class="col-sm-1 col-xs-0"></div>
                <div class="col-sm-10 col-xs-12">
                    <hr class="eduSolutionHr" style="width:100%">
                </div>
                <div class="col-sm-1 col-xs-0"></div>
            </div>
            <div class="row centered-xs">
                <div class="col-sm-2 col-xs-1"></div>
                <div class="col-sm-4 col-xs-12">
                    <div class="image-button">
                        <a href="{{route('fitbit')}}">
                            <img src="{{asset('images/fitbit.png')}}" style="height: 200px;" alt="Login to your fitbit account">
                        </a>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12">
                    <div class="image-button">
                        <a href="{{route('polar')}}">
                            <img class="polar-button" src="{{asset('images/polar-logo.jpg')}}" alt="Login to your polar account">
                        </a>
                    </div>
                </div>
                <div class="col-sm-2 col-xs-1"></div>
            </div>
            <br>
            <br>
        </div>
    </div>
@endsection

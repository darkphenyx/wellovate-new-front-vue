@extends('index')

@section('content')
    <div>
        <div class="headerImageContainerDiv">
            <span class="headerTitle">Contact Us</span>
        </div>
        <div class="pageContainer">
            <div>
                <form method="POST" action="/contactform" autocomplete="off">
                    @if(count($errors))
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.
                            <br/>
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="form-horizontal" id="contact_form" style="padding-top: 40px;">

                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                            <label class="col-md-3 control-label" ></label>
                            <div class="col-md-5 inputGroupContainer">
                                <div>
                                    <input  name="name" id="name" placeholder="Your Name" class="contactForm form-control"  type="text" value="{{ old('name') }}">
                                    <span class="text-danger">{{ $errors->first('name') }}</span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" ></label>
                            <div class="col-md-5 inputGroupContainer">
                                <div>
                                    <input name="companyName" id="companyName" placeholder="Company Name" class="contactForm form-control"  type="text" value="{{ old('companyName') }}">
                                </div>
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                            <label class="col-md-3 control-label" ></label>
                            <div class="col-md-5 inputGroupContainer">
                                <div>
                                    <input name="email" id="email" placeholder="E-Mail Address" class="contactForm form-control"  type="text" valuer="{{ old('email') }}">
                                    <span class="text-danger">{{ $errors->first('email') }}</span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" ></label>
                            <div class="col-md-5 inputGroupContainer">
                                <div>
                                    <input name="phone" id="phone" placeholder="(845)555-1212" class="contactForm form-control" value="{{ old('phone') }}" type="text" >
                                </div>
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('comment') ? 'has-error' : '' }}">
                            <label class="col-md-3 control-label" ></label>
                            <div class="col-md-5 inputGroupContainer">
                                <div>
					                <textarea class="contactForm form-control" name="comment" id="comment" placeholder="Please give us some more information about you" rows="4" value="{{ old('comment') }}"></textarea>
                                    <span class="text-danger">{{ $errors->first('comment') }}</span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"></label>
                            <div class="col-md-5">
                                <button type="submit" class="btn btn-warning" id="submitBNT"> Submit <span class="glyphicon glyphicon-send"></span></button>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
@stop
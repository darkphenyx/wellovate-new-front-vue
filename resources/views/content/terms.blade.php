
<b>WELLOVATE.COM and WELLOVATE.ORG Web Site Terms and Conditions (“Terms”)</b><br/><br/>
Please read the following Terms carefully before using WELLOVATE.COM’S or WELLOVATE.ORG'S web site,
including, but not limited to, the www.WELLOVATE.com Web Site, the www.WELLOVATE.org Web Site, and
any online features, services and/or programs offered by WELLOVATE.COM and/or WELLOVATE.ORG
(collectively, "WELLOVATE" or the “Web Site”).</b> By accessing or using the Web Site, you agree
to the following Terms. You should review these Terms regularly as they may change at any time
at the sole discretion of WELLOVATE. If you do not agree to any portion of these Terms, you should
not access or otherwise use the Web Site. “Content” refers to any text, materials, documents, images,
graphics, logos, design, audio, video and any other information provided from or on, uploaded to
and/or downloaded from the Web Site.<br/><br/>
We will make an effort to update this web page with any changes to these Terms and/or to the services
described in these Terms, and you are encouraged to review these Terms frequently (the date of the
most recent revision to these Terms appear at the end of these Terms).<br/>

1.	<b>CONVENIENCE AND INFORMATION ONLY; ACCEPTANCE OF TERMS.</b> By merely providing access to
the Web Site, WELLOVATE does not warrant or represent that: (a) the Content is accurate, complete,
up-to-date or current; (b) WELLOVATE has any obligation to update any Content; (c) the Content is
free from technical inaccuracies or typographical errors; (d) that the Content does not infringe
on the intellectual property rights of any third party; (e) that the Content is free from changes
caused by a third party; (f) your access to the Web Site will be free from interruptions, errors,
computer viruses or other harmful components; and/or (g) any information obtained in response to
questions asked through, or postings made on, the Web Site is accurate or complete. Your use of
the Web Site and the services offered therein are subject to federal law, the law of the state
where WELLOVATE maintains your Account, or, if WELLOVATE transfers your Account to another location,
where WELLOVATE currently maintains your Account (“Applicable Law”).<br/><br/>

You affirm that you are either more than 18 years of age, or an emancipated minor, or possess legal
parental or guardian consent, and are fully able and competent to enter into the terms, conditions,
obligations, affirmations, representations, and warranties set forth in these Terms, and to abide
by and comply with these Terms. In any case, you affirm that you are over the age of 13, as <b>THE WEB
    SITE IS NOT INTENDED FOR CHILDREN UNDER 13 THAT ARE UNACCOMPANIED BY HIS OR HER PARENT OR LEGAL GUARDIAN.</b><br/><br/>

2.	<b>SITE USE AND CONTENT.</b> You may view, copy or print pages from the Web Site solely for personal,
non-commercial purposes. You may not otherwise use, modify, copy, print, display, reproduce,
distribute or publish any information from the Web Site without the express, prior, written consent
of WELLOVATE. You may not rent, lease, sell, sublicense, assign, reverse engineer, disassemble, modify,
loan, distribute, export or otherwise transfer  technology or other information, including any printed
materials of the same, nor may you create derivative works of or otherwise modify the same. This
license will automatically terminate if you do not comply with the terms of this Agreement. At
any time, we may, without further notice, make changes to the Web Site, to these Terms and/or
to the services described in these Terms.<br/><br/>
3.	<b>ADDITIONAL TERMS OF USE.</b><br/>

	You understand that we may use your feedback or suggestions without any obligation to compensate you
	for them (just as you have no obligation to offer them).<br/>
	You will not post unauthorized commercial communications (such as spam) on WELLOVATE.<br/>
	You will not collect users' content or information, or otherwise access WELLOVATE, using
    automated means (such as harvesting bots, robots, spiders, or scrapers) without our prior permission.<br/>
    You will not upload viruses or other malicious code.<br/>
    You will not solicit login information or access an account belonging to someone else.<br/>
    You will not bully, intimidate, or harass any user.<br/>
    You will not post content that: is hate speech, threatening, or pornographic; incites violence;
    or contains nudity or graphic or gratuitous violence.<br/>
    You will not use WELLOVATE to do anything unlawful, misleading, malicious, or discriminatory.<br/>
    You will not do anything that could disable, overburden, or impair the proper working
    or appearance of WELLOVATE.<br/>
    You agree to not create more than one personal account.<br/>
    You agree to not provide any false personal information.<br/>
    You agree to keep your contact information up-to-date and accurate.<br/>
    You agree that you will not share your password, allow others to access your account, or do anything
    that may endanger the security of your account.<br/>
    You agree that you will not do anything that infringes upon or violates the rights of others.<br/><br/>

We reserve the right to add, remove, or edit any content or information on WELLOVATE
(including information or content that you post) at any time for any reason.
If you are found to be in violation of these terms, we reserve the right to disable your account.<br/><br/>

You provide consent and all the necessary rights to enable users to sync their mobile, wearable,
and other electronic devices with any information that is visible to them on WELLOVATE. This
includes through the use of an application.<br/><br/>

You will not modify, create derivative works of, decompile, or otherwise attempt to extract
source code from us, unless you are expressly permitted to do so under an open source license,
or we give you express written permission.<br/><br/>

Your continued use of the WELLOVATE Services, following notice of the changes to our terms,
policies or guidelines, constitutes your acceptance of our amended terms, policies or guidelines.<br/><br/>
If you violate the letter or spirit of this Statement, or otherwise create risk or possible legal
exposure for us, we can stop providing all or part of WELLOVATE to you.<br/><br/>

If anyone brings a claim against us related to your actions, content or information on WELLOVATE,
you will indemnify and hold us harmless from and against all damages, losses, and expenses of any
kind (including reasonable legal fees and costs) related to such claim. Although we provide rules
for user conduct, we do not control or direct users' actions on WELLOVATE and are not responsible
for the content or information users transmit or share on WELLOVATE. We are not responsible for a
ny offensive, inappropriate, obscene, unlawful or otherwise objectionable content or information
you may encounter on WELLOVATE. We are not responsible for the conduct, whether online or offline,
of any user of WELLOVATE.<br/><br/>

WE TRY TO KEEP WELLOVATE UP, BUG-FREE, AND SAFE, BUT YOU USE IT AT YOUR OWN RISK. WE ARE PROVIDING
WELLOVATE AS IS WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES INCLUDING, BUT NOT LIMITED TO, IMPLIED
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT. WE DO NOT
GUARANTEE THAT WELLOVATE WILL ALWAYS BE SAFE, SECURE OR ERROR-FREE OR THAT WELLOVATE WILL ALWAYS
FUNCTION WITHOUT DISRUPTIONS, DELAYS OR IMPERFECTIONS. WELLOVATE IS NOT RESPONSIBLE FOR THE ACTIONS,
CONTENT, INFORMATION, OR DATA OF THIRD PARTIES, AND YOU RELEASE US, OUR DIRECTORS, OFFICERS,
EMPLOYEES, AND AGENTS FROM ANY CLAIMS AND DAMAGES, KNOWN AND UNKNOWN, ARISING OUT OF OR IN ANY
WAY CONNECTED WITH ANY CLAIM YOU HAVE AGAINST ANY SUCH THIRD PARTIES.<br/><br/>

4.	<b>DISCLAIMERS.</b><br/>
    (a)	<b>NO WARRANTIES; INDEMNIFICATION.</b> YOU EXPRESSLY AGREE THAT YOUR USE OF THE WEB SITE IS
    AT YOUR SOLE RISK. THE WEB SITE, THE ONLINE SERVICE AND THE CONTENT IS PROVIDED “AS IS” AND
    “AS AVAILABLE” FOR YOUR USE, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, UNLESS
    SUCH WARRANTIES ARE LEGALLY INCAPABLE OF EXCLUSION. WELLOVATE PROVIDES THE WEB SITE AND THE ONLINE
    SERVICE ON A COMMERCIALLY REASONABLE BASIS AND WELLOVATE MAKES NO REPRESENTATIONS OR WARRANTIES THAT
    THE WEB SITE, THE ONLINE SERVICE, THE CONTENT OR ANY SERVICES OFFERED IN CONNECTION WITH THE WEB
    SITE ARE OR SHALL REMAIN UNINTERRUPTED OR ERROR-FREE, THE CONTENT SHALL BE NON-INFRINGING ON ANY
    THIRD PARTY’S INTELLECTUAL PROPERTY RIGHTS, THAT DEFECTS SHALL BE CORRECTED, THAT THE WEB PAGES ON
    THE WEB SITE, THE ONLINE SERVICE, ANY ELECTRONIC COMMUNICATION OR THE SERVERS USED IN CONNECTION
    WITH THE WEB SITE ARE OR SHALL REMAIN FREE FROM ANY VIRUSES, WORMS, TIME BOMBS, DROP DEAD DEVICES,
    TROJAN HORSES OR OTHER HARMFUL COMPONENTS, OR THAT ANY PERSON USING THE WEB SITE WILL BE THE PERSON
    THAT HE OR SHE REPRESENTS HIMSELF OR HERSELF TO BE. WELLOVATE DOES NOT GUARANTEE THAT YOU WILL BE
    ABLE TO ACCESS OR USE THE WEB SITE AND/OR THE ONLINE SERVICE AT TIMES OR LOCATIONS OF YOUR CHOOSING,
    OR THAT WELLOVATE SHALL HAVE ADEQUATE CAPACITY FOR THE WEB SITE AND/OR THE ONLINE SERVICE AS A WHOLE
    OR IN ANY SPECIFIC GEOGRAPHIC AREA.<br/>

    (b)	<b>REGULATORY EVALUATION.</b> THIS WEBSITE/APPLICATION HAS NOT BEEN EVALUATED BY THE FOOD AND
    DRUG ADMINISTRATION OR ANY EQUIVALENT REGULATORY AUTHORITY IN ANY OTHER JURISDICTION. THIS
    WEBSITE/APPLICATION IS NOT INTENDED TO DIAGNOSE, TREAT, MITIGATE, ALLEVIATE, MONITOR, CURE, PREVENT,
    OR COMPENSATE FOR ANY DISEASE, CONDITION, INJURY, HANDICAP OR DISABILITY. PLEASE CONSULT WITH YOUR
    DOCTOR OR OTHER QUALIFIED HEALTH CARE PROFESSIONAL.<br/>

    (c)	<b>INDEMNIFICATION.</b> You agree to defend, indemnify and hold WELLOVATE and its affiliates,
    subsidiaries, owners, directors, officers, employees and agents harmless from and against any and
    all claims, demands, suits, proceedings, liabilities, judgments, losses, damages, expenses and costs
    (including without limitation reasonable attorneys’ fees) assessed or incurred by WELLOVATE, directly
    or indirectly, with respect to or arising out of: (i) your failure to comply with these Terms;
    (ii) your breach of your obligations under these Terms; (iii) your use of the rights granted hereunder,
    including without limitation any claims made by any third parties; and/or (iv) your violation of any
    third party right, including without limitation any copyright, property, or privacy right.<br/>

    (d)	<b>NOT INVESTMENT ADVICE.</b> WELLOVATE DOES NOT INTEND TO PROVIDE ANY INVESTMENT ADVICE OR
    INFORMATION RELATING TO ITSELF OR ANY WELLOVATE IDENTIFIED ON THE WEB SITE. Nevertheless, the Web Site
    may, from time to time, contain information on the current or prospective financial condition of this
    and/or certain other companies. WELLOVATE cautions that there are various important factors that could
    cause actual results to differ materially from those indicated in the information you may encounter on
    the Web Site. Accordingly, there can be no assurance that such indicated results will be realized. These
    factors include, among other things, legislative and regulatory initiatives regarding regulation of
    American companies doing business abroad; political and economic conditions and developments in the
    United States and in foreign countries in which the companies discussed on the Web Site operate;
    financial market conditions and the results of financing efforts; and changes in commodity prices
    and interest rates.<br/><br/>

5.	<b>LIMITATION OF LIABILITY.</b> WELLOVATE’S ENTIRE LIABILITY AND YOUR EXCLUSIVE REMEDY WITH RESPECT TO
THE USE OF THE WEB SITE, THE ONLINE SERVICE AND/OR ANY SERVICE PROVIDED IN CONNECTION WITH THE WEB SITE
SHALL BE THE CANCELLATION OF YOUR USER ACCOUNT WITH WELLOVATE. EXCEPT FOR THE ACTUAL CHARGES DESCRIBED
IN PARAGRAPH 3(L) ABOVE, IN NO EVENT WILL WELLOVATE BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, PUNITIVE OR CONSEQUENTIAL DAMAGES ARISING FROM YOUR USE OF THE WEB SITE AND/OR
ANY SERVICE PROVIDED IN CONNECTION WITH THE WEB SITE, THE ONLINE SERVICE AND/OR ANY SERVICE PROVIDED
IN CONNECTION WITH THE WEB SITE, OR FOR ANY OTHER CLAIM RELATED IN ANY WAY TO YOUR USE OF THE WEB SITE,
THE ONLINE SERVICE AND/OR ANY SERVICE PROVIDED IN CONNECTION WITH THE WEB SITE, INCLUDING, BUT NOT LIMITED
TO, (A) ERRORS, MISTAKES, OR INACCURACIES OF CONTENT, (B) PERSONAL INJURY OR PROPERTY DAMAGE, OF ANY NATURE
WHATSOEVER, RESULTING FROM YOUR ACCESS TO AND USE OF THE WEB SITE, THE ONLINE SERVICE AND/OR ANY SERVICE
PROVIDED IN CONNECTION WITH THE WEB SITE, (C) ANY UNAUTHORIZED ACCESS TO OR USE OF OUR COMPUTER SERVERS
AND/OR ANY AND ALL PERSONAL INFORMATION AND/OR FINANCIAL INFORMATION STORED THEREIN, (D) ANY INTERRUPTION
OR CESSATION OF TRANSMISSION TO OR FROM THE WEB SITE, THE ONLINE SERVICE AND/OR ANY SERVICE PROVIDED IN
CONNECTION WITH THE WEB SITE, AND/OR (E) ANY VIRUSES, WORMS, TIME BOMBS, DROP DEAD DEVICES, TROJAN HORSES
OR OTHER HARMFUL COMPONENTS THAT MAY BE TRANSMITTED TO OR THROUGH THE WEB SITE, THE ONLINE SERVICE AND/OR
ANY SERVICE PROVIDED IN CONNECTION WITH THE WEB SITE BY ANY THIRD PARTY OR FOR ANY LOSS OR DAMAGE OF ANY
KIND. BECAUSE SOME STATES OR JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF LIABILITY FOR
CONSEQUENTIAL OR INCIDENTAL DAMAGES, IN SUCH STATES OR JURISDICTIONS WELLOVATE’S LIABILITY WILL BE LIMITED
TO THE GREATEST EXTENT PERMITTED BY LAW.<br/><br/>

YOU UNDERSTAND AND AGREE THAT THE WEBSITE/APPLICATION IS PROVIDED ON AN "AS IS" AND "AS AVAILABLE" BASIS.
WELLOVATE DOES NOT MAKE ANY WARRANTY THAT THE WEBSITE/APPLICATION WILL MEET YOUR REQUIREMENTS OR THAT USE
OF THE WEBSITE/APPLICATION WILL BE UNINTERRUPTED, TIMELY, SECURE OR ERROR FREE; NOR DOES WELLOVATE MAKE ANY
WARRANTY AS TO THE RESULTS THAT MAY BE OBTAINED FROM USE OF THE WEBSITE/APPLICATION OR THE ACCURACY OR
RELIABILITY OF ANY INFORMATION OBTAINED THROUGH THE WEBSITE/APPLICATION (INCLUDING THIRD PARTY CONTENT)
OR THAT ANY DEFECTS IN THE WEBSITE/APPLICATION WILL BE CORRECTED. WELLOVATE AND ITS SUPPLIERS HEREBY
DISCLAIM ALL WARRANTIES OF ANY KIND, WHETHER EXPRESS, IMPLIED OR STATUTORY REGARDING THE WEBSITE/APPLICATION,
INCLUDING ANY IMPLIED WARRANTY OF TITLE, MERCHANTIBILITY, FITNESS FOR A PARTICULAR PURPOSE OR NON­INFRINGEMENT
OF THIRD PARTY RIGHTS. YOU UNDERSTAND AND AGREE THAT ANY MATERIAL OR DATA OBTAINED THROUGH USE OF THE
WEBSITE/APPLICATION IS AT YOUR OWN DISCRETION AND RISK AND THAT YOU WILL BE SOLELY RESPONSIBLE FOR ANY
DAMAGE TO YOUR COMPUTER SYSTEM OR LOSS OF DATA THAT RESULTS FROM THE DOWNLOAD OF ANY SUCH CONTENT OR MATERIAL.<br/><br/>

YOU UNDERSTAND AND AGREE THAT WELLOVATE AND ITS RESPECTIVE LICENSORS, TO THE EXTENT PERMITTED BY LAW,
SHALL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, CONSEQUENTIAL OR EXEMPLARY DAMAGES,
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, AND EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE,
INCLUDING, WITHOUT LIMITATION, DAMAGES FOR PERSONAL INJURY, LOST DATA, LOST PROFITS, OR BUSINESS INTERRUPTION
ARISING FROM OR RELATING TO: (i) YOUR USE OF THE WEBSITE/APPLICATION OR USE OF THE WEBSITE/APPLICATION YOU
DOWNLOADED BY ANYONE ELSE; (ii) THE COST OF PROCUREMENT OF SUBSTITUTE DATA, INFORMATION OR SOFTWARE; (iii)
UNAUTHORIZED ACCESS TO OR ALTERATION OF YOUR TRANSMISSIONS OR DATA; OR (iv) ANY OTHER MATTER RELATING TO THE
WEBSITE/APPLICATION OR ITS USE. ADDITIONALLY, YOU ARE SOLELY RESPONSIBLE FOR ANY AND ALL DECISIONS REGARDING
USE OF THE WEBSITE/APPLICATION TO INPUT, STORE AND/OR TRANSFER PERSONAL DATA.<br/><br/>

6.	<b>PRIVACY.</b> Personal data that you provide regarding yourself will be handled in accordance with WELLOVATE’s
Privacy Policy located at www.WELLOVATE.org/privacy.<br/><br/>

7.	<b>ADDITIONAL PRIVACY TERMS AND CONDITIONS.</b><br/>
    (a)	You agree that WELLOVATE or others working for WELLOVATE may collect, use, retain and process technical
    data and related information, including but not limited to technical information about your device, system
    and Website/Application software, and peripherals, that is gathered periodically to facilitate the provision
    of software updates, support and other services to you (if any) related to the Website/Application. WELLOVATE
    or others working for WELLOVATE may use this information to improve and/or to provide to you its products,
    processes, services or technologies, or as required by law or legal process. WELLOVATE or others working for
    WELLOVATE may collect and/or use information that identifies you personally if you choose to provide such
    information to WELLOVATE or those working for WELLOVATE (e.g., by using a feedback function within the
    Website/Application) or otherwise give your permission for such use, or if required by law or legal process.<br/>
    (b)	WELLOVATE will have no obligation to provide this Website/Application, may change the form and nature of
    this Website/Application at any time with or without notice to you, will have no liability whatsoever to you
    or any third party for any failure of this Website/Application, may cease providing this Website/Application
    at any time with or without notice to you, and will have no obligation to  retain any materials you may provide
    to WELLOVATE on its servers or return any such materials to you before deleting them from WELLOVATE's servers.<br/><br/>
8.	<b>THIRD PARTY CONTENT.</b><br/>
    (a)	WELLOVATE may provide hyperlinks to other web sites maintained by third parties, or WELLOVATE may provide
    third party content on the Web Site by framing or other methods. THE LINKS TO THIRD PARTY WEB SITES ARE PROVIDED
    FOR YOUR CONVENIENCE AND INFORMATION ONLY. THE CONTENT ON ANY LINKED WEB SITE IS NOT UNDER WELLOVATE’S CONTROL
    AND WELLOVATE IS NOT RESPONSIBLE FOR THE CONTENT OF LINKED WEB SITES, INCLUDING ANY FURTHER LINKS CONTAINED IN
    A THIRD PARTY WEB SITE. IF YOU DECIDE TO ACCESS ANY OF THE THIRD PARTY WEB SITES LINKED TO THE WEB SITE, YOU DO
    SO ENTIRELY AT YOUR OWN RISK.<br/>
    (b)	If a third party links to the Web Site, it is not necessarily an indication of an endorsement, authorization,
    sponsorship, affiliation, joint venture or partnership by or with WELLOVATE. In most cases, WELLOVATE is not even
    aware that a third party has linked to the Web Site. A web site that links to the Web Site: (i) may link to, but
    not replicate, WELLOVATE’s Content; (ii) may not create a browser, border environment or frame WELLOVATE’s Content;
    (iii) may not imply that WELLOVATE is endorsing it or its products; (iv) may not misrepresent its relationship with WELLOVATE;
    (v) may not present false or misleading information about WELLOVATE’s products or services; and (vi) should not include
    content that could be construed as distasteful, offensive or controversial, and should contain only Content that is
    appropriate for all age groups.<br/>
    (c)	This Website/Application may contain, use or present information derived from third party sources. While care
    has been taken to confirm the accuracy of the information presented based on the sources used and, where applicable,
    to describe generally accepted practices, WELLOVATE, and its respective licensors, authors, editors, reviewers,
    contributors and publishers are not responsible for errors or omissions or for any consequences from Website/Application
    of the information herein and make no warranty, expressed or implied, with respect to the currency, completeness,
    usefulness, or accuracy of the contents of the Website/Application. Health- and wellness­related information changes
    and therefore, information contained in the Website/Application may be outdated, incomplete or incorrect. WELLOVATE
    does not endorse and is not responsible for the accuracy of the content from non­WELLOVATE sources.<br/>
    (d)	In addition, while there may be information on this Website/Application related to certain medical conditions,
    consult with your own physician or health care professional. The Website/Application is not to be used as a substitute
    for medical judgment, advice, diagnosis or treatment of any health condition or problem. Users of the Website/Application
    should not rely on information contained therein for diagnosing, treating, curing, preventing, managing or otherwise
    addressing health problems. Questions should be addressed to a physician or other health care professional. Use of this
    Website/Application does not create an express or implied physician­patient relationship. In using the Website/Application,
    you agree that WELLOVATE is not, or will not be, liable or otherwise responsible for any decision made or any action taken
    or any action not taken due to your use of this Website/Application.<br/>
    (e)	In this Website/Application, WELLOVATE may provide, or third parties may provide, links to other World Wide Web sites
    or resources (each a "Linked Site"). WELLOVATE does not endorse and is not responsible for any data, software or other
    content available from non­WELLOVATE sites or resources and you acknowledge and agree that WELLOVATE shall not be liable,
    directly or indirectly, for any damage or loss relating to your use of or reliance on such data, software or other content.
    You are responsible for viewing and abiding by the privacy statements and terms of use posted at a Linked Site. YOU AGREE
    THAT WELLOVATE WILL HAVE NO RESPONSIBILITY OR LIABILITY FOR ANY INFORMATION, SOFTWARE, OR MATERIALS FOUND AT ANY LINKED SITE.</span><br/>

9.	<b>COPYRIGHT AND TRADEMARKS.</b> The trademarks, service marks and logos used and displayed on the Web Site are WELLOVATE’s,
or its subsidiaries’ or affiliates’, registered and unregistered trademarks. WELLOVATE is the copyright owner or authorized
licensee of all text and all graphics contained on the Web Site. All trademarks and service marks of WELLOVATE that
may be referred to on the Web Site are the property of WELLOVATE. Other parties’ trademarks and service marks that may
be referred to on the Web Site are the property of their respective owners. Nothing on the Web Site should be construed
as granting, by implication, estoppel or otherwise, any license or right to use any of WELLOVATE’s trademarks or service
marks without WELLOVATE’s prior written permission. WELLOVATE aggressively enforces its intellectual property rights.
Neither the name of WELLOVATE nor any of WELLOVATE other trademarks, service marks or copyrighted materials may be used in
any way, including in any advertising, hyperlink, publicity or promotional materials of any kind, whether relating to the
Web Site or otherwise, without WELLOVATE’s prior written permission, except that a third party web site that desires to
link to the Web Site and that complies with the requirements of Paragraph 7(b) above may use the name “WELLOVATE” in or
as part of that URL link. If you believe that any Content on the Web Site violates any intellectual property right of yours,
please contact WELLOVATE at the address, email address or telephone number set forth at the bottom of these Terms.<br/><br/>

10.	<b>LOCAL LAWS.</b> WELLOVATE makes no representation that content or materials in the Web Site are appropriate or
available for use in jurisdictions outside the United States. Access to the Web Site from jurisdictions where such access
is illegal is prohibited. If you choose to access the Web Site from other jurisdictions, you do so on your own initiative
and are responsible for compliance with applicable local laws. WELLOVATE is not responsible for any violation of law. You
may not use or export the Content or materials in the Web Site in violation of U.S. export laws and regulations. You agree
that the Web Site, these Terms and the Online Service shall be interpreted and governed in accordance with federal law and,
to the extent not preempted by federal law, with the laws of the state where WELLOVATE maintains your Account, or, if WELLOVATE
transfers your Account to another location, where WELLOVATE currently maintains your Account. The Web Site and the Online
Service shall be deemed a passive website and service that does not give rise to personal jurisdiction over WELLOVATE,
either specific or general, in jurisdictions other than the states covered by the preceding sentence. You agree and hereby
submit to the exclusive personal jurisdiction of the state and federal courts located where WELLOVATE maintains your Account,
or, if WELLOVATE transfers your Account to another location, where WELLOVATE currently maintains your Account. You further
agree to comply with all applicable laws regarding the transmission of technical data exported from the United States and
the country in which you reside (if different from the United States).<br/><br/>

11.	<b>AVAILABILITY.</b> Information that WELLOVATE publishes in the Web Site may contain references or cross-references
to products, programs or services of WELLOVATE that are not necessarily announced or available in your area. Such
references do not mean that WELLOVATE will announce any of those products, programs or services in your area at any
time in the future. You should contact WELLOVATE for information regarding the products, programs and services that
may be available to you, if any.<br/><br/>

12.	<b>NON-TRANSFERABILITY OF USER ACCOUNT.</b> User Accounts and User IDs are non-transferable, and all users are
obligated to take preventative measures to prohibit unauthorized users from accessing the Web Site with his or her
User ID and password. You may not assign these Terms, in whole or in part, or delegate any of your responsibilities
hereunder to any third party. Any such attempted assignment or delegation will not be recognized by WELLOVATE unless
acknowledge by WELLOVATE in writing. WELLOVATE has no obligation to provide you with written acknowledgment. WELLOVATE
may, at any time and in its sole discretion, assign these Terms, in whole or in part, or delegate any of our rights
and responsibilities under these Terms to any third party or entity.<br/><br/>

13.	<b>TERMINATION OF SERVICE.</b> We may terminate your User Account or right to access secured portions of the Web
Site at any time, without notice, for conduct that we believe violates these Terms and/or is harmful to other users
of the Web Site, to WELLOVATE, to the business of the Web Site’s Internet service provider, or to other information
providers.<br/><br/>

14.	<b>CUSTOMER COMMENTS.</b> We welcome the submission of comments, information or feedback through the Web Site.
By submitting information through the Web Site, you agree that the information submitted shall be subject to the
WELLOVATE Web Site Privacy Policy.<br/><br/>

15.	<b>MISCELLANEOUS.</b> If any provision of these Terms is deemed invalid by a court of competent jurisdiction,
the invalidity of such provision shall not affect the validity of the remaining provisions of these Terms, which
shall remain in full force and effect. No waiver of any term of these Terms shall be deemed a further or continuing
waiver of such term or any other term, and WELLOVATE’s failure to assert any right or provision under these Terms
shall not constitute a waiver of such right or provision. These Terms and the WELLOVATE Web Site Privacy Policy are
the entire agreement between you and WELLOVATE with respect to your use of the Web Site and the Online Service,
and supersede any and all prior communications and prior agreements, whether written or oral, between you and
WELLOVATE regarding the Web Site and the Online Service.<br/><br/>
<b>Your Consent To This Agreement</b><br/>
By accessing or using the Web Site, you consent to and agree to be bound by the foregoing Terms and Conditions.
If we decide to change these Terms, we will make an effort to post those changes on the web page so that you will
always be able to understand the terms and conditions that apply to your use of the Web Site and/or the Online Service.
Your use of the Web Site and/or the Online Service following any amendment of these Terms will signify your assent to
and acceptance of its revised terms.<br/><br/>
If you have additional questions or comments of any kind, or if you see anything on the Web Site that you think is
inappropriate, please let us know by email contact@wellovate.org or by phone at:<br/><br/>
  WELLOVATE<br/>
 (828) 278-8264<br/>
  contact@wellovate.org<br/>
  EFFECTIVE AS OF: March 11, 2017<br/>
  LAST UPDATED: April 2, 2017<br/>

@extends('index')

@section('content')
    <div>
        <br>
        <br>
            <div class="row">
                <div class="col-md-2 col-sm-1 col-xs-1"></div>
                <div class="col-md-8 col-sm-10 col-xs-10" style="color: white; margin: 80px 0px; ">
                    <span style="font-size: x-large">Wellovate Mixed Reality Platform</span>
                    <br>
                    <br>
                    <span> Our physician and developer experts in virtual and augmented reality</span><br>
                    <span>are excited to bring you the next generation of health and wellness</span><br>
                    <span>technology solutions.</span><br><br>
                    <span>Launching 2018. </span>
                </div>
                <div class="col-md-2 col-sm-1 col-xs-1"></div>
            </div>
    </div>
@stop
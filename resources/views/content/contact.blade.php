@extends('index')

@section('content')
    <div>
        <div class="headerImageContainerDiv"></div>
        <div class="row tint">
            <br>
            <br>
            <p style="font-size: x-large; font-weight: bold;text-align: center;">Contact</p>
            <div class="row">
                <div class="col-sm-1 col-xs-0"></div>
                <div class="col-sm-10 col-xs-12">
                    <hr class="eduSolutionHr" style="width: 100%">
                </div>
                <div class="col-sm-1 col-xs-0"></div>
            </div><div class="row">
                <div class="col-sm-2 col-xs-0"></div>
                <div class="col-sm-8 col-xs-12">
                    <input type="text" maxlength="40" class="form-control" name="contactName" placeholder="Name">
                </div>
                <div class="col-sm-2 col-xs-0"></div>
            </div>
            <br>
            <div class="row">
                <div class="col-sm-2 col-xs-0"></div>
                <div class="col-sm-8 col-xs-12">
                    <input type="text" maxlength="100" class="form-control" name="companyName" placeholder="Company Name">
                </div>
                <div class="col-sm-2 col-xs-0"></div>
            </div>
            <br>
            <div class="row">
                <div class="col-sm-2 col-xs-0"></div>
                <div class="col-sm-8 col-xs-12">
                    <input type="email" maxlength="80" class="form-control" name="contactEmail" placeholder="Email">
                </div>
                <div class="col-sm-2 col-xs-0"></div>
            </div>
            <br>
            <div class="row">
                <div class="col-sm-2 col-xs-0"></div>
                <div class="col-sm-8 col-xs-12">
                    <input type="text" maxlength="11" class="form-control" name="contactPhone" placeholder="Phone">
                </div>
                <div class="col-sm-2 col-xs-0"></div>
            </div>
            <br>
            <div class="row">
                <div class="col-sm-2 col-xs-0"></div>
                <div class="col-sm-8 col-xs-12 form-group">
                    <textarea class="form-control" rows="5" name="contactMessage" onfocus="this.value=''"
                    >Please give us some more information about you
                    </textarea>
                </div>
                <div class="col-sm-2 col-xs-0"></div>
            </div>
            <br>
            <div class="row">
                <div class="col-sm-2 col-xs-0"></div>
                <div class="col-sm-8 col-xs-12 form-group text-center">
                    <input type="button" class="btn btn-primary" value="Send">
                </div>
                <div class="col-sm-2 col-xs-0"></div>
            </div>
            <br>
            <br>
        </div>
    </div>
@stop
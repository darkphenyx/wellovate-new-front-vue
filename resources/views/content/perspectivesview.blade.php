<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Welcome To Wellovate</title>
    <link href="{{asset('css/app.css')}}" rel="stylesheet" type="text/css">
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
</head>
<body>
    <div class="main">
        <nav class="navbar navbar-fixed-top navbar-light container" style="box-shadow: 0px 3px 1px rgba(245, 245, 245, 0.8); background-color: white; width:100%;">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" style="height: 77px;" href="">
                        <img src="{{asset('images/logo.png')}}" style="height: 100%; margin-left: 0px;"  alt="Wellovate Home"/>
                    </a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right" style="color: black;">
                        <li><a href="{{  route('perspectives') }}" class="nonActivePage">Back to Perspectives</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <div style="margin-top: 5%">
            <div class="pageContainer">
                <br>
              <div id="showareyounuts" style="display: none">
                <div class="row blogPostEntry" style="line-height: 2; padding: 0px 15px;margin-top: 30px;">
                    <div class="row">
                        <div class="col-sm-1 col-xs-0"></div>
                        <div class="col-sm-10 col-xs-12">
                            <div style="margin-bottom: 10px; font-size: large">Are you nuts?!?!</div>
                        </div>
                        <div class="col-sm-1 col-xs-0"></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-1 col-xs-0"></div>
                        <div class="col-sm-10 col-xs-12">
                            <div style="border-top: 1px lightgray solid;border-bottom: 1px lightgray solid; padding: 5px 0px 5px;">
                                <span class="glyphicon glyphicon-calendar"></span><a href="#"> July 27, 2017</a> /
                                <span class="glyphicon glyphicon-folder-close"></span><a href="#"> future of health and wellness</a>,
                                <a href="#">nextgen health and wellness</a>
                            </div>
                        </div>
                        <div class="col-sm-1 col-xs-0"></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-1 col-xs-0"></div>
                        <div class="col-sm-10 col-xs-12">
                            <div class="wrapper" style="padding: 20px 0px">
                                <p>
                                    I read an interesting article today in IEEE Spectrum titled “Robots Could Act as
                                    Ethical Mediators Between Patients and Caregivers”. The article highlights some
                                    impressive work done at Georgia Tech in developing a robotic solution that attempts
                                    to serve as an ethical mediator during medical encounters between patients
                                    and their caregivers. The piece describes how the
                                    robot’s software uses voice volume and facial tracking to determine if a “human’s dignity
                                    becomes threatened due to other’s inappropriate behavior”. If such an algorithmic threshold
                                    is reached, the robot then interjects a mediating voice response into the patient-caregiver
                                    interaction. Although in the closing paragraph, the article states that “there’s no way a
                                    robot can replace empathetic interactions between two people”, the video examples and
                                    descriptions of such a solution had me rhetorically asking “are you nuts?!?!”.<br><br>

                                    There’s no doubt in my mind that the individuals who developed the solution are extremely
                                    bright, and a similar technology may be useful (in a fly-on-the-wall capacity) for secretly
                                    giving caregivers feedback on their interactions. However, I continue to find the common sense
                                    disconnect between the technology sector and healthcare, well, quite concerning…<br><br>

                                    To developers: The practice of medicine is an art that combines years of intensive study in
                                    the sciences with the common sense and practicalities of human interactions. Clinicians are
                                    already inundated with paperwork, regulation, and oversight, and more attention needs to be
                                    paid to making technologies more viable in terms of clinician adoption and patient experience.
                                    <br><br>

                                    To clinicians: Developers are keenly focused on innovation in healthcare (in case you haven’t noticed).
                                    Clinicians need to actively engage with developers and do a much better job at articulating how
                                    technology can provide value for their patients.</p>
                                </p>
                           </div>
                        </div>
                        <div class="col-sm-1 col-xs-0"></div>
                    </div>
                </div>
              </div>
                <br>
              <div id="showthepatientclinicianteam" style="display: none">
                <div class="row blogPostEntry" style="line-height: 2; padding: 0px 15px;">
                    <div class="row">
                        <div class="col-sm-1 col-xs-0"></div>
                        <div class="col-sm-10 col-xs-12">
                            <div style="margin-bottom: 10px; font-size: large" >The Patient-Clinician Team</div>
                        </div>
                        <div class="col-sm-1 col-xs-0"></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-1 col-xs-0"></div>
                        <div class="col-sm-10 col-xs-12">
                            <div style="border-top: 1px lightgray solid;border-bottom: 1px lightgray solid; padding: 5px 0px 5px;" >
                                <span class="glyphicon glyphicon-calendar"></span><a href="#"> December 15, 2016</a> /
                                <span class="glyphicon glyphicon-folder-close"></span><a href="#"> future of health and wellness</a>,
                                <a href="#">nextgen health and wellness</a>
                            </div>
                        </div>
                        <div class="col-sm-1 col-xs-0"></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-1 col-xs-0"></div>
                        <div class="col-sm-10 col-xs-12">
                            <div style="padding: 20px 0px" class="wrapper">
                                <p>
                                    Why You Should Form Your Own Hand-Picked Health and Wellness Team: <br><br>
                                    In medicine, there are several different buzz-phrases that either relate to or describe
                                    what I often refer to as the ‘patient-clinician team’.  Whether you call it ‘shared decision
                                    making’, ‘patient-centered care’, ‘patient-clinician team’, or perhaps most accurately,
                                    the ‘nextgen team’, the most important concept to keep in mind is that individuals need
                                    to play an active role in formulating and participating in their own health and wellness.
                                    Although seemingly obvious, this point is absolutely crucial, and it serves as a
                                    foundational prerequisite for realizing the promises of nextgen health and wellness.
                                    <br><br>
                                    At the heart of the patient-clinician team are several key concepts. First, understand
                                    that the patients’ values and preferences are just as (if not more) important than the
                                    clinicians’ or coaches’.  Second, it should be understood that the team simply will not
                                    work without consistent and active communication between an individual and his or her
                                    hand-picked team of caregivers. Of course, neither of these concepts are novel, however,
                                    recent advances in sensing, and in information and communication technology have enabled
                                    this team to become much more of a practical possibility. Finally, as I already alluded
                                    to, the true nextgen team is not just a patient and his or her primary care physician
                                    (which is often blindly auto-assigned by an individual’s insurance company), but the
                                    specific group of people that an individual feels will best help him or her live a
                                    better, healthier life. In other words, the true nextgen team is the individual and
                                    his or her handpicked ‘all-star’ team of providers, coaches, advisers, instructors,
                                    and educators that are best-positioned to deliver the best possible care for that individual.
                                    <br><br>
                                    Recently, there has been an exponential increase in the amount of medical publications
                                    that highlight the pros of various ‘digital medicine’ or nextgen health and wellness-related
                                    efforts. However, one commonly cited reason for long-term failure in such publications
                                    is the lack of sustained, individual-level engagement between individuals and caregivers.
                                    In much of the cutting-edge literature, individuals initially gravitate towards
                                    technology-centric interventions, but almost universally, both the interest and the
                                    associated positive behaviors quickly fade. What’s missing, and what’s needed, are the
                                    relationships that effectuate positive behavior change.</p>
                            </div>
                        </div>
                        <div class="col-sm-1 col-xs-0"></div>
                    </div>
                </div>
                <br>
              </div>
            </div>
        </div>
<script src="{{asset('js/app.js')}}"></script>
@yield('scripts')
    <script>

        $(document).ready(function() {
            var url = window.location.href;
           console.log(url);
           var res = url.split('=');
            console.log(res[1]);
       //     console.log(e.target.id);
           $( "#show"+res[1] ).show();

        });
    </script>

</body>
</html>
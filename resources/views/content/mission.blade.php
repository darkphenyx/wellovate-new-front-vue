@extends('index')

@section('content')
    <div>
        <div class="headerImageContainerDiv">
            <span class="headerTitle">Our Mission</span>
        </div>
            <div class="pageContainer">
                 <div class="row centered-xs">
                        <br>
                        <br>
                     <div class="row">
                            <div class="col-sm-1 col-xs-0"></div>
                            <div class="col-sm-10 col-xs-12">
                                <p style="font-size: small; text-align: left">
                                    The new millennia has brought about tremendous advancements in health and
                                    wellness. From digital education, to physiological trackers, to remote delivery
                                    systems, to virtual reality, technology continues expand at an ever faster rate.
                                    Everyone here is ready to "revolutionize" everything. But, what does that
                                    really mean for you? How do you know what is the real deal, and what is just
                                    smoke-and-mirrors? What is a passing fad and what is lasting progress?
                                   </p>
                                <p style="font-size: small; text-align: left">
                                    Our mission at Wellovate is simple: help you cut through all the noise by providing
                                    novel and proven solutions to improve the quality of life for your people, while
                                    saving you precious time and money. We understand that every organization is
                                    unique, so we never waste your time trying to sell you one-size-fits-all.
                                    We are in the business of developing solutions with you that are
                                    catered for you. Our work is always backed by our own in-house clinical domain
                                    experts to ensure what you get is of the highest quality. These are our
                                    commitments to you.</p>
                                <p style="font-size: small; text-align: left">
                                    Welcome to a better kind of partnership. Welcome to Wellovate.
                                </p>
                            </div>
                         <div class="col-sm-1 col-xs-0"></div>
                 </div>
            </div>
    </div>

@stop
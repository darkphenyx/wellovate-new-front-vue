The www.WELLOVATE.com Web Site, the www.WELLOVATE.org Web Site, and any online features, services
and/or programs offered by WELLOVATE.COM and/or WELLOVATE.ORG (collectively, "WELLOVATE" or the “Web Site”)
are committed to safeguarding your online privacy with respect to the personally identifiable information
that we may obtain from you. Our Privacy and Information Security Policy (“Privacy Policy”) answers frequently
asked questions about your online privacy, including what personally identifiable information we may obtain
from you and how it will be used and protected. WELLOVATE may from time to time change this Privacy Policy,
so please check back periodically. <br/><br/>
Please note, by accessing or using the Web Site, you consent to and agree
to be bound by our Terms of Use policy.<br/><br/>

<i>WHAT PERSONALLY IDENTIFIABLE INFORMATION DO WE OBTAIN FROM YOU? WHEN DO WE OBTAIN SUCH INFORMATION?</i><br/>

If, upon visiting our Web Site, your use is limited to browsing our informational content, we will not require
that you provide us with any personally identifiable information. However, we may request personal information
from you at other times. For example:<br/>

    •	If you enroll for an account membership through our Web Site, we may ask that you provide us with certain personal
        information, including your name, credit card number, expiration date, e-mail address, mailing address and
        telephone number.

    •	We collect the information you provide when you use our Services, including when you register for an account.
        This may include any information contained within our about any content that you provide.

    •	We may collect information obtained from wearable, mobile, or other devices where you install or access our Services.
        This information may assist us in our ability to provide you with Services, and may include, but is not limited to:

            a.	Physiological information<br/>

            b.	Device location information<br/>

            c.	Information about device hardware and operating systems, device settings, and device identifiers.<br/>

            d.	Information provided to us when you import or synch information contained on a wearable, mobile, or other device.

    •	We may collect information about how you use our Services, such as your content usage patterns.<br/>

    •	We may receive information about you from our third-party partners in an effort to improve our Services.<br/>

    •	If you post to our discussion forums, we will ask that you provide us with your name, e-mail address and password.<br/>

    •	If you want to enter any sweepstakes, contests or promotions sponsored by us or by one of our business partners,
        we will need your name, e-mail address and other information as may be required by the rules of the specific contest.<br/>

    •	If you choose to participate in a customer survey conducted by us or by one of our business partners, we may ask
        for your name, e-mail address and other information as may be required by the particular survey.<br/>

    •	If you report a problem or submit a customer review, we will ask that you provide your name, e-mail address, membership
        number, address, phone number and fax number. Should you contact us for any reason other than to report a problem
        and/or submit a review, we may also keep a record and/or copy of your correspondence with us.<br/>

<br/><br/>

<i>HOW DO WE PROTECT THE SECURITY AND QUALITY OF YOUR PERSONAL INFORMATION? </i><br/>
To protect the security and quality of your personal information, we have implemented technical and managerial
procedures to maintain accurate, current and complete information as well as to protect your personal information
against loss, misuse or alteration when it is under our control. Your personally identifiable information will be
encrypted and stored on our secured servers. Your personal information is also password protected so that access is
limited to yourself and those with whom you share your password, WELLOVATE, as well as third party access facilitated
by WELLOVATE with your prior permission. We have also taken steps to help protect the integrity of your personal
financial information when you complete a purchase transaction on our Web Site. We use Secure Socket Layer (SSL)
encryption to facilitate confidential online business transactions. SSL helps prevent your credit information from
being read by unauthorized persons as this information is transmitted over the Internet.<br/><br/>

HOW DO WE USE YOUR INFORMATION?<br/>

Our primary use of your information is to administer, maintain and improve your experience on our Web Site generally
as well as provide you with customized, personalization services and interactive communications.<br/><br/>

    •	We use your information to provide you with Services aimed at improving wellness.<br/>

    •	We use your information to improve and develop our Services aimed at improving wellness.<br/>

    •	Although we may provide you with personalized tools or content to improve wellness, including personalized
        estimates of disease risks, WE DO NOT USE YOUR INFORMATION TO MAKE OR IMPLY ANY DESCISIONS REGARDING THE DIAGNOSIS,
        MANAGEMENT, MITIGATION, PREVENTION, TREATMENT, OR CURE OF ANY DISEASE OR MEDICAL CONDITION.<br/>

    •	With your consent, we may provide your information to your physician.<br/>

    •	We may use the information available to us to help verify the appropriate use of our Services, and to help
        identify any violations in our Terms of Service.<br/>

    •	If you check the “opt-in” feature on our Web Site, or if you do not uncheck a pre-checked “opt-in” box we may
        from time to time send you e-mails regarding our Web Site and special promotions. Also, we occasionally may send you
        direct mail about products or services that we believe may be of interest to you.<br/>

    •	We use your financial information (e.g., your credit card number) only to verify your credit and to bill you
        for memberships purchased through our Web Site. We also use your contact information as necessary to send you
        information about the memberships that you have purchased on our Web Site.<br/>

    •	When you enter any sweepstakes, contests or promotions sponsored by us or by one of our business partners,
        we may use your e-mail address to send you status updates.<br/>

    •	We use your IP address to help diagnose problems with our server and to administer the services offered on our
        Web Site. We also use your IP address to help identify you and to gather broad demographic information that we may
        share with our business partners, but only in the aggregate without any of your personally identifiable information.<br/>

    •	We may research the demographics, interests and behavior of our customers based on the information provided to
        us during membership registration, during sweepstakes, contests and promotions, from our server log files, from
        cookies and from surveys. Our research may be compiled and analyzed on an aggregate basis. We may share this aggregate
        data with business partners, but only in the aggregate, without any of your personally identifiable information.<br/>
<br/><br/>

CAN YOU “OPT-OUT” OF RECEIVING COMMUNICATIONS FROM WELLOVATE?<br/>

If you change your mind and decide that you no longer want to receive promotional e-mails and/or direct mailings,
you may opt-out at any time by simply sending an e-mail request to contact@wellovate.org.<br/><br/>

PRIVACY POLICIES OF WELLOVATE PARTNERS<br/>

Third parties that have links on our Web Site may collect personally identifiable information about you.
We are not responsible for the privacy policies or practices of such sites and the practices of these sites
are not governed by this Privacy Policy. If you have questions about the privacy policies or practices of a
third party site, you should contact the site administrator or web-master of the specific site. We may from time
to time partner with other companies to offer co-branded services as well as sweepstakes, contests and promotions.
Any information that you provide in connection with the co-branded services or any jointly sponsored sweepstakes,
contests or promotions will become the joint property of WELLOVATE and its business partners. We will maintain your
information in accordance with the terms of this Privacy Policy. However, this Privacy Policy does not govern the
privacy policies and practices of our business partners. If you have questions about the privacy policies or practices
of our business partners, you should contact them directly.<br/><br/>

DO WE SELL OR RENT YOUR PERSONALLY IDENTIFIABLE INFORMATION?<br/>

No, as a general rule, we do not sell or rent your personally identifiable information to any one. If and whenever
we intend to share your personally identifiable information with a third party (other than to a business partner as
provided herein), you will be notified at the time of data collection or transfer, and you will have the option of
not permitting the transfer. However, we may from time to time rent or sell demographic information in the aggregate
that does not contain your personally identifiable information.<br/><br/>

WITH WHOM DO WE SHARE INFORMATION?<br/>

We generally will not disclose any of your personally identifiable information except when we have your permission
to do so or under some special circumstances described below.<br/><br/>

    •	We may share non-personally identifiable user information to third parties for the purposes of delivering or
        improving our Services or for business purposes. Personally identifiable information is information that can by
        itself be used to identify who you are (such as your name or email address).<br/>

    •	We may share your personal information with our partners or clients strictly for the purposes of carrying
        out our Services. Otherwise, WE DO NOT SHARE PERSONALLY IDENTIFIABLE INFORMATION TO THIRD PARTIES UNLESS
        PROVIDED WITH YOUR CONSENT.<br/>

    •	As noted previously, we may from time to time partner with other companies to offer co-branded services as well
        as sweepstakes, contests and promotions. Any information that you provide in connection with the co-branded services
        or any jointly sponsored sweepstakes, contests or promotions will become the joint property of WELLOVATE and its business
        partners. We may also disclose other personal information about you to our business partners, but only if we have obtained
        your permission to make the disclosure before data collection or before transferring the data.<br/>

    •	We may, from time to time, offer you the opportunity to receive materials or special offers from third parties.
        If you want to receive this information, we may (but only with your permission) share your name and e-mail address
        with them.<br/>

    •	Under confidentiality agreements, we may match user information with third party data. We also may disclose
        aggregate demographic and/or user information and statistics in order to describe our customer base to prospective
        partners and other third parties, and for other lawful purposes.<br/>

    •	We may disclose your personally identifiable information without your prior permission in special cases. For
        example, we may have reason to believe that disclosing the information is necessary to identify, contact or bring
        legal action against someone who may be violating the User Terms and Conditions, or may be causing intentional or
        unintentional injury or interference to the rights or property of WELLOVATE or any third party, including other
        customers. Also, we may disclose or access your personally identifiable information when we believe in good faith
        that law or regulation requires disclosure.<br/>
<br/><br/>

HOW CAN YOU UPDATE, CORRECT OR DELETE YOUR PERSONALLY IDENTIFIABLE INFORMATION?<br/>

You may edit your personally identifiable information and your password at any time by sending an e-mail request
to contact@wellovate.org.

WHAT ARE COOKIES? HOW DO WE USE COOKIES?<br/>

Cookies enable us to customize and personalize your experience on our Web Site, including the products and promotions
that are offered to you. Essentially, a cookie is a small amount of data that is sent to your browser from a web server
and is stored on your computer’s hard drive. We use cookies for several purposes in connection with the operation of
our Web Site<br/><br/>

    •	We may use cookies to identify you and access your information stored on our computers in order to deliver
        you a better and more personalized experience. For example, we may use cookies to tell you about products and
        services specific to your interests.<br/>

    •	Upon request, we will save your “user name” so that you do not have to re-enter it every time you visit our
        Web Site. In providing you with this service, we use cookies.<br/>

    •	We may use cookies to estimate our customer base and customer usage patterns. Each browser accessing our
        Web Site may use given a unique cookie that is then used to determine the extent of repeat visits and the customer
        activity during those visits. We may use the historical information to help target promotions based on customer
        interests and behavior, both specifically to individual customers and on an aggregate basis with respect to all
        customers.<br/>

    •	We also may use cookies to track your progress and number of entries in some promotions, sweepstakes and contests,
        or through a meeting registration process. For example, when a promotion uses cookies, the information coded to the
        cookie indicates your progress through the promotion, and may be used to track entries, submissions and status of
        prize drawings.<br/>
<br/>

Business partners that offer co-branded services and jointly-sponsored sweepstakes, contests and promotions on our
Web Site, may use their own cookies. We have no control over those cookies, nor does this Privacy Policy cover how
your personal information contained in those cookies may be used or protected. If you have any questions about the
cookies of such third parties, or about the use of your personal information by such third parties, you should
contact the site administrator or web-master of the third party site.<br/><br/>

DO YOU HAVE CHOICES ABOUT COOKIES?<br/>

Yes, you have several choices with respect to cookies. You can modify your browser preferences to accept all cookies,
to notify you when a cookie is set, or to reject all cookies. However, our Web Site uses cookie-based authentication.
Accordingly, if you choose to reject all cookies, you may not be able to log onto our Web Site and/or use our services
or participate in our sweepstakes, contests or promotions.<br/><br/>

WHAT ELSE SHOULD YOU KNOW ABOUT YOUR ONLINE PRIVACY?<br/>

It is important to remember that whenever you voluntarily disclose personal information on-line, your information can
be collected and used by others. If you transmit or post personal information on-line that is accessible to others,
you will not be able to control how that information is used by others. When we receive the transmitted information,
we will use the procedures summarized in this Privacy Policy to ensure the integrity and security of that information
in our systems. Unfortunately, notwithstanding any of the steps taken by us, it is not possible to guarantee the
security and integrity of data transmitted over the Internet. Consequently, while we take the above-described reasonable
steps to protect your personal information, we cannot and do not warrant the security or integrity of any information
you transmit to us when registering for our Web Site or otherwise. All such transmission of information is at your own
risk. Moreover, though we are committed to having our Web Site comply with this Privacy Policy, you are ultimately
responsible for maintaining the secrecy of your password and your personally identifiable information. If you are
careless with your password, or you decide to share your password with third parties, you must be aware of the risk
that such third parties will have access to all your personally identifiable information.<br/><br/>

CONTACT US.<br/><br/>

If you have any questions or comments about this Privacy Statement or the practices of our Web Site, please feel free
to e-mail us at contact@wellovate.org or visit our contact page.<br/>
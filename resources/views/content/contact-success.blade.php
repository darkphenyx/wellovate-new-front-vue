@extends('index')

@section('content')
    <div>
        <div class="headerImageContainerDiv">
            <span class="headerTitle">Contact Us Success</span>
        </div>
        <div class="pageContainer">

            <div class="form-horizontal" id="contact_form" style="padding-top: 40px;">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">Success</h3>
                        </div>
                        <div class="panel-body">
                            Thank you for contacting Wellovate. A member of our team will contact you back in the next 24-48 hours. If you haven't heard from us by then, feel free to send us another email.
                        </div>
                    </div>
                </div>
                <div class="col-md-3"></div>
            </div>
        </div>
    </div>
@stop
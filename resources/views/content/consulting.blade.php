@extends('index')

@section('content')
    <div>
        <div class="headerImageContainerDiv">
            <span class="headerTitle">Consulting Services</span>
        </div>
        <div class="pageContainer">
            <div class="row centered-xs">
                <br>
                <br>
                <div class="row">
                    <div class="col-sm-1 col-xs-0"></div>
                    <div class="col-sm-10 col-xs-12">
                        <p style="font-size: small; text-align: left">
                            Your organization's health and wellness efforts are not just about the optics. It's about empowering your people to thrive.
                            Having a strong workforce that is healthy and motivated can prevent injury, cut costs, improve productivity, improve retention,
                            and inspire loyalty.</p>
                        <p style="font-size: small; text-align: left">
                            From building a program, to hitting your metrics, to worksite specific issues, Wellovate will help you create customized solutions
                            that are right for your organization.</p>
                    </div>
                    <div class="col-sm-1 col-xs-0"></div>
                </div>
            </div>
            <div class="row centered-xs">
                    <div class="col-sm-1 col-xs-0"></div>
                    <div class="col-sm-10 col-xs-12">
                        <img src="images/consulting_diagram.png" style="width: 70%; height:auto;display: block;margin: 0 auto;" alt="">
                    </div>
                    <div class="col-sm-1 col-xs-0"></div>
            </div>
        </div>
    </div>

@stop
@extends('index')

@section('content')
    <div >
        <div class="headerImageContainerDiv">
            <span class="headerTitle">Perspectives</span>
        </div>
        <div class="pageContainer">
            <br>
            <br>
            <div class="row blogPostEntry" style="line-height: 2; padding: 0px 15px;">
                <div class="row">
                    <div class="col-sm-1 col-xs-0"></div>
                    <div class="col-sm-10 col-xs-12">
                        <div id="titlenuts" style="margin-bottom: 10px; font-size: large" >Are you nuts?!?!</div>
                    </div>
                    <div class="col-sm-1 col-xs-0"></div>
                </div>
                <div class="row">
                    <div class="col-sm-1 col-xs-0"></div>
                    <div class="col-sm-10 col-xs-12">
                        <div style="border-top: 1px lightgray solid;border-bottom: 1px lightgray solid; padding: 5px 0px 5px;" id="linknuts">
                            <span class="glyphicon glyphicon-calendar"></span><a href="#"> July 27, 2017</a> /
                            <span class="glyphicon glyphicon-folder-close"></span><a href="#"> future of health and wellness</a>,
                            <a href="#">nextgen health and wellness</a>
                        </div>
                    </div>
                    <div class="col-sm-1 col-xs-0"></div>
                </div>
                <div class="row">
                    <div class="col-sm-1 col-xs-0"></div>
                    <div class="col-sm-10 col-xs-12">
                        <div class="wrapper" style="padding: 20px 0px">
                            <p>
                                I read an interesting article today in IEEE Spectrum titled
                                “Robots Could Act as Ethical Mediators Between Patients and Caregivers”.
                                The article highlights some impressive work done at Georgia Tech in developing a
                                robotic solution that attempts to serve as an ethical mediator during medical
                                encounters between patients and their caregivers.</p>
                            </p>
                            <a href="{!! route('perspectivesview',['title'=>'areyounuts']) !!}" >Open to read full article</a>
                        </div>
                    </div>
                    <div class="col-sm-1 col-xs-0"></div>
                </div>
            </div>
            <br>

            <div class="row blogPostEntry" style="line-height: 2; padding: 0px 15px;">
                <div class="row">
                    <div class="col-sm-1 col-xs-0"></div>
                    <div class="col-sm-10 col-xs-12">
                        <div style="margin-bottom: 10px; font-size: large" id="titleclinician">The Patient-Clinician Team</div>
                    </div>
                    <div class="col-sm-1 col-xs-0"></div>
                </div>
                <div class="row">
                    <div class="col-sm-1 col-xs-0"></div>
                    <div class="col-sm-10 col-xs-12">
                        <div style="border-top: 1px lightgray solid;border-bottom: 1px lightgray solid; padding: 5px 0px 5px;" id="linkclinician">
                            <span class="glyphicon glyphicon-calendar"></span><a href="#"> December 15, 2016</a> /
                            <span class="glyphicon glyphicon-folder-close"></span><a href="#"> future of health and wellness</a>,
                            <a href="#">nextgen health and wellness</a>
                        </div>
                    </div>
                    <div class="col-sm-1 col-xs-0"></div>
                </div>
                <div class="row">
                    <div class="col-sm-1 col-xs-0"></div>
                    <div class="col-sm-10 col-xs-12">
                        <div style="padding: 20px 0px" class="wrapper">
                            <p>
                            Why You Should Form Your Own Hand-Picked Health and Wellness Team: <br><br>
                            In medicine, there are several different buzz-phrases that either relate to or describe
                            what I often refer to as the ‘patient-clinician team’.  Whether you call it ‘shared decision
                            making’, ‘patient-centered care’, ‘patient-clinician team’, or perhaps most accurately,
                                the ‘nextgen team’,</p>
                            <a href="{!! route('perspectivesview',['title'=>'thepatientclinicianteam']) !!}" >Open to read full article</a>
                        </div>
                    </div>
                    <div class="col-sm-1 col-xs-0"></div>
                </div>
            </div>
            <br>
        </div>
    </div>

@stop

@section('scripts')
    <script>
           /* $('.wrapper').find('a[href="#"]').on('click', function (e) {
                e.preventDefault();
                this.expand = !this.expand;
                $(this).text(this.expand?"Click to collapse":"Click to read more");
                $(this).closest('.wrapper').find('.small, .big').toggleClass('small big');
            });*/
    </script>
@stop
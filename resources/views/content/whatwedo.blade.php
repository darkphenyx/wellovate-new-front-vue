@extends('index')

@section('content')

    <div>
        <div class="headerImageContainerDiv">
            <span class="headerTitle">What We Do</span>
        </div>
        <div class="pageContainer">
                <div class="row">
                    <br>
                    <div class="row" style="text-align: center">
                         <span style="font-size: medium;">
                             We offer a wide range of tailored solutions for you organization's health and wellness needs.</p>
                         </span>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-1 col-xs-0"></div>
                        <div class="col-sm-10 col-xs-12 textBorder" >
                            <div style="margin:20px;">
                            <a href="{{ route('solutions') }}" class="hyper">
                             <span style="font-weight: bold;color: #2a2a2a"> Education & Content</span><br>
                             <span style="font-size: small;color:#636b6f">
                                Wellovate's core digital library of highly-curated educational content is made directly from primary
                                source evidence-based scientific research. With it we are readily able to provide you audience-specific,
                                 quality content across all major digital platforms.</span>
                            </a>
                            </div>
                        </div>
                        <div class="col-sm-1 col-xs-0"></div>
                </div>
                    <br>
                <div class="row">
                        <div class="col-sm-1 col-xs-0"></div>
                        <div class="col-sm-10 col-xs-12 textBorder" >
                            <div style="margin:20px;">
                                <a href="{{ route('solutionsvalue') }}" style="text-decoration: none;">
                                <span style="font-weight: bold;color: #2a2a2a"> Value Added Solutions</span><br>
                                <span style="font-size:small;color:#636b6f">
                                A natural extension of any wellness program. These products and services are additive to your
                                existing wellness infrastructure and efforts. They are designed to augment and enhance key
                                areas such as quality control and engagement.</span>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-1 col-xs-0"></div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-1 col-xs-0"></div>
                        <div class="col-sm-10 col-xs-12 textBorder" >
                            <div style="margin:20px;">
                                <a href="{{ route('consulting') }}" style="text-decoration: none;">
                                <span style="font-weight: bold;color: #2a2a2a"> Consulting Services</span><br>
                                <span style="font-size: small;color:#636b6f">
                                We partner with your organization to solve high-level health and wellness goals. From building
                                a program, to improving meterics, to specific workplace issues, Wellovate can help create
                                customized solutions to meet your unique needs.</span>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-1 col-xs-0"></div>
                    </div>
                    <br>

            </div>
        </div>
    </div>

@stop
@extends('index')

@section('content')
    <div>
        <div class="headerImageContainerDiv">
            <span class="headerTitle">Education & Content</span>
        </div>
        <div class="pageContainer">
            <div class="row centered-xs">
                <br>
                <br>
                <div class="row">
                    <div class="col-sm-1 col-xs-0"></div>
                    <div class="col-sm-4 col-xs-12">
                        <img src="images/devices.png" style="width: 100%; height:auto;display: block;margin: 0 auto;" alt="">
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <span style="font-size: x-large;text-align: center">Quality resources from quality sources</span>
                        <div style="font-size: small; text-align: left;margin-right: 25px;margin-left: 25px">
                            <br>
                            Our content is created by board certified physicians
                            that have systematically curated our core library from
                            thousands of evidence-based research articles to
                            ensure accuracy and quality.<br><br>
                            From full-length articles, to content libraries, to blogs,
                            to push notifications - Wellovate can cater a solution
                            that's a right fit for you.</div>
                    </div>
                    <div class="col-sm-1 col-xs-0"></div>
                </div>
            </div>

            <br>
            <div class="row">
                <div class="row">
                    <div class="col-sm-1 col-xs-0"></div>
                    <div class="col-sm-1 col-xs-0"></div>
                    <div class="col-sm-4 col-xs-12">
                        <span style="font-size: x-large;margin-right: 30px;margin-bottom: 20px;">Key highlights</span>
                        <p class="highYieldParagraph">

                            - Customized to your audience<br>
                            - Supported for all your devices<br>
                            - Single use or subscription service<br>
                            - White-labelling options<br>
                            - Flexible and scalable
                        </p>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <span style="font-size: x-large;text-align: center">High-Yield Topics</span>
                        <p class="highYieldParagraph">
                            - Heart / Cardiovascular<br>
                            - Metabolic / Diabetes<br>
                            - Weight Loss<br>
                            - Physical Activity & Exercise<br>
                            - Diet & Nutrition<br>
                            - Smoking Cessation<br>
                            - Sleep<br>
                            - Mental Health & Stress<br>
                            - Occupational/Workplace<br>
                            - Chronic Pain<br>
                        </p>
                    </div>
                    <div class="col-sm-1 col-xs-0"></div>
                    <div class="col-sm-1 col-xs-0"></div>
                </div>
            </div>
        </div>
    </div>

@stop
@section('scripts')
    <script>
        $('.responsive-tabs').responsiveTabs({
            accordionOn: ['xs'] // xs, sm, md, lg
        });
        $('.nav-tabs > li > a').click( function() {
            $('.nav-tabs > li > a').css('backgroundColor','');
            $('.nav-tabs > li > a').css('color','#3097D1');
            $(this).css('color','#555555');
            $(this).css('backgroundColor',$($(this).attr('href')).css('backgroundColor'));
            if($(this).attr('href') == "#VASol"){
                $(this).css('color','white');
            }console.log($(this).attr('href'));
        } );
    </script>
@stop


@extends('index')

@section('content')
    <div>
        <div class="headerImageContainerDiv" >
                <span class="headerTitle" >Who We Are</span>
        </div>
            <div class="pageContainer">
                <div class="row">
                    <div class="col-sm-1 col-xs-0"></div>
                    <div class="col-sm-10 col-xs-12">

                    </div>
                    <div class="col-sm-1 col-xs-0"></div>
                </div>
                <div class="row" style="margin-top: 20px;">
                    <div class="col-sm-1 col-xs-1"></div>
                    <div class="col-sm-2 col-xs-12">
                        <img src="{{asset('images/JM-professional-pic-100x150.jpg')}}" alt="">
                    </div>
                    <div class="col-sm-8 col-xs-12">
                        <span style="font-weight: bold">Joe Morgan MD</span><br>
                        <span style="font-size: medium">President & Founder</span><br>
                        <span style="font-size: small">Dr. Morgan is a board certified anesthesiologist. He completed
                            is medical degree at SUNY Upstate Medical University and residency at the Cleveland Clinic.
                            He also holds a BS Finance from Clemson University and BA Chemistry from SUNY Buffalo.
                            Before Wellovate, Dr. Morgan served as a consultant in physiological
                            tracking and innovative systems of care.</span><br><br>
                    </div>
                    <div class="col-sm-3 col-xs-1"></div>
                </div>
                <br>
                <div class="row">
                    <div class="col-sm-1 col-xs-1"></div>
                    <div class="col-sm-2 col-xs-12">
                        <img src="{{asset('images/patel-100x133.jpg')}}"  alt="" >
                    </div>
                    <div class="col-sm-8 col-xs-12">
                        <span style="font-weight: bold">Rahul Patel MD</span><br>
                        <span style="font-size: medium">Vice President</span><br>
                        <span style="font-size: small">Dr. Patel is a board certified emergency medicine physician. He completed his medical
                        degree at SUNY Upstate Medical University and residency at the University of Connecticut.
                        He also holds a BA Philosophy from the University of Florida. Previously Dr. Patel served as
                        an academic consultant and clinical research coordinator.</span><br><br>
                    </div>
                    <div class="col-sm-3 col-xs-1"></div>
                </div>
                <br>
                <div class="row">
                    <div class="col-sm-1 col-xs-1"></div>
                    <div class="col-sm-2 col-xs-12">
                        <img src="{{asset('images/Pagano-Nunzio-100x150.jpg')}}"  alt="" >
                    </div>
                    <div class="col-sm-8 col-xs-12">
                        <span style="font-weight: bold">Nunzio Pagano DO</span><br>
                        <span style="font-size: medium">Vice President</span><br>
                        <span style="font-size: small">Dr. Pagano is a board certified internal medicine physician. He completed his medical
                        degree at West Virginia School of Osteopathic Medicine and residency at the University of
                        West Virginia. He also holds a BS Exercise Physiology from West Virginia University.
                        Dr. Pagano has received numerous awards for teaching and patient care.</span><br><br>
                    </div>
                    <div class="col-sm-3 col-xs-1"></div>
                </div>
                <br>
                <br>
        </div>
    </div>
@stop
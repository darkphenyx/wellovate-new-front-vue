@extends('index')

@section('content')
    <div>
        <div class="headerImageContainerDiv">
            <span class="headerTitle">Value Added Solutions</span>
        </div>
        <div class="pageContainer">
            <div class="row centered-xs">
                <br>
                <br>
                <div class="row">
                    <div class="col-sm-1 col-xs-0"></div>
                    <div class="col-sm-10 col-xs-12">
                        <p style="font-size: small; text-align: left">
                            A natural extension of any wellness program. These products and services are additive to your existing wellness infrastructure
                            and efforts. They are designed to augment and enhance key areas such quality control and engagement. Below is a sample of
                            just some of the solutions we offer. Customized solutions and packages are also available.</p>
                    </div>
                    <div class="col-sm-1 col-xs-0"></div>
                </div>
            </div>
            <div class="row centered-xs">
                <div class="col-sm-1 col-xs-0"></div>
                <div class="col-sm-10 col-xs-12">
                    <img src="images/value_added_diagram.png" style="width: 70%; height:auto;display: block;margin: 0 auto;" alt="">
                </div>
                <div class="col-sm-1 col-xs-0"></div>
            </div>
        </div>
    </div>

@stop
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><b>Your Wellovate Account has been verified. If you are not redirected back to the home page please click the link below.</b></div>

                <div class="panel-body">
                    <br>
                    <div>
                        <div style="text-align: center;">
                            <a href="https://www.wellovate.com">
                                <button type="button" class="btn btn-block btn-primary">Go back to the home page</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>